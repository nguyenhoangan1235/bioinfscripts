#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

my $frame = shift(@ARGV);
my $frameFrac = ($frame / 10);
my $oldFrameFrac = $frameFrac;

my $secFrac = $frameFrac - int($frameFrac);
$secFrac = (1 - sin((0.5 - $secFrac) * pi))/2;

$frameFrac = int($frameFrac) + $secFrac;

#printf(STDERR "frameFrac %f -> %f\n", $oldFrameFrac, $frameFrac);

$frameFrac = $frameFrac * (360 / 60);


while(<>){
    if(/gTClone([0-9]+)/){
        my $baseAngle = ($frameFrac * $1) % 720;
        my $fillColour = ($baseAngle < 360) ? "#ff8b60" : "#9494ff";
        s#transform=".*?"#sprintf('transform="rotate(%0.1f 200 200)"',$baseAngle)#e;
    }
    print;
}
