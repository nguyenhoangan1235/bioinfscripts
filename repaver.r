#!/usr/bin/env Rscript
## repaver.r - REpetitive PAttern VisualisER
## input: fasta sequence file
## output: a visualisation of repetitive sequences within the sequence file

## Note: this depends on a C++ library, 'hashasher.cc', which should be
## placed in the same directory as this script
sourceDir <- ".";
scriptName <- "repaver.r";
libName <- "hashasher.cc";
if(any(grepl("--file=", commandArgs()))){
    sourceDir <- dirname(sub("--file=", "",
                             grep("--file=", commandArgs(), value=TRUE)));
    scriptName <- basename(sub("--file=", "",
                               grep("--file=", commandArgs(), value=TRUE)));
}

argLoc <- 1;

usage <- function(){
  cat(sprintf("usage: ./%s", scriptName),
      "<fasta/fastq file> [options]\n");
  cat("\nOther Options:\n");
  cat("-help                : Only display this help message\n");
  cat("-k <int>             : Set kmer length\n");
  cat("-bg <string>         : Background colour\n");
  cat("-scheme <string>     : Colour scheme (paired (default); sunset; dusk)\n");
  cat("-search <string>     : Kmer search pattern",
      "(FCRV: everything; FR: only forward/RC)\n");
  cat("-annotate <string>   : GTF annotation file\n");
  cat("-highlight <int int> : highlight region\n");
  cat("-style <string>      : Output file style",
      "(dotplot|profile*|splay|track|circular|semicircular|spiral)\n");
  cat("-loops <int>         : Change number of spiral loops (default 3)\n");
  cat("-outfmt <string>     : Output file type (*png*|svg)\n");
  cat("-nokey               : Disable key\n");
  cat("-notext              : Disable text annotation\n");
  cat("-vflip               : Flip plot vertically (only implemented for dotplot)\n");
  cat("-prefix <string>     : Output file prefix (default: 'repaver')\n");
  cat("-size <x>x<y>        : Image size",
      "(default 1920x1080 for profile/semicircular, 1080x1080 for other)\n");
  cat("-res <int>           : Set resolution in bases per pixel\n");
  cat("-height <int>        : Set image height (in pixels)\n");
  cat("-width <int>         : Set image width (in pixels)\n");
  cat("-tile <int>          : Split image into tiles (tile width in pixels)\n");
  cat("-makedirs            : Create JBrowse tile directories\n");
  cat("-overlap <frac>      : Add overlap in tiled images\n");
  cat("-ptscale <float>     : Adjust point size\n");
  cat("-nocheck             : Bypass C++ source checksum validation\n");
  cat("\n");
}

## Default arguments

outputStyle <- "profile"; ## dotplot, circular, semicircular, profile, splay
outputType <- "png";
kmerLength <- 17;
filePrefix <- "repaver";
bgColour <- "#FFFFFF"; # dusk: '#202080'; # solarised: '#fdf6e3'
fileName <- "";
kSearch <- "FCRV"; # Forward/Complement/Revcomp/reVerse
sizeX <- -1;
tileX <- -1;
overlapX <- 0;
resBPP <- 0;
sizeY <- -1;
numLoops <- 3;
annFile <- "";
annData <- list();
makeJBrowseDirs <- FALSE;
cexScale <- 1;
extraPtScale <- 1;
showKey <- TRUE;
showText <- TRUE;
flipVert <- FALSE;
highlightRegion <- NULL;
colourScheme <- "paired";
plotCols <- list();
keyNames <- c(F="Repeat", R="Reverse", RC="RevComp", C="Complement");
dnaSeqFile <- "";
ignoreChecksum <- FALSE;

## SHA256 digest of c++ library code; used to maintain version consistency
cppDigest <- "a26a85a88744d2ae713ce53b9b92f4bebace99cb874952e3642d2074f70293bd";

required.packages <- c("digest", "Rcpp");
cat("Loading R libraries...");
for(pack in required.packages){
    if(!suppressMessages(require(pack, character.only=TRUE))){
        cat(sprintf("Error: '%s' package must be installed\n", pack));
        usage();
        quit(save="no");
    }
}
cat(" done!\n");

cat("Loading C++ libraries...");
sourceCpp(file.path(sourceDir, libName),
          cacheDir=file.path(sourceDir, "cppCache"));
cat(" done!\n");

## helper R functions
fib.divs <- round(10^((0:4)/5) * 2) * 0.5; ## splits log decades into 5

#' A split linear/logarithmic function with a smooth transition
#'
#' This function transforms data into a combined linear/logarithmic
#' form, being linearly transformed prior to a specified split point,
#' and log transformed after that split point. The gradients of the
#' two functions are matched at the split point so that there is a
#' smooth transition from the linear to the log scale.
#'
#' @param x values to transform
#' @param splitPoint point where graph transitions from linear to log
#' @param zeroPoint adjustment for zero point prior to transformation
#'
#' @return transformed values
#'
#' @examples
#' ## Demonstrate transformation with negative numbers
#' plot(x=-2000:10000, y=linlog(-2000:10000, 40), type="l")
#' ## Mark linear portion of the graph
#' points(x=c(-40,40), y=linlog(c(-40, 40), 40), col="red", pch=16)
#'
#' @export
linlog <- function(x, splitPoint, zeroPoint=0){
    x <- (x - zeroPoint);
    a <- splitPoint;
    xtr <- ifelse(abs(x) < a,
                  ## linear up to the split point
                  x / (a * log(a)),
                  ## logarithmic beyond the split point
                  sign(x) * (log(abs(x)) / log(a) + (1 / log(a) - 1)));
    return(xtr);
}

#' A split logarithmic/linear function with a smooth transition
#'
#' This function transforms data into a combined logarithmic/linear
#' form, being logarithmically transformed prior to a specified split
#' point, and linearly transformed after that split point. The
#' gradients of the two functions are matched at the split point so
#' that there is a smooth transition from the log to the linear scale.
#'
#' @param x values to transform
#' @param splitPoint point where graph transitions from linear to log
#' @param scale optional scaling factor; 1 .. <scale> is transformed to 0 .. 1
#'
#' @return transformed values
#'
#' @examples
#' plot(x=1:10000, y=loglin(1:10000, 400, 10000), type="l")
#' ## Mark logarithmic portion of the graph
#' points(x=c(1,400), y=loglin(c(1, 400), 400, 10000), col="red", pch=16)
#'
#' @export
loglin <- function(x, splitPoint, scale=NA){
    a <- splitPoint;
    xtr <- ifelse(x < a,
                  ## logarithmic up to the split point
                  log(x) / log(a),
                  (x-a) / (a * log(a)) + 1);
    if(!is.na(scale)){
        endPos <- (scale - a) / (a * log(a)) + 1;
        xtr <- xtr / endPos;
    }
    return(xtr);
}

valToSci <- function(val, unit = ""){
    sv <- sign(val);
    val <- abs(val);
    sci.prefixes <- c("", "k", "M", "G", "T", "P", "E", "Z", "Y");
    units <- rep(paste(sci.prefixes,unit,sep=""), each=3);
    logRegion <- floor(log10(val))+1;
    conv.units <- units[logRegion];
    conv.div <- 10^rep(0:(length(sci.prefixes)-1) * 3, each = 3)[logRegion];
    conv.val <- val / conv.div;
    conv.val[val == 0] <- 0;
    conv.units[val == 0] <- unit;
    return(sub(" +$", "", sprintf("%s %s", sv * conv.val, conv.units)));
}

if(length(commandArgs(TRUE)) < 1){
      usage();
      quit(save = "no", status=0);
}

while(!is.na(commandArgs(TRUE)[argLoc])){
    if(file.exists(commandArgs(TRUE)[argLoc])){ # file existence check
        dnaSeqFile <- commandArgs(TRUE)[argLoc];
    } else {
        if(commandArgs(TRUE)[argLoc] %in% c("-h", "-help", "--help")){
            usage();
            quit(save = "no", status=0);
        } else if(commandArgs(TRUE)[argLoc] == "-annotate"){
            annFile <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-highlight"){
            highlightRegion <- as.numeric(commandArgs(TRUE)[argLoc+(1:2)]);
            argLoc <- argLoc + 2;
        } else if(commandArgs(TRUE)[argLoc] == "-bg"){
            bgColour <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-scheme"){
            colourScheme <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-k"){
            kmerLength <- as.numeric(commandArgs(TRUE)[argLoc+1]);
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-nokey"){
            showKey <- FALSE;
        } else if(commandArgs(TRUE)[argLoc] == "-notext"){
            showText <- FALSE;
        } else if(commandArgs(TRUE)[argLoc] == "-vflip"){
            flipVert <- TRUE;
        } else if(commandArgs(TRUE)[argLoc] == "-prefix"){
            filePrefix <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-search"){
            kSearch <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-size"){
            arg <- unlist(strsplit(commandArgs(TRUE)[argLoc+1], "x"));
            argLoc <- argLoc + 1;
            sizeX <- as.numeric(arg[1]);
            sizeY <- as.numeric(arg[2]);
        } else if(commandArgs(TRUE)[argLoc] == "-res"){
            resBPP <- as.numeric(commandArgs(TRUE)[argLoc+1]);
            cat(sprintf("Setting resolution to %d bases per pixel\n", resBPP));
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-height"){
            sizeY <- as.numeric(commandArgs(TRUE)[argLoc+1]);
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-width"){
            sizeX <- as.numeric(commandArgs(TRUE)[argLoc+1]);
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-tile"){
            tileX <- as.numeric(commandArgs(TRUE)[argLoc+1]);
            cat(sprintf("Tiling with approx %d horizontal pixels per tile\n",
                        tileX));
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-makedirs"){
            makeJBrowseDirs <- TRUE;
            cat("Creating JBrowse tile directory structure\n");
        } else if(commandArgs(TRUE)[argLoc] == "-overlap"){
            overlapX <- as.numeric(commandArgs(TRUE)[argLoc+1]);
            cat(sprintf("Overlapping %0.2fx for each tile\n", overlapX));
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-ptscale"){
            extraPtScale <- as.numeric(commandArgs(TRUE)[argLoc+1]);
            cat(sprintf("Setting point scale factor to %0.1fx\n", extraPtScale));
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-table"){
            cat(paste0("Error: table output has been deprecated; ",
                       "see 'hashasher.r' for BED/BedGraph output.\n\n"));
            usage();
            quit(save = "no", status = 1);
        } else if((commandArgs(TRUE)[argLoc] == "-style") ||
                  (commandArgs(TRUE)[argLoc] == "-type")){
            outputStyle <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-loops"){
            numLoops <- as.numeric(commandArgs(TRUE)[argLoc+1]);
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-outfmt"){
            outputType <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-nocheck"){
            cat("Note: will not verify C++ code checksum\n");
            ignoreChecksum <- TRUE;
        } else {
            cat("Error: Argument '",commandArgs(TRUE)[argLoc],
                "' is not understood by this program\n\n", sep="");
            usage();
            quit(save = "no", status=1);
        }
    }
    argLoc <- argLoc + 1;
}

codeDigest <- digest(readLines(file.path(sourceDir, libName)), algo="sha256");
if(ignoreChecksum){
    cat("Code checksum: ", codeDigest, "\n", sep="");
} else if(!identical(codeDigest, cppDigest)) {
    cat("Error: C++ code checksum does not match expected. Has the 'hashasher.cc' code been updated?\n");
    cat("    Code checksum: ", codeDigest, "\n", sep="");
    cat("    Expected     : ", cppDigest, "\n", sep="");
    cat("To continue anyway, use option '-nocheck'\n\n");
    usage();
    quit(save = "no", status=1);
}

if(!showText){ ## Disable key if no text
    showKey <- FALSE;
}

if(dnaSeqFile == ""){
    cat(paste0("Error: sequence file *must* be specified\n"));
    usage();
    quit(save = "no", status=1);
}

if(overlapX >= 1){
    cat(paste0("Error: overlap should be a positive fractional value ",
               "less than 1\n"));
    usage();
    quit(save = "no", status=1);
}

if(numLoops < 0.5){
    cat(paste0("Error: Number of loops should be at least 0.5\n"));
    usage();
    quit(save = "no", status=1);
}

if(outputStyle == "circle"){
    outputStyle <- "circular";
}

if(outputStyle == "semicircle"){
    outputStyle <- "semicircular";
}

if(outputStyle == "semicircular"){
    numLoops <- 1;
}

if(!(outputStyle %in% c("dotplot","profile","track","circular","semicircular", "spiral", "splay"))){
    cat("Error: Plot style should be one of ",
        "'(dotplot|profile|track|circular|semicircular|spiral|splay)'\n");
    usage();
    quit(save = "no", status=1);
}

if(makeJBrowseDirs && !require(rjson)){
    cat("Error: JBrowse track output requires the 'rjson' package\n");
    usage();
    quit(save = "no", status=1);
}

if(makeJBrowseDirs && (outputStyle != "track")){
    cat("Error: JBrowse track output *must* use '-style track'\n");
    usage();
    quit(save = "no", status=1);
}

if(makeJBrowseDirs && (overlapX > 0)){
    cat("Error: JBrowse track output cannot use any overlap\n");
    usage();
    quit(save = "no", status=1);
}

if(makeJBrowseDirs && (tileX == -1)){
    cat(paste0("Error: JBrowse track output requires ",
               "a specified tile width, '-tile <BPP>'\n"));
    usage();
    quit(save = "no", status=1);
}

if(!(outputType %in% c("svg","png"))){
    cat("Error: File output type should be one of ",
        "'(svg|png)'\n");
    usage();
    quit(save = "no", status=1);
}

if((resBPP > 0) && (tileX == -1)){
    cat(paste0("Error: setting resolution also requires a specified tile width, ",
               "'-tile <int>'\n"));
    usage();
    quit(save = "no", status=1);
}

if(colourScheme == "paired"){
    plotCols <- list("Single" = c(F="#8b0000",RC="#FF7F00",
                                  C="#0000FF",R="#00A000"),
                     "Left"   = c(F="#9000A0",RC="#FF7F00",
                                  C="#00A090",R="#A09000"),
                     "Right"  = c(F="#8b0000",RC="#FDC086",
                                  C="#0000FF",R="#00A000"),
                     "None" = c());
} else if(colourScheme == "sunset"){
    plotCols <- list("Single" = c(F="#9000A0",RC="#FF7F00",
                                  C="#0000FF",R="#00A000"),
                     "Left"   = c(F="#9000A0",RC="#FF7F00",
                                  C="#0000FF",R="#00A000"),
                     "Right"  = c(F="#9000A0",RC="#FF7F00",
                                  C="#0000FF",R="#00A000"),
                     "None" = c());
} else if(colourScheme == "dusk"){
    plotCols <- list("Single" = c(F="#9000A0",R="#FFD700",
                                  C="#00A000",RC="#202080"),
                     "Left"   = c(F="#9000A0",R="#FFD700",
                                  C="#00A000",RC="#202080"),
                     "Right"  = c(F="#9000A0",R="#FFD700",
                                  C="#00A000",RC="#202080"),
                     "None" = c());
}

bgGrey <- colSums(col2rgb(bgColour)/255 * c(0.299,0.587,0.114));
fgGrey <- ifelse(identical(bgColour, "NA") || (bgGrey > 0.5), "#202020", "#D0D0D0");

bgRGB <- c(col2rgb(bgColour, alpha=FALSE));
bgAlpha <- ifelse(bgColour == "NA", NA,
                  rgb(bgRGB[1], bgRGB[2], bgRGB[3], alpha=200,
                      maxColorValue=255));

if(kSearch == "FR"){
    tmpCols <- list("Single" = c("F" =unlist(plotCols$Single["F"]),
                                 "RC"=plotCols$Single["C"]),
                    "Left"   = c("F" =plotCols$Left["F"],
                                 "RC"=plotCols$Left["C"]),
                    "Right"  = c("F" =plotCols$Right["F"],
                                 "RC"=plotCols$Right["C"]),
                    "None" = c());
}

if(outputStyle %in% c("profile", "splay", "track", "semicircular")){
    if(sizeX == -1){
        sizeX <- 1920;
    }
    if(sizeY == -1){
        sizeY <- 1080;
    }
} else {
    if(sizeX == -1){
        sizeX <- 1080;
    }
    if(sizeY == -1){
        sizeY <- 1080;
    }
}

cexScale <- sizeY / 1080;
ptCexScale <- 1 * extraPtScale;

if(annFile != ""){
    ##gtf.df <- read.delim(annFile,
    ##                      col.names=c("seqName","source","feature","start","end",
    ##                                  "score","strand","frame","attribute"));
    gff3.df <- read.delim(annFile,
                          col.names=c("seqId","source","type","start","end",
                                      "score","strand","phase","attributes"));
    for(sid in unique(gff3.df$seqId)){
        oldList <- NULL;
        sName <- paste0("sid_",sid);
        if(sName %in% annData){
            oldList <- annData[[sName]];
        }
        oldList <- rbind(oldList, data.frame(gff3.df[,c("start","end")]));
        annData[[sName]] <- oldList;
    }
}

if(length(annData)>0){
    print(annData);
}

fileCounter <- 1;

cat("Preparing fasta file...");
if(!prepareFastxFile(dnaSeqFile, kmerLength)){
    cat(paste0("Error: no sequences to process!\n"));
    usage();
    quit(save = "no", status=1);
}
cat(" done!\n");

while(loadNextSequence()){
    sLen <- getSeqLen();
    print(system.time(hashRepeats()));
    missingPoss <- getMissingRanges();
    ## TODO: use actual number of pixels in plot for sizeY, rather than
    ##       total image height
    xRes <- ifelse(resBPP > 0, resBPP, ceiling(sLen / sizeX));
    ## Additional adjustments for radial representations
    if(outputStyle == "semicircular"){
        xRes <- ceiling(xRes / (pi/2));
    }
    if(outputStyle == "circular"){
        xRes <- ceiling(xRes / pi);
    }
    if(outputStyle == "spiral"){
        xRes <- ceiling(xRes / numLoops);
    }
    yBins <- sizeY;
    dnaSeqMap <- getDiffResults(bpc=xRes,
                                yBins=yBins,
                                doLog=!identical(outputStyle, "dotplot"),
                                circular=identical(outputStyle, "circular"));
    dnaSeqMap$chunks$type[dnaSeqMap$chunks$type == "X"] <- "RC";
    cat(sprintf("DNA sequence map object size in memory: %s\n",
                valToSci(signif(object.size(dnaSeqMap), 4), "B")));
    dnaSeqMapName <- dnaSeqMap$name;
    sLen <- dnaSeqMap$len;
    blockSize <- dnaSeqMap$blockSize;
    if(resBPP > 0){
        sizeX <- ceiling(sLen / resBPP);
    }
    ## Adjust point size and alpha for short sequences
    ptAlpha <- ifelse(sLen < 100, 128, 64);
    ptCexScale <- ifelse(sLen < 100, cexScale * (400 / sLen), cexScale) * extraPtScale;
    pcl <- rgb(t(col2rgb(plotCols[["Left"]])), alpha=ptAlpha,
               maxColorValue=255);
    pcr <- rgb(t(col2rgb(plotCols[["Right"]])), alpha=ptAlpha,
               maxColorValue=255);
    pcs <- rgb(t(col2rgb(plotCols[["Single"]])), alpha=ptAlpha,
               maxColorValue=255);
    names(pcl) <- names(plotCols[["Left"]]);
    names(pcr) <- names(plotCols[["Right"]]);
    names(pcs) <- names(plotCols[["Single"]]);
    cat(sprintf("Processing %s [length: %d; %d bases per block]\n",
                dnaSeqMapName, sLen, blockSize));
    xOfs <- 0;
    posSF <- 3;
    if(grepl("\\[([0-9]+)\\.\\.[0-9]+\\]",dnaSeqMapName)){
        rangePortion <-
            unlist(regmatches(dnaSeqMapName,
                              gregexpr("\\[([0-9]+)\\.\\.[0-9]+\\]",
                                       dnaSeqMapName)));
        if(length(rangePortion) == 1){
            rangePortion <-
                as.numeric(unlist(strsplit(gsub("(\\[|\\])",
                                                "", rangePortion), "\\.\\.")));
            xOfs <- rangePortion[1];
            posSF <-
                max(3, ceiling(log10(rangePortion[1]) -
                               log10(diff(rangePortion)) + 1));
        }
    }
    if(grepl(":[0-9]+\\-[0-9]+", dnaSeqMapName)){
        rangePortion <-
            unlist(regmatches(dnaSeqMapName,
                       gregexpr(":[0-9]+\\-[0-9]+", dnaSeqMapName)));
        if(length(rangePortion) == 1){
            rangePortion <- as.numeric(
                unlist(strsplit(substring(rangePortion, 2), "\\-")));
            xOfs <- rangePortion[1];
            posSF <-
                max(3, ceiling(log10(rangePortion[1]) -
                               log10(diff(rangePortion)) + 1));
        }
    }
    ## f,c,rc,r : red, orange, blue, green
    plotPointsAll <- dnaSeqMap$chunks;
    plotPointsAll$type <- factor(plotPointsAll$type,
                                 levels=c("F", "RC", "R", "C"));
    plotPointsAll$dirLeft <- (plotPointsAll$y > 0);
    if(kSearch == "FR"){
        plotPointsAll <- subset(plotPointsAll, type %in% c("F", "RC"));
    }
    cat(sprintf("Point list object size in memory: %s\n",
                valToSci(signif(object.size(plotPointsAll), 4), "B")));
    numChunks <- 1;
    newTileX <- tileX;
    plotWindowSize <- if(newTileX == -1) {sLen} else {newTileX * blockSize};
    plotStride <- floor(plotWindowSize - (overlapX * plotWindowSize));
    plotWindowPX <- if(newTileX == -1) {sizeX} else {newTileX};
    plotStrideCount <- 0;
    ## JBrowse track generation: create directories and metadata
    fileBaseDir <- "";
    if(makeJBrowseDirs){
        fileBaseDir <-
            sprintf("%s/%s/%dBPP_k%d",
                    filePrefix, dnaSeqMapName, blockSize, kmerLength);
        dir.create(fileBaseDir, recursive=TRUE, showWarnings=FALSE);
        jsonBaseDir <- sprintf("%s/%s", filePrefix, dnaSeqMapName);
        jsonFileName <- file.path(jsonBaseDir, "trackData.json");
        ## Pre-populate JSON structure
        jsonData <- list(tileWidth=tileX,
                         zoomLevels=list());
        ## Retrieve existing data (if it exists)
        if(file.exists(jsonFileName)){
            jsonData <- fromJSON(file=jsonFileName);
            if(jsonData$tileWidth != tileX){
                cat(sprintf(
                    paste0("Error: Tile width mismatch in JSON file\n",
                           "       [%d in command-line arguments vs ",
                           "%d in JSON file]\n"), tileX, jsonData$tileWidth));
                usage();
                quit(save = "no", status=1);
            }
        }
        ## Add in data for files that will be generated
        jsonData$zoomLevels <-
            c(jsonData$zoomLevels,
              list(list(urlPrefix=sprintf("%dBPP_k%d/", blockSize, kmerLength),
                        basesPerTile=plotWindowSize,
                        height=sizeY)));
        ## Write out to the file (overwriting, if anything's already there)
        cat(toJSON(jsonData, indent=2), file=jsonFileName);
    }
    for(plotStartX in seq(1, sLen, by=plotStride)){
        plotEndX <- plotStartX + plotWindowSize - 1;
        if(makeJBrowseDirs){
            ## make last tile smaller if creating JBrowse tiles
            ## [this should be done with *all*, but just for the plot region]
            plotWindowPX <- ceiling((plotEndX - plotStartX + 1) / blockSize);
            cat("PlotWindowPX: ", plotWindowPX, "\n");
        }
        cat("Drawing plot...\n");
        my.time <- Sys.time();
        plotPoints <-
            plotPointsAll[(plotPointsAll$x >= plotStartX) &
                          (plotPointsAll$x <= plotEndX), , drop=FALSE];
        if(plotWindowSize < sLen){
            cat(sprintf("[from %d to %d]... ",
                        plotStartX, max(sLen, plotEndX)));
        }
        ## Create file
        fileName <- sprintf("%s_k%d_%03d%s.%s",
                            filePrefix, kmerLength, fileCounter,
                            if(newTileX == -1) {
                                "";
                            } else {
                                sprintf(".%dBPP.%09d", blockSize, plotStartX);
                            },
                            outputType);
        ## For JBrowse output: place into generated dir
        if(makeJBrowseDirs){
            fileName <- sprintf("%s/%s/%dBPP_k%d/%d.png",
                                filePrefix, dnaSeqMapName,
                                blockSize, kmerLength, plotStrideCount);
        }
        if(outputStyle %in% c("profile", "splay", "track", "semicircular")){
            if(outputType == "svg"){
                svg(filename=fileName, width=plotWindowPX/96, height=sizeY/96,
                    pointsize=22);
            } else {
                if(outputStyle == "splay"){
                    ##par(mar=c(6.5,6.5,3,3) * cexScale,
                    png(filename=fileName,
                        ## this should ensure that the graph portion is the specified size
                        width=plotWindowPX + 19 * 0.2 * 72 * cexScale,
                        height=sizeY + 23 * 0.2 * 72 * cexScale,
                        pointsize=22, antialias="gray");
                } else {
                    png(filename=fileName, width=plotWindowPX, height=sizeY,
                        pointsize=22, antialias="gray");
                }
            }
        } else {
            if(outputType == "svg"){
                svg(filename=fileName, width=plotWindowPX/96, height=sizeY/96,
                    pointsize=18);
            } else {
                png(filename=fileName, width=plotWindowPX, height=sizeY,
                    pointsize=18, antialias="gray");
            }
        }
        ## Set up plot
        par(col=fgGrey, col.lab=fgGrey, col.main=fgGrey,
            col.sub=fgGrey, col.axis=fgGrey);
        if(outputStyle == "dotplot"){
            par(mgp=c(3, 1, 0) * cexScale,
                mar=c(5, 5, 3, 3) * cexScale,
                bg=bgColour,
                cex.main=2*cexScale,
                cex.axis=1.5*cexScale, cex.lab=1.5*cexScale,
                xaxs="i", yaxs="i");
            plot(NA, xlim=c(plotStartX, plotEndX),
                 ylim=if(flipVert) { # invert Y axis from default orientation
                          c(plotStartX, plotEndX)
                      } else {
                          c(plotEndX, plotStartX)
                      },
                 xlab=ifelse(xOfs + sLen >= 10^6, "Base Location (Mb)",
                             "Base Location (kb)"),
                 ylab=ifelse(xOfs + sLen >= 10^6, "Base Location (Mb)",
                             "Base Location (kb)"),
                 axes=FALSE,
                 main=if(showText){sprintf("%s (k=%d)", dnaSeqMapName, kmerLength)}
                      else {""});
            if((sLen+xOfs) >= 10^6){
                axis(1, at=axTicks(1),
                     labels=valToSci(signif((axTicks(1)+xOfs), 4)), col=fgGrey);
                axis(2, at=axTicks(2),
                     labels=valToSci(signif((axTicks(2)+xOfs), 4)), col=fgGrey);
            } else {
                axis(1, at=axTicks(1), labels=(axTicks(1)+xOfs)/1000, col=fgGrey);
                axis(2, at=rev(axTicks(2)), labels=(axTicks(2)+xOfs)/1000, col=fgGrey);
            }
        } else if(outputStyle == "profile"){
            par(mgp=c(3.5,1.5,0), mar=c(5,6.5,3,3) * cexScale,
                cex.axis=1.5*cexScale, cex.lab=1.5*cexScale,
                cex.main=2*cexScale, bg=bgColour,
                xaxs="i", yaxs="i");
            plot(NA, xlim=c(plotStartX, plotEndX), ylim=c(0,1),
                 xlab="",
                 ylab="",
                 axes=FALSE,
                 main=if(showText){sprintf("%s (k=%d)",
                                           dnaSeqMapName, kmerLength)}
                      else{""});
            mtext("Base Location (bp)", side=1, line=5, cex=1.5 * cexScale);
        } else if(outputStyle == "splay"){
            par(mgp=c(3.5,1.5,0), mar=c(8.5,6.5,3,3) * cexScale,
                cex.axis=1.5*cexScale, cex.lab=1.5*cexScale,
                cex.main=2*cexScale, bg=bgColour,
                xaxs="i", yaxs="i", las=2);
            plot(NA, xlim=c(plotStartX, plotEndX), ylim=c(1,-1),
                 xlab="",
                 ylab="",
                 axes=FALSE,
                 main=if(showText){sprintf("%s (k=%d)",
                                           dnaSeqMapName, kmerLength)}
                      else{""}, las=0);
            mtext("Base Location (bp)", side=1, line=7 * cexScale, cex=1.5 * cexScale, las=0);
        } else if(outputStyle == "track"){
            par(mar=c(0,0,0,0),
                cex.axis=1.5*cexScale, cex.lab=1.5*cexScale,
                cex.main=2*cexScale, bg=bgColour,
                xaxs="i", yaxs="i");
            plot(NA, xlim=c(plotStartX, plotEndX), ylim=c(-0.01, 1.01),
                 xlab="",
                 ylab="",
                 axes=FALSE);
            if(showText){
                text(plotStartX, 1, adj=c(-0.25,1.5),
                     labels=sprintf("[k=%d bp]", kmerLength), col="grey40",
                     cex=2 * cexScale);
            }
        } else if(outputStyle %in% c("circular", "spiral")){
            par(mgp=c(2.5,1,0), mar=c(1.5,3,4.5,3) * cexScale,
                cex.axis=1.5*cexScale, cex.lab=1.5*cexScale,
                cex.main=2*cexScale, bg=bgColour);
            plot(NA, xlim=c(-1.1,1.1), ylim=c(-1.2,1),
                 axes=FALSE, xlab="", ylab="",
                 main=if(showText){sprintf("%s (k=%d)", dnaSeqMapName, kmerLength)}
                      else{""});
        } else if(outputStyle == "semicircular"){
            par(mgp=c(2.5,1,0), mar=c(2,2,4,2) * cexScale, bg=bgColour,
                cex.axis=1.5*cexScale, cex.lab=1.5*cexScale,
                cex.main=2*cexScale);
            xMul <- (1.2 / par()$pin[2]) * par()$pin[1];
            plot(NA, xlim=c(-xMul/2, xMul/2), ylim=c(-0.2, 1),
                 axes=FALSE, xlab="", ylab="",
                 main=if(showText){sprintf("%s (k=%d)", dnaSeqMapName, kmerLength)}
                      else{""});
        }
        if(outputStyle == "dotplot"){
            plotPoints$y = (plotPoints$y / (sizeY*2)) * sLen + plotPoints$x;
            segments(x0=0, y0=0, x1=sLen, y1=sLen, cex=0.5 * cexScale, pch=15,
                     col="#8b0000A0", lwd=3);
            for(tType in levels(plotPoints$type)){
                pl <- subset(plotPoints, type == tType);
                if(nrow(pl) == 0){
                    next;
                }
                cat(sprintf("  Plotting %d %s repeats...", nrow(pl), tType));
                points(x=pl$x+1, y=pl$y+1, pch=20,
                       col=ifelse(pl$dirLeft, pcl[tType], pcr[tType]),
                       cex=0.5 * ptCexScale, xpd=NA);
                cat(" done!\n");
            }
            if(showKey){
                legend("bottomleft",
                       legend=c(paste(keyNames[names(plotCols[["Left"]])],
                                      "(L)"),
                                paste(keyNames[names(plotCols[["Right"]])],
                                      "(R)")),
                       ncol=2,
                       fill=c(plotCols[["Left"]], plotCols[["Right"]]),
                       bg=bgAlpha, inset=0.05, cex=cexScale, border=fgGrey, box.col=fgGrey);
            }
        } else if(outputStyle %in% c("profile", "splay", "track")){
            pwFun <- function(d){
                if(sLen <= 100){
                    return(d / sLen);
                }
                ds <- sign(d);
                d <- abs(d);
                a <- sLen/25;
                aProp <- sLen / a;
                ds * ifelse(d < a,
                            log(d) / log(a),
                            d/(a * log(a)) + (1 - 1/log(a))) /
                    ((aProp-1) / log(a) + 1);
            }
            pointsPreFilt <- nrow(plotPoints);
            ## Round adjusted y position to half-pixel boundaries
            pht.hpx <- par("pin")[2] * 72 * 2;
            ## horizontal axis
            drMax <- ceiling(log10(sLen));
            distPts <- (1:99)*10^(drMax-2);
            distPts <- c(1, head(distPts[distPts < sLen], -1), sLen);
            if(length(distPts) > (20 * (sizeX / 1000))){
                distPts <- (1:9)*10^(drMax-1);
                sigFig <- 3;
                distPts <- c(1, signif(c(distPts[distPts < sLen], sLen),sigFig));
            }
            if(outputStyle %in% c("profile", "splay")){
                axis(1, at=distPts, labels=valToSci(signif(distPts+xOfs,4)),
                     cex.axis=1.5 * cexScale, col=fgGrey);
            }
            ## plot vertical axis annotation
            scalePtsMajor <- rep(1, each=drMax+1) * 10^(0:drMax);
            sig4Len <- signif(sLen, 4);
            scalePtsMajor <- c(scalePtsMajor[scalePtsMajor < sig4Len], sig4Len);
            scalePtsMinor <- rep(1:9, each=drMax+1) * 10^(0:drMax);
            scalePtsMinor <- scalePtsMinor[scalePtsMinor < sig4Len];
            if(outputStyle == "splay"){
                scalePtsMajor <- unique(c(-rev(scalePtsMajor), scalePtsMajor));
                scalePtsMinor <- unique(c(-rev(scalePtsMinor), scalePtsMinor));
            }
            if(outputStyle %in% c("profile", "splay")){
                axis(2, at= pwFun(scalePtsMajor), las=2, lwd=3,
                     cex.axis=ifelse(outputStyle == "splay", 1, 1.5) * cexScale,
                     labels=valToSci(scalePtsMajor), col=fgGrey);
                axis(2, at= pwFun(scalePtsMinor), labels=FALSE, col=fgGrey);
                if(showText){
                    mtext("Feature distance (bp)", 2, line=4.5*cexScale,
                          cex=1.5*cexScale,
                          col=fgGrey, las=0);
                }
                ## show missing positions
                rect(xleft=missingPoss$starts, xright=missingPoss$ends,
                     ybottom=-2, ytop=2, col="#80808010", border="#80808010");
            }
            ## guide lines for major/minor axis points
            abline(h=pwFun(scalePtsMajor), lwd=3, col="#80808060");
            abline(h=pwFun(scalePtsMinor), lwd=1, col="#80808030");
            if(!(outputStyle %in% c("profile", "splay"))){
                if(showText){
                    text(plotStartX,
                         pwFun(tail(head(scalePtsMajor, -1), -1)),
                         pos=4,
                         labels=sprintf("[%s]",
                                        valToSci(tail(head(scalePtsMajor, -1), -1), "bp")),
                         col="grey40", cex=1.5 * cexScale);
                }
            }
            for(tType in levels(plotPoints$type)){
                pl <- subset(plotPoints, type == tType);
                if(nrow(pl) == 0){
                    next;
                }
                cat(sprintf("  Plotting %d %s repeats...", nrow(pl), tType));
                if(outputStyle == "splay"){
                    points(x=pl$x+1, y=(pl$y / (sizeY * 2)), ## A splayed plot has no abs!
                           pch=ifelse(identical(outputStyle, "track"), 18, 20),
                           cex=ifelse(identical(outputStyle, "track"),
                                      2, 0.5) * ptCexScale,
                           col=ifelse(pl$dirLeft, pcl[tType], pcr[tType]),
                           xpd=NA);
                } else {
                    points(x=pl$x+1, y=abs(pl$y / (sizeY * 2)),
                           pch=ifelse(identical(outputStyle, "track"), 18, 20),
                           cex=ifelse(identical(outputStyle, "track"),
                                      2, 0.5) * ptCexScale,
                           col=ifelse(pl$dirLeft, pcl[tType], pcr[tType]),
                           xpd=NA);
                }
                cat(" done!\n");
            }
            if(length(annData) > 0){
                ##dnaSeqMapName
            }
            if(length(highlightRegion) > 0){
                rect(xleft=highlightRegion[1]-xOfs,
                     xright=highlightRegion[2]-xOfs,
                     ybottom=0, ytop=1, col="#8040FF30", border=NA);
            }
            if((outputStyle %in% c("profile", "splay")) && showKey){
                legend(ifelse(outputStyle == "splay", "topleft", "top"),
                       legend=paste(rbind(keyNames[names(plotCols[["Left"]])],
                                          keyNames[names(plotCols[["Right"]])]),
                                    rep(c("(L)","(R)"),
                                        length(plotCols[["Left"]]))),
                       fill=rbind(plotCols[["Left"]], plotCols[["Right"]]),
                       bg=bgAlpha, inset=0.05, ncol=length(plotCols[["Left"]]),
                       cex=cexScale, border=fgGrey, box.col=fgGrey);
            }
        } else if(outputStyle == "circular"){
            pwFun <- function(d){
                if(sLen <= 100){
                    return((d / sLen) * 0.75 + 0.25);
                }
                a <- sLen / 50;
                aProp <- (sLen / 2) / a;
                ifelse(d < a,
                       log(d) / log(a),
                       d/(a * log(a)) + (1 - 1/log(a))) /
                    ((aProp-1) / log(a) + 1) * 0.75 + 0.25;
            }
            cat("  Converting distances...");
            plotPoints$r <- abs(plotPoints$y / (sizeY * 2)) * 0.75 + 0.25;
            plotPoints$y <- plotPoints$r * sin(plotPoints$x / sLen * 2 * pi);
            plotPoints$x <- plotPoints$r * cos(plotPoints$x / sLen * 2 * pi);
            cat(" done!\n");
            cat("  Setting up plot...");
            drMax <- ceiling(log10(sLen));
            scalePtsMajor <- rep(1, each=drMax+1) * 10^(0:drMax);
            scalePtsMajor <- c(scalePtsMajor[scalePtsMajor < sLen/2], sLen/2);
            scalePts <- rep(1:9, each=drMax+1) * 10^(0:drMax);
            scalePts <- scalePts[scalePts <= sLen/2];
            for(p in scalePtsMajor){ # rings for log scale
                points(x=pwFun(p)*cos(seq(0,2*pi, length.out=360)),
                       y=pwFun(p)*sin(seq(0,2*pi, length.out=360)),
                       type="l", lwd=3, col="#808080A0");
            }
            distPts <- (1:99)*10^(drMax-2);
            distPts <- c(1, head(distPts[distPts < sLen], -1), sLen);
            if(length(distPts) > 20){
                distPts <- (1:9)*10^(drMax-1);
                sigFig <- 3;
                distPts <- c(1, signif(c(distPts[distPts < sLen], sLen),sigFig));
            }
            if(showText){
                segments(x0=0.18*cos(distPts / sLen * 2*pi), # tick marks
                         x1=0.2*cos(distPts / sLen * 2*pi),
                         y0=0.18*sin(distPts / sLen * 2*pi),
                         y1=0.2*sin(distPts / sLen * 2*pi),
                         lwd=2, col="#000000A0");
                for(dpi in seq_along(distPts)){ # tick labels for base location
                    text(x=0.14*cos(distPts[dpi] / sLen * 2*pi),
                         y=0.14*sin(distPts[dpi] / sLen * 2*pi),
                         labels=valToSci(signif(distPts[dpi],4)),
                         cex=0.5 * cexScale,
                         srt=if((distPts[dpi]/sLen * 360 >= 90) &&
                                (distPts[dpi]/sLen * 360 < 270)){
                                 (distPts[dpi]/sLen * 360 + 180);
                             } else {
                                 (distPts[dpi]/sLen * 360);
                             },
                         col=fgGrey);
                }
                ## tick circle
                points(x=0.2*cos(seq(10^(drMax-2)/sLen * 2*pi/2,
                                     2*pi, length.out=360)),
                       y=0.2*sin(seq(10^(drMax-2)/sLen * 2*pi/2,
                                     2*pi, length.out=360)),
                       type="l", lwd=3, col=rgb(t(col2rgb(fgGrey)),
                                                alpha=160, maxColorValue=255));
            }
            cat(" done!\n");
            for(tType in levels(plotPoints$type)){
                pl <- subset(plotPoints, type == tType);
                if(nrow(pl) == 0){
                    next;
                }
                cat(sprintf("  Plotting %d %s repeats...", nrow(pl), tType));
                points(x=pl$x, y=pl$y,
                       pch=ifelse(identical(outputType, "png"), 20, "•"),
                       cex=ifelse(outputType=="png",0.5,1),
                       col=ifelse(pl$dirLeft, pcl[tType], pcr[tType]),
                       xpd=NA);
                cat(" done!\n");
            }
            cat("annotations... ");
            rect(xleft=pwFun(head(scalePtsMajor,1))-0.025,
                 xright=pwFun(tail(scalePtsMajor,1))+0.05,
                 ytop=0.13, ybottom=-0.13, col="#FFFFFFA0", border=NA);
            arrows(x0=pwFun(head(scalePts,-1)), x1=pwFun(tail(scalePts,-1)),
                   y0=0, angle=90, code=3, length=0.1, lwd=2, col="#80808080");
            arrows(x0=pwFun(head(scalePtsMajor,-1)),
                   x1=pwFun(tail(scalePtsMajor,-1)),
                   y0=0, angle=90, code=3, length=0.15, lwd=3, col=rgb(t(col2rgb(fgGrey)), alpha=128, maxColorValue=255));
            if(showText){
                text(x=pwFun(scalePtsMajor), y=0, col=fgGrey,
                     labels=valToSci(signif(scalePtsMajor,3)), pos=1, offset=1,
                     cex=0.5 * cexScale);
                text(x=mean(range(pwFun(scalePtsMajor))), y=0, col=fgGrey,
                     labels="Feature Distance (bases)", pos=3, offset=1,
                     cex=0.75 * cexScale);
                text(x=0, y=0, labels="Sequence\nLocation\n(bases)", col=fgGrey,
                     cex=0.75 * cexScale);
            }
            if(showKey){
                legend("bottom",
                       legend=paste(rbind(keyNames[names(plotCols[["Left"]])],
                                          keyNames[names(plotCols[["Right"]])]),
                                    rep(c("(L)","(R)"),
                                        length(plotCols[["Left"]]))),
                       fill=rbind(plotCols[["Left"]], plotCols[["Right"]]),
                       bg=bgAlpha, inset=0.01, ncol=length(plotCols[["Left"]]),
                       cex=cexScale, border=fgGrey, box.col=fgGrey);
            }
        } else if(outputStyle %in% c("semicircular", "spiral")){
            pwFun <- function(d, logFrac = 1/25){
                if(sLen <= 100){
                    return(d / sLen);
                }
                a <- sLen * logFrac;
                ## endPos: value of the linear component at x=sLen
                endPos <- (sLen - a) / (a * log(a)) + 1;
                aProp <- sLen / a;
                ifelse(d < a,
                       log(d) / log(a),
                       ## 1/(a *  log(a)): gradient at x=a
                       (d-a)/(a * log(a)) + 1) / endPos * 0.75 + 0.25;
            }
            ## Convert <dist, (0..1)> to <x, y>
            spiralise <- function(sDist, radProp){
                ## assume s = 1/2 * a * theta^2
                a <- 1 / (numLoops+2);
                thetaStart <- 2 * pi;
                sStart <- 1/2 * a * (thetaStart)^2;
                thetaEnd <- 2 * pi * (numLoops + 1);
                sEnd <- 1/2 * a * thetaEnd^2;
                sRing <- (1 - sDist / sLen) * (sEnd - sStart) + sStart;
                ringTheta <- sqrt(2 * sRing / a);
                ringThetaRot <- (((ringTheta - thetaEnd) / pi) * 180 + 90) %% 360;
                ringThetaRot <-
                    ifelse((ringThetaRot > 95) & (ringThetaRot < 270),
                           ringThetaRot + 180,
                           ringThetaRot);
                ringRad <- a * ringTheta / (2 * pi) + a * radProp;
                return(list(x=-ringRad*cos(-ringTheta + thetaEnd),
                            y=ringRad*sin(-ringTheta + thetaEnd),
                            srt=ringThetaRot));
            }
            cat("  Converting distances...");
            plotPoints$r <-
                round(abs(plotPoints$y / (sizeY*2)) * 0.75  + 0.25, 3);
            plotPoints$y <- NULL;
            plotPoints$theta <- plotPoints$x/sLen * pi;
            if(outputStyle == "spiral"){
                res.out.pl <- spiralise(plotPoints$x, plotPoints$r);
                plotPoints$x <- res.out.pl$x;
                plotPoints$y <- res.out.pl$y;
            } else {
                plotPoints$x <- -plotPoints$r*cos(plotPoints$theta);
                plotPoints$y <- plotPoints$r*sin(plotPoints$theta);
            }
            cat(" done!\n");
            cat("  Setting up plot...");
            drMax <- ceiling(log10(sLen));
            scalePtsMajor <- rep(1, each=drMax+1) * 10^(0:drMax);
            scalePtsMajor <- c(scalePtsMajor[scalePtsMajor < sLen], sLen);
            scalePts <- rep(1:9, each=drMax+1) * 10^(0:drMax);
            scalePts <- scalePts[scalePts <= sLen];
            if(outputStyle == "spiral"){
                scalePts <- rep(1:9, each=drMax+1) * 10^(0:drMax);
                scalePts <- scalePts[scalePts <= sLen];
                ringTheta <- seq(0,2 * pi * numLoops, length.out=360 * numLoops);
            } else {
                ringTheta <- seq(0,pi, length.out=180);
            }
            for(p in scalePtsMajor){ # rings for log scale
                ringRad <- pwFun(p);
                if(outputStyle == "spiral"){
                    ringRad <- (1 + (1 - ringTheta / (2 * pi * numLoops)) *
                                numLoops + (ringRad)) /
                        (numLoops+2);
                }
                points(x=-ringRad*cos(ringTheta),
                       y=ringRad*sin(ringTheta),
                       type="l", lwd=3, col="#808080A0");
            }
            distPts <- (1:99)*10^(drMax-2);
            distPts <- c(1, head(distPts[distPts < sLen], -1), sLen);
            if((length(distPts) / numLoops) > 25){
                distPts <- (1:9)*10^(drMax-1);
                distPts <-
                    signif(c(1, distPts[distPts < sLen], sLen) + xOfs, posSF) - xOfs;
            }
            if(showText){
            }
            while (max(table(signif(distPts[seq_along(distPts)] + xOfs,
                                    posSF))) > 1) {
                posSF <- posSF + 1;
                if(posSF > 6){
                    break;
                }
            }
            if(showText){
                if(outputStyle == "spiral"){
                    spiralMul <- ifelse(numLoops > 2, (1 / numLoops),  0.5);
                    ## tick labels for base location
                    res.labelLoc <- spiralise(sDist=distPts, radProp=0.1);
                    for(dpi in seq_along(distPts)){
                        text(x=res.labelLoc$x[dpi],
                             y=res.labelLoc$y[dpi],
                             labels=valToSci(signif(distPts[dpi] + xOfs, posSF)),
                             cex=0.65 * cexScale * spiralMul,
                             srt=res.labelLoc$srt[dpi]);
                    }
                    ## tick circle
                    points(spiralise(sDist=seq(0, sLen,
                                               length.out=360 * numLoops),
                                     radProp = 0.2),
                           type="l", lwd=3,
                           col=rgb(t(col2rgb(fgGrey)),
                                   alpha=160, maxColorValue=255));
                    res.outer <- spiralise(sDist=distPts, radProp=0.2);
                    res.inner <- spiralise(sDist=distPts, radProp=0.15);
                    segments(x0=res.outer$x, y0=res.outer$y,
                             x1=res.inner$x, y1=res.inner$y,
                             lwd=2, col=rgb(t(col2rgb(fgGrey)),
                                            alpha=160, maxColorValue=255));
                } else {
                    ## tick labels for base location
                    for(dpi in seq_along(distPts)){
                        text(x=-0.14*cos(distPts[dpi] / sLen * pi),
                             y=0.14*sin(distPts[dpi] / sLen * pi),
                             labels=valToSci(signif(distPts[dpi] + xOfs, posSF)),
                             cex=0.5,
                             srt=-if((distPts[dpi]/sLen * 180 >= 90) &&
                                     (distPts[dpi]/sLen * 180 < 270)){
                                      (distPts[dpi]/sLen * 180 + 180);
                                  } else {
                                      (distPts[dpi]/sLen * 180);
                                  },
                             col=fgGrey);
                    }
                    ## tick circle
                    points(x=0.2*cos(seq(0, pi, length.out=180)),
                           y=0.2*sin(seq(0, pi, length.out=180)),
                           type="l", lwd=3,
                           col=rgb(t(col2rgb(fgGrey)),
                                   alpha=160, maxColorValue=255));
                    ## tick marks for circle
                    segments(x0=-0.18*cos(distPts / sLen * pi),
                             x1=-0.2*cos(distPts / sLen * pi),
                             y0=0.18*sin(distPts / sLen * pi),
                             y1=0.2*sin(distPts / sLen * pi),
                             lwd=2, col=rgb(t(col2rgb(fgGrey)),
                                            alpha=160, maxColorValue=255));
                }
            }
            if(outputStyle == "spiral"){
                for(p in scalePts){ # minor tick marks for log scale
                    ## tick marks (start)
                    ringRad <- pwFun(p);
                    ringTheta <- c(0.02,-0.02);
                    if(outputStyle == "spiral"){
                        ringRad <- (1 + (1 - ringTheta / (2 * pi * numLoops)) *
                                    numLoops + (ringRad)) /
                            (numLoops+2);
                    }
                    points(x=-ringRad*cos(ringTheta),
                           y=ringRad*sin(ringTheta),
                           type="l", lwd=3, col="#808080A0");
                    ## tick marks (end)
                    ringRad <- pwFun(p);
                    ringTheta <- 2 * pi * numLoops + c(0.03,-0.03);
                    if(outputStyle == "spiral"){
                        ringRad <- (1 + (1 - ringTheta / (2 * pi * numLoops)) *
                                    numLoops + (ringRad)) /
                            (numLoops+2);
                    }
                    points(x=-ringRad*cos(ringTheta),
                           y=ringRad*sin(ringTheta),
                           type="l", lwd=3, col="#808080A0");

                }
                ## start/end radial lines
                ringTheta <- rep(c(0, 2 * pi * numLoops), each=2);
                ringRad <- c(0.25,1);
                ringRad <- (1 + (1 - ringTheta / (2 * pi * numLoops)) *
                                numLoops + (ringRad)) /
                    (numLoops+2);
                segments(x0=-ringRad[c(1,3)]*cos(ringTheta[c(1,3)]),
                         x1=-ringRad[c(2,4)]*cos(ringTheta[c(2,4)]),
                         y0=ringRad[c(1,3)]*sin(ringTheta[c(1,3)]),
                         y1=ringRad[c(2,4)]*sin(ringTheta[c(2,4)]),
                         lwd=3, col="#808080A0");
                if(showText){
                    ## end text
                    ringTheta <- 2 * pi * numLoops + 0.05;
                    ringThetaDeg <- ((-ringTheta / pi) * 180 + 90) %% 360;
                    if((ringThetaDeg >= 95) &&
                       (ringThetaDeg < 270)){
                        ringThetaDeg <- ringThetaDeg + 180;
                    }
                    ringRad <- pwFun(scalePtsMajor);
                    ringRad <- (1 + (1 - ringTheta / (2 * pi * numLoops)) *
                                numLoops + (ringRad)) /
                        (numLoops+2);
                    text(x=-ringRad*cos(ringTheta + 0.015),
                         y=ringRad*sin(ringTheta + 0.015),
                         srt=ringThetaDeg, adj=c(0,0.5),
                         cex=0.65 * cexScale * spiralMul,
                         labels=valToSci(signif(scalePtsMajor,3)), col=fgGrey);
                }
            } else {
                ## tick marks (left + right)
                segments(x0=c(-pwFun(scalePts), pwFun(scalePts)),
                         y0=-0.01, y1=0.01, lwd=2 * cexScale,
                         col="#80808080");
                segments(x0=c(-pwFun(scalePtsMajor), pwFun(scalePtsMajor)),
                         y0=-0.01, y1=0.01, lwd=3 * cexScale,
                         col=rgb(t(col2rgb(fgGrey)), alpha=128, maxColorValue=255));
                segments(x0=c(-1,1), x1=c(-0.25,0.25),
                         y0=0, lwd=3/numLoops * cexScale,
                         col=rgb(t(col2rgb(fgGrey)), alpha=128, maxColorValue=255));
            }
            spiralMul <- 1;
            if(outputStyle == "spiral"){
                spiralMul <- ifelse(numLoops > 2, (1 / numLoops),  0.5);
            }
            cat(" done!\n");
            for(tType in levels(plotPoints$type)){
                pl <- subset(plotPoints, type == tType);
                if(nrow(pl) == 0){
                    next;
                }
                cat(sprintf("  Plotting %d %s repeats...", nrow(pl), tType));
                points(x=pl$x, y=pl$y,
                       pch=ifelse(identical(outputType, "png"), 20, "•"),
                       cex=ifelse(outputType=="png", 0.5, 1) * spiralMul,
                       col=ifelse(plotPoints$dirLeft,
                                  pcl[tType],
                                  pcr[tType]), xpd=NA);
                cat(" done!\n");
            }
            cat("  annotations...");
            if(showText){
                ## tick labels for distance axis
                if(outputStyle == "spiral"){
                    spiralMul <- ifelse(numLoops > 2, (1 / numLoops),  0.5);
                    text(x=-pwFun(scalePtsMajor) / (numLoops + 2) -
                             (numLoops+1) / (numLoops+2), y=-0.03, col=fgGrey,
                         labels=valToSci(signif(scalePtsMajor,3)),
                         srt=90, adj=c(1,0.5),
                         cex=0.65 * cexScale * spiralMul);
                } else {
                    text(x=-pwFun(scalePtsMajor), y=-0.03, col=fgGrey,
                         labels=valToSci(signif(scalePtsMajor,3)),
                         adj=c(0.5,1), cex=0.65 * cexScale);
                    text(x=pwFun(scalePtsMajor), y=-0.03, col=fgGrey,
                         labels=valToSci(signif(scalePtsMajor,3)),
                         adj=c(0.5,1), cex=0.65 * cexScale);
                }
                if(outputStyle == "spiral"){
                    text(x=-1 + 0.375 / (numLoops + 2), y=-0.075, col=fgGrey,
                         labels="Feature Distance (bases)", pos=1, offset=1,
                         cex=1 * cexScale * 1/(numLoops));
                } else {
                    text(x=c(-0.625, 0.625), y=-0.05, col=fgGrey,
                         labels="Feature Distance (bases)", pos=1, offset=1,
                         cex=1 * cexScale);
                }
                text(x=0, y=0, labels="Sequence\nLocation\n(bases)", col=fgGrey,
                     cex=1 * cexScale);
            }
            if(showKey){
                legend("bottom",
                       legend=paste(rbind(keyNames[names(plotCols[["Left"]])],
                                          keyNames[names(plotCols[["Right"]])]),
                                    rep(c("(L)","(R)"),
                                        length(plotCols[["Left"]]))),
                       fill=rbind(plotCols[["Left"]], plotCols[["Right"]]),
                       bg=bgAlpha, inset=0.01, ncol=length(plotCols[["Left"]]),
                       cex=cexScale, border=fgGrey, box.col=fgGrey);
            }
            cat(" done!\n");
        } ## closes 'if(outputStyle %in% c("semicircular", "spiral"))'
        cat(sprintf("done in %0.2f %s\n", Sys.time() - my.time,
                    attr(Sys.time() - my.time, "units")));
        invisible(dev.off());
        cat("Written to '",fileName,"'\n",sep="");
        my.time <- Sys.time();
        plotStrideCount <- plotStrideCount + 1;
    } ## closes 'for(plotStartX in seq(1, slen, by=plotStride))'
    fileCounter <- fileCounter+1;
}
cat("finished!\n");
