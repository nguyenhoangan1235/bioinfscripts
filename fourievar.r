#!/usr/bin/env Rscript

if(length(commandArgs(TRUE)) < 1){
    cat("Error: no fasta file specified\n");
    cat("syntax: ./fouriervar.r <input.fa>\n");
    quit(save="no");
}

library(Biostrings, quietly=TRUE, warn.conflicts=FALSE, verbose=FALSE);

fuzz <- 2;
doPlot <- TRUE;

fileName <- "/bioinf/voluntary/bioinfcomp18/prob3/test2.fasta";
fileName <- commandArgs(TRUE)[1];
seqs <- readDNAStringSet(fileName);

print(seqs);

for(si in 1:length(seqs)){
    mySeq <- as.vector(seqs[[si]]);
    slen <- length(mySeq);
    fLimit <- 1000;
    fLimit <- min(slen-1, fLimit);
    
    baseVal <- c(A=1, C=2, G=3, T=4)[mySeq];

    dotPlot <- outer(baseVal, baseVal, "==");
    
    spectrum <- sapply(2:min(slen/2,fLimit), function(cycle){
        (head(c(baseVal,rep("0",slen)), -cycle) == tail(c(baseVal,rep(0,slen)), -cycle))[1:slen];
    });

    png(sprintf("out_%03d.png", si));
    image(spectrum);
    invisible(dev.off());
    print(c(si, which(rowSums(spectrum) == max(rowSums(spectrum)))));

}
