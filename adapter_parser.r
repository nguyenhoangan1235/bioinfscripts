#!/usr/bin/env Rscript
ad.df <- read.delim('adapter_assignments_all.tsv.gz',
                    header=FALSE, col.names = c("bc","target","query","dir"),
                    stringsAsFactors=FALSE);

library(dplyr);
library(tidyr);

## Create table of adapter additions
ad.tbl <- group_by(ad.df, query, bc, target, dir) %>%
     summarise() %>%
     unite(tdir, target, dir, sep='.') %>% mutate(present=TRUE) %>%
    spread(tdir, present, fill=FALSE);

## Clean up adapter names
colnames(ad.tbl) <- sub("\\.\\-","rev",colnames(ad.tbl));
colnames(ad.tbl) <- sub("\\.\\+","fwd",colnames(ad.tbl));
colnames(ad.tbl) <- sub("^ONT_","",colnames(ad.tbl));

## read is considered 'valid' if there is only one SSP primer match
ad.valid.tbl <- filter(ad.tbl, (SSPrev | SSPfwd), !(SSPfwd & SSPrev));

## read is considered 'ideal' if the [single] VNP primer is the complement of the SSP primer
ad.ideal.tbl <- filter(ad.valid.tbl, (SSPrev & VNPfwd) | (SSPfwd & VNPrev), !(VNPrev & VNPfwd));

write.csv(ad.ideal.tbl, row.names=FALSE,
  file=gzfile('barcode-adapter_assignments_ideal.csv.gz'), quote=FALSE);
write.csv(ad.valid.tbl, row.names=FALSE,
  file=gzfile('barcode-adapter_assignments_valid.csv.gz'), quote=FALSE);
