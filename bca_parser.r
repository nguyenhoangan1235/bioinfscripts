#!/usr/bin/env Rscript
bc.df <- read.csv("barcode_assignments_all.csv.gz", stringsAsFactors=FALSE);
ad.df <- read.csv("adapter_assignments_all.csv.gz", stringsAsFactors=FALSE);

library(dplyr);
library(tidyr);

## Create table of adapter additions
ad.tbl <- group_by(ad.df, query, target, dir) %>% summarise() %>%
    unite(tdir, target, dir, sep=".") %>% mutate(present=TRUE) %>%
    spread(tdir, present);

## collapse multiple query/target pairs into one
bc.tbl <- group_by(bc.df, query, target) %>% summarise(dir=paste(unique(dir), collapse="/"));
bc.wide <- spread(bc.tbl, target, dir);

## identify reads with a unique barcode
bc.unique.tbl <- group_by(bc.tbl, query) %>% summarise(n = n()) %>%
    filter(n == 1) %>% select(-n) %>% left_join(bc.tbl, by="query") %>%
    left_join(ad.tbl, by="query", copy=TRUE);

bc.unique.tbl$`ONT_SSP.-`[is.na(bc.unique.tbl$`ONT_SSP.-`)] <- FALSE;
bc.unique.tbl$`ONT_SSP.+`[is.na(bc.unique.tbl$`ONT_SSP.+`)] <- FALSE;
bc.unique.tbl$`ONT_VNP.-`[is.na(bc.unique.tbl$`ONT_VNP.-`)] <- FALSE;
bc.unique.tbl$`ONT_VNP.+`[is.na(bc.unique.tbl$`ONT_VNP.+`)] <- FALSE;

colnames(bc.unique.tbl) <- c("query","target","bcDir","SSPrev","SSPfwd","VNPrev","VNPfwd");

## read is considerd "valid" (for now) if at least one primer matches
bc.valid.tbl <- filter(bc.unique.tbl, (SSPrev | VNPfwd | VNPrev | SSPfwd));
## ideal reads have forward and reverse cDNA adapters in opposing orientations
bc.ideal.tbl <- filter(bc.unique.tbl, ((SSPrev & !SSPfwd & VNPfwd & !VNPrev) | (!SSPrev & SSPfwd & !VNPfwd & VNPrev)));

write.csv(bc.ideal.tbl, row.names=FALSE, file=gzfile("barcode-adapter_assignments_ideal.csv.gz"), quote=FALSE);
write.csv(bc.valid.tbl, row.names=FALSE, file=gzfile("barcode-adapter_assignments_valid.csv.gz"), quote=FALSE);
