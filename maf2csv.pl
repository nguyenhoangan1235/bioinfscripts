#!/usr/bin/env perl

## maf2csv.pl -- convert MAF output into CSV format, showing start and end
## sequence locations, as well as matched proportions and gap-compressed
## identities.

use warnings;
use strict;

use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

my $seqFileName = "";
my $ignoreSelf = 0;
my $includeQuery = 0;
my $includeTarget = 0;
my $lengthPctTarget = 0;
my $lengthPctQuery = 0;
my $minLength = 0;

GetOptions("seqfile=s" => \$seqFileName, "overlap!" => \$ignoreSelf,
           "query!" => \$includeQuery, "target!" => \$includeTarget,
           "min=i" => \$minLength,
           "pct=i" => \$lengthPctTarget, "pcq=i" => \$lengthPctQuery) or
  die("Error in command line arguments");

my %seqs = ();
my %quals = ();

if(keys(%seqs)){
  printf(STDERR "Read in %d sequences\n", scalar(keys(%seqs)));
}

if($minLength){
  printf(STDERR "Setting minimum length to %d\n", $minLength);
}

my $qSeq = "";
my $qStart = 0;
my $qEnd = 0;
my $qLen = 0;
my $qStrand = "";
my $qMatchLen = 0;
my $qName = "";
my $tSeq = "";
my $tStart = 0;
my $tEnd = 0;
my $tMatchLen = 0;
my $tLen = 0;
my $tName = "";

my %matches = ();

print("query,target,dir,qS,qE,qML,qL,qPct,tS,tE,tML,tL,tPct,gci,mm");
print(",qStr") if($includeQuery);
print(",tStr") if($includeTarget);
print("\n");

while(<>){
  if(!/^[as]/){
    next;
  }
  my @F = split(/\s+/);
  if($F[0] eq "a"){
    $qSeq = "";
    $tSeq = "";
  } elsif($F[0] eq "s"){
    if($tSeq){
      $qName = $F[1];
      $qStart = $F[2];
      $qMatchLen = $F[3];
      $qEnd = $qStart + $qMatchLen;
      $qStrand = $F[4];
      $qLen = $F[5];
      $qSeq = uc($F[6]);
      if($qStrand eq "-"){ ## correct for reverse complement
        $qEnd = $qLen - $qStart;
        $qStart = $qEnd - $qMatchLen;
      }
      ## calculate gap-compressed identity using Heng Li's algorithm
      ## [https://lh3.github.io/2018/11/25/on-the-definition-of-sequence-identity\#gap-compressed-identity]
      my $idStr = "";
      my @qss = split(//, $qSeq);
      my @tss = split(//, $tSeq);
      for(my $i = 0; $i < length($qSeq); $i++){
        ## If either is a gap, then treat as "I", otherwise "=" for identity,
        ## and "X" for mismatch
        $idStr .= (($qss[$i] eq "-") || ($tss[$i] eq "-")) ? "I" :
          ($qss[$i] eq $tss[$i]) ? "=" : "X";
      }
      ## gap compression
      $idStr =~ s/I+/I/g;
      my $idCnt = ($idStr =~ tr/=//);
      my $mmCnt = ($idStr =~ tr/XI//);
      my $gci = $idCnt / ($idCnt + $mmCnt) * 100;
      my $qpct = ($qMatchLen / $qLen) * 100;
      my $tpct = ($tMatchLen / $tLen) * 100;
      my $matchLine =
        sprintf("%s,%s,%s,%d,%d,%d,%d,%0.2f,%d,%d,%d,%d,%0.2f,%0.2f,%d",
                $qName, $tName, $qStrand,
                $qStart, $qEnd, $qMatchLen, $qLen, $qpct,
                $tStart, $tEnd, $tMatchLen, $tLen, $tpct,
                $gci, $mmCnt);
      if((!$ignoreSelf || ($qName ne $tName)) &&
         ($qLen > $minLength) &&
         (($qpct > $lengthPctQuery) || ($tpct > $lengthPctTarget))){
        print("${matchLine}");
        print(",${qSeq}") if($includeQuery);
        print(",${tSeq}") if($includeTarget);
        print("\n");
      }
      $matches{$qName}{$qStart} .= ":matchLine";
   } else {
      $tName = $F[1];
      $tStart = $F[2];
      $tMatchLen = $F[3];
      $tEnd = $tStart + $tMatchLen;
      $tLen = $F[5];
      $tSeq = uc($F[6]);
    }
  }
}

