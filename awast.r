#!/usr/bin/RScript
library(dplyr);
library(tidyr);

data.df <- read.csv("GC327_DS7_1_mapped_aggregateData_targetBarcodes.csv", stringsAsFactors=FALSE);

barcodeMap <- list("chrM" = c("bc09","bc10","bc11","bc12"),
                   "CD45_genomic" = c("bc01","bc02","bc03","bc05", "bc06"));
dataRanges <- list("chrM" = c(9013,11147),
                   "CD45_genomic" = c(19688,20168));

dateStr <- format(Sys.Date(), "%Y-%b-%d");

pdf(sprintf("awast_output_%s.pdf", dateStr), width=11, height=8);
par(mfrow=c(2,1));
for(assembly in unique(data.df$Assembly)){
    ## only concentrate on data with the correct barcode/sample pairs
    sub.data.tbl <- as.tbl(data.df) %>%
        filter(Assembly == assembly, Sample %in% barcodeMap[[assembly]]);
    ## identify the range of data that is covered by all samples
    dataRange <- dataRanges[[assembly]];
    ## #dataRange <- range((group_by(sub.data.tbl, Sample, Position) %>% summarise() %>%
    ## #                    group_by(Position) %>% summarise(bcCount=length(Sample)) %>%
    ## #                    filter(bcCount == max(bcCount)))$Position)
    ## filter out the data based on this range
    fullData <- filter(sub.data.tbl, between(Position, dataRange[1], dataRange[2])) %>%
        arrange(Position, Sample) %>% mutate(A=NULL, C=NULL, G=NULL, T=NULL, ref=NULL, Assembly=NULL) %>%
        gather("mutType","count",-Position, -Sample);
    ## create structured array from the data range
    fullData.arr <- unclass(xtabs(count ~ Sample + mutType + Position, data=fullData));
    ## determine allelic chi^2 at each location, where the expected values represent
    ## the average proportion of each allele (ref, Trs, Trv, Cmp, d, i) in all samples (with prior
    ## normalisation to account for read count total differences)
    pos.chisq <-
        data.frame(X=as.numeric(dimnames(fullData.arr)$Position),
                   Y=apply(fullData.arr, c(3), function(x){
                       chi.arr.obs <- x;
                       mean.profile <- colSums(chi.arr.obs * 1/rowSums(chi.arr.obs),
                                               na.rm=TRUE) / sum(rowSums(chi.arr.obs) > 0)
                       ## expected: assume proportions of each variant type are similar
                       chi.arr.exp <-
                           matrix(rep(mean.profile, each=nrow(chi.arr.obs)) *
                                  rep(rowSums(chi.arr.obs), ncol(chi.arr.obs)),
                                  nrow=nrow(chi.arr.obs));
                       sum((chi.arr.obs-chi.arr.exp)^2 * 1/chi.arr.exp, na.rm=TRUE);
                   }));
    ## make a graph of the differences
    par(mgp=c(2.5,1,0), las=1, mar=c(4,5,2.5,0.5));
    plot(x=pos.chisq$X, y=log(pos.chisq$Y), ylab=expression(log(chi^2)),
         xlab=sprintf("%s Location", assembly),
         main=sprintf("Suspiciously-variable locations (%s)", assembly));
    ## plot outliers (wouldn't expect these to be seen in any bases)
    outlier.threshold <- qnorm((nrow(pos.chisq)*10-1)/(nrow(pos.chisq)*10),
                               mean=mean(log(pos.chisq$Y)), sd=sd(log(pos.chisq$Y)));
    sigPoints <- which(log(pos.chisq$Y) > outlier.threshold);
    text(x=pos.chisq$X[sigPoints], y=log(pos.chisq$Y[sigPoints]), labels=pos.chisq$X[sigPoints],
         pos=4, cex=0.5);
}
invisible(dev.off());
