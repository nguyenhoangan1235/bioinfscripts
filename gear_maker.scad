// Creates gear teeth along a path in 2D space

// The tooth maps a triangle wave to a path; when that path is a circle,
// the resultant curve is an involute curve
// Note: gear tooth points are placed in the middle of the path points to simplify
//       calculation of path normals

// the nuts and bolts of the triangle wave function
function toothOffset(toothHeight, toothWidth, traversedDist) =
   (abs((traversedDist % toothWidth) - (toothWidth/2)) - toothWidth/4) *
      (toothHeight / toothWidth);


function mapTooth(pt, normAng, toothHeight, toothWidth, traversedDist) =
   [pt[0] + cos(normAng) * toothOffset(toothHeight, toothWidth,
    traversedDist),
    pt[1] + sin(normAng) * toothOffset(toothHeight, toothWidth,
    traversedDist)];
    
function mapBevel(sr, startAng=0, endAng=359, res=1) = 0;

function getNormal(p1,p2) =
     atan2((p2-p1)[1],(p2-p1)[0])+90;

// cumulative distance function
function cumDist(distPath, distArray = [0]) =
   (len(distArray) >= len(distPath)) ?
     distArray :
     cumDist(distPath, concat(distArray,
       [norm(distPath[len(distArray)-1] - distPath[len(distArray)])
        + distArray[len(distArray)-1]]));

pi=3.141592654;
xw=0.4; // extrude width

module toothExtrude(
    extrudePath = [for (t = [-180:0.25:179]) [cos(t), sin(t)]*20],
    toothWidth = 40 * pi / 6,
    toothHeight = 40
    ){
    extrudePathDist = cumDist(extrudePath);
    toothPath = [for (i = [1 : (len(extrudePath)-1)] )
        mapTooth((extrudePath[i-1]+extrudePath[i])/2,
                  getNormal(extrudePath[i-1],extrudePath[i]),
                  toothHeight=toothHeight, toothWidth=toothWidth,
                  traversedDist = (extrudePathDist[i-1] + extrudePathDist[i])/2)];
    polygon(points = toothPath);
}

module toothExtrudeBevel(
    extrudePath = [for (t = [-180:0.25:179]) [cos(t), sin(t)]*20],
    toothWidth = 40 * pi / 6,
    toothHeight = 40,
    bevelAngle = 45
    ){
    extrudePathDist = cumDist(extrudePath);
    toothPath = [for (i = [1 : (len(extrudePath)-1)] )
        mapTooth((extrudePath[i-1]+extrudePath[i])/2,
                  getNormal(extrudePath[i-1],extrudePath[i]),
                  toothHeight=toothHeight*sin(bevelAngle), toothWidth=toothWidth*sin(bevelAngle),
                  traversedDist = (extrudePathDist[i-1] + extrudePathDist[i])/2)];
    polygon(points = toothPath);
}


module circularGear(radius = -1, toothPitch = -1, toothCount = 7,
                    addendum = 7, dedendum = 6, pressureAngle = 15.5){
    if(((radius == -1) && (toothPitch == -1)) || ((radius != -1) && (toothPitch != -1))){
        echo("one (and only one) of radius or toothPitch must be specified");
        echo("Tooth pitch: ", toothPitch, "Radius: ", radius);
    }
    tw = (toothPitch == -1) ? 2 * radius * pi / toothCount : toothPitch;
    gr = (radius == -1) ? (tw * toothCount) / (2 * pi) : radius;
    union(){
        intersection(){
            th = (tw/2) / tan(pressureAngle);
            toothExtrude(extrudePath = [for (t = [-180:1:179]) [cos(t), sin(t)]*gr],
                toothWidth = tw, toothHeight = th);
            circle(r=gr+addendum, $fn=360);
        }
        circle(r=gr-dedendum, $fn=360);
    }
    echo("Tooth width: ", tw, "Radius: ", gr);
}

module bevelGear(radius = -1, toothPitch = -1, bevelAngle = 45,
                 width = 10, toothCount = 11, addendum = 7, dedendum = 6,
                 pressureAngle = 15.5){
    if(((radius == -1) && (toothPitch == -1)) || ((radius != -1) && (toothPitch != -1))){
        echo("one (and only one) of radius or toothPitch must be specified");
        echo("Tooth pitch: ", toothPitch, "Radius: ", radius);
    }
    tw = (toothPitch == -1) ? 2 * radius * pi / toothCount : toothPitch;
    gr = (radius == -1) ? (tw * toothCount) / (2 * pi) : radius;
    sr = gr / sin(bevelAngle); // sphere radius on which the circle lies
    so = gr / tan(bevelAngle); // sphere centre offset that describes circle
    gr1 = gr + cos(bevelAngle) * width/2;
    gr2 = gr - cos(bevelAngle) * width/2;
    echo("gr1: ", gr1, "gr2: ", gr2);
    tw1 = 2 * gr1 * pi / toothCount;
    tw2 = 2 * gr2 * pi / toothCount;
    th = (tw/2) / tan(pressureAngle);
    th1 = (tw1/2) / tan(pressureAngle);
    th2 = (tw2/2) / tan(pressureAngle);
    gang = atan((gr2-gr1) / width);
    // TODO: the actual path should blend smoothly from triangle wave (angle == 0) to 
    //       to involute (angle == 90)
    epath0 = (bevelAngle == 0) ?
      [for (t = [-tw/2:0.02:(tw/2-0.01)]) [gr,t]] :
          [for (t = [-180:5:179]) [cos(t/(toothCount)), sin(t/(toothCount))]*gr];
    epath1 = (bevelAngle == 0) ?
      [for (t = [-tw1/2:0.02:(tw1/2-0.01)]) [gr1,t]] :
          [for (t = [-180:5:179]) [cos(t/(toothCount)), sin(t/(toothCount))]*gr1];
    epath2 = (bevelAngle == 0) ?
      [for (t = [-tw2/2:0.02:(tw2/2-0.01)]) [gr2,t]] :
          [for (t = [-180:5:179]) [cos(t/(toothCount)), sin(t/(toothCount))]*gr2];
    !intersection(){
        for(i = [0:(toothCount-1)*0]){
            rotate(360*i / toothCount){
                translate([gr,0,width - sin(bevelAngle)*(width/2)]) 
                    rotate([0,bevelAngle,0]) translate([-gr,0,0]) hull(){
                        translate([gr+width/2,0,dedendum*0]) rotate([0,-90,0])
                            translate([-gr1,0,0]) linear_extrude(height=0.01) difference(){
                                toothExtrude(extrudePath = epath1,
                                    toothWidth = tw1, toothHeight = th1);
                            }
                        translate([gr,0,dedendum*0]) rotate([0,-90,0])
                            translate([-gr,0,0]) linear_extrude(height=0.01) difference(){
                                toothExtrude(extrudePath = epath0,
                                    toothWidth = tw, toothHeight = th);
                            }
                        translate([gr-width/2,0,dedendum*0]) rotate([0,-90,0])
                            translate([-gr2,0,0]) linear_extrude(height=0.01) difference(){
                                toothExtrude(extrudePath = epath2,
                                    toothWidth = tw2, toothHeight = th2);
                            }
                    }
            }
        }
        translate([0,0,-so + (width-width/2*sin(bevelAngle))]) sphere(r=sr+addendum, $fn = toothCount*4);
        echo(so);
        %translate([0,0,-so + (width-width/2*sin(bevelAngle))]) sphere(r=sr-dedendum, $fn = toothCount*4);
    }
    if(bevelAngle == 0){
        translate([0,0,0]) cylinder(r=gr1, h=width, $fn=60);
    } else { // this could alternatively be a sliced sphere of radius sr
        hull(){
            translate([0,0,(width-width*sin(bevelAngle))])
                cylinder(r1=gr1, r2=gr2, h=width*sin(bevelAngle), $fn=60);
            translate([0,0,0])
                cylinder(r=gr1, h=width-width*sin(bevelAngle), $fn=60);
        }
    }
    echo("Tooth widths: ", tw1, tw2, "Radiuses: ", gr1, gr2);
}

adj=-0.000;
translate([-65,0,0]) union(){
    color("blue") {
        translate([0,0,9]) linear_extrude(height=11){
            difference(){
                circularGear(toothPitch=5, toothCount=7, addendum=1.5, dedendum=1.5);
                circle(d=5+xw, $fn=60);
            }
        }
    }
    color("yellow") {
         linear_extrude(height=10){
            difference(){
                circularGear(toothPitch=5, toothCount=29, addendum=1.5, dedendum=1.5);
                difference(){
                    circle(r=19.5, $fn=60);
                    circle(r=8, $fn=60);
                    for(r = [0:120:359]){
                        rotate(r) translate([0,-2.5]) square([50,5]);
                    }
                }
                circle(d=5+xw, $fn=60);
            }
        }
    }
}

difference(){
    union(){
        color("red") difference() {
            translate([0,0,0]) difference(){
                linear_extrude(height=10) {
                    difference(){
                        circularGear(toothPitch=5, toothCount=47, addendum=1.5, dedendum=1.5);
                        difference(){
                            circle(r=34, $fn=60);
                            circle(r=12.5, $fn=60);
                            for(r = [0:120:359]){
                                rotate(r) translate([0,-2.5]) square([50,5]);
                            }
                        }
                    }
                }
            }
            translate([0,0,-1]) cylinder(d=5+xw, h=27, $fn=60);
        }
        color("green") difference() {
            union(){
                translate([0,0,9]) cylinder(r=12.5, h=11, $fn=60);
                translate([0,0,20-(6-sin(45)*6)])
                    bevelGear(toothPitch=5, toothCount = 11, width=5,
                              addendum=1.5, dedendum=1.5);
            }
            translate([0,0,-1]) cylinder(d=5+xw, h=27, $fn=60);
        }
    }
}

translate([-45, 50, 5]) union(){
    difference(){
        translate([0,0,0]) bevelGear(toothPitch=5, toothCount = 29, width=5,
                  addendum=1.5, dedendum=1.5);
        translate([0,0,-1]) cylinder(d=5+xw, h=6+xw, $fn=60);
        translate([15,0,-1]) cylinder(d=8+xw, h=7, $fn=60);
    }
}