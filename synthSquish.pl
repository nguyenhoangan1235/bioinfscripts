#!/usr/bin/env perl

use warnings;
use strict;

use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

our $VERSION = "0.1";

## synthSquish.pl -- Squish sequences from synthetic BD barcodes to ease parsing

my $fastqFileName = "";
my $bcFileNames = "";
my $bcType = "short";
my %linkerSeqs = (
  "long" => ["ACTGGCCTGCGA", "GGTAGCGGTGACA"],
      "short" => ["GTGA", "GACA"]);

##print(join(";", @{$linkerSeqs{"long"}})."\n");
##print(join(";", @{$linkerSeqs{"short"}})."\n");

sub usage {
  printf(STDERR "Usage: ./synthSquish.pl [opts] -fastq <reads_R1.fastq.gz>\n");
  printf(STDERR "\nOther Options:\n");
  printf(STDERR "  -bc f1,f2,f3 : Load barcode lists from f1,f2,f3\n");
  printf(STDERR "  -type t      : Set barcode type to short / 50bp or long / 75bp\n");
}

GetOptions("fastq=s" => \$fastqFileName, "bc=s" => \$bcFileNames, "bctype=s" => \$bcType)
    or exit(1);

if($bcType eq "50bp"){
  $bcType = "short";
}

if($bcType eq "75bp"){
  $bcType = "long";
}

if(($bcType ne "short") && ($bcType ne "long")){
  printf(STDERR "Error: barcode type '%s' not understood; should be 'short' or 'long'\n");
  usage();
  exit(1);
}

## Note from "BD Bioinformatics Handbook 54169 Rev 6.0":
## By design, each CLS has one of 96 predefined sequences, which has a
## Hamming distance of at least four bases and an edit distance of at
## least two bases apart.
## This script uses lookup hashes to identify sequences that have hamming
## distances of two or less, or edit distances of 1.

our %cellLookups = ();
our %linkerLookups = ();
our %tLookups = ();

sub printSequence {
  my ($id, $seq, $qual) = @_;
  my $endBlobSeq = "";
  my $endBlobQual = "";
  # Long sequence:
  # 0123456789012345678901234567890123456789012345678901234567890123456789012345
  # [--BC1--][----L1----][--BC2--][----L02----][--BC3--][-UMI1-][TTTTTTTTTTTTTT]
  # Short sequence:
  # 012345678901234567890123456789012345678901234567890
  # [--BC1--][L1][--BC2--][L2][--BC3--][-UMI1-][TTTTTT]
  # Note: short sequence can also be prepended with A/GT/TCA to improve Illumina base
  # call distributions, i.e.
  # [--BC1--][L1][--BC2--][L2][--BC3--][-UMI1-][TTTTTT]
  # A[--BC1--][L1][--BC2--][L2][--BC3--][-UMI1-][TTTTT]
  # GT[--BC1--][L1][--BC2--][L2][--BC3--][-UMI1-][TTTT]
  # TCA[--BC1--][L1][--BC2--][L2][--BC3--][-UMI1-][TTT]
  my ($bc1pos, $bc2pos, $bc3pos,  $l1pos, $l2pos,  $l1len, $l2len,  $UMIpos, $polyTpos) =
      (0, 21, 43,   9, 30,   12, 13,   52, 60);
  if($bcType eq "short"){
    ($bc1pos, $bc2pos, $bc3pos,   $l1pos, $l2pos,  $l1len, $l2len,  $UMIpos, $polyTpos) =
        (0, 13, 26,   9, 22,   4, 4,   35, 43);
    ## Check to find the most reasonable linker sequence match position
    my $linkOffset = $linkerLookups{substr($seq, $l1pos, $l1len)} ? 0 :
        $linkerLookups{substr($seq, $l1pos + 1, $l1len)} ? 1 :
        $linkerLookups{substr($seq, $l1pos + 2, $l1len)} ? 2 :
        $linkerLookups{substr($seq, $l1pos + 3, $l1len)} ? 3 : 0;
    $endBlobSeq  = ($linkOffset == 0) ? "" : substr($seq,  0, $linkOffset, "");
    $endBlobQual = ($linkOffset == 0) ? "" : substr($qual, 0, $linkOffset, "");
  }
  if(length($seq) <= $polyTpos){
    printf("@%s [short]\n%s\n+\n%s\n", $id, $seq, $qual);
    return;
  }
  my $c1 = substr($seq, $bc1pos, 9);
  my $c2 = substr($seq, $bc2pos, 9);
  my $c3 = substr($seq, $bc3pos, 9);
  ## Correct cell barcode sequence
  $c1 = $cellLookups{1}{$c1} if ($cellLookups{1}{$c1});
  $c2 = $cellLookups{2}{$c2} if ($cellLookups{2}{$c2});
  $c3 = $cellLookups{3}{$c3} if ($cellLookups{3}{$c3});
  my $qcPassed = "";
  $qcPassed .= ($cellLookups{1}{$c1}) ? "T" : "F";
  $qcPassed .= ($cellLookups{2}{$c2}) ? "T" : "F";
  $qcPassed .= ($cellLookups{3}{$c3}) ? "T" : "F";
  my $clss = $c1.$c2.$c3;
  my $clsq =
    substr($qual, $bc1pos, 9).
    substr($qual, $bc2pos, 9).
    substr($qual, $bc3pos, 9);
  my $l1 = substr($seq, $l1pos, $l1len);
  my $l2 = substr($seq, $l2pos, $l2len);
  ## Correct linker sequence
  my $ls = $l1.$l2;
  $l1 = $linkerLookups{$l1} if ($linkerLookups{$l1});
  $l2 = $linkerLookups{$l2} if ($linkerLookups{$l2});
  $qcPassed .= ($linkerLookups{$l1}) ? "T" : "F";
  $qcPassed .= ($linkerLookups{$l2}) ? "T" : "F";
  my $lq =
    substr($qual, $l1pos, $l1len).
    substr($qual, $l2pos, $l2len);
  my $umis = substr($seq, $UMIpos, 8);
  my $umiq = substr($qual, $UMIpos, 8);
  my $tRegion = substr($seq, $polyTpos, 8);
  $qcPassed .= ($tLookups{$tRegion}) ? "T" : "F";
  my $rems = substr($seq, $polyTpos);
  my $remq = substr($qual, $polyTpos);
  printf("@%s prefix='%s' cell=%s UMI=%s QCPass=%s\n%s\n+\n%s\n",
         $id, $endBlobSeq,
         $clss, $umis, $qcPassed,
         $clss.$umis.$ls.$rems,
         $clsq.$umiq.$lq.$remq);
}

## Pre-generate cell ID lookups with hamming distance of two, or edit distance of 1
if($bcFileNames){
  my @bcFileNames = split(/,/, $bcFileNames);
  my $bcPos = 1;
  foreach my $bcFileName (@bcFileNames){
    open(my $bcFile, "<", $bcFileName) or
      die("Unable to open $bcFileName\n");
    while(<$bcFile>){
      chomp;
      chomp;
      my $baseSeq = $_;
      my $seq = $baseSeq;
      ## Two substitutions
      for(my $i1 = 0; $i1 < length($baseSeq); $i1++){
        foreach my $b1 ("A", "C", "G", "T", "N"){
          for(my $i2 = 0; $i2 < length($baseSeq); $i2++){
            foreach my $b2 ("A", "C", "G", "T", "N"){
              $seq = $baseSeq;
              substr($seq, $i1, 1, $b1);
              substr($seq, $i2, 1, $b2);
              ##printf(STDERR "Adding $seq -> $baseSeq\n");
              $cellLookups{$bcPos}{$seq} = $baseSeq;
            }
          }
        }
      }
      # One deletion
      for(my $i1 = 0; $i1 < length($baseSeq); $i1++){
        foreach my $b1 ("A", "C", "G", "T", "N"){
	    $seq = $baseSeq . $b1;
	    substr($seq, $i1, 1, "");
	    ##printf(STDERR "Adding $seq -> $baseSeq\n");
	    $cellLookups{$bcPos}{$seq} = $baseSeq;
	}
      }
      # One insertion
      for(my $i1 = 0; $i1 < length($baseSeq); $i1++){
        foreach my $b1 ("A", "C", "G", "T", "N"){
	    $seq = $baseSeq;
	    substr($seq, $i1, 0, $b1);
	    substr($seq, length($baseSeq), 1, "");
	    ##printf(STDERR "Adding $seq -> $baseSeq\n");
	    $cellLookups{$bcPos}{$seq} = $baseSeq;
	}
      }
    }
    $bcPos++;
  }
} else {
  printf(STDERR "Error: barcode file names not provided\n");
  usage();
  exit(1);
}

## Pre-generate linker lookups with hamming distance of two, or edit distance of one
foreach my $linker (@{$linkerSeqs{$bcType}}){
  for(my $i1 = 0; $i1 < length($linker); $i1++){
    foreach my $b1 ("A","C","G","T"){
      for(my $i2 = 0; $i2 < length($linker); $i2++){
        foreach my $b2 ("A","C","G","T"){
          my $seq = $linker;
          substr($seq, $i1, 1, $b1);
          if($bcType eq "long"){ # only use hamming distance 2 for long linkers
            substr($seq, $i2, 1, $b2);
          }
          $linkerLookups{$seq} = $linker;
        }
      }
    }
  }
  if($bcType eq "long"){ # only do INDELs for long linkers
    # One deletion
    for(my $i1 = 0; $i1 < length($linker); $i1++){
      foreach my $b1 ("A","C","G","T"){
        my $seq = $linker . $b1;
        substr($seq, $i1, 1, "");
        $linkerLookups{$seq} = $linker;
      }
    }
    # One insertion
    for(my $i1 = 0; $i1 < length($linker); $i1++){
      foreach my $b1 ("A","C","G","T"){
        my $seq = $linker;
        substr($seq, $i1, 0, $b1);
        substr($seq, length($linker), 1, "");
        $linkerLookups{$seq} = $linker;
      }
    }
  }
}


## Pre-generate polyT lookups with hamming distance of two
foreach my $tRegion ("TTTTTTTT"){
  for(my $i1 = 0; $i1 < length($tRegion); $i1++){
    foreach my $b1 ("A","C","G","T"){
      for(my $i2 = 0; $i2 < length($tRegion); $i2++){
        foreach my $b2 ("A","C","G","T"){
          my $seq = $tRegion;
          substr($seq, $i1, 1, $b1);
          substr($seq, $i2, 1, $b2);
          $tLookups{$seq} = $tRegion;
        }
      }
    }
  }
  ## [edit distance of 1 formed from INDELs is redundant]
}

my $inQual = 0;
my $seq = "";
my $seqID = "";
my $qual = "";
my $qualID = "";

if($fastqFileName){
  # read sequence IDs from input file
  my $fastqFile = new IO::Uncompress::Gunzip "$fastqFileName" or
    die("Unable to open $fastqFileName\n");
  while (<$fastqFile>) {
    chomp;
    chomp;
    if (/^\s+$/) {
      next;
    }
    if (!$inQual) {
      if (/^@(.+)$/) {
        $seqID = $1;
        $seq = "";
      } elsif (/^\+(.*)$/) {
        $inQual = 1;            # true
        $qualID = $1;
        $qual = "";
      } else {
        $seq .= $_;
      }
    } else {
      $qual .= $_;
      if (length($qual) >= length($seq)) {
        printSequence($seqID, $seq, $qual);
        $inQual = 0;            # false
      }
    }
  }
  close($fastqFile);
}
