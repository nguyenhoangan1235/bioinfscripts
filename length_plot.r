#!/usr/bin/env Rscript

## Read length distribution plotter; expects input files named 'lengths_<id>.txt[.gz]'
## The first [space-separated] column will be processed as sequence lengths

## example usage: ./length_plot.r lengths_[0-9]*.txt

scriptArgs <- commandArgs(TRUE);

fileNames <-
    c("/mnt/gg_nanopore/gringer/PacBio/melanoma2019_IsoSeq/lengths_flnc.txt.gz");

plotVals <- TRUE;
plotHoriz <- TRUE;
plotCombined <- FALSE;
plotRange <- c(20, Inf);

fileNames <- scriptArgs[file.exists(scriptArgs)];
scriptArgs <- scriptArgs[!file.exists(scriptArgs)];

if("-n" %in% scriptArgs){
    plotVals <- FALSE;
}

if("-h" %in% scriptArgs){
    plotHoriz <- FALSE;
}

if("-c" %in% scriptArgs){
    plotCombined <- TRUE;
}

if("-r" %in% scriptArgs){
    rangePos <- which("-r" %in% scriptArgs);
    plotRange <- as.numeric(scriptArgs[c(rangePos+1, rangePos+2)]);
}

if(length(fileNames) == 0){
    fileNames <- list.files(pattern="lengths_.*\\.txt(\\.gz)?");
}

valToSci <- function(val, unit = ""){
    sci.prefixes <- c("", "k", "M", "G", "T", "P", "E", "Z", "Y");
    units <- rep(paste(sci.prefixes,unit,sep=""), each=3);
    logRegion <- floor(log10(val))+1;
    conv.units <- units[logRegion];
    conv.div <- 10^rep(0:(length(sci.prefixes)-1) * 3, each = 3)[logRegion];
    conv.val <- val / conv.div;
    conv.val[val == 0] <- 0;
    conv.units[val == 0] <- unit;
    return(sprintf("%s %s",conv.val,conv.units));
}

sequence.hist <- function(lengths, horiz = TRUE, barValues=TRUE, invert = TRUE, main = "", ...){
    fib.divs <- round(10^((0:4)/5) * 2) * 0.5; ## splits log decades into 5
    histBreaks <- round(rep(10^(0:16),each=5) * fib.divs);
    lengthRange <- range(lengths);
    ## filter on actual data range
    histBreaks <- unique(histBreaks[(which.min(histBreaks < lengthRange[1])-1):
                                    which.max(histBreaks > lengthRange[2])]);
    seqd.bases <- seqd.na.bases <- tapply(lengths,cut(lengths, breaks=histBreaks), sum);
    seqd.counts <- seqd.na.counts <- tapply(lengths,cut(lengths, breaks=histBreaks), length);
    seqd.bases[is.na(seqd.bases)] <- 0;
    seqd.counts[is.na(seqd.counts)] <- 0;
    xBreaks <- round(rep(10^(0:16),each=5) * fib.divs);
    axRange <- range(seqd.bases);
    xBreaks <- xBreaks[(which.min(xBreaks < axRange[1])):
                       (which.max(xBreaks > axRange[2])-1)];
    xBreaksMajor <- xBreaks[log10(xBreaks) - floor(log10(xBreaks)) < 0.001];
    xBreaksMinor <- rep(xBreaksMajor,each=9) * 1:9;
    xBreaksMinor <- xBreaksMinor[(which.min(xBreaksMinor < axRange[1])):
                                 (which.max(xBreaksMinor > axRange[2])-1)];
    barPos <- barplot(if(invert){seqd.na.bases} else {rev(seqd.na.bases)}, main = "",
                      log = ifelse(horiz, "x", "y"), axes = FALSE, col = "steelblue",
                      horiz = horiz, names.arg = rep("",length(seqd.bases)), ann=FALSE,
                      ...);
    barGap <- diff(barPos)[1];
    barOffset <- barPos[1] - barGap/2;
    axis(ifelse(horiz,2,1), at = if(!invert){rev(seq(barOffset,by=barGap,
                                    length.out = length(histBreaks)))}
         else {seq(barOffset,by=barGap,length.out = length(histBreaks))},
         labels = valToSci(histBreaks,"b"), las = 2, cex.axis=ifelse(horiz,1,1.41));
    mtext(main, side=3, line=ifelse(horiz,0,1.5), cex=2);
    mtext("Fragment size", side=ifelse(horiz,2,1), line=4);
    mtext("Aggregate sequence length (number of sequences)", side=ifelse(horiz,1,2), line=ifelse(horiz,4,5));
    axis(ifelse(horiz,1,2), at = xBreaksMajor, labels = valToSci(xBreaksMajor, "b"), lwd=3, las=1, cex.axis=1.41);
    axis(ifelse(horiz,1,2), at = c(head(xBreaksMinor,1),tail(xBreaksMinor,1)), labels = FALSE, lwd=3);
    axis(ifelse(horiz,1,2), at = xBreaksMinor, labels = FALSE);
    if(barValues && horiz){
        text.poss <- ((log10(seqd.bases) < mean(par("usr")[1:2]))+1)*2;
        text.poss[is.na(text.poss)] <- 4;
        text.col <- c("white","black")[((log10(seqd.bases) <
                                         mean(par("usr")[1:2]))+1)];
        text(seqd.bases,if(!invert){rev(barPos)} else {barPos},
             paste(valToSci(signif(seqd.bases,4)),
                   " (", seqd.counts, ")", sep = ""),
             pos=text.poss, col=text.col, cex = 0.65);
    }
}

plain.hist <- function(lengths, horiz=TRUE, barValues=TRUE, invert = TRUE, main = "", ...){
    fib.divs <- round(10^((0:4)/5) * 2) * 0.5; ## splits log decades into 5
    histBreaks <- round(rep(10^(0:16),each=5) * fib.divs);
    lengthRange <- range(lengths);
    ## filter on actual data range
    histBreaks <- unique(histBreaks[(which.min(histBreaks < lengthRange[1])-1):
                                    which.max(histBreaks > lengthRange[2])]);
    seqd.bases <- seqd.na.bases <- tapply(lengths,cut(lengths, breaks=histBreaks), sum);
    seqd.counts <- seqd.na.counts <- tapply(lengths,cut(lengths, breaks=histBreaks), length);
    seqd.bases[is.na(seqd.bases)] <- 0;
    seqd.counts[is.na(seqd.counts)] <- 0;
    xBreaks <- round(rep(10^(0:16),each=5) * fib.divs);
    axRange <- range(seqd.counts);
    xBreaks <- xBreaks[(which.min(xBreaks < axRange[1])):
                       (which.max(xBreaks > axRange[2])-1)];
    xBreaksMajor <- xBreaks[log10(xBreaks) - floor(log10(xBreaks)) < 0.001];
    xBreaksMinor <- rep(xBreaksMajor,each=9) * 1:9;
    xBreaksMinor <- xBreaksMinor[(which.min(xBreaksMinor < min(xBreaksMajor))):
                                 (which.max(xBreaksMinor > max(xBreaksMajor))-1)];
    barPos <- barplot(log10(if(!invert){rev(seqd.na.counts)} else {seqd.na.counts})+0.025,
                      las = 1, axes = FALSE, ann=FALSE, col = "steelblue", main = "",
                      horiz = horiz, names.arg = rep("",length(seqd.counts)),
                      xlim=if(!horiz){NULL} else {c(log10(max(seqd.na.counts, na.rm=TRUE))+0.025,
                                                   log10(min(seqd.na.counts, na.rm=TRUE))-0.025)},
                      ylim=if(horiz){NULL} else {c(log10(max(seqd.na.counts, na.rm=TRUE))+0.025,
                                                   log10(min(seqd.na.counts, na.rm=TRUE))-0.025)},
                      ...);
    mtext(main, side=ifelse(horiz,3,1), line=ifelse(horiz,0,1.5), cex=2);
    mtext("Number of sequences (Aggregate length)", side=ifelse(horiz,1,2), line=ifelse(horiz,4,5));
    barGap <- diff(barPos)[1];
    barOffset <- barPos[1] - barGap/2;
    axis(ifelse(horiz,4,3), at = if(!invert){rev(seq(barOffset,by=barGap,
                                                    length.out = length(histBreaks)))}
                                 else {seq(barOffset,by=barGap,length.out = length(histBreaks))},
         labels = valToSci(histBreaks,"b"), las = 2, pos=0, cex.axis=ifelse(horiz,1,1.41));
    mtext("Fragment size", side=ifelse(horiz,4,1), line=5);
    axis(ifelse(horiz,1,2), at = log10(xBreaksMajor)+0.025, labels = valToSci(xBreaksMajor),
         lwd=3, cex.axis=1.41, las=1);
    axis(ifelse(horiz,1,2), at = c(head(xBreaksMinor,1),tail(xBreaksMinor,1)), labels = FALSE, lwd=3);
    axis(ifelse(horiz,1,2), at = log10(xBreaksMinor)+0.025, labels = FALSE);
    if(barValues && horiz){
        text.poss <- ((log10(seqd.counts) > mean(par("usr")[1:2]))+1)*2;
        text.poss[is.na(text.poss)] <- 2;
        text.col <- c("white","black")[((log10(seqd.counts) <
                                         mean(par("usr")[1:2]))+1)];
        text(log10(seqd.counts)+0.025,if(!invert){rev(barPos)} else {barPos},
             paste(seqd.counts,
                   " (", valToSci(signif(seqd.bases,4),"b"), ")", sep = ""),
             pos=text.poss, col=text.col, cex = 0.65);
    }
}

## generate fibonacci bins for logarithmic data storage
sampled.lengths <- NULL;
readStats <- data.frame(bc=NULL, n=NULL, mean=NULL, l10=NULL, l50=NULL, l90=NULL);
fib.divs <- round(10^(seq(0,1-1/5,length.out=5)) * 2) * 0.5; ## splits log decades into 5
fine.divs <- 10^head(seq(0,1,length.out=100),-1);
histBreaks <- unique(c(0,round(rep(10^(0:16),each=5) * fib.divs)));
fineBreaks <- unique(c(0,round(rep(10^(0:16),each=length(fine.divs)) * fine.divs)));
histCentres <- 10^((log10(head(histBreaks,-1)) + log10(tail(histBreaks,-1)))/2);
fineCentres <- 10^((log10(head(fineBreaks,-1)) + log10(tail(fineBreaks,-1)))/2);
## fetch data and store in a table, binned as above
countData <- data.frame(row.names=tail(histBreaks, -1));
baseData <- data.frame(row.names=tail(histBreaks, -1));
fineCounts <- data.frame(row.names=tail(fineBreaks, -1));
fineBases <- data.frame(row.names=tail(fineBreaks, -1)); ## for electrophoresis plots
dens.mat <- sapply(fileNames, function(x){
    cat(x,"...");
    seqd.label <- sub(".txt(.gz)?$","",
                      sub("^lengths_(called_)?(barcode)?","",basename(x)));
    file.data <- scan(x, comment.char=" ", quiet=TRUE);
    ## ignore any reads with length outside the target Range
    file.data <- file.data[(file.data >= plotRange[1]) & (file.data <= plotRange[2])];
    if(length(file.data) == 0){
        cat(" done [no data; discarded]\n");
        return(NULL);
    }
    len.sort <- sort(file.data, decreasing=TRUE);
    len.cumSum <- cumsum(len.sort);
    len.total <- sum(file.data);
    len.mean <- round(len.total / length(file.data));
    len.l10 <- len.sort[min(which(len.cumSum >= len.total * 0.1))];
    len.l50 <- len.sort[min(which(len.cumSum >= len.total * 0.5))];
    len.l90 <- len.sort[min(which(len.cumSum >= len.total * 0.9))];
    readStats <<- rbind(readStats,
                       data.frame(bc=seqd.label, n=length(file.data), mean=len.mean,
                                  l10=len.l10, l50=len.l50, l90=len.l90));
    cat(" done\n");
    #cat("Number of sequences:",length(file.data),"\n");
    #cat("Length quantiles:\n");
    #print(quantile(file.data, probs=seq(0,1,by=0.1)));
    lengthRange <- range(file.data);
    seqd.bases <- tapply(file.data, cut(file.data, breaks=histBreaks), sum);
    seqd.counts <- tapply(file.data, cut(file.data, breaks=histBreaks), length);
    seqd.finecounts <- tapply(file.data, cut(file.data, breaks=fineBreaks), length);
    seqd.finebases <- tapply(file.data, cut(file.data, breaks=fineBreaks), sum);
    seqd.finebases[is.na(seqd.finebases)] <- 0;
    seqd.finecounts[is.na(seqd.finecounts)] <- 0;
    seqd.bases[is.na(seqd.bases)] <- 0;
    seqd.counts[is.na(seqd.counts)] <- 0;
    names(seqd.bases) <- valToSci(tail(histBreaks,-1));
    names(seqd.counts) <- valToSci(tail(histBreaks,-1));
    names(seqd.finecounts) <- valToSci(tail(fineBreaks,-1));
    names(seqd.finebases) <- valToSci(tail(fineBreaks,-1));
    baseData[[seqd.label]] <<- seqd.bases;
    countData[[seqd.label]] <<- seqd.counts;
    fineBases[[seqd.label]] <<- seqd.finebases;
    fineCounts[[seqd.label]] <<- seqd.finecounts;
});
fileNames <- names(fineCounts);
rownames(readStats) <- readStats$bc;
readStats$bc <- NULL;
print(readStats);

## Length 1 reads are meaningless
countData[1,] <- 0;
fineCounts[1,] <- 0;

sumLimitMin <- min(apply(countData,2,function(x){min(which(x > (max(x)/100)))}));
sumLimitFineMin <- min(apply(fineCounts,2,function(x){min(which(x > (max(x)/100)))}));

baseRange <- c(sumLimitMin,
               max(which(rowSums(countData) > 1)) + 15);

baseRangeFine <- c(sumLimitFineMin,
                   max(which(rowSums(fineCounts) > 1)) + 3);

fineCounts <- fineCounts[baseRangeFine[1]:baseRangeFine[2],,drop=FALSE];
fineBases <- fineBases[baseRangeFine[1]:baseRangeFine[2],,drop=FALSE];
fineCounts.mat <- as.matrix(fineCounts);
fineBases.mat <- as.matrix(fineBases);
fineBases.norm.mat <- t(t(fineBases.mat) / colSums(fineBases.mat));
fineBases.cumProp.mat <- apply(fineBases.norm.mat,2,cumsum);

library(viridis);

lineCols <-
    (viridis_pal(option="H"))(ncol(countData));
    ## if(ncol(countData) <= 3){
    ##     brewer.pal(3, "Set3")[1:ncol(countData)];
    ## } else {
    ##     if(ncol(countData) <= 12){
    ##         brewer.pal(ncol(countData), "Set3");
    ##     } else {
    ##         colorRampPalette(brewer.pal(11, "Spectral"))(ncol(countData));
    ##     }
    ## }

pdf("Sequence_curves.pdf", width=16, height=8);
cat("Creating plots...\n");
#### Create density plots ####
## read counts (fine scale)
par(mar=c(4.5, 6.5, 1, 1), las=1, mgp=c(5,1,0), cex.lab=1.5, bg="white");
plot(NA, type="l", log="x", xaxt="n", yaxt="n",
     xlim=c(fineCentres[baseRangeFine[1]], fineCentres[baseRangeFine[2]]),
     ylim=c(0,max(fineCounts.mat)*1.1),
     ylab="Sequenced Reads", xlab = "");
drMax <- max(log10(histCentres[baseRange[2]]));
u <- par("usr");
rect(10^u[1], u[3], 10^u[2], u[4], col="grey40");
axis(1, at=c(1,2,5) * 10^rep(0:15, each=3), cex.axis=1.5,
     lwd = 3,
     labels=valToSci(as.numeric(paste0(c(1,2,5),
                                       rep(substring("00000",first=0,last=0:15),each=3)))));
axis(1, at= (rep(1:9, each=drMax+1) * 10^(0:drMax)), labels=FALSE);
axis(2, at= pretty(fineCounts.mat),
     labels=valToSci(pretty(fineCounts.mat)), cex.axis = 1.5 );
mtext("Read Length (bp)", side=1, line=3, cex=1.5);
for(pcol in 1:ncol(countData)){
    fine.data <- data.frame(x=as.numeric(rownames(fineCounts.mat)), y=fineCounts.mat[,pcol]);
    points(fine.data, type="l", lwd=3,
           col=lineCols[pcol]);
};
legend("topright", fill=lineCols[1:ncol(baseData)],
       legend=colnames(baseData), inset=c(0.025,0.05), cex=1.5,
       bg="#FFFFFFD0");
#### Create density plots ####
## sequenced lengths (fine scale)
par(mar=c(4.5, 6.5, 1, 1), las=1, mgp=c(5,1,0), cex.lab=1.5);
plot(NA, type="l", log="x", xaxt="n", yaxt="n",
     xlim=c(fineCentres[baseRangeFine[1]], fineCentres[baseRangeFine[2]]),
     ylim=c(0,max(fineBases.mat)*1.1),
     ylab="Sequenced Bases", xlab = "");
drMax <- max(log10(fineCentres[baseRangeFine[2]]));
u <- par("usr");
rect(10^u[1], u[3], 10^u[2], u[4], col="grey40");
axis(1, at=c(1,2,5) * 10^rep(0:15, each=3), cex.axis=1.5,
     lwd = 3,
     labels=valToSci(as.numeric(paste0(c(1,2,5),
                                       rep(substring("00000",first=0,last=0:15),each=3)))));
axis(1, at= (rep(1:9, each=drMax+1) * 10^(0:drMax)), labels=FALSE);
axis(2, at= pretty(fineBases.mat),
     labels=valToSci(pretty(fineBases.mat)), cex.axis = 1.5 );
mtext("Read Length (bp)", side=1, line=3, cex=1.5);
for(pcol in 1:ncol(countData)){
    fine.data <- data.frame(x=as.numeric(rownames(fineBases.mat)), y=fineBases.mat[,pcol]);
    points(fine.data, type="l", lwd=3,
           col=lineCols[pcol]);
};
legend("topright", fill=lineCols[1:ncol(baseData)],
       legend=colnames(baseData), inset=c(0.025,0.05), cex=1.5,
       bg="#FFFFFFD0");
## normalised sequenced lengths (fine scale)
par(mar=c(4.5, 6.5, 1, 1), las=1, mgp=c(5,1,0), cex.lab=1.5);
plot(NA, type="l", log="x", xaxt="n", yaxt="n",
     xlim=c(fineCentres[baseRangeFine[1]], fineCentres[baseRangeFine[2]]),
     ylim=c(0,max(fineBases.norm.mat)*1.1),
     ylab="Sequenced Base Proportion", xlab = "");
drMax <- max(log10(fineCentres[baseRangeFine[2]]));
u <- par("usr");
rect(10^u[1], u[3], 10^u[2], u[4], col="grey40");
axis(1, at=c(1,2,5) * 10^rep(0:15, each=3), cex.axis=1.5,
     lwd = 3,
     labels=valToSci(as.numeric(paste0(c(1,2,5),
                                       rep(substring("00000",first=0,last=0:15),each=3)))));


axis(1, at= (rep(1:9, each=drMax+1) * 10^(0:drMax)), labels=FALSE);
axis(2, cex.axis = 1.5 );
mtext("Read Length (bp)", side=1, line=3, cex=1.5);
for(pcol in 1:ncol(countData)){
    fine.data <- data.frame(x=as.numeric(rownames(fineBases.mat)), y=fineBases.norm.mat[,pcol]);
    points(fine.data, type="l", lwd=3,
           col=lineCols[pcol]);
};
legend("topright", fill=lineCols[1:ncol(baseData)],
       legend=colnames(baseData), inset=c(0.025,0.05), cex=1.5,
       bg="#FFFFFFD0");
## sequenced base proportion (fine scale)
par(mar=c(4.5, 6.5, 1, 1), las=1, mgp=c(5,1,0), cex.lab=1.5);
plot(NA, type="l", log="x", xaxt="n", yaxt="n",
     xlim=c(fineCentres[baseRangeFine[1]], fineCentres[baseRangeFine[2]]),
     ylim=c(0,100),
     ylab="Cumulative Sequenced Bases (%)", xlab = "");
u <- par("usr");
rect(10^u[1], u[3], 10^u[2], u[4], col="grey40");
drMax <- max(log10(fineCentres[baseRangeFine[2]]));
axis(1, at=c(1,2,5) * 10^rep(0:15, each=3), cex.axis=1.5,
     lwd = 3,
     labels=valToSci(as.numeric(paste0(c(1,2,5),
                                       rep(substring("00000",first=0,last=0:15),each=3)))));
axis(1, at= (rep(1:9, each=drMax+1) * 10^(0:drMax)), labels=FALSE);
axis(2, at= (0:10 * 10), cex.axis = 1.5 );
mtext("Read Length (bp)", side=1, line=3, cex=1.5);
lStats <- data.frame(n=NULL, mean=NULL, l10=NULL, l50=NULL, l90=NULL);
for(pcol in 1:ncol(fineBases.cumProp.mat)){
    prop.data <- data.frame(x=as.numeric(rownames(fineBases.cumProp.mat)), y=(1-fineBases.cumProp.mat[,pcol])*100);
    points(x=prop.data$x, y=prop.data$y, type="l", lwd=3,
           col=lineCols[pcol]);
    lStats.new <- data.frame(
        n=sum(fineCounts.mat[,pcol]),
        l10=prop.data$x[order(abs((prop.data$y) - 10))[1]],
        l50=prop.data$x[order(abs((prop.data$y) - 50))[1]],
        l90=prop.data$x[order(abs((prop.data$y) - 90))[1]]);
    points(c(rep(lStats.new$l90,2), rep(lStats.new$l50,2), rep(lStats.new$l10,2)),
           c(90,0,50,0,10,0), col=lineCols[pcol]);
    segments(x0=c(lStats.new$l90, lStats.new$l50, lStats.new$l10),
             y0=c(90,50,10), y1=c(0,0,0), lty=c("dotted","dashed"),
             col=lineCols[pcol], lwd=2);
    lStats <- rbind(lStats, lStats.new);
}
abline(h=c(10,50,90), lty="dashed", col="#80808080", lwd=2);
legend("topright", fill=lineCols[1:ncol(baseData)],
       legend=colnames(baseData), inset=c(0.025,0.05), cex=1,
       bg="#FFFFFFD0");
## Density plot
cat(" Making density plot\n");
##png("MinION_Bases_DigElec_black.png", pointsize=24,
##        width=120*ncol(fineBases.mat)+120, height=960);
colFrac <- (1/3);
sideFrac <- (1 - colFrac) / 2;
layout(matrix(c(0,1,0),ncol=3), widths=c(sideFrac, colFrac, sideFrac));
fine.data.smoothed <- NULL;
fine.smoothed.points <- NULL;
for(pcol in 1:ncol(fineBases.norm.mat)){
    fine.data <- data.frame(x=log10(as.numeric(rownames(fineBases.norm.mat))),
                            y=fineBases.mat[,pcol]);
    fine.smoothed <- spline(fine.data, n=5*nrow(fine.data));
    fine.smoothed.points <- fine.smoothed$x;
    fine.smoothed$x <- 10^fine.smoothed$x;
    fine.data.smoothed <- cbind(fine.data.smoothed, fine.smoothed$y);
    rownames(fine.data.smoothed) <- signif(fine.smoothed$x,4);
};
fine.data.smoothed.mat <- as.matrix(fine.data.smoothed);
if(is.null(dim(fine.data.smoothed.mat))){
    dim(fine.data.smoothed.mat) <- c(length(fine.data.smoothed.mat), 1);
}
colnames(fine.data.smoothed.mat) <- colnames(fineBases.mat);
## reverse order of columns to make sure the sort is from top to bottom
fine.data.smoothed.mat <-
    fine.data.smoothed.mat[,rev(1:ncol(fine.data.smoothed.mat)), drop=FALSE];
par(mar=c(4.5*3/2, 6.5, 1*3/2, 1), las=1, mgp=c(5,1 * 3/2,0), cex.lab=1.5 * 3/2);
image(x=fine.smoothed.points, ann=TRUE, axes=FALSE,
      y=(seq_len(ncol(fine.data.smoothed.mat))), useRaster=TRUE,
      z=fine.data.smoothed.mat, col=colorRampPalette(hsv(h=27/360,s=1,v=seq(0,1,by=0.001)), bias=1.25)(100),
      xlab = "Read Length (bp)", ylab="");
abline(v=log10(c(1,2,5)) + rep(0:5, each=3),
       lty="dashed", col="#FFFFFF40");
abline(h=seq_len(ncol(fineBases.mat)+1)-0.5, lwd=3);
drMax <- max(log10(fineCentres[baseRangeFine[2]]));
axis(1, at= log10((rep(1:9, each=drMax+1) * 10^(0:drMax))), labels=FALSE);
axis(1, at=log10(c(1,2,5)) + rep(0:15, each=3), cex.axis=1.5 * 3/2,
     lwd = 3,
     labels=valToSci(as.numeric(paste0(c(1,2,5),
                                       rep(substring("00000",first=0,last=0:15),each=3)))));
axis(2,at=seq_len(length(fileNames)), cex.axis=1.5 * 3/2,
     labels=gsub("_", " ", colnames(fine.data.smoothed.mat)),
     lwd=0, las=2);
## Normalised density plot
##rownames(lStats) <- colnames(fineBases.norm.mat);
##print(lStats);
cat(" Making normalised density plot\n");
fine.data.smoothed <- NULL;
for(pcol in 1:ncol(fineBases.norm.mat)){
    fine.data <- data.frame(x=log10(as.numeric(rownames(fineBases.norm.mat))),
                            y=fineBases.norm.mat[,pcol]);
    fine.smoothed <- spline(fine.data, n=5*nrow(fine.data));
    fine.smoothed.points <- fine.smoothed$x;
    fine.smoothed$x <- 10^fine.smoothed$x;
    fine.data.smoothed <- cbind(fine.data.smoothed, fine.smoothed$y);
    rownames(fine.data.smoothed) <- signif(fine.smoothed$x,4);
};
fine.data.smoothed.mat <- as.matrix(fine.data.smoothed);
colnames(fine.data.smoothed.mat) <- colnames(fineBases.mat);
## reverse order of columns to make sure the sort is from top to bottom
fine.data.smoothed.mat <-
    fine.data.smoothed.mat[,rev(1:ncol(fine.data.smoothed.mat)), drop=FALSE];
image(x=fine.smoothed.points, ann=TRUE, axes=FALSE,
      y=(seq_len(ncol(fine.data.smoothed.mat))), useRaster=TRUE,
      z=fine.data.smoothed.mat, col=colorRampPalette(hsv(h=27/360,s=1,v=seq(0,1,by=0.001)), bias=1.25)(100),
      xlab = "Read Length (bp)", ylab="");
abline(v=log10(c(1,2,5)) + rep(0:5, each=3),
       lty="dashed", col="#FFFFFF40");
abline(h=seq_len(ncol(fineBases.mat)+1)-0.5, lwd=3);
drMax <- max(log10(fineCentres[baseRangeFine[2]]));
axis(1, at= log10((rep(1:9, each=drMax+1) * 10^(0:drMax))), labels=FALSE);
axis(1, at=log10(c(1,2,5)) + rep(0:15, each=3), cex.axis=1.5 * 3/2,
     lwd = 3,
     labels=valToSci(as.numeric(paste0(c(1,2,5),
                                       rep(substring("00000",first=0,last=0:15),each=3)))));
axis(2,at=seq_len(length(fileNames)), cex.axis=1.5 * 3/2,
     labels=gsub("_", " ", colnames(fine.data.smoothed.mat)),
     lwd=0, las=2);
#axis(4,at=log10(unique(signif(unlist(lStats),2))), las=2,
#     labels=valToSci(unique(signif(unlist(lStats),2))),
                                        #     cex.axis=0.71);
cat("done! Created 'Sequence_curves.pdf'\n");
invisible(dev.off());
