#!/usr/bin/python

# foxyReader.py -- a program to read data from the FoxyR1 Fraction
# collector and output as CSV

# Author: David Eccles (gringer) <david.eccles@mpi-muenster.mpg.de>

# see http://pyserial.sourceforge.net/shortintro.html

# Note: there is an undocumented streaming data format for FoxyR1:

# [from email to David Eccles 2011-08-25]

# The 23 character run status serial string that is sent every 100
# milliseconds while the FoxyR1 is running and after the run is
# completed looks like this:

# SxxxxNULLRnnNULLTnnnnNULLPnNULLWnNULLCR

# Where:
# S is for signal.
# xxxx is a hexadecimal number between 0ABE(hex) 2750(dec) and
# F001(hex) -4095(dec) with 0A6B(hex) 2667(dec) = 100% of full-scale.

# R is for run number.
# nn is a decimal number between 1 and 99.

# T is for rack-tube number.
# nnnn is a decimal number where the most significant digit is the
# rack number between 0 and 2 (NOTE: rack number = 0 signifies the end
# of collection).  The three least significant digits are the tube
# number between 0 and 256.

# P is for peak.
# n = 0 for non-peak &
# n = 1 for peak.

# W is for Waste.
# n = 0 for collect &
# n = 1 for waste.

# NULL is a zero (0x00).
# CR is a Carriage Return (0x0D).

import sys
import os
import time
import re
import atexit
import telnetlib

# load serial module from directory program was run from
sys.path.append(os.path.abspath(os.path.dirname(sys.argv[0])))
import serial


def usage():
    sys.stderr.write("Usage: ./foxyReader.py [options]\n")
    sys.stderr.write("The program will create a file with a name like:\n" +
                     "  FoxyR1_YYYY-Mon-DD_HHMM.csv")
    sys.stderr.write("\nOther options:\n")
    sys.stderr.write(" -h              : only show this help screen\n")
    sys.stderr.write(" -timeout <int>  : timeout (in seconds) before stopping program\n")
    sys.stderr.write(" -norun          : only monitor a run already in progress\n")
    sys.stderr.write(" -nostream       : use an alternate result processing method\n")
    sys.stderr.write(" -port <name>    : choose a different serial port\n")
    sys.stderr.write(" -ip <address>   : connect using web server\n")
    sys.stderr.write(" -outpath <name> : set the output file directory\n")

def sendAndWait(sendString, waitString, device, maxTicks,
                errorMessage, useTelnet, delimiter = '\r'):
    # Transmit sendString to the collector, wait for waitString. If
    # no response in maxTicks attempts, then output errorMessage and
    # end program
    sys.stderr.write('Sending %s' % sendString)
    device.write(sendString + delimiter)
    wfTimeOut = maxTicks
    buffer = ''
    while((wfTimeOut > 0) and (not(waitString in buffer))):
        if(useTelnet):
            saw = device.read_until(delimiter, 0.1) # wait 0.1s
        else:
            saw = device.read(255) # will read _up to_ 255 chars
        count = len(saw)
        if (count > 0) :
            buffer += saw
            wfTimeOut = maxTicks
        else :
            sys.stderr.write('.')
            wfTimeOut -= 1
    if(not(waitString in buffer)):
        sys.stderr.write(" " + errorMessage + "\n")
        sys.exit(1)
    sys.stderr.write(" command was successful\n")
    return buffer

def getLineFromPort(serialObj, delimiter, useTelnet):
    if(not(useTelnet)):
        buffer = ''
        saw = ''
        saw = serialObj.read(1) # will keep reading until delimiter is found
        while(saw != delimiter):
            if(len(saw) != 1):
                # no more input, so spit this out
                return(buffer)
            buffer += saw
            saw = serialObj.read(1)
        return(buffer + delimiter)
    else:
        return(serialObj.read_until(delimiter, 0.1))

def cleanPorts(serialObj, outputFile):
    sys.stderr.write("Cleaning up and closing ports...")
    # Close files to clean up
    if(serialObj is not None):
        sys.stderr.write(" closing reader connection...");
        serialObj.close()
    if(outputFile is not None):
        sys.stderr.write(" closing output file '%s'..." % outputFile.name);
        outputFile.close()
    sys.stderr.write(" done\n")

# Default program parameters
sumRecording = 0
countRecording = 0
oldTime = 0
oldPrintTime = 0
portName = ''
portObj = None
outFile = None
ipMode = False
ipAddress = None
stream = True
startRun = True
timeout = 5 # number of seconds to wait for a signal (can be fractional)

timeStruct = time.localtime()
# Output file will be of the format FoxyR1_2011-Aug-18_1440.csv
# (year-month-day_time)
outPath = ''
outFileBase = time.strftime('FoxyR1_%Y-%b-%d_%H%M')
outFileName = outFileBase + '.csv'
outFile = sys.stdout

argv = sys.argv[1:]
while argv:
    argument = argv.pop(0)
    if(argument == '-h'):
        usage()
        exit(0)
    elif((argument == '-norun') or (argument == '-nostart')):
        startRun = False
        sys.stderr.write("Will not attempt to start a run\n")
    elif(argument == '-nostream'):
        stream = False
        sys.stderr.write("Results will not be streamed from the reader\n")
    elif(argument == '-outpath'):
        outPath = os.path.abspath(argv.pop(0))
        sys.stderr.write("Setting output path to '%s'\n" % outPath)
    elif(argument == '-timeout'):
        timeout = int(argv.pop(0))
        sys.stderr.write("Setting timeout to %d\n"
                         % timeout)
    elif(argument == '-ip'):
        ipAddress = argv.pop(0)
        ipMode = True
        sys.stderr.write("Reading from IP address '%s' (non-streaming)\n"
                         % ipAddress)
    elif(argument == '-port'):
        portName = argv.pop(0)
        sys.stderr.write("Setting reader port to '%s'\n" % portName)
    else :
        sys.stderr.write("Error: argument '%s' not understood\n" % argument)
        usage()
        exit(1)

if(ipMode and stream):
    sys.stderr.write("Disabling streaming mode for telnet communication\n")
    stream = False

if(outPath == ''):
    outPath = os.path.dirname(sys.argv[0])
    outPath = os.path.abspath(outPath)
    sys.stderr.write("No output path defined, setting to '%s'\n" % 
                     outPath)
else:
    outPath = os.path.abspath(outPath)

if(not(ipMode) and (portName == '')):
    foundPort = False
    sys.stderr.write("No port defined, hunting for appropriate ports\n")
    if(os.path.exists('/dev/cu.UC-232AC')):
        portName = '/dev/cu.UC-232AC'
        foundPort = True
    elif(os.path.exists('/dev/ttyUSB0')):
        portName = '/dev/ttyUSB0'
        foundPort = True
    elif(os.path.exists('/dev/ttyS0')):
        portName = '/dev/ttyS0'
        foundPort = True
    if(portName != ''):
        sys.stderr.write("Found a port at '%s'\n" % portName)
    else:
        sys.stderr.write("No appropriate port found\n")
        sys.stderr.write("Did you mean to try the '-ip <address>' option?\n")
        usage();
        exit(3)

if(not(ipMode)):
    portObj = serial.Serial(portName, baudrate = 19200, timeout = 0.1,
                            parity = 'N', stopbits=1, bytesize = 8)
else:
    portObj = telnetlib.Telnet(ipAddress)
    

STALL_DEFAULT = 100
numTubes = 4

buffer = ''

buffer = sendAndWait('ECHO=0;REMOTE;RSVP',
                     'READY', portObj, STALL_DEFAULT, 
                     'unable to change to remote mode', ipMode)

# Have now established that a connection can be made to a reader, so
# create output file and begin
if(outFileName != ""):
    outFileName = outPath + "/" + outFileName
    if(os.path.exists(outFileName)):
        outFileBase = outPath + "/" + outFileBase
        fileIncrement = 0;
        while(os.path.exists(outFileBase+'.'+str(fileIncrement)+'.csv')):
            fileIncrement += 1
        outFileName = outFileBase+'.'+str(fileIncrement)+'.csv'
    outFile = open(outFileName, 'a')

# make sure ports are cleaned up on exit
atexit.register(cleanPorts, portObj, outFile)

sys.stderr.write("Writing to file '%s'\n" % outFileName)

outFile.write("Time,Run,Rack,Tube,Reading,Peak,Waste,RawSignal\n")
t0 = time.time()
# Send first detection command
initCommand = ''
if(stream):
    initCommand += 'STREAM=1;'
else:
    initCommand += 'STREAM=0;'
initCommand += 'RSVP';
buffer = sendAndWait(initCommand,
                     'READY', portObj, STALL_DEFAULT, 
                     'unable to start streaming mode', ipMode)
startCommand = ''
sys.stderr.write("Getting run start status:\n")
buffer = sendAndWait('RTUBE',
                     'RTUBE=', portObj, STALL_DEFAULT, 
                     'Cannot determine if run has already started', ipMode)
if(not('RTUBE=0000' in buffer)):
    # run already started, no point in trying to start again
    sys.stderr.write("[Run has already started]\n")
    startRun = False
if(startRun):
    sys.stderr.write("Sending RUN command... ")
    portObj.write('RUN\r')
    time.sleep(0.5) # a little sleep to allow run to start
    # at the start of the run, RTUBE is 0000 until the collector is at the first tube
    # [waiting for 'READY' doesn't seem to successfully wait until this event]
    sys.stderr.write("waiting for run to start..")
    buffer = 'RTUBE=0000'
    portObj.write('RTUBE\r')
    sleepTime = 0
    while(('RTUBE=0000' in buffer) and (sleepTime < 10)):
        sys.stderr.write(".")
        buffer = getLineFromPort(portObj, '\r', ipMode)
        time.sleep(0.5) # a little sleep to allow run to start
        sleepTime += 0.5
    sys.stderr.write(" started\n")
if(not stream):
    portObj.write('RTUBE;DETECTOR;\r')
chars = 0
buffer = ''
run = -1
rack = -1
tube = -1
peak = False
waste = False
signal = 0x0000
gotResult = False
lastSignal = time.time()
while (((time.time() - lastSignal) < timeout) and (rack != 0)) :
    count = 0
    t1 = time.time()
    timeDiff = t1-t0
    if(not stream):
        if(int(timeDiff*10) != int(oldTime*10)):
            # non-streaming mode attempts 10 reads per second
            oldTime = timeDiff
            if(gotResult):
                # if results have been received, get more
                portObj.write("RTUBE;DETECTOR\r")
                gotResult = False
    saw = getLineFromPort(portObj, '\r', ipMode)
    count = len(saw)
    buffer += saw
    if(len(buffer.rstrip()) == 0) :
        buffer = ""
    # else :
    #     sys.stderr.write("%0.2f [%s]\n" % (t1 - lastSignal, saw.rstrip()));
    if((not '\r' in buffer) and (t1 - lastSignal > (timeout / 2))) :
        sys.stderr.write("[%0.2f seconds since last signal]\n" % (
                t1-lastSignal));
    # if no message in last second, ask for another measurement
    if((not '\r' in buffer) and (t1 - lastSignal > 1)) :
        portObj.write('RTUBE;DETECTOR;\r');
    while ('\r' in buffer) :
        # Get next line from output
        nextLine =  buffer[0:(buffer.index('\r')+1)]
        # remove first reading from buffer
        buffer = buffer[(buffer.index('\r')+1):]
        lastSignal = t1
	# if data is received, print it out
        # Note: this parses the streaming format (see comments above)
        #       SxxxxNULLRnnNULLTnnnnNULLPnNULLWnNULLCR
        res = re.search("S(....)\0"+
                        "R(..)\0"+
                        "T(.)(...)\0"+
                        "P(.)\0"+
                        "W(.)\0", nextLine)
	if(res):
            signal = res.group(1)
            run = res.group(2)
            rack = res.group(3)
            tube = res.group(4)
            peak = (res.group(5) == "1")
            waste = (res.group(6) == "1")
            recording = int(signal, 16) # convert from hex to int
            if(recording > 0x7fff) :
                # convert to signed int
                recording = 0 - (0xffff - recording + 1)
            # sys.stderr.write(("signal: %s; recording: %s; run: %s; " + 
            #                   "tube: %s; " +
            #                   "peak: %s; waste: %s\n") 
            #                  % (signal, recording, run, tube, peak, waste))
            recording = (recording * 1.0) / (0x0a6b)
            sumRecording += recording
            countRecording += 1
            # note: recording is multiplied by 100 for graph output
            # note: output time is in minutes
            outFile.write("%0.4f,%s,%s,%s,%0.4f,%s,%s,%s\n" % 
                          ((timeDiff / 60), run, rack, tube, (recording * 100),
                           peak, waste, signal))
        if("DETECTOR=" in nextLine):
	    t1 = time.time()
	    timeDiff = t1-t0
            gotResult = True
            res = re.search("DETECTOR= *([0-9\.\-]+)", nextLine)
            if(res):
                recording = float(res.group(1))
                sumRecording += recording
                countRecording += 1
                outFile.write("%0.4f,%s,%s,%s,%0.4f,%s,%s,NA\n" % 
                              ((timeDiff / 60), run, rack, tube, (recording * 100),
                               peak, waste))
        if("RTUBE=" in nextLine):
            res = re.search("RTUBE= *([0-9])([0-9]+)", nextLine)
            if(res):
                rack = int(res.group(1))
                tube = int(res.group(2))
        if((countRecording > 0) and (int(t1) != int(oldPrintTime))):
            # print out graphical representation once per second
            actualRecording = sumRecording / countRecording
            reading = 0 if (actualRecording<0) else actualRecording
            reading = 1 if (actualRecording>1) else actualRecording
            textPos = int(reading * 65) if (reading < 1) else 64
            textPos = textPos if (textPos > 0) else 0
            sys.stderr.write('[Tube: %3s, Record count: %3d]' % (
                    tube, countRecording))
            sys.stderr.write('|')
            sys.stderr.write((' ') * textPos)
            sys.stderr.write('*')
            sys.stderr.write((' ') * (64-textPos))
            sys.stderr.write('|')
            sys.stderr.write('(%3.3f)' % (actualRecording * 100))
            sys.stderr.write("\n")
            oldPrintTime = t1
            sumRecording = 0
            countRecording = 0

if ((time.time() - lastSignal) >= timeout) :
    sys.stderr.write(("No output for %d seconds, " + 
                     "program will finish\n") % timeout)
else :
    sys.stderr.write("Machine has changed to tube 0, program will finish\n")

if(stream):
    buffer = sendAndWait('STREAM=0;RSVP',
                         'READY', portObj, STALL_DEFAULT, 
                         'Unable to stop streaming mode', ipMode)
