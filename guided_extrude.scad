// Path extrusion script, various functions for properly orienting faces

// Path extrusion, where a shape is extruded along a 3D path, can be
// challenging to properly calculate, because it's difficult to work out
// the proper rotations of the shape, especially along the axis of
// extrusion between two successive points. This path extrusion
// library provides many different functions to help lock that
// rotation to the intended angle, depending on the intended
// application. Examples are provided at the end of the library to
// demonstrate different classes of rotation locking.

// OpenSCAD_User_Manual/List_Comprehensions#Flattening_a_nested_vector   
function flatten(l) = [ for (a = l) for (b = a) b ];

function mapfuncConstantX(shape, pathPoint) =
   [ for(i = [0 : (len(shape) - 1)])
       [0, shape[i][0], shape[i][1]] + pathPoint ];

function mapfuncConstantY(shape, pathPoint) =
   [ for(i = [0 : (len(shape) - 1)])
       [shape[i][1], 0, shape[i][0]] + pathPoint ];
   
function mapfuncConstantZ(shape, pathPoint) =
   [ for(i = [0 : (len(shape) - 1)])
       [shape[i][0], shape[i][1], 0] + pathPoint ];

function mapfuncVertCylinder(shape, pathPoint) =
   let(pAng = atan2(pathPoint[1], pathPoint[0]),
       pRad = sqrt(pathPoint[0]*pathPoint[0] + pathPoint[1]*pathPoint[1]))
   [ for(i = [0 : (len(shape) - 1)])
       [(shape[i][0]) * cos(pAng),
        (shape[i][0]) * sin(pAng),
        shape[i][1]] + pathPoint ];

function mapfuncRadialCylinder(shape, pathPoint) =
   let(pAng = atan2(pathPoint[1], pathPoint[0]),
       pRad = sqrt(pathPoint[0]*pathPoint[0] + pathPoint[1]*pathPoint[1]),
       pCirc = 2 * PI * pRad)
   [ for(i = [0 : (len(shape) - 1)])
       [pRad * cos(pAng + (shape[i][0] / pCirc) * 360),
        pRad * sin(pAng + (shape[i][0] / pCirc) * 360),
        shape[i][1] + pathPoint[2]]  ];

function mapFunc(extrudeType, shape, pathPoint) =
   (extrudeType == "constantX") ?
     mapfuncConstantX(shape, pathPoint) :
   (extrudeType == "constantY") ?
     mapfuncConstantY(shape, pathPoint) :
   (extrudeType == "constantZ") ?
     mapfuncConstantZ(shape, pathPoint) :
   (extrudeType == "vertCylinder") ?
     mapfuncVertCylinder(shape, pathPoint) :
   (extrudeType == "radialCylinder") ?
     mapfuncRadialCylinder(shape, pathPoint) :
     [0,0,0];

module mapExtrude(extrudeType, shape, path) {
   shapeCount = len(shape); pathCount = len(path);
   extrudedPoints = flatten([ for(i = [0 : (pathCount - 1)])
       mapFunc(extrudeType, shape, path[i]) ]);
   pointsLen = len(extrudedPoints);
   faceBottom = [ for(i = [0 : (shapeCount - 1)]) i ];
   faceTop = [ for(i = [0 : (shapeCount - 1)]) 
       (pointsLen - i - 1) ];
   faceLinksA = [ for(l = [0:(pathCount - 2)], i = [0:(shapeCount - 1)])
           [i + (l * shapeCount), i + ((l+1) * shapeCount),
            (i+1) % shapeCount + (l * shapeCount)] ];
   faceLinksB = [ for(l = [0:(pathCount - 2)], i = [0:(shapeCount - 1)])
           [(i+1) % shapeCount + (l * shapeCount),
            i + ((l + 1) * shapeCount),
            (i+1) % shapeCount + ((l + 1) * shapeCount)] ];
   polyhedron( points = extrudedPoints,
       faces  = concat([faceBottom], faceLinksA, faceLinksB, [faceTop]));
}

// weave pattern -- extrude example with constant X
module exampleWeave(){
    function movePathXZ(x, w, h, y) = [x, y, h * cos(x/w * 360)];
    function movePathXY(x, w, h, y) = [x, h * cos(x/w * 360), y];
    function movePathZX(z, w, h, x) = [x, -h * cos(z/w * 360), z];
    function movePathYZ(y, w, h, x) = [x, y, -h * cos(y/w * 360)];
    
    for(rt = [0, 90]) rotate(rt) for(py = [-25 : 5 : 25]) {
        s = (rt == 0 ? -1 : 1) * (((abs(py) / 5) % 2) * 2 - 1);
        mapExtrude("constantX",
           [for(theta = [22.5:45:359]) [cos(theta), sin(theta)]], 
           [for(px = [-27.5 : 27.5]) movePathXZ(px, 10, s, py) ]);
    }
    linear_extrude(height=2, center=true) offset(0.9, $fn=2) difference(){
        square(27.6*2, center=true);
        square(27.4*2, center=true);
    }
}

module exampleThreadedRod(height=20, D=10, P=1.5, theta=30){
    // https://en.wikipedia.org/wiki/ISO_metric_screw_thread
    // a threaded rod of diameter D has a trapezoid groove cut into it
    // along a helical path
    // the thread is usually chamfered at the ends to make it easier
    // to attach nuts
    xw = 0.4; // extrude width
    H = 1 / (2 * tan(theta));
    $fn = 24;
    numLoops = ceil(height / P);
    trapezoidShape = [[H/8, -P/2 - xw/2], [H/8-xw/2, -P/2 - xw/2],
                      [-7*H/8-xw/2, 0],
                      [H/8-xw/2, P/2 +xw/2], [H/8, P/2 + xw/2]];
    translate([0,0,-height/2]) difference(){
      cylinder(d=D-xw, h=height);
      // in order to avoid object self-intersections, the cut paths are
      // 3/4 turns in length, and overlap by 1/4 turn
      for(loopPos = [0 : 0.5 : numLoops]) {
        helicalPath = [ for (th = [(loopPos*360) : 
                                   (360 / $fn) : 
                                   ((loopPos + 0.75)*360)]) 
          [((D+xw)/2) * cos(th), ((D+xw)/2) * sin(th), (th/360) * P]];
          mapExtrude("vertCylinder", trapezoidShape, helicalPath);
      }
      rotate_extrude(){
          polygon([[D/2+2, -1], [D/2+2, 4], [D/2-2, -1]]);
      }
      rotate_extrude(){
          polygon([[D/2+2, height+1],
                   [D/2+2, height-4],
                   [D/2-2, height+1]]);
      }
    }
    *cylinder(d=D+2, h=P/8);
    *rotate(-90) translate([D/2,0,0]) rotate([90,0,0]) linear_extrude(height=1)
      polygon(trapezoidShape);
}

// example demonstrating wrapping around a cylinder
module examplePunchedCircle(){
  cylinder(d=10, h=20, center=true, $fn=24);
  mapExtrude("radialCylinder", 
    shape=[for(theta = [0:20:359]) [4 * cos(theta), 4 * sin(theta)]],
    path=[[4,0,0], [6,0,0]]);
}

*exampleWeave();
*exampleThreadedRod();
examplePunchedCircle();