#!/bin/sh

# usage ./getMedian.sh <file.csv> <column> [quantile]
# quantile is represented as a value from 0 to 1

if [ -z "$3" ]
then quant=5;
else quant="$3";
fi;

tmpFile=$(mktemp medianData.XXXXXXX);
less ${1} | cut -d ',' -f ${2} > ${tmpFile};

numLines=$(cat ${tmpFile} | wc -l);

head -n 1 ${tmpFile};

cat ${tmpFile} | sort -rn | head -n $(( (${numLines} * ${quant})/100 )) | tail -n 1;

rm ${tmpFile};
