#!/usr/bin/env Rscript
## stompPlotter.r - Plot coverage / variant results from stomped pileup results

library(ragg);

## Accessory functions
valToSci <- function(val, unit = ""){
    sci.prefixes <- c("", "k", "M", "G", "T", "P", "E", "Z", "Y");
    units <- rep(paste(sci.prefixes,unit,sep=""), each=3);
    logRegion <- floor(log10(val))+1;
    conv.units <- units[logRegion];
    conv.div <- 10^rep(0:(length(sci.prefixes)-1) * 3, each = 3)[logRegion];
    conv.val <- val / conv.div;
    conv.val[val == 0] <- 0;
    conv.units[val == 0] <- unit;
    return(sprintf("%s %s",conv.val,conv.units));
}

## Argument processing
argLoc <- 1;

usage <- function(){
  cat("usage: ./stompPlotter.r",
      "( -f <fwd.csv> -r <rev.csv> | -b <both.csv> ) [options]\n");
  cat("\nOther Options:\n");
  cat("-help            : Only display this help message\n");
  cat("-prefix <string> : Prefix to attach to title / file name\n");
  cat("-nodels          : Ignore deletion variants\n");
  cat("-nolabels        : Turn off variant label display\n");
  cat("-notitle         : Turn off plot title\n");
  cat("-novars          : Turn off variant display\n");
  cat("-showdels        : Show deletions, even if not reported\n");
  cat("-mindepth <int>  : Minimum depth for variant calling\n");
  cat("-name <str>      : Change reference sequence name\n");
  cat("-f <fwd.csv>     : File containing stomped pileup (forward strand)\n");
  cat("-r <fwd.csv>     : File containing stomped pileup (reverse strand)\n");
  cat("-b <fwd.csv>     : File containing stomped pileup (both strands)\n");
  cat("-l <lengths.csv> : Mapped length file containing read lengths (both strands)\n");
  cat("-angle <int>     : Variant display angle (default 30)\n");
  cat("-circular <len>  : Assume that the reference is a circular contig with length <len>\n");
  cat("-adj <offset>    : Adjust position by <offset>\n");
  cat("-legend <string> : Change position of legend (default: right)\n");
  cat("-scale           : Add a scale bar [to circular plots]\n");
  cat("-log             : Show log of coverage, rather than linear coverage\n");
  cat("-col <f> <r>     : Set colours for coverage\n");
  cat("-agap            : Leave a gap for an additional annotation track\n");
  cat("-max <value>     : Set maximum scale value to <value>\n");
  cat("-type (png|svg)  : Define output type\n");
  cat("\n");
}

refType <- "linear";
fwdFile <- "";
revFile <- "";
bothFile <- "";
lengthFile <- "";
prefix <- "";
refName <- "";
contigLength <- 0;
posOffset <- 0;
minDepth <- 0;
covColours <- c("lightgreen","lightblue");
showLog <- FALSE;
annotationGap <- FALSE;
reportDels <- TRUE;
showDels <- FALSE;
showScale <- FALSE;
showTitle <- TRUE;
outType <- "png";
maxVal <- -1;
showLabels <- TRUE;
showVars <- TRUE;
legendLoc <- "topright";
varAng <- 30;

if(length(commandArgs(TRUE)) < 1){
      usage();
      quit(save = "no", status=0);
}

argLoc <- 1;

while(!is.na(commandArgs(TRUE)[argLoc])){
    if(commandArgs(TRUE)[argLoc] == "-help"){
        usage();
        quit(save = "no", status=0);
    }
    else if(commandArgs(TRUE)[argLoc] == "-f"){
        fwdFile <- commandArgs(TRUE)[argLoc+1];
        argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-r"){
        revFile <- commandArgs(TRUE)[argLoc+1];
        argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-b"){
        bothFile <- commandArgs(TRUE)[argLoc+1];
        argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-l"){
        lengthFile <- commandArgs(TRUE)[argLoc+1];
        argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-angle"){
        varAng <- as.numeric(commandArgs(TRUE)[argLoc+1]);
        argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-prefix"){
        prefix <- commandArgs(TRUE)[argLoc+1];
        argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-nodels"){
        reportDels <- FALSE;
    }
    else if(commandArgs(TRUE)[argLoc] == "-showdels"){
        showDels <- TRUE;
    }
    else if(commandArgs(TRUE)[argLoc] == "-nolabels"){
        showLabels <- FALSE;
    }
    else if(commandArgs(TRUE)[argLoc] == "-notitle"){
        showTitle <- FALSE;
    }
    else if(commandArgs(TRUE)[argLoc] == "-novars"){
        showVars <- FALSE;
    }
    else if(commandArgs(TRUE)[argLoc] == "-mindepth"){
        minDepth <- as.numeric(commandArgs(TRUE)[argLoc+1]);
        argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-name"){
        refName <- commandArgs(TRUE)[argLoc+1];
        argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-circular"){
        contigLength <- as.numeric(commandArgs(TRUE)[argLoc+1]);
        refType <- "circular";
        argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-log"){
        showLog <- TRUE;
    }
    else if(commandArgs(TRUE)[argLoc] == "-col"){
        covColours <- commandArgs(TRUE)[(argLoc+1):(argLoc+2)];
        argLoc <- argLoc + 2;
    }
    else if(commandArgs(TRUE)[argLoc] == "-agap"){
        annotationGap <- TRUE;
    }
    else if(commandArgs(TRUE)[argLoc] == "-scale"){
        showScale <- TRUE;
    }
    else if(commandArgs(TRUE)[argLoc] == "-adj"){
        posOffset <- as.numeric(commandArgs(TRUE)[argLoc+1]);
        argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-legend"){
        legendLoc <- commandArgs(TRUE)[argLoc+1];
        argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-max"){
        maxVal <- as.numeric(commandArgs(TRUE)[argLoc+1]);
        argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-type"){
        outType <- commandArgs(TRUE)[argLoc+1];
        argLoc <- argLoc + 1;
    }
    else {
      cat("Error: Argument '",commandArgs(TRUE)[argLoc],
          "' is not understood by this program\n\n", sep="");
      usage();
      quit(save = "no", status=0);
    }
    argLoc <- argLoc + 1;
}

if(prefix != ""){
    prefix <- paste0(prefix, "_");
}

if(showVars){
    covColours = c(NA, NA);
}


## Check parameters
if((fwdFile == "" | revFile == "") && (bothFile == "") && (lengthFile == "")){
    cat("Error: forward (-f) and reverse (-r) results are both required\n",
        "       alternatively, a combined result file (-b) is required\n",
        "       alternatively, a mapped length file (-l) is required\n",
        sep="");
    usage();
    quit(save = "no", status=0);
}
if((fwdFile != "") && (bothFile != "")){
    cat("Error: forward (-f) and both (-b) results are not compatible\n",
        sep="");
    usage();
    quit(save = "no", status=0);
}

## Check file existence
if((fwdFile != "") && !file.exists(fwdFile)){
    cat(sprintf("Error: forward file '%s' does not exist\n", fwdFile));
    usage();
    quit(save = "no", status=0);
}
if((revFile != "") && !file.exists(revFile)){
    cat(sprintf("Error: reverse file '%s' does not exist\n", revFile));
    usage();
    quit(save = "no", status=0);
}
if((bothFile != "") && !file.exists(bothFile)){
    cat(sprintf("Error: combined file '%s' does not exist\n", bothFile));
    usage();
    quit(save = "no", status=0);
}
if((lengthFile != "") && !file.exists(lengthFile)){
    cat(sprintf("Error: length file '%s' does not exist\n", bothFile));
    usage();
    quit(save = "no", status=0);
}

#bothFile <- "";
#fwdFile <- "/bioinf/MIMR-2017-Jul-01-GBIS/MB/GC_430/varProp_GC_430_DS19_2019-May-20/varProp_LAST_BC05_vs_chrM_CD45_fwd.csv";
#revFile <- "/bioinf/MIMR-2017-Jul-01-GBIS/MB/GC_430/varProp_GC_430_DS19_2019-May-20/varProp_LAST_BC05_vs_chrM_CD45_rev.csv";
#fwdFile <- "/mnt/BigBirdStornext/Nanopore/reads_TM1912/TM1912/demultiplexed/mapped_to_addGene/BC01/stomped_BC01_vs_reference_fwd.csv.gz";
#revFile <- "/mnt/BigBirdStornext/Nanopore/reads_TM1912/TM1912/demultiplexed/mapped_to_addGene/BC01/stomped_BC01_vs_reference_rev.csv.gz";

library(tidyverse, warn.conflicts=FALSE);

data.tbl <-
    if(lengthFile != ""){
        ## Shoehorn the length-based format into the stomp format
        tmpRes <- read_csv(lengthFile) %>%
            group_by(dir,start,mappedLength) %>%
            summarise(count=length(dir)) %>%
            ungroup();
        fwdCov <- rep(0, max(tmpRes$start + tmpRes$mappedLength - 1));
        revCov <- rep(0, max(tmpRes$start + tmpRes$mappedLength - 1));
        startPossF <- rep(0, max(tmpRes$start + tmpRes$mappedLength - 1));
        endPossF <- rep(0, max(tmpRes$start + tmpRes$mappedLength - 1));
        startPossR <- rep(0, max(tmpRes$start + tmpRes$mappedLength - 1));
        endPossR <- rep(0, max(tmpRes$start + tmpRes$mappedLength - 1));
        ## Create one line for each covered position
        for(l in seq_len(nrow(tmpRes))){
            sp <- tmpRes$start[l];
            ep <- tmpRes$start[l] + tmpRes$mappedLength[l] - 1;
            if(tmpRes$dir[l] == 1){
                fwdCov[sp:ep] <- fwdCov[sp:ep] + tmpRes$count[l];
                startPossF[sp] <- startPossF[sp] + tmpRes$count[l];
                endPossF[ep] <- endPossF[ep] + tmpRes$count[l];
            } else {
                revCov[sp:ep] <- revCov[sp:ep] + tmpRes$count[l];
                startPossR[ep] <- startPossR[ep] + tmpRes$count[l];
                endPossR[sp] <- endPossR[sp] + tmpRes$count[l];
            }
        }
        bind_rows(
            tibble(Position = seq_along(fwdCov), Coverage=fwdCov,
                   startPoss = startPossF, endPoss = endPossF, dir="fwd"),
            tibble(Position = seq_along(revCov), Coverage=revCov,
                   startPoss = startPossR, endPoss = endPossR, dir="rev")) %>%
            mutate(ref = "N", A = 0, C = 0, G = 0, T = 0, d = 0, i = 0,
                   Assembly = ifelse(refName != "", refName, "UNKNOWN"));
    } else if(fwdFile != ""){
        as_tibble(read.csv(fwdFile, stringsAsFactors=FALSE)) %>%
            mutate(dir="fwd", startPoss = 0, endPoss = 0);
    } else {
        as_tibble(read.csv(bothFile, stringsAsFactors=FALSE)) %>%
            mutate(dir="both", startPoss = 0, endPoss = 0);
    }

warnings();
if(revFile != ""){
    data.tbl <- bind_rows(data.tbl,
                          as_tibble(read.csv(revFile,
                                             stringsAsFactors=FALSE)) %>%
                          mutate(dir="rev"));
}

if(refName != ""){
    data.tbl$Assembly <- refName;
}

## Check for proportional data
seenProportional <- FALSE;
if(any((data.tbl[,c("A","C","G","T")] > 0) & (data.tbl[,c("A","C","G","T")] < 1))){
    cat("Error: 'count' for variant genotypes seen between 0 and 1; this looks like proportional stomped data.\n",
        "       Please use count data (-c) as input.\n", sep="");
    usage();
    quit(save = "no", status=0);
}

## Determine Transition / Transversion / Complementary variants
data.tbl$Tv <- ifelse(data.tbl$ref %in% c("A","C"),
                      data.tbl$A + data.tbl$C,
                      data.tbl$G + data.tbl$T);
data.tbl$Ts <- ifelse(data.tbl$ref %in% c("A","G"),
                      data.tbl$A + data.tbl$G,
                      data.tbl$C + data.tbl$T);
data.tbl$Cp <- ifelse(data.tbl$ref %in% c("A","T"),
                      data.tbl$A + data.tbl$T,
                      data.tbl$C + data.tbl$G);

if(refType == "linear"){
    for(ref in unique(data.tbl$Assembly)){
        cat("Creating plot...");
        symname <- gsub("/",".",ref);
        if(outType == "png"){
            agg_png(sprintf("%sstompPlot_%s.png", prefix, symname), width=1800, height=900, pointsize=18);
        } else if(outType == "svg"){
            agg_svg(sprintf("%sstompPlot_%s.svg", prefix, symname), width=14, height=7, pointsize=18);
        }
        par(mar=c(0.5,5.5,2.5,0.5), xpd=NA);
        allsub.tbl <- data.tbl[data.tbl$Assembly == ref,];
        maxCov <- max(maxVal, allsub.tbl$Coverage);
        plot(NA, xlim=range(allsub.tbl$Position),
             ylim=if(bothFile != ""){c(0, 1.5)} else {c(-1.5, 1.5)},
             xlab="Location (bp)", ann=FALSE, axes=FALSE);
        ## Title
        mtext(sprintf("%sReads Mapped to Reference Sequence: %s", sub("_"," ", prefix), ref), side=3, cex=1.5);
        ## Horizontal axis
        segments(x0=min(allsub.tbl$Position), x1=max(allsub.tbl$Position),
                 y0=0);
        ppos <- pretty(allsub.tbl$Position);
        ppos <- ppos[(ppos > min(allsub.tbl$Position)) & (ppos < max(allsub.tbl$Position))];
        segments(x0=ppos, y0=-0.05, y1=0.05);
        rect(xleft=ppos - strwidth(ppos),
             xright=ppos + strwidth(ppos),
             ybottom=-0.07, ytop=0.07, col="#FFFFFF", border=NA);
        text(x=ppos, y=0, labels=ppos,
             cex=1.5);
        text(x=mean(range(allsub.tbl$Position)), y=c(0.08, -0.08), pos=c(3,1),
             labels=c("Forward Mapped Reads", "Reverse-complement Mapped Reads"));
        ## Vertical axis
        if(showLog){
            mtext("Read coverage (log scale)", 2, line=4, cex=1.5);
        } else {
            mtext("Read coverage", 2, line=4, cex=1.5);
        }
        validPoss <- NULL;
        for(pDir in c("fwd","rev","both")){
            mc2 <- maxCov * 0.2;
            mul <- c("fwd"=1, "rev"=-1, "both"=1)[pDir];
            sub.tbl <- filter(allsub.tbl, dir == pDir);
            if(nrow(sub.tbl) == 0){
                next;
            }
            maxTrueCov <- max(sub.tbl$Coverage, na.rm=TRUE);
            if(showLog){
                maxCovLog <- log(maxCov+1);
                sub.tbl$Ts <- log(sub.tbl$Ts+1) / maxCovLog;
                sub.tbl$Tv <- log(sub.tbl$Tv+1) / maxCovLog;
                sub.tbl$Cp <- log(sub.tbl$Cp+1) / maxCovLog;
                sub.tbl$d <- log(sub.tbl$d+1) / maxCovLog;
                sub.tbl$i <- log(sub.tbl$i+1) / maxCovLog;
                sub.tbl$CovHalf <- log(sub.tbl$Coverage/2+1) / maxCovLog;
                sub.tbl$Coverage <- log(sub.tbl$Coverage+1) / maxCovLog;
            } else {
                sub.tbl$Ts <- sub.tbl$Ts / maxCov;
                sub.tbl$Tv <- sub.tbl$Tv / maxCov;
                sub.tbl$Cp <- sub.tbl$Cp / maxCov;
                sub.tbl$d <- sub.tbl$d / maxCov;
                sub.tbl$i <- sub.tbl$i / maxCov;
                sub.tbl$CovHalf <- ((sub.tbl$Coverage)/2) / maxCov;
                sub.tbl$Coverage <- (sub.tbl$Coverage) / maxCov;
            }
            ## Variant Distribution plot
            points(x=sub.tbl$Position, pch = 20, cex=sub.tbl$Ts / sub.tbl$Coverage * 2,
                   y=(sub.tbl$Ts+0.2) * mul, col="blue");
            points(x=sub.tbl$Position, pch = 20, cex=sub.tbl$Tv / sub.tbl$Coverage * 2,
                   y=(sub.tbl$Tv+0.2) * mul, col="gold");
            points(x=sub.tbl$Position, pch = 20, cex=sub.tbl$Cp / sub.tbl$Coverage * 2,
                   y=(sub.tbl$Cp+0.2) * mul, col="red");
            if(reportDels || showDels){
                points(x=sub.tbl$Position, pch = 20, cex=sub.tbl$d / sub.tbl$Coverage * 2,
                       y=(sub.tbl$Coverage - sub.tbl$d + 0.2) * mul, col="darkcyan");
                inPos <- (sub.tbl$Coverage + sub.tbl$i + 0.2) * mul;
                offScale <- (abs(inPos) > 1.4);
                if(sum(offScale) > 0){
                    inPos[offScale] <- sign(inPos[offScale]) * 1.4;
                }
                points(x=sub.tbl$Position, pch = ifelse(offScale, ifelse(inPos > 0, 24, 25), 20),
                       cex=sub.tbl$i / sub.tbl$Coverage * 2,
                       y=inPos, col="darkmagenta", bg=ifelse(offScale, "darkmagenta", NA));
            }
            ## Read Coverage plot
            points(x=sub.tbl$Position, y=(sub.tbl$Coverage + 0.2) * mul,
                   type="l", lwd=3);
            points(x=sub.tbl$Position, y=(sub.tbl$CovHalf + 0.2) * mul,
                   type="l", lwd=3,
                   col="#00000020");
            if(showVars){
                legend(legendLoc, legend=c("Ts","Tv","Cp","Ins","Del"),
                       fill=c("blue","gold","red","darkmagenta","darkcyan"),
                       ncol=ifelse(legendLoc %in% c("top", "bottom"),
                                   5, 2), bg="#FFFFFF80", inset=0.05);
            }
            validPoss <- c(validPoss,
                           sub.tbl$Position[(apply(sub.tbl[,c("Ts","Tv","Cp","i","d")],1,max) >
                                             sub.tbl$Coverage * 0.4) & (sub.tbl$Coverage > minDepth)]);
            if(showLog){
                axPoss <- sort(rep(1:9, 10)*10^(0:9));
                ax10Poss <- 10^(0:9);
                axPoss <- axPoss[c(which(axPoss < maxTrueCov),
                                   max(which(axPoss < maxTrueCov))+1)];
                options(scipen=20);
                ax10Poss <- c(ax10Poss[ax10Poss < max(axPoss)], max(axPoss));
                axLogs <- log(axPoss+1) / log(maxCov+1);
                ax10Logs <- log(ax10Poss+1) / log(maxCov+1);
                axis(2, at=axLogs + 0.2,
                     las=2, labels=FALSE);
                axis(2, at=ax10Logs + 0.2,
                     las=2, labels=valToSci(ax10Poss));
            } else {
                axis(2, at=(pretty(1:maxCov)/maxCov + 0.2) * mul,
                     las=2, labels=valToSci(pretty(1:maxCov)));
            }
        }
        validPoss <- table(validPoss);
        if(bothFile == ""){
            validPoss <- validPoss[validPoss == 2];
        }
        adj = 0;
        posAdj <- as.numeric(names(validPoss));
        if(length(posAdj) > 0){
            posAdj <- seq(min(posAdj)/2, (max(allsub.tbl$Position)+max(posAdj))/2, length.out=length(posAdj));
            names(posAdj) <- names(validPoss);
            for(pos in as.numeric(names(validPoss))){
                sub.tbl <- allsub.tbl[allsub.tbl$Position == pos,,drop=FALSE];
                psub <- head(allsub.tbl[allsub.tbl$Position == pos,c("ref","A","C","G","T","d","i")],1);
                ref <- psub$ref;
                psub <- unlist(c(psub[,-1]));
                varAll <- c("A", "C", "G", "T", "d", "i")[psub == max(psub)];
                for(var in varAll){
                    varYPos <- unlist(sub.tbl[,var] / maxCov + 0.2) * mul;
                    if(var == "d"){
                        varYPos <- unlist((sub.tbl[,"Coverage"] - sub.tbl[,"d"])/maxCov + 0.2) * mul;
                    }
                    if(var == "i"){
                        varYPos <- min(1.4, unlist((sub.tbl[,"Coverage"] + sub.tbl[,"i"])/maxCov + 0.2)) * mul;
                    }
                    if(reportDels || (var != "d")){
                        print(sub.tbl %>% select(Assembly, Position, dir));
                        segMul <- ifelse(sub.tbl$dir == "rev", -1, 1);
                        varYBase <- unlist((sub.tbl[,"Coverage"])/maxCov + 0.2);
                        segments(x0=pos, y0=0.2*segMul, y1=varYPos*segMul, lwd=5, col="#FA807230");
                        if(showLabels){
                            if(var != "i"){
                                if(varAng == 90){
                                    text(x=posAdj[as.character(pos)], y=1.3, adj=c(0, 0.5),
                                         labels=sprintf("%d: %s -> %s", pos, ref, var), srt=varAng);
                                    segments(x0=posAdj[as.character(pos)], x1=pos, y0=1.29, y1=varYPos);
                                } else {
                                    text(x=pos, y=1.3 - ((adj-1) * 0.05),
                                         labels=sprintf("%d: %s -> %s", pos, ref, var), srt=varAng);
                                }
                            } else {
                                if(varAng == 90){
                                    text(x=posAdj[as.character(pos)], y=1.3, adj=c(0, 0.5),
                                         labels=sprintf("%d: %s -> %s%s", pos, ref, ref, var), srt=varAng);
                                    segments(x0=posAdj[as.character(pos)], x1=pos, y0=1.29, y1=varYPos);
                                } else {
                                    text(x=pos, y=1.3 - ((adj-1) * 0.05),
                                     labels=sprintf("%d: %s -> %s%s", pos, ref, ref, var), srt=varAng);
                                }
                            }
                        }
                        adj <- (adj + 1) %% 3;
                    }
                }
            }
        }
        invisible(dev.off());
        cat(sprintf(" done! Created '%s'\n", sprintf("%sstompPlot_%s.png", prefix, symname)));
    }
} else if((refType == "circular") && (contigLength > 0)){
    for(ref in unique(data.tbl$Assembly)){
        cat("Creating plot...");
        symname <- gsub("/",".",ref);
        if(outType == "png"){
            agg_png(sprintf("%sstompPlot_%s.png", prefix, symname), width=900, height=900, pointsize=12);
        } else if(outType == "svg"){
            agg_svg(sprintf("%sstompPlot_%s.svg", prefix, symname), width=14, height=14, pointsize=12);
        }
        par(mar=c(2.5,2.5,2.5,2.5), xpd=NA);
        allsub.tbl <- data.tbl[data.tbl$Assembly == ref,];
        allsub.tbl <- mutate(allsub.tbl, actPos=((Position-1) %% contigLength + 1)) %>%
            group_by(actPos, dir) %>%
            summarise(Coverage = sum(Coverage),
                      Position = first(actPos),
                      ref = first(ref),
                      startPoss = sum(startPoss), endPoss = sum(endPoss),
                      Ts=sum(Ts), Tv=sum(Tv), Cp=sum(Cp), d=sum(d), i=sum(i),
                      A=sum(A), C=sum(C), G=sum(G), T=sum(T));
        maxCov <- max(maxVal, allsub.tbl$Coverage);
        scalePoints <- scalePointsOrig <- unique(sort(c(pretty(c(0, maxCov), n=5), maxCov)));
        scalePointsMinor <- scalePoints;
        if(showLog){
            scalePoints <- rep(1, 9) * 10^(0:8);
            scalePointsMinor <- rep(1:9, 9) * 10^rep(0:8, each=9);
            scalePoints <- scalePointsOrig <- c(scalePoints[scalePoints < maxCov], maxCov);
            scalePointsMinor <- c(scalePointsMinor[scalePointsMinor < maxCov], maxCov);
            ## note: assumes at least two points on the scale
            spm.diff <- diff(tail(scalePointsMinor,3));
            if(spm.diff[2] < (spm.diff[1] / 4)){
                # remove second-to-last point, as it's too close to the final point
                scalePointsMinor <- scalePointsMinor[-(length(scalePointsMinor)-1)];
            }
            sp.diff <- diff(tail(scalePoints,3));
            if(sp.diff[2] < sp.diff[1]){
                # remove second-to-last point, as it's too close to the final point
                scalePoints <- scalePointsOrig <- scalePoints[-(length(scalePoints)-1)];
            }
        }
        ## note: X axis is inverted so that the positive direction is clockwise
        plot(NA, xlim=c(1,-1), ylim=c(-1,1), xlab="", ann=FALSE, axes=FALSE);
        ## Title
        if(showTitle){
            mtext(sprintf("%sReads Mapped to Reference Sequence: %s", sub("_"," ", prefix), ref), side=3, cex=1.5);
        }
        validPoss <- NULL;
        for(pDir in c("fwd", "rev", "both")){
            ## Circular axis base lines
            baseAngs <- head(seq(0, 2*pi, length.out=360), -1);
            if(annotationGap){
                if(pDir == "rev"){
                    polygon(x=0.59 * cos(baseAngs), y=0.59 * sin(baseAngs), lwd = 2, col=covColours[2]);
                }
            } else {
                if(pDir == "rev"){
                    polygon(x=0.7 * cos(baseAngs), y=0.7 * sin(baseAngs), lwd = 2, col=covColours[2]);
                }
            }
            mul <- c("fwd"=1, "rev"=-1, "both"=1)[pDir];
            base <- c("fwd"=0.8, "rev"=0.7, "both"=0.8)[pDir];
            sub.tbl <- filter(allsub.tbl, dir == pDir);
            if(nrow(sub.tbl) == 0){
                ## make sure there are actually points to plot
                next;
            }
            ## Read Coverage plot
            covArr <- rep(0, contigLength);
            covArr[sub.tbl$Position] <- sub.tbl$Coverage;
            ## Read Start / End
            baseCov <- 0;
            covStart <- rep(0, contigLength);
            covEnd <- rep(0, contigLength);
            if("startPoss" %in% colnames(sub.tbl)){
                covStart[sub.tbl$Position] <- sub.tbl$startPoss;
                covEnd[sub.tbl$Position] <- sub.tbl$endPoss;
            }
           covStart[covStart == 0] <- NA;
            covEnd[covEnd == 0] <- NA;
            maxCov <- oldMaxCov <- max(maxVal, allsub.tbl$Coverage);
            scalePoints.m <- scalePoints;
            scalePointsMinor.m <- scalePointsMinor;
            if(showLog){
                covArr <- log1p(covArr);
                baseCov <- log1p(baseCov);
                covStart <- log1p(covStart);
                covEnd <- log1p(covEnd);
                maxCov <- max(log1p(maxVal), log1p(data.tbl$Coverage));
                scalePoints.m <- log1p(scalePoints.m);
                scalePointsMinor.m <- log1p(scalePointsMinor.m);
            }
            oldCovArr <- covArr;
            adjFunc <- function(x){
                ifelse(annotationGap, 0.67, 0.75) +
                    (0.05 + (x / maxCov) * 0.2) * mul -
                    ifelse(annotationGap && (mul < 0), 0.03, 0);
            }
            covHalf <- 0.75 + (0.05 + ((oldCovArr/2) / maxCov) * 0.2) * mul;
            covArr <- adjFunc(covArr);
            baseCov <- adjFunc(baseCov);
            covStart <- adjFunc(covStart);
            covEnd <- adjFunc(covEnd);
            scalePoints.m <- adjFunc(scalePoints.m);
            scalePointsMinor.m <- adjFunc(scalePointsMinor.m);
            covAng <- ((seq_along(covArr) - 0.5) / contigLength) * 2 * pi;
            ## Adjust rotation given specified offset
            covAng <- covAng + ((posOffset / contigLength) * 2 * pi) + (pi / 2);
            ## Precalculate variant positions
            covTs <- adjFunc(sub.tbl$Ts / sub.tbl$Coverage *
                             oldCovArr[sub.tbl$Position]);
            covTv <- adjFunc(sub.tbl$Tv / sub.tbl$Coverage *
                             oldCovArr[sub.tbl$Position]);
            covCp <- adjFunc(sub.tbl$Cp / sub.tbl$Coverage *
                             oldCovArr[sub.tbl$Position]);
            covIns <- adjFunc((sub.tbl$Coverage + sub.tbl$i) /
                              sub.tbl$Coverage * oldCovArr[sub.tbl$Position]);
            covDel <- adjFunc((sub.tbl$Coverage - sub.tbl$d) /
                              sub.tbl$Coverage * oldCovArr[sub.tbl$Position]);
            ## Variant Distribution plot
            if(showVars){
                if(reportDels || showDels){
                    points(x=covIns * cos(covAng[sub.tbl$Position]),
                           y=covIns * sin(covAng[sub.tbl$Position]), pch = 20,
                           cex=(sub.tbl$i / sub.tbl$Coverage) * 2,
                           col="darkmagenta");
                    points(x=covDel * cos(covAng[sub.tbl$Position]),
                           y=covDel * sin(covAng[sub.tbl$Position]), pch = 20,
                           cex=(sub.tbl$d / sub.tbl$Coverage) * 2,
                           col="darkcyan");
                }
                points(x=covTs * cos(covAng[sub.tbl$Position]),
                       y=covTs * sin(covAng[sub.tbl$Position]), pch = 20,
                       cex=(sub.tbl$Ts / sub.tbl$Coverage) * 2,
                       col="blue");
                points(x=covTv * cos(covAng[sub.tbl$Position]),
                       y=covTv * sin(covAng[sub.tbl$Position]), pch = 20,
                       cex=(sub.tbl$Tv / sub.tbl$Coverage) * 2,
                       col="gold");
                points(x=covCp * cos(covAng[sub.tbl$Position]),
                       y=covCp * sin(covAng[sub.tbl$Position]), pch = 20,
                       cex=(sub.tbl$Cp / sub.tbl$Coverage) * 2,
                       col="red");
                if(pDir == "fwd"){
                    print(head(sort(sub.tbl$i, decreasing=TRUE)));
                    points(x=covIns * cos(covAng[sub.tbl$Position]),
                           y=covIns * sin(covAng[sub.tbl$Position]), pch = 20,
                           cex=(sub.tbl$i / sub.tbl$Coverage) * 2,
                           col="darkmagenta");
                }
            }
            ## Overlay coverage
            polygon(x=covArr * cos(covAng),
                    y=covArr * sin(covAng), lwd=2, col=ifelse(pDir == "rev", NA, covColours[1]));
            ## Circular axis base lines
            baseAngs <- head(seq(0, 2*pi, length.out=360), -1);
            if(annotationGap){
                if(pDir == "fwd"){
                    polygon(x=0.72 * cos(baseAngs), y=0.72 * sin(baseAngs), lwd = 2, col="white");
                }
            } else {
                if(pDir == "fwd"){
                    polygon(x=0.8 * cos(baseAngs), y=0.8 * sin(baseAngs), lwd = 2, col="white");
                }
            }
            if(pDir != "rev"){
                ## Circular axis tick marks / labels
                ppos <- pretty(c(0, allsub.tbl$Position, contigLength), n=10);
                ppos <- ppos[(ppos > 0) & (ppos < contigLength)];
                if((contigLength - max(ppos)) < (contigLength / 50)){
                    ppos <- head(ppos, -1);
                }
                ppos <- unique(c(ppos, contigLength));
                ## Add pi/2 to make top 0 point
                pposAng <- (ppos / contigLength) * 2 * pi + (pi/2);
                if(!annotationGap){
                    segments(x0=0.7 * cos(pposAng), y0=0.7 * sin(pposAng),
                             x1=0.8 * cos(pposAng), y1=0.8 * sin(pposAng), lwd=2);
                    segments(x0=0.72 * cos(pposAng), y0=0.72 * sin(pposAng),
                             x1=0.78 * cos(pposAng), y1=0.78 * sin(pposAng), col="#FFFFFF", lwd=4);
                    for(i in seq_along(ppos)){
                        text(x=0.75 * cos(pposAng[i]), y=0.75 * sin(pposAng[i]),
                             labels=ppos[i],
                             srt=(((-180 * pposAng[i] / pi - 90) + 89) %% 180) - 89,  cex=1);
                    }
                }
            }
            ## Overlay start / end positions
            csp <- which(!is.na(covStart));
            if(length(csp) > 0){
                segments(x0=baseCov * cos(covAng[csp]), y0=baseCov * sin(covAng[csp]),
                         x1=covStart[csp] * cos(covAng[csp]), y1=covStart[csp] * sin(covAng[csp]), col="red");
            }
            cep <- which(!is.na(covEnd));
            if(length(cep) > 0){
                segments(x0=baseCov * cos(covAng[cep]), y0=baseCov * sin(covAng[cep]),
                         x1=covEnd[cep] * cos(covAng[cep]), y1=covEnd[cep] * sin(covAng[cep]), col="blue");
            }
            ## Add in legend
            if(showVars){
                legend(legendLoc, legend=c("Ts","Tv","Cp","Ins","Del"),
                       ncol=ifelse(legendLoc %in% c("top", "bottom"),
                                   5, 2), bg="#FFFFFF80", inset=0.05,
                       fill=c("blue","gold","red","darkmagenta","darkcyan"));
            }
            validPoss <-
                c(validPoss,
                  sub.tbl$Position[(apply(sub.tbl[,c("Ts","Tv","Cp","i","d")],
                                          1,max) > sub.tbl$Coverage * 0.4) &
                                   (sub.tbl$Coverage > minDepth)]);
            ## Add in radial scale
            if(showScale && (pDir != "rev")){
                segments(y0=-scalePointsMinor.m,
                         x0=rep(-0.05, length(scalePointsMinor.m)),
                         x1=rep( 0.05, length(scalePointsMinor.m)),
                         col="#000000");
                text(x=-0.05,
                     y=-scalePoints.m, pos = 4,
                     labels = scalePointsOrig,
                     col="#000000");
            }
        }
        validPoss <- table(validPoss);
        if(bothFile == ""){
            validPoss <- validPoss[validPoss == 2];
        }
        adj <- 0;
        lastPos  <- -contigLength;
        if(showLabels){
            for(pos in as.numeric(names(validPoss))){
                if((abs(pos - lastPos) %% contigLength) > (contigLength / 50)){
                    adj <- 2;
                }
                lastPos <- pos;
                psub <- head(allsub.tbl[allsub.tbl$Position == pos,
                                        c("ref","A","C","G","T","d","i")],1);
                ref <- psub$ref;
                psub <- unlist(c(psub[,-1]));
                var <- c("A", "C", "G", "T", "d", "i")[psub == max(psub)];
                if(reportDels || (var != "d")){
                    covAng <- ((pos - 0.5) / contigLength) * 2 * pi;
                    covAng <- covAng +
                        ((posOffset / contigLength) * 2 * pi) + (pi / 2);
                    text(x=(1 + (adj-0.5) * 0.05)*cos(covAng),
                         y=(1 + (adj-0.5) * 0.05)*sin(covAng),
                         srt=(((-180 * covAng / pi - 90) + 89) %% 180) - 89,
                         labels=sprintf("%d: %s -> %s",
                         (pos + posOffset - 1) %% contigLength + 1, ref, var));
                    ## text(x=1*cos(covAng),
                    ##      y=1*sin(covAng),
                    ##      srt=(((-180 * covAng / pi - 90) + 89) %% 180) - 89,
                    ##      labels=sprintf("%d: %s -> %s",
                    ##      (pos + posOffset - 1) %% contigLength + 1, ref, var));
                    if(!annotationGap){
                        segments(x0=(0.98 + (adj-0.5) * 0.05)*cos(covAng),
                                 y0=(0.98 + (adj-0.5) * 0.05)*sin(covAng),
                                 x1=0.82*cos(covAng),
                                 y1=0.82*sin(covAng),
                                 lwd=2);
                        ## segments(x0=0.98*cos(covAng),
                        ##          y0=0.98*sin(covAng),
                        ##          x1=0.82*cos(covAng),
                        ##          y1=0.82*sin(covAng),
                        ##          lwd=2);
                    } else {
                        segments(x0=(0.98 + (adj-0.5) * 0.05)*cos(covAng),
                                 y0=(0.98 + (adj-0.5) * 0.05)*sin(covAng),
                                 x1=0.73*cos(covAng),
                                 y1=0.73*sin(covAng),
                                 lwd=2);
                        ## segments(x0=0.98*cos(covAng),
                        ##          y0=0.98*sin(covAng),
                        ##          x1=0.73*cos(covAng),
                        ##          y1=0.73*sin(covAng),
                        ##          lwd=2);
                    }
                    adj <- (adj + 1) %% 3;
                }
            }
        }
        invisible(dev.off());
        cat(sprintf(" done! Created '%s'\n", sprintf("%sstompPlot_%s.%s", prefix, symname, outType)));
    }
}
