#!/bin/sh
## Scripts to reload/relaunch docker images

## Sam's example
# docker run --restart always -d -p 8688:80/tcp -p 8688:80/udp jc19_shinyapp

## Useful 'docker -run' options
#  -d               : run in the background
#  -e <var=value>   : setting environment variables
#  -p portH:portC   : expose container portC as host portH
#  -v dirH:dirC     : expose host dirH as container dirC
#  -w dirC          : start container in dirC
#  --restart always : restart policy after quitting

## For debugging containers
# docker ps # to see container IDs
# docker exec -it <container id> bash
# docker images # to see stored images
# docker rmi name:version # to remove stored image

## Web file browser [https://10.0.0.15/]
# image name: fraoustin/fancyindex
# port: 80 (host); 80 (container)
# volume mapping:
#  /mnt/Bioinformatics/R_data/WebService/:/share
# Note: this image is run with restart enabled, so that the web browser is available
#       for others to use, providing basic working functionality
docker run --name fancyindex -d -p 80:80 -v /mnt/Bioinformatics/R_data/WebService/:/share \
  -e DISABLE_AUTH=true --restart=unless-stopped fraoustin/fancyindex

## Single Cell Browser [http://10.0.0.15:4242/]
# image name: shiny_cell_browser:latest
# command: R -e "shiny::runApp('./', port=4242)"
# port: 4242 (host); 3838 (container)
# volume mapping:
#  /mnt/Bioinformatics/R_data/RStudioProjects/Seurat:/app/data
# Note: this needs to be run directly to stop timeouts and time-expensive reloads
docker run --rm --name shiny_cell_browser -d -p 4242:3838 -v /mnt/Bioinformatics/R_data/RStudioProjects/Seurat:/app/data \
  shiny_cell_browser:latest R -e "shiny::runApp('/app', port=3838, host='0.0.0.0')"

## JBrowse MB [http://10.0.0.15:8001/]
# [see http://gmod.org/wiki/JBrowse_FAQ#What_webserver_is_needed_for_JBrowse]
# [see https://hub.docker.com/_/nginx]
docker run --rm --name jbrowse_mb -d -p 8001:80 \
  -v /mnt/Bioinformatics/JBrowse-MB:/usr/share/nginx/html nginx:latest

## JBrowse FR [http://10.0.0.15:8002/]
docker run --rm --name jbrowse_fr -d -p 8002:80 \
  -v /mnt/Bioinformatics/JBrowse-FR:/usr/share/nginx/html nginx:latest

## JBrowse2 [http://10.0.0.15:8003/]

## Microbrowser [http://10.0.0.15:3850/]
docker run --rm --name microbrowser -d -p 3850:3838 -v /mnt/Bioinformatics/R_data/ShinyApps/MicroBrowser:/srv/shiny-server \
  shinyapp:latest R -e "shiny::runApp('/srv/shiny-server', port=3838, host='0.0.0.0')"

## RStudio1 [http://10.0.0.15:8787/]
# image name: rocker/rstudio:latest
# environment variable PASSWORD=barPassword
docker run --rm --name RStudio1 -d -p 8787:8787 -v /mnt/Bioinformatics/R_data:/home/rstudio/R_data \
  -e PASSWORD=barPassword rocker/rstudio:latest

## RStudio2 [http://10.0.0.15:8788/]
# [same as above, just an alternate server]
docker run --rm --name RStudio2 -d -p 8788:8787 -v /mnt/Bioinformatics/R_data:/home/rstudio/R_data \
  -e PASSWORD=barPassword rocker/rstudio:latest

## Shiny Server [http://10.0.0.15:3838/]
docker run --rm --name shinyapp_base -d -p 3838:3838 -v /mnt/Bioinformatics/R_data/:/R_data \
  -v /mnt/Bioinformatics/R_data/ShinyApps/:/srv/shiny-server \
  shinyapp:latest

## opt-SNE [http://10.0.0.15:5000]
docker run --rm --name opt-sne -d -p 5000:5000 opt-sne python app/app.py

## Pavian [http://10.0.0.15:4280]

docker run --rm --name pavian -d -p 4280:80 florianbw/pavian:latest

## Galaxy [http://10.0.0.15:????]

## Repaver [http://10.0.0.15:4949]

docker run --rm --name repaver_GUI -d -p 4949:3838 repaver

## docker image listing

#REPOSITORY                           TAG                IMAGE ID       CREATED         SIZE
#bgruening/galaxy-stable              latest             d0ef2bae2440   23 months ago   2.53GB
#combinelab/salmon                    1.3.0              70a8625eec35   2 years ago     1.38GB
#comics/trimmomatic                   0.36               a6927281199c   3 years ago     683MB
#ewels/multiqc                        latest             e59e7a63498b   3 years ago     169MB
#florianbw/pavian                     latest             f356c4268191   3 years ago     1.78GB
#fraoustin/fancyindex                 latest             e6a9fa145ae1   2 years ago     222MB
#gawbul/docker-samtools               latest             5bdb0560b606   5 years ago     1.47GB
#givanna/spectre                      v0.3.3             279ee00b73f0   2 years ago     4.77GB
#hadrieng/mothur                      latest             a217cc6ea8b4   4 years ago     519MB
#halverneus/static-file-server        latest             4d1bfa9aadda   3 years ago     8.47MB
#idssniaid/joes-flow                  latest             9efbf2142ea3   5 weeks ago     2.08GB
#jbrowse/gmod-jbrowse                 latest             6856e20d2ce7   6 years ago     1.16GB
#jiahuikchen/globus-server            latest             47d265061ae0   4 years ago     631MB
#microbrowser                         2022-01-25         a515b9dc6201   9 months ago    5.74GB
#microbrowser                         latest             a515b9dc6201   9 months ago    5.74GB
#ndslabs/gcp-docker                   latest             c8c20f3812c5   4 years ago     773MB
#nginx                                latest             bc9a0695f571   23 months ago   133MB
#opt-sne                              2021-May-18        9399daed8afb   17 months ago   1.06GB
#opt-sne                              latest             9399daed8afb   17 months ago   1.06GB
#pegi3s/fastqc                        latest             5a439982c750   3 years ago     579MB
#quay.io/bgruening/galaxy-deeptools   latest             a6edcea062f7   5 years ago     4.3GB
#r-base                               latest             4b361dfebd4f   9 months ago    767MB
#rocker/r-base                        4.1.2              91af7f4c94cd   11 months ago   814MB
#rocker/rstudio                       latest             33485965c8d5   9 months ago    1.89GB
#rocker/shiny-verse                   latest             a016a1ba1a28   21 months ago   1.97GB
#rocker/tidyverse                     latest             26e26b23bb6d   3 years ago     2.1GB
#rocker/verse                         latest             974df4184f20   3 years ago     3.24GB
#room_labeller                        2021-06-15         782ca94414e3   16 months ago   1.44GB
#room_labeller                        latest             782ca94414e3   16 months ago   1.44GB
#shinyapp                             2021-04-07         ab3a1508228d   18 months ago   5.72GB
#shiny_cell_browser                   2022-08-19         592502ae3cb3   2 months ago    1.75GB
#shiny_cell_browser                   latest             592502ae3cb3   2 months ago    1.75GB

