#!/usr/bin/env Rscript

fast5File <- "called/workspace/filtered.fast5";
rawSignal <- NULL;
rawSignals <- list();
signalPositions <- NULL;
signalPoss <- list();


if(length(fast5File) != 0){
    ## Load data from called fast5 file
    ## [see https://community.nanoporetech.com/posts/mapping-of-signal-to-basec#comment_25169]
    if(!file.exists(fast5File)){
        cat(sprintf("Error: fast5 file '%s' does not exist\n",
                    fast5File));
        usage();
        quit(save = "no", status=0);
    }
    cat("Loading library 'rhdf5' to read fast5 file... ");
    library(rhdf5);
    cat("done!\n");
    f5Listing <- h5ls(fast5File);
    if(!any("Fastq" %in% f5Listing$name)){
        cat(sprintf(paste0("Error: fast5 file '%s' has no basecalls. ",
                           "Please use a basecalled fast5 file\n"), fast5File));
        usage();
        quit(save = "no", status=0);
    }
    dnaSeqFile <- tempfile(fileext=".fasta");
    firstSequence <- TRUE;
    for(fastqPath in which(f5Listing$name == "Fastq")){
        seqPath <- f5Listing$group[fastqPath];
        fastqSeq <- unlist(strsplit(
            h5read(fast5File, paste0(seqPath, "/Fastq")), "\\n"));
        seqID <- sub("^.(.*?) .*$", "\\1", fastqSeq[1]);
        DNASeq <- fastqSeq[2];
        cat(paste0(">", seqID, "\n", DNASeq, "\n"), file=dnaSeqFile,
            append=!firstSequence);
        firstSequence <- FALSE;
        ## Extract associated move table
        stridePath <- sub("/[^/]+$", "/Summary/basecall_1d_template", seqPath);
        signalStride <- h5readAttributes(fast5File, stridePath)$block_stride;
        moveTable <- cumsum(h5read(fast5File, paste0(seqPath, "/Move")));
        moveTable <- rep(moveTable, each=signalStride);
        ## Invert to create lookup from positions to moves
        signalPositions <- match(1:max(moveTable), moveTable);
        signalPoss[[seqID]] <- signalPositions;
        ## Extract first sample width
        trimPath <- sub("/[^/]+/[^/]+$",
                        "/Segmentation_000/Summary/segmentation", seqPath);
        trimWidth <- h5readAttributes(fast5File, trimPath)$first_sample_template;
        ## Extract associated signal
        sigPath <- sub("(/.*?)/.*$", "\\1/Raw/Signal", seqPath);
        rawSignal <- h5read(fast5File, sigPath);
        ## Trim off first sample fragment
        if(trimWidth > 0){
            rawSignal <- tail(rawSignal, -trimWidth);
        }
        rawSignals[[seqID]] <- rawSignal;
    }
}


sig.1 <- rawSignals[[1]];
sig.2 <- rawSignals[[2]];

library(dtw);

dtwPlotTwoWay <- function (d, xts = NULL, yts = NULL,
                           offset = 0, ts.type = "l", 
    pch = 21, match.indices = NULL, match.col = "gray70", match.lty = 3, 
    xlab = "Index", ylab = "Query value", ...) 
{
    if (is.null(xts) || is.null(yts)) {
        xts <- d$query
        yts <- d$reference
    }
    if (is.null(xts) || is.null(yts)) 
        stop("Original timeseries are required")
    ytso <- yts + offset
    maxlen <- max(length(xts), length(ytso))
    length(xts) <- maxlen
    length(ytso) <- maxlen
    def.par <- par(no.readonly = TRUE)
    if (offset != 0) {
        par(mar = c(5, 4, 4, 4) + 0.1)
    }
    matplot(cbind(xts, ytso), type = ts.type, pch = pch, xlab = xlab, 
        ylab = ylab, axes = FALSE, ...)
    box()
    axis(1)
    axis(2, at = pretty(xts))
    if (offset != 0) {
        rightTicks <- pretty(yts)
        axis(4, at = rightTicks + offset, labels = rightTicks)
    }
    if (is.null(match.indices)) {
        ml <- length(d$index1)
        idx <- 1:ml
    }
    else if (length(match.indices) == 1) {
        idx <- seq(from = 1, to = length(d$index1), length.out = match.indices)
    }
    else {
        idx <- match.indices
    }
    segments(d$index1[idx], xts[d$index1[idx]], d$index2[idx], 
        ytso[d$index2[idx]], col = match.col, lty = match.lty)
    par(def.par)
}

dtw.1 <- head(tail(sig.1, -1), -1);
dtw.1 <- (dtw.1 - median(dtw.1)) / mad(dtw.1);
dtw.2 <- head(tail(sig.2, -1), -150);
dtw.2 <- (dtw.2 - median(dtw.2)) / mad(dtw.2);

aligned <- dtw(dtw.1, dtw.2, keep=TRUE);

ill <- length(aligned$index1);
png("out.png", width=1200, height=600, pointsize=16);
par(mar=c(0.5,0.5,0.5,0.5));
plot(NA, xlim=c(0, ill/3+1), ylim=c(-4,20), axes=FALSE,
     ann=FALSE);
text(x=ill/9, y=20, names(rawSignals)[1], col="#FF000080");
text(x=ill*2/9, y=20, names(rawSignals)[2], col="#0000FF80");
for(slice in 0:2){
    rect(xleft=1, xright=ill/3,
         ybottom=(8 * (2-slice)) - 3,
         ytop=(8 * (2-slice)) + 3);
    points(x=((0 * ill)/3):((1 * ill)/3) + 1,
           y=(8 * (2-slice)) +
               dtw.1[aligned$index1[((slice * ill)/3):
                                    (((slice+1) * ill)/3) + 1]], type="l",
           lwd=2, col="#FF000060")
    points(x=((0 * ill)/3):((1 * ill)/3) + 1,
           y=(8 * (2-slice)) +
               dtw.2[aligned$index2[((slice * ill)/3):
                                    (((slice+1) * ill)/3) + 1]], type="l",
           lwd=2, col="#0000FF60")
    if(slice > 0){
        points(x=((-1 * ill)/3):((0 * ill)/3) + 1,
               y=(8 * (2-slice)) +
                   dtw.1[aligned$index1[(((slice-1) * ill)/3):
                                        (((slice) * ill)/3) + 1]], type="l",
               lwd=2, col="#FF000060")
        points(x=((-1 * ill)/3):((0 * ill)/3) + 1,
               y=(8 * (2-slice)) +
                   dtw.2[aligned$index2[(((slice-1) * ill)/3):
                                        (((slice) * ill)/3) + 1]], type="l",
               lwd=2, col="#0000FF60")
    }
    if(slice < 2){
        points(x=((1 * ill)/3):((2 * ill)/3) + 1,
               y=(8 * (2-slice)) +
                   dtw.1[aligned$index1[(((slice+1) * ill)/3):
                                        (((slice+2) * ill)/3) + 1]], type="l",
               lwd=2, col="#FF000060")
        points(x=((1 * ill)/3):((2 * ill)/3) + 1,
               y=(8 * (2-slice)) +
                   dtw.2[aligned$index2[(((slice+1) * ill)/3):
                                        (((slice+2) * ill)/3) + 1]], type="l",
               lwd=2, col="#0000FF60")
    }
    for(xpts in 0:5){
        text(x=ill/15 * xpts, y=8 * (2-slice) - 3.5,
             labels=paste0(round(mean(aligned$
                                      index1[ill/15 * xpts +
                                             (slice * ill)/3 +
                                             ifelse(xpts == 5, 0, 1)],
                                      aligned$
                                      index2[ill/15 * xpts +
                                             (slice * ill)/3 +
                                             ifelse(xpts == 5, 0, 1)]) /
                                 4000, 2), " s"));
    }
}
invisible(dev.off());

