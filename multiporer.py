#!/usr/bin/env python3

'''
reads elements from ONT multi-fast5 files and writes them to standard output.

Copyright 2019-2020, David Eccles (gringer) <bioinformatics@gringene.org>

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted. The software is
provided "as is" and the author disclaims all warranties with regard
to this software including all implied warranties of merchantability
and fitness. In other words, the parties responsible for running the
code are solely liable for the consequences of code execution.
'''

import os
import sys
import h5py
import numpy
import wave
from collections import deque, Counter, OrderedDict, defaultdict
from itertools import islice, repeat
from bisect import insort, bisect_left
from struct import pack
from array import array
from multiprocessing import Pool, cpu_count
from math import sin, asin, pi, log, exp, sqrt


## Carry out frequency modulation; vary the frequency of a wave based
## on a signal
##
## Here is the rough process:
# 1) find out the phase of the current frequency at the previous position
# 2) calculate the new amplitude by adding one time slice to that phase

def fmod(outFileName, signal, minFreq, maxFreq, oldRate, newRate,
         speed=1.0, volume=0.8, logScale=True):
    fmodOut = wave.open(outFileName, 'w')
    fmodOut.setparams((1, 2, newRate, 0, 'NONE', 'not compressed'))
    oldRate = oldRate * speed
    logRange = log(maxFreq) - log(minFreq)
    linRange = maxFreq - minFreq
    meanSig = sum(signal) / len(signal)
    madSig = sum(map(lambda x: abs(x - meanSig), signal)) / len(signal)
    minSig = meanSig - madSig * 4
    maxSig = meanSig + madSig * 4
    if(min(signal) > minSig):
        minSig = min(signal)
    if(max(signal) < maxSig):
        maxSig = max(signal)
    sys.stderr.write("Min: %f, Max: %f, Signal: %d\n" %
                     (minSig, maxSig, len(signal)))
    ## limit signal to within MAD range, scale to 0..100
    signal = map(lambda x: float(0) if (x < minSig) else
                 (float(99) if x > maxSig else
                  float(99) * (x - minSig) / (maxSig - minSig)), signal)
    newFreqs = (map(
        lambda x: exp((log(x + 1) / log(100)) * logRange + log(minFreq)),
        signal)) if logScale else (
            map(lambda x: (x/100) * linRange + minFreq, signal))
    # number of sound samples per signal sample
    newPerOld  = (float(newRate) / (float(oldRate)))
    amp = [0] * int(len(list(signal)) * newPerOld)
    # start in first quadrant [0..pi/2)
    quadrant = 0
    oldPhase = 0
    oldAmplitude = 0
    for s in range(len(list(signal))-1):
        ## work out phase of previous signal
        if(quadrant == 0):
            oldPhase = asin(oldAmplitude)
        if(quadrant == 1):
            oldPhase = pi - asin(oldAmplitude)
        if(quadrant == 2):
            oldPhase = pi - asin(oldAmplitude)
        if(quadrant == 3):
            oldPhase = 2 * pi + asin(oldAmplitude)
        newFreq = newFreqs[s+1]
        ## determine phase step (for input)
        sigPhaseStep = (newFreq * 2 * pi / oldRate)
        ## determine phase step (for output)
        outPhaseStep = (newFreq * 2 * pi / newRate)
        ## determine new phase
        newPhase = (oldPhase + sigPhaseStep) % (2 * pi)
        ## calculate new quadrant
        quadrant = int(newPhase / (pi/2))
        ## determine new amplitude
        oldAmplitude = sin(newPhase)
        ## write signal to file
        sStart = int(s*newPerOld);
        sEnd = int((s+1)*newPerOld);
        packedSamples = map(
            lambda x: struct.pack('h',int(sin(oldPhase + x*outPhaseStep) *
                                          volume * 32767)),
            xrange(sEnd-sStart))
        fmodOut.writeframes(''.join(packedSamples))
    fmodOut.close()

def generate_fastq(fileName, modThreshold=128, fWidth=150):
    '''write out fastq sequence(s) from fast5 file'''
    callStr = ""
    try:
        h5File = h5py.File(fileName, 'r')
        h5File.close()
    except:
        return False
    with h5py.File(fileName, 'r') as h5File:
        mt = modThreshold
        fw = fWidth
        for readName in h5File:
            if("Analyses" in h5File[readName]):
                for ia in h5File[readName]["Analyses"]:
                    if(ia.startswith("Basecall_1D")):
                        bcData = h5File[readName][
                            "Analyses"][ia]["BaseCalled_template"]
                        fqData = bcData["Fastq"][()].decode('UTF-8')
                        if("ModBaseProbs" in bcData):
                            mbp = bcData["ModBaseProbs"]
                            mbpStr = mbp.attrs["modified_base_long_names"]
                            fqData = bcData["Fastq"][()].decode('UTF-8').split("\n")
                            fqData[0] = fqData[0][1:] # clip '@'
                            sys.stdout.write("@%s [methyl - %s]\n" % (fqData[0], mbpStr))
                            fqSeq = fqData[1]
                            if(mbp.attrs["modified_base_long_names"] == b'6mA 5mC'):
                                fqSeq = fqData[1]
                                mbpArr = mbp[()]
                                for bi in range(len(fqSeq)):
                                    if((fqSeq[bi] == "A") and (mbpArr[bi][1] >= mt)):
                                        sys.stdout.write("Y")
                                    elif((fqSeq[bi] == "C") and (mbpArr[bi][3] >= mt)):
                                        sys.stdout.write("Z")
                                    else:
                                        sys.stdout.write(fqSeq[bi])
                            elif(mbp.attrs["modified_base_long_names"] == b'5mC'):
                                fqSeq = fqData[1]
                                mbpArr = mbp[()]
                                for bi in range(len(fqSeq)):
                                    if((fqSeq[bi] == "C") and (mbpArr[bi][2] >= mt)):
                                        sys.stdout.write("m")
                                    else:
                                        sys.stdout.write(fqSeq[bi])
                            sys.stdout.write("\n+\n")
                            fqQual = fqData[3]
                            sys.stdout.write("%s\n" % fqQual)
                        else:
                            sys.stdout.write("%s\n" % fqData)

def generate_fastm(fileName, modThreshold=128, fWidth=150):
    '''write out fastm sequence(s) from fast5 file'''
    callStr = ""
    try:
        h5File = h5py.File(fileName, 'r')
        h5File.close()
    except:
        return False
    with h5py.File(fileName, 'r') as h5File:
        mt = modThreshold
        fw = fWidth
        for readName in h5File:
            if("Analyses" in h5File[readName]):
                for ia in h5File[readName]["Analyses"]:
                    if(ia.startswith("Basecall_1D")):
                        bcData = h5File[readName][
                            "Analyses"][ia]["BaseCalled_template"]
                        fqData = bcData["Fastq"][()].decode('UTF-8').split("\n")
                        fqData[0] = fqData[0][1:] # clip '@'
                        sys.stdout.write("#seq:%s\n" % fqData[0])
                        fqSeq = fqData[1]
                        while(len(fqSeq) > 0):
                            sys.stdout.write(" %s\n" % fqSeq[:fw])
                            fqSeq = fqSeq[fw:]
                        fqID = fqData[0].split(' ')[0]
                        sys.stdout.write("#qual:%s\n" % fqID)
                        fqQual = fqData[3]
                        while(len(fqQual) > 0):
                            sys.stdout.write(" %s\n" % fqQual[:fw])
                            fqQual = fqQual[fw:]
                        if("ModBaseProbs" in bcData):
                            mbp = bcData["ModBaseProbs"]
                            if(mbp.attrs["modified_base_long_names"] == b'6mA 5mC'):
                                sys.stdout.write("#methyl:%s\n" % fqID)
                                fqSeq = fqData[1]
                                mbpArr = mbp[()]
                                for bi in range(len(fqSeq)):
                                    if(bi % fw == 0):
                                        sys.stdout.write(" ")
                                    if((fqSeq[bi] == "A") and (mbpArr[bi][1] >= mt)):
                                        sys.stdout.write("6")
                                    elif((fqSeq[bi] == "C") and (mbpArr[bi][3] >= mt)):
                                        sys.stdout.write("5")
                                    else:
                                        sys.stdout.write(" ")
                                    if((bi+1) % fw == 0):
                                        sys.stdout.write("\n")
                                if(not len(fqSeq) % fw == 0):
                                        sys.stdout.write("\n")

def generate_txt(fileName, extraArgs):
    readCount = 0
    try:
        h5File = h5py.File(fileName, 'r')
        h5File.close()
    except:
        sys.stderr.write("Error: cannot read fast5 information from file '%s'" % fileName)
        return 0
    outFileName = "read_names.txt"
    if("-o" in extraArgs):
        outFileName = extraArgs[extraArgs.index("-o")+1]
    fileBase = os.path.basename(fileName)
    sys.stderr.write('File base: "%s"' % fileBase)
    with h5py.File(fileName, 'r') as h5File:
        with open(outFileName, 'a') as f:
            for readName in h5File:
                readBase = readName[5:]
                f.write('%s\t%s\n' % (readBase, fileBase))
                readCount += 1
    sys.stderr.write(' done [written to "%s"]!\n' % outFileName)
    return(readCount)

def filter_fast5(fileName, extraArgs):
    try:
        h5File = h5py.File(fileName, 'r')
        h5File.close()
    except:
        sys.stderr.write("Error: cannot read fast5 information from file '%s'" % fileName)
        return False
    selectReads = defaultdict(bool)
    readCount = 0
    removeAnalyses = False
    addAllReads = False
    if("-i" in extraArgs):
        readFileName = extraArgs[extraArgs.index("-i")+1]
        with open(readFileName, 'r') as readFile:
            for l in readFile.readlines():
                selectReads[l.rstrip()] = True
    elif("-strip" in extraArgs):
        removeAnalyses = True
    else:
        for r in extraArgs:
            selectReads[r] = True
    outFileName = "filtered.fast5"
    if("-o" in extraArgs):
        outFileName = extraArgs[extraArgs.index("-o")+1]
    if(len(selectReads) == 0):
        addAllReads = True
    readsToAdd = dict()
    # note: I tried to use 'core' driver so that deleting attributes
    #       won't remove them from the original file... but it seems
    #       to delete them anyway
    with h5py.File(fileName) as h5File:
        for readName in h5File:
            readBase = readName[5:]
            if(addAllReads or selectReads[readBase]):
                readsToAdd[readName] = h5File[readName]
        sys.stderr.write('    Writing %d of %s reads from file: %s...' %
                         (len(readsToAdd), len(h5File), fileName))
        ## Copy over attributes [from all files]
        oldAttrs = h5File.attrs
        if(len(readsToAdd) > 0):
            with h5py.File(outFileName, 'a') as newH5:
                for readName, readVal in readsToAdd.items():
                    if (removeAnalyses and 'Analyses' in readVal):
                        del readVal['Analyses']
                    if (not readName in newH5):
                        h5File.copy(readVal, newH5)
                        readCount += 1
                for attrName in oldAttrs:
                    newH5.attrs[attrName] = oldAttrs[attrName]
        sys.stderr.write(' done [written to "%s"]!\n' % outFileName)
    return(readCount)

def generate_wav(fileName, extraArgs):
    try:
        h5File = h5py.File(fileName, 'r')
        h5File.close()
    except:
        return False
    outFileBase = "signal"
    if("-o" in extraArgs):
        outFileBase = extraArgs[extraArgs.index("-o")+1]
    readsToAdd = dict()
    with h5py.File(fileName, 'r') as h5File:
        readPos = 0
        for readName in h5File:
            readPos += 1
            readBase = readName[5:11] # chop off 'read_' from front
            outFileName = '%s_%04d_%s.wav' % (outFileBase, readPos, readBase)
            sys.stderr.write('Creating audio file: %s\n' % outFileName)
            readSignal = h5File['%s/Raw/Signal' % readName][()]
            sampleRate = h5File['%s/channel_id' % readName].attrs['sampling_rate']
            sys.stderr.write('Number of samples: %d (%s Hz)\n' % (len(readSignal), sampleRate))
            fmod(outFileName=outFileName, signal=readSignal, minFreq=100,
                 maxFreq=2000, oldRate=sampleRate, speed=450, newRate=44100)
    return(readPos)


def usageQuit(message):
    sys.stderr.write(message + "\n\n")
    sys.stderr.write('Usage: %s <dataType> <fast5 file name> [options]\n' % sys.argv[0])
    sys.stderr.write(' where <dataType> is one of the following:\n')
    sys.stderr.write('  txt       - write read IDs only\n')
    sys.stderr.write('  fastq     - extract base-called data as fastq\n')
    sys.stderr.write('  fastm     - extract base-called data as fastm\n')
    sys.stderr.write('  fast5     - filter fast5 file(s) to extract reads\n')
    sys.stderr.write('Other options:\n')
    sys.stderr.write('  -strip    - strip analyses (e.g. basecalls) from file\n')
    sys.stderr.write('  -i <file> - file containing read IDs to extract\n')
    sys.stderr.write('  -o <file> - file to put results into\n')
    sys.exit(1)

def processFile(dataType, fileName, extraArgs):
    if(dataType == "fastm"):
        generate_fastm(fileName)
    elif(dataType == "fastq"):
        generate_fastq(fileName)
    elif(dataType == "wav"):
        generate_wav(fileName, extraArgs)
    elif(dataType == "txt"):
        generate_txt(fileName, extraArgs)
    elif(dataType == "fast5"):
        readCount = 0
        readCount += filter_fast5(fileName, extraArgs)
        #if(readCount > 0):
        #        sys.stderr.write(('Done! Created / appended %d reads to ' +
        #                          '"filtered.fast5"\n') %
        #                         readCount)

if (len(sys.argv) < 3):
    usageQuit('Error: No file or directory provided in arguments')

validOptions = ("fastm", "fastq", "fast5", "txt", "wav")

dataType = sys.argv[1]
if(not dataType in validOptions):
    usageQuit('Error: Incorrect dataType "%s"' % dataType)

fileArg = sys.argv[2]
extraArgs = []
if (len(sys.argv) > 3):
    extraArgs = sys.argv[3:]
seenHeader = False

if("-strip" in extraArgs):
    sys.stderr.write('[will strip analyses from both output *and* input file]\n')

if(os.path.isdir(fileArg)):
    sys.stderr.write("Processing directory '%s':\n" % fileArg)
    for dirPath, dirNames, fileNames in os.walk(fileArg):
        fileNames = filter(lambda x: x.endswith(".fast5"), fileNames)
        fc = len(str(fileNames))
        sys.stderr.write('  %d files to visit\n' % fc)
        for fileName in fileNames:
            if(not (dataType in ('fast5'))):
                sys.stderr.write(".");
            if(fileName.endswith(".fast5")): # only process fast5 files
                processFile(dataType, os.path.join(dirPath, fileName),
                            extraArgs)
                if((fc == 2) or ((fc-1) % 100 == 0)):
                    seenHeader = True
                    if((fc-1) == 1):
                        sys.stderr.write("  [%d more file to process]\n" %
                                         (fc-1))
                    elif(fc % 100 == 0):
                        sys.stderr.write("  [%d more files to process]\n" %
                                         (fc-1))
            fc -= 1
elif(os.path.isfile(fileArg)):
    processFile(dataType, fileArg, extraArgs)
else:
    usageQuit('Unknown file argument "%s"' % fileArg)
