#!/usr/bin/env Rscript

contribString <- "MJ, BB, JRT, DE, RML, TPS, ALD, TKN, LHH, KR, TB: manuscript preparation; MJ, JRT: chairs for regular MARC meetings; KR, JoG, TKN, LHH: method development; DE, JRT, BB, KR, JoG, DMD, ALD, RML, NP, DH, TKN, LHH, AB, SO, JRT ...: sample preparation & sequencing; MJ, JRT: base calling & data transfer; DE: read-length QC & kraken2 analyses; BB: MG-RAST analyses.";

contribSplit <- unlist(strsplit(contribString, "[:;.]+ *"));

contrib.df <-
    data.frame(authors=contribSplit[seq(1, length(contribSplit), by=2)],
               tasks=contribSplit[seq(2, length(contribSplit), by=2)],
               stringsAsFactors=FALSE);
authors.list <- strsplit(contrib.df$authors,", *");
unique.authors <- unique(gsub(" ","",unlist(authors.list)));

contrib.mat <- matrix(0, ncol=length(unique.authors),
                      nrow=nrow(contrib.df),
                      dimnames=list(contrib.df$tasks, unique.authors));

for(row in seq_len(nrow(contrib.df))){
    task.authors <-
        gsub(" ","",unlist(strsplit(contrib.df$authors[row],", *")));
    contrib.mat[row, task.authors] <- 1;
}

png("contribution_matrix.png", width=1000, height=500);
par(mar=c(4,15,0.5,0.5), las=2);
image(x=seq_len(ncol(contrib.mat)),
      y=seq_len(nrow(contrib.mat)),
      z=t(contrib.mat), col=c("white","black"),
      axes=FALSE, ann=FALSE);
box();
grid(ny=nrow(contrib.mat), nx=ncol(contrib.mat));
axis(2, seq_len(nrow(contrib.mat)), labels=rownames(contrib.mat));
axis(1, seq_len(ncol(contrib.mat)), labels=colnames(contrib.mat));
invisible(dev.off());
cat("Created 'contribution_matrix.png'\n");
