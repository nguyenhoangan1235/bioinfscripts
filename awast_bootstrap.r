#!/usr/bin/env Rscript

## identify file names for bootstrap sub-samples
library(dplyr);
library(tidyr);

## Set up initial variables [these will be eventually argumentised for command-line use]

dataDir <- "mapped_all/"; # directory containing stomped reads
##                                           # (in sub-directories per-barcode)
dataRanges <-
    list("chrM"         = c( 9013,11147),  # sequence ranges for targets
         "CD45_genomic" = c(19688,20168)); # "target" = (start, end)
numBootstraps <- 100; # Number of bootstrap sub-samples to carry out
covThreshold <- 100;  # Don't consider anything with coverage below this

## Identify barcode IDs
barcode.ids <- sort(list.dirs(dataDir, recursive=FALSE, full.names=FALSE));

## Create skeleton for input files
bs.files <- matrix("", nrow=numBootstraps, ncol=length(barcode.ids),
                   dimnames=list(1:100, barcode.ids));

## Load read resampling into the input file array
for(bid in barcode.ids){
    stompedFiles <- list.files(paste0(dataDir,bid), full.names=TRUE);
    bs.files[,bid] <- sample(stompedFiles, numBootstraps, replace=TRUE);
}

varLookup <- list("Trs" = c(A="G", C="T", G="A", T="C"),
                  "Trv" = c(A="C", C="A", G="T", T="G"),
                  "Cmp" = c(A="T", C="G", G="C", T="A"));

bsRes <- list();

for(s1 in head(barcode.ids,-1)){
    for(s2 in (barcode.ids[barcode.ids > s1])){
        cat(sprintf("** %s vs %s **\n", s1, s2));
        resMats <- list(ranks = list(), chisq = list());
        for(assembly in names(dataRanges)){
            aR <- dataRanges[[assembly]];
            resMats$ranks[[assembly]] <-
                matrix(aR[2]+1, nrow=diff(aR)+1, ncol=numBootstraps,
                       dimnames=list(aR[1]:aR[2],
                                     seq_len(numBootstraps)));
            resMats$chisq[[assembly]] <-
                matrix(0, nrow=diff(aR)+1, ncol=numBootstraps,
                       dimnames=list(aR[1]:aR[2],
                                     seq_len(numBootstraps)));
        }
        for(bs in seq_len(numBootstraps)){
            cat(sprintf("  Bootstrap #%03d...", bs));
            ## fetch data from input files
            data1.tbl <-
                as.tbl(read.csv(bs.files[bs,s1], stringsAsFactors=FALSE));
            data2.tbl <-
                as.tbl(read.csv(bs.files[bs,s2], stringsAsFactors=FALSE));
            for(assembly in names(dataRanges)){
                aRange <- dataRanges[[assembly]];
                data1.sub.tbl <-
                    filter(data1.tbl, Assembly == assembly,
                           between(Position, aRange[1], aRange[2]));
                data2.sub.tbl <-
                    filter(data2.tbl, Assembly == assembly,
                           between(Position, aRange[1], aRange[2]));
                ## If maximum coverage is too low in either sample, move along
                if((nrow(data1.sub.tbl) < 1) |
                   (nrow(data2.sub.tbl) < 1)){
                       next;
                }
                if((max(data1.sub.tbl$Coverage < covThreshold))|
                   (max(data2.sub.tbl$Coverage < covThreshold))){
                    next;
                }
                ## If not enough data in either sample, move along
                if((nrow(data1.sub.tbl) < (diff(aRange)/2)) ||
                   (nrow(data2.sub.tbl) < (diff(aRange)/2))){
                    next;
                }
                aggregateData.tbl <- group_by(rbind(data1.sub.tbl, data2.sub.tbl),
                                              Sample, Position) %>%
                    summarise(ref=first(ref), cR=sum(cR), A=sum(A), C=sum(C), G=sum(G),
                              T=sum(T), d=sum(d), i=sum(i));
                ## set up variantTypes variable columns
                aggregateData.tbl$Trs <- 0; ## Transition:    YR-preserving
                aggregateData.tbl$Trv <- 0; ## Transversion:  MK-preserving
                aggregateData.tbl$Cmp <- 0; ## Complementary: SW-preserving
                ## populate variantTypes with corresponding values
                for(base in c("A","C","G","T")){
                    mutBases <- sapply(names(varLookup), function(x){ varLookup[[x]][base] });
                    names(mutBases) <- sub("\\..$","",names(mutBases));
                    aggregateData.tbl[aggregateData.tbl$ref == base,names(mutBases)] <-
                        aggregateData.tbl[aggregateData.tbl$ref == base,mutBases];
                }
                plainData.tbl <-
                    arrange(aggregateData.tbl, Position, Sample) %>%
                    mutate(A=NULL, C=NULL, G=NULL, T=NULL, ref=NULL, Assembly=NULL) %>%
                    gather("mutType","count",-Position, -Sample);
                ## create structured array from the data range
                plainData.arr <-
                    unclass(xtabs(count ~ Sample + mutType + Position, data=plainData.tbl));
                ## determine allelic chi^2 at each location, where the expected values represent
                ## the average proportion of each allele (ref, Trs, Trv, Cmp, d, i) in all samples (with prior
                ## normalisation to account for read count total differences)
                pos.chisq <-
                    tibble(X=as.numeric(dimnames(plainData.arr)$Position),
                           Y=apply(plainData.arr, c(3), function(x){
                               chi.arr.obs <- x;
                               mean.profile <- colSums(chi.arr.obs * 1/rowSums(chi.arr.obs),
                                                       na.rm=TRUE) / sum(rowSums(chi.arr.obs) > 0)
                               ## expected: assume proportions of each variant type are similar
                               chi.arr.exp <-
                                   matrix(rep(mean.profile, each=nrow(chi.arr.obs)) *
                                          rep(rowSums(chi.arr.obs), ncol(chi.arr.obs)),
                                          nrow=nrow(chi.arr.obs));
                               sum((chi.arr.obs-chi.arr.exp)^2 * 1/chi.arr.exp, na.rm=TRUE);
                           }));
                pos.chisq$rank <- rank(-pos.chisq$Y);
                ranksOld <- resMats$ranks[[assembly]];
                chisqOld <- resMats$chisq[[assembly]];
                ranksOld[as.character(pos.chisq$X),bs] <- pos.chisq$rank;
                chisqOld[as.character(pos.chisq$X),bs] <- pos.chisq$Y;
                resMats$ranks[[assembly]] <- ranksOld;
                resMats$chisq[[assembly]] <- chisqOld;
            }
            cat(" done\n");
        }
        bsRes <- append(bsRes, list(list(s1=s1, s2=s2, ranks=resMats$ranks, chisq=resMats$chisq)));
    }
}

## ** visualisation ** ##
library(scales);
pdf("out_bsRank_allMapped_GC_323_DS7_2018-Jul-30.pdf", width=11, height=8);
for(bsResult in bsRes){
    for(assembly in rev(names(bsResult$ranks))){
        s1 <- bsResult$s1;
        s2 <- bsResult$s2;
        cat(sprintf("Assembly: %s; %s vs %s\n", assembly, s1, s2));
        data.mat <- bsResult$ranks[[assembly]];
        if(all(data.mat == data.mat[1])){
            next;
        }
        data.mat <- data.mat[,apply(data.mat,2,min) < 10];
        data.mat <- t(apply(data.mat,1,sort));
        nd <- ncol(data.mat);
        layout(matrix(1:2,ncol=2), widths=c(0.85,0.15));
        par(mar=c(5,5,3.5,1.5));
        image(x = as.numeric(rownames(data.mat)),
              main = sprintf("AWAsT rank plot (%s; %s vs %s)",
                             assembly, s1, s2),
              y=(1.05^(1:nd)) / (1.05^nd), z=data.mat,
              zlim=c(1,nrow(data.mat)+1),
              col=viridis_pal(direction=-1)(nrow(data.mat)),
              ylab="bootstrap difference rank (sorted, arbitrary scale)",
              xlab="base location (bp)");
        ## Draw key
        par(mar=c(3.5,0.5,3.5,3.5));
        image(x=1, y=seq_len(nrow(data.mat)), main="Rank",
              z=matrix(seq_len(nrow(data.mat)),nrow=1),
              col=viridis_pal(direction=-1)(nrow(data.mat)),
              axes=FALSE);
        box();
        axis(4, las=2);
        axis(4, at=c(1,nrow(data.mat)), las=2);
    }
}
invisible(dev.off());
