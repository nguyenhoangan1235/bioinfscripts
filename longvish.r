#!/usr/bin/env Rscript

argLoc <- 1;
doDebug <- FALSE;

usage <- function(){
  cat("usage: ./seqmat.r <fasta file> <repeat unit size> [options]\n");
  cat("\nOther Options:\n");
  cat("-help               : Only display this help message\n");
  cat("-type (png/pdf/svg) : Image type (default 'png')\n");
  cat("-ps <factor>        : Magnification factor for points\n");
  cat("-bg <string>        : Background colour\n");
  cat("-title <string>     : Title for image\n");
  cat("-prefix <string>    : Prefix for output file (default 'sequence_')\n");
  cat("-puzzle <n>         : Create puzzle cut lines; <n> pieces per ring\n");
  cat("-nobases            : Draw only puzzle lines (exclude base sequence)\n");
  cat("-solid              : Make colours solid (not translucent)\n");
  cat("-spiral             : Add a white spiral to the plot\n");
  cat("-hflip              : Flip horizontally\n");
  cat("-comp               : Complement the sequence\n");
  cat("-rev                : reverse the spiral direction\n");
  cat("-min                : Set minimum circle radius\n");
  cat("-max                : Set maximum circle radius\n");
  cat("-clabel             : center the label/key\n");
  cat("-nokey              : Remove key\n");
  cat("-nocircle           : Don't draw the circle plot\n");
  cat("-size <x>x<y>       : Image size (default 1200x1200 for png, 12x12 for PDF)\n");
  cat("-col (ef|cb|gg)     : Colour palette (electrophoresis, colour-blind [default], ggplot)\n");
 cat("\n");
}

puzzleLine <- function(x0=-0.5, y0=0.5, x1=2, y1=3, curve=FALSE,
                       ils=0.5, endTab=0.0025){
    ## The line is built on sub-arcs of three circles plus a straight line,
    ## mirrored at the centre point of the base curve / line
    ## Assuming the line is vertical, centred on the Y axis,
    ## and d units in length, with interlock size s,
    ## centres of these circles are at:
    ## (x,y) ; (s/2, d/2-s-s/2) ; (-s/2,d/2) with radius
    ##   x         sqrt(s^2/2)    sqrt(2*s^2)
    ## where (x,y) is the centre of a circle arc to (s, d/2-2*s)
    ## that grazes the y axis and touches the intersect point at a 45 degree angle
    ## As additional clarification, the interlock circle and joining circle
    ## have diagonal points that intersect with the x=0 line, whereas the
    ## entry curve touches the x=0 line on its edge
    flip <- FALSE;
    res <- 48;
    d.real <- sqrt((y1-y0)^2 + (x1-x0)^2) - endTab*2;
    endTabAngle <- 0;
    if(curve){
        ## note: d for curve should be arc length, which would be
        ## slightly greater than the straight-line distance this is
        ## approximated as the circle arc length at the segment start
        ## rather than the spiral arc length
        ## just use mean radius for calculating circle arc length
        r0 <- sqrt(x0^2+y0^2); r1 <- sqrt(x1^2+y1^2);
        ## work out end tab angle delta
        endTabAngle <- (endTab * 2) / ((r0+r1)/2);
        a0 <- atan2(y0,x0) + endTabAngle; a1 <- atan2(y1,x1) - endTabAngle;
        ad <- abs(a1 - a0);
        if(ad > pi){ ## it chose the long way round
            ad <- 2*pi - ad;
        }
        ## calculate adjusted arc-length distance
        d.real <- (r0+r1)/2 * ad;
    }
    d <- d.real;
    cr1 <- ils * (sqrt(2) + 2);
    cy1 <- d/2 - 2*ils - cr1 / sqrt(2);
    #cat(sprintf("(%0.2f,%0.2f) - (%0.3f,%0.3f); %0.3f; %s; d=%0.3f\n",
    #                x0, y0, x1, y1, ils, curve, d.real));
    if(cy1 < 0){ ## entry curve is too large, so scale curve down
        ## minimum distance
        d <- 4*ils + cr1 * sqrt(2);
    }
    cr1 <- ils * (sqrt(2) + 2);
    cx1 <- cr1;
    cy1 <- d/2 - 2*ils - cr1 / sqrt(2);
    cx2 <- ils/2;
    cy2 <- d/2-(ils*1.5);
    cr2 <- sqrt((ils/2)^2+(ils/2)^2);
    cx3 <- -ils;
    cy3 <- d/2;
    cr3 <- sqrt(ils^2+ils^2);
    cy4 <- d/2 + (ils*1.5);
    cy5 <- d/2 + 2*ils + cr1 / sqrt(2);
    tc <- c(seq(0, 2*pi, length.out=res));
    t1 <- c(seq(0, 1, length.out=res));
    res <- rbind(
        if(d.real < d){
            data.frame(x=NA, y=NA);
        } else {
            data.frame(x=0, y=t1*cy1);
        },
        data.frame(x=cr1*cos(-tc/8-pi) + cx1,
                   y=cr1*sin(-tc/8-pi) + cy1),
        data.frame(x=cr2*cos(tc/2-pi/4) + cx2,
                   y=cr2*sin(tc/2-pi/4) + cy2),
        data.frame(x=cr3*cos(-tc*3/4-pi/4) + cx3,
                   y=cr3*sin(-tc*3/4-pi/4) + cy3),
        data.frame(x=cr2*cos(tc/2-pi*3/4) + cx2,
                   y=cr2*sin(tc/2-pi*3/4) + cy4),
        data.frame(x=cr1*cos(-tc/8-pi*3/4) + cx1,
                   y=cr1*sin(-tc/8-pi*3/4) + cy5),
        if(d.real < d){
            data.frame(x=NA, y=NA);
        } else {
            data.frame(x=0, y=cy5 + t1*(d-cy5));
        }
    );
    ## skip over duplicated points
    res <- unique(res);
    ## rescale if lengths don't match
    if(d.real < d){
        res$x <- res$x * (d.real / d);
        res$y <- res$y * (d.real / d);
        d <- d.real;
    }
    if(!curve){
        ## incorporate tab adjustment
        res <- t(t(res) + c(0, endTab));
        ## rotate to match line
        rotAng <- atan2(x1-x0, y1-y0);
        rtMat <- matrix(c(cos(rotAng), sin(rotAng),
                          -sin(rotAng), cos(rotAng)), nrow=2);
        res <- (as.matrix(res) %*% rtMat);
        ## reposition to match start point
        ##res <- t(t(res) + c(x0,y0));
        res <- t(t(res) + c(x0,y0));
    } else { ## Note: we already know the circle centre; it's (0,0)
        ## scale y direction to make it fit on the line (0,0) - (0,1)
        res$y <- res$y / d;
        ## Because this could be on a spiral, start/end points might have a
        ## different radius
        r0 <- sqrt(x0^2+y0^2); r1 <- sqrt(x1^2+y1^2);
        r.base <- res$y * (r1-r0) + r0;
        ## Ignore the spiral shape for calculating tangent angle,
        ## just assume it's a circle
        a0 <- atan2(y0,x0) + endTabAngle/2;
        a1 <- atan2(y1,x1) - endTabAngle/2;
        ad <- abs(a1 - a0);
        if(abs(ad) > pi){ ## it chose the long way round
            np0 <- c(x1,y1); np1 <- c(x0,y0);
            x0 <- np0[1]; y0 <- np0[2];
            x1 <- np1[1]; y1 <- np1[2];
            r0 <- sqrt(x0^2+y0^2); r1 <- sqrt(x1^2+y1^2);
            r.base <- res$y * (r1-r0) + r0;
            a0 <- atan2(y0,x0) + endTabAngle/2;
            a1 <- atan2(y1,x1) - endTabAngle/2;
            if(a0 > 0){
                a0 <- a0 - 2*pi;
            }
            if(a1 > 0){
                a1 <- a1 - 2*pi;
            }
            flip <- TRUE;
        }
        ad <- (a1 - a0); ## work out the line orientation
        if(ad < 0){
            flip <- !flip;
        }
        a.base <- res$y * (a1-a0) + a0;
        ## flip order if point order is flipped
        ##if(flip){
        ##    a.scale <- -a.scale;
        ##}
        ## make adjustments
        ## note: adding tab means radius will be slightly off,
        ##   in addition to spiral vs circle error
        res$a <- a.base; res$r <- r.base + res$x;
        ## convert back to rectangular coordinates
        res$x <- res$r * cos(res$a);
        res$y <- res$r * sin(res$a);
        ## remove additional columns
        res$a <- NULL; res$r <- NULL;
    }
    ## add (NA,NA) point to make sure it won't be combined with the next line
    res <- rbind(res, c(NA,NA));
    ##plot(res, type="l", xlim=c(-5,5), ylim=c(-2,8))
    ##points(rbind(c(x0, y0), c(x1, y1)));
    invisible(res);
}

if(doDebug){
    if(!doDebug){
        plot(NA, xlim=c(-2,10), ylim=c(-6,6));
        points(puzzleLine(x0=0, y0=-5, x1=0, y1=5, ils=0.3), type="l");
        points(puzzleLine(x0=2, y0=-4, x1=2, y1=4), type="l");
        points(puzzleLine(x0=4, y0=-3, x1=4, y1=3), type="l");
        points(puzzleLine(x0=6, y0=-2, x1=6, y1=2), type="l");
        points(puzzleLine(x0=8, y0=-1, x1=8, y1=1), type="l");
    }
    if(!doDebug){
        ec <- 16;
        loops <- 4;
        sr <- .3;
        er <- .8;
        loopGap <- (er-sr) / loops;
        par(mar=c(4,4,4,4));
        plot(6*cos(seq(0,2*pi,length.out=100)),
             6*sin(seq(0,2*pi,length.out=100)), type="l",
             xlim=c(-1,1), ylim=c(-1,1), col="#00000020");
        points(.8*cos(seq(0,2*pi,length.out=100)),
               .8*sin(seq(0,2*pi,length.out=100)), type="l", col="#00000020");
        points(.5*cos(seq(0,2*pi,length.out=100)),
               .5*sin(seq(0,2*pi,length.out=100)), type="l", col="#00000020");
        points(.2*cos(seq(0,2*pi,length.out=100)),
               .2*sin(seq(0,2*pi,length.out=100)), type="l", col="#00000020");
        for(apos in 1:(ec*loops)){
            ang <- seq(0,2 * pi * loops, length.out=ec*loops+1)[apos];
            ad <- 2*pi / ec;
            pRad <- sr + ((er-sr)*(ang) / (2*pi*loops));
            pRad1 <- sr + ((er-sr)*(ang+ad) / (2*pi*loops));
            points(puzzleLine(x0=(pRad)*cos(ang),
                              y0=(pRad)*sin(ang),
                              x1=(pRad1)*cos(ang+ad),
                              y1=(pRad1)*sin(ang+ad),
                              curve=TRUE, ils=0.015),
                   type="l", lwd=2);
            if(apos <= (ec * (loops-1) + 1)){
                points(puzzleLine(x0=(pRad)*cos(ang),
                                  y0=(pRad)*sin(ang),
                                  x1=(pRad+loopGap)*cos(ang),
                                  y1=(pRad+loopGap)*sin(ang),
                                  curve=FALSE),
                       type="l", lwd=2);
            }
        }
    }

    png("~/out.png", width=960, height=480);
    layout(matrix(1:2,1));
    plot(NA,xlim=c(0,10), ylim=c(0,10),
         main=paste0("Interlocking with reflexive symmetry\n",
                     "but not self-interlocking"));
    points(puzzleLine(2,2,2,4), type="l", lwd=4);
    points(puzzleLine(2,4,4,4), type="l", lwd=4);
    points(puzzleLine(4,4,4,2), type="l", lwd=4);
    points(puzzleLine(4,2,2,2), type="l", lwd=4);
    points(puzzleLine(8,4,6,4), type="l", lwd=4);
    points(puzzleLine(8,2,8,4), type="l", lwd=4);
    points(puzzleLine(6,2,8,2), type="l", lwd=4);
    points(puzzleLine(6,4,6,2), type="l", lwd=4);
    points(puzzleLine(6,6,6,8), type="l", lwd=4);
    points(puzzleLine(6,8,8,8), type="l", lwd=4);
    points(puzzleLine(8,8,8,6), type="l", lwd=4);
    points(puzzleLine(8,6,6,6), type="l", lwd=4);
    points(puzzleLine(4,8,2,8), type="l", lwd=4);
    points(puzzleLine(4,6,4,8), type="l", lwd=4);
    points(puzzleLine(2,6,4,6), type="l", lwd=4);
    points(puzzleLine(2,8,2,6), type="l", lwd=4);
    plot(NA,xlim=c(0,10), ylim=c(0,10),
         main=paste0("Interlocks with itself\n",
                     "but no reflexive symmetry"));
    points(puzzleLine(2,8,2,2), type="l", lwd=4);
    points(puzzleLine(8,8,2,8), type="l", lwd=4);
    points(puzzleLine(8,8,8,2), type="l", lwd=4);
    points(puzzleLine(8,2,2,2), type="l", lwd=4);
    invisible(dev.off());

}

fileName <- "";
filePrefix <- "sequence";
rptSize <- -1;
type <- "png";
sizeX <- -1;
sizeY <- -1;
colType <- "cb";
pointFactor <- 1;
puzzleCount <- 0;
linesOnly <- FALSE;
compSeq <- FALSE;
revSeq <- FALSE;
useKey <- TRUE;
solid <- FALSE;
centreLabel <- FALSE;
minRad <- 0.3;
maxRad <- 1.0;
spiral <- FALSE;
hFlip <- FALSE;
plotTitle <- "";
plotCircle <- TRUE;
bgColour <- "white";

seqRange <- FALSE;

if(length(commandArgs(TRUE)) == 0){
      usage();
      quit(save = "no", status=0);
}

while(!is.na(commandArgs(TRUE)[argLoc])){
    arg <- commandArgs(TRUE)[argLoc];
    argLoc <- argLoc + 1;
    if(arg == "-help"){
      usage();
      quit(save = "no", status=0);
    } else if(arg == "-size"){
        arg <- unlist(strsplit(commandArgs(TRUE)[argLoc], "x"));
        argLoc <- argLoc + 1;
        sizeX <- as.numeric(arg[1]);
        sizeY <- as.numeric(arg[2]);
    } else if(arg == "-ps"){
        arg <- commandArgs(TRUE)[argLoc];
        pointFactor <- as.numeric(arg);
        argLoc <- argLoc + 1;
    } else if(arg == "-bg"){
        bgColour <- commandArgs(TRUE)[argLoc];
        argLoc <- argLoc + 1;
    } else if(arg == "-title"){
        arg <- commandArgs(TRUE)[argLoc];
        plotTitle <- arg;
        argLoc <- argLoc + 1;
    } else if(arg == "-prefix"){
        arg <- commandArgs(TRUE)[argLoc];
        filePrefix <- arg;
        argLoc <- argLoc + 1;
    } else if(arg == "-puzzle"){
        arg <- commandArgs(TRUE)[argLoc];
        puzzleCount <- as.numeric(arg);
        argLoc <- argLoc + 1;
    } else if(arg == "-nobases"){
        linesOnly <- TRUE;
    } else if(arg == "-min"){
        arg <- commandArgs(TRUE)[argLoc];
        minRad <- as.numeric(arg);
        argLoc <- argLoc + 1;
    } else if(arg == "-max"){
        arg <- commandArgs(TRUE)[argLoc];
        maxRad <- as.numeric(arg);
        argLoc <- argLoc + 1;
    } else if(arg == "-clabel"){
        centreLabel <- TRUE;
    } else if(arg == "-type"){
        arg <- commandArgs(TRUE)[argLoc];
        argLoc <- argLoc + 1;
        type <- arg;
    } else if(arg == "-rev"){
        revSeq <- TRUE;
    } else if(arg == "-comp"){
        compSeq <- TRUE;
    } else if(arg == "-solid"){
        solid <- TRUE;
    } else if(arg == "-spiral"){
        spiral <- TRUE;
    } else if(arg == "-hflip"){
        hFlip <- TRUE;
    } else if(arg == "-nokey"){
        useKey <- FALSE;
    } else if(arg == "-nocircle"){
        plotCircle <- FALSE;
    } else if(arg == "-col"){
        arg <- commandArgs(TRUE)[argLoc];
        argLoc <- argLoc + 1;
        colType <- arg;
    } else {
        if(grepl(":\\-?[0-9]+\\-[0-9]+$",arg)){
            prefix <- sub(":[^:]+$","",arg);
            suffix <- sub("^.*:","",arg);
            arg <- prefix;
            r1 <- as.numeric(sub("^(.*)\\-(.*)$", "\\1", suffix));
            r2 <- as.numeric(sub("^(.*)\\-(.*)$", "\\2", suffix));
            seqRange <- c(r1,r2);
        }
        if(file.exists(arg)){
            fileName <- arg;
        } else if ((rptSize == -1) && grepl("^[0-9]+$", arg)){
            rptSize <- as.numeric(arg);
        } else {
            cat("Error: Argument '", arg,
                "' is not understood by this program\n\n", sep="");
            usage();
            quit(save = "no", status=0);
        }
    }
}

if(puzzleCount > 1){
    centreLabel <- TRUE;
}

if(rptSize == -1){
    cat("Error: repeat unit size has not been specified\n\n");
    usage();
    quit(save = "no", status=0);
}

inLines <- readLines(fileName);

cat("Reading from file:",fileName,"...");
inName <- substring(inLines[1],2);
if(substring(inLines[1],1,1) == "@"){
    ## assume FASTQ files are one line per sequence (four lines per record)
    inLines <- inLines[c(-seq(3,length(inLines), by=4),
                         -seq(4,length(inLines), by=4))];
}
inSeq <- if(!compSeq){
             c(A=1, C=2, G=3, T=4, N=5, "-"=5,
               a=1, c=2, g=3, t=4, n=5)[unlist(strsplit(paste(inLines[-1],collapse=""),""))];
         } else {
             c(A=4, C=3, G=2, T=1, N=5, "-"=5,
               a=4, c=3, g=2, t=1, n=5)[unlist(strsplit(paste(inLines[-1],collapse=""),""))];
         }
cat(" done\n");

if(seqRange[1] != FALSE){
    if(seqRange[2] > length(inSeq)){
        inSeq <- c(inSeq, rep(5,seqRange[2] - length(inSeq)));
    }
    if(seqRange[1] < 0){
        inSeq <- c(rep(5,-seqRange[1]+1), inSeq);
        inSeq <- inSeq[(seqRange[1]:seqRange[2]) + -seqRange[1] + 1];
    } else {
        inSeq <- inSeq[seqRange[1]:seqRange[2]];
    }
}

if(revSeq){
    inSeq <- rev(inSeq);
}

if(type == "png"){
    if(sizeX == -1){
        sizeX <- 700;
    }
    if(sizeY == -1){
        sizeY <- 700;
    }
} else if((type == "pdf") || (type == "svg")){
    if(sizeX == -1){
        sizeX <- 12;
    }
    if(sizeY == -1){
        sizeY <- 12;
    }
}

colPal <-
    if(colType == "cb"){
        c("#006400","#0000FF","#FFD700","#FF6347","#FFFFFF");
    } else if(colType == "gg") {
        c("#F8766D","#7CAE00","#00BFC4","#C77CFF","#FFFFFF");
    } else {
        c("#006400","#0000FF","#FFD700","#8B0000","#FFFFFF");
    }

fileNameMatrix <- sprintf("%s_matrix.%s", filePrefix, type);

cat("Creating matrix plot...");
if(type == "png"){
    png(fileNameMatrix, width=sizeX, height=sizeY,
        pointsize=24 * sizeX/1000);
} else if(type == "pdf"){
    pdf(fileNameMatrix, width=sizeX, height=sizeY,
        pointsize=16 * sizeX/10);
} else if(type == "svg"){
    svgDim <- 7 * sizeX/10;
    svg(fileNameMatrix,
        width=svgDim, height=svgDim,
        pointsize=12 * sizeX/10);
}
lis <- length(inSeq);
numLines <- floor(lis/rptSize + 1);
inSeq <- c(inSeq,rep(5,rptSize));
subSeq <- inSeq[1:(numLines*rptSize)];
par(mar=c(0.5,6,1,0.5), mgp=c(4.5,1,0), bg=bgColour);
image(x=1:rptSize, y=1:numLines-1, matrix(subSeq,nrow=rptSize),
      main=sprintf("%s%s (%0.3f kb, %d bases / line)", sub(" .*$","",inName),
                   ifelse(seqRange[1] == FALSE,"",
                          paste0(": ",seqRange[1]," .. ",seqRange[2])),
                   lis/1000, rptSize), ylab="Base location",
      cex.main=0.8, xaxt="n", yaxt="n", useRaster=(type != "svg"),
      col=colPal);
axis(2, at=round(seq(0, numLines, length.out=20)),
     labels=round(seq(0, numLines, length.out=20)) * rptSize+1,
     las=2, cex.axis=1);
invisible(dev.off());
cat(" done [", fileNameMatrix ,"]\n");

numLoops <- (length(subSeq) / rptSize);
##startCount <- rptSize / 1.2;
startCount <- rptSize;
startRadius <- minRad;
endRadius <- maxRad;
##loopIncrement <- ((rptSize * 1.2) - (rptSize / 1.2)) / numLoops;

if(plotCircle){
    fileNameSpiral <- sprintf("%s_spiral.%s", filePrefix, type);
    cat("Creating spiral plot...");
    if(type == "png"){
        png(fileNameSpiral, width=max(sizeX,sizeY),
            height=max(sizeX,sizeY), pointsize=24 * max(sizeX,sizeY)/1000);
    } else if(type == "pdf"){
        pdf(fileNameSpiral, width=max(sizeX,sizeY),
            height=max(sizeX,sizeY),
            pointsize=16);
    } else if(type == "svg"){
        svg(fileNameSpiral, width=max(sizeX,sizeY) / 96,
            height=max(sizeX,sizeY) / 96,
            pointsize=16);
    }
    if(puzzleCount > 1){
        if(linesOnly){
            par(mar=c(0,0,0,0), xaxs="i", yaxs="i", bg="transparent");
        } else {
            par(mar=c(0,0,0,0), xaxs="i", yaxs="i", bg=bgColour);
        }
    } else {
        if(nchar(plotTitle) > 0){
            par(mar=c(2.5,2.5,2.5,2.5), bg=bgColour);
        } else {
            par(mar=c(0.5,1.5,2.5,1.5), bg=bgColour);
        }
    }
    if(hFlip){
        plot(NA, xlim=c(1,-1), ylim=c(-1,1), axes=FALSE, ann=FALSE);
    } else {
        plot(NA, xlim=c(-1,1), ylim=c(-1,1), axes=FALSE, ann=FALSE);
    }
    tSeqName <- sub("( ).*$","",inName);
    if(grepl(":\\-?[0-9]+\\-[0-9]+$",tSeqName)){
        prefix <- sub(":[^:]+$","",tSeqName);
        suffix <- sub("^.*:","",tSeqName);
        tSeqName <- prefix;
        r1 <- as.numeric(sub("^(.*)\\-(.*)$", "\\1", suffix));
        r2 <- as.numeric(sub("^(.*)\\-(.*)$", "\\2", suffix));
        seqRange <- c(r1,r2);
    }
    if(centreLabel && (nchar(tSeqName) > 15)){
        tSeqName <- paste0(substring(tSeqName, 1, 8),"...",
                           substring(tSeqName, nchar(tSeqName) - 4));
    }
    rSize <- sub("\\.$", "", sub("0+$", "", sprintf("%0.3f", lis/1000)));
    rLabel <-
        sprintf("%s%s\n(%s kb, %d bases / ring)", tSeqName,
                ifelse(seqRange[1] == FALSE,"",
                       paste0(": ",seqRange[1]," .. ",seqRange[2])),
                rSize, rptSize);
    if(!centreLabel){
        mtext(rLabel, side=3, cex=1, line=0);
        mtext(plotTitle, side=1, cex=2, line=0);
    }
    ## Pre-population plot variables
    ## integrate(2*pi*r,r=startRadius..endRadius)
    ## => pi((endRadius)²-(startRadius)²)
    dTot <- pi*(endRadius^2 - startRadius^2); ## total "distance" travelled
    theta <- seq(0, (numLoops+1+2/rptSize) * 2*pi,
                 length.out=(length(subSeq)+rptSize+3)); ## traversed angle
    deg <- (theta / (2*pi)) * 360;
    r <-
        if(puzzleCount > 1){
            seq(startRadius, endRadius,
                length.out=length(subSeq)+rptSize+2); ## path radius
        } else {
            seq(startRadius, endRadius,
                length.out=length(subSeq)+rptSize+2)^2; ## path radius
        }
    s <- pi * (r^2 - startRadius^2); ## traversed distance at each pos
    ds <- c(s[2],diff(s)); ## distance difference at each pos
    cr <- 2*pi*r / numLoops; ## circumference of one loop
    ss <- rev(seq_along(subSeq))+1;
    d <- rptSize;
    if(!linesOnly){
        polygon(x=c(rbind(
                    r[ss-1+0] * cos(theta[ss-1+0]),
                    r[ss+0+0] * cos(theta[ss+0+0]),
                    r[ss+0+d] * cos(theta[ss+0+d]),
                    r[ss-1+d] * cos(theta[ss-1+d]), NA)),
                y=c(rbind(
                    r[ss-1+0] * sin(theta[ss-1+0]),
                    r[ss+0+0] * sin(theta[ss+0+0]),
                    r[ss+0+d] * sin(theta[ss+0+d]),
                    r[ss-1+d] * sin(theta[ss-1+d]), NA)),
                ##lwd = 5 * sqrt(r)[ss], lend=(ifelse(numLoops>20,1,0)),
                ##border="#00000040",
                border = paste0(colPal[rev(subSeq)],ifelse(solid,"FF","A0")),
                col = paste0(colPal[rev(subSeq)],ifelse(solid,"FF","A0")),
                pch=15, cex=sqrt(r)[ss] * (13/log(numLoops)) * pointFactor);
        if(spiral && !linesOnly){
            sps <- rev(seq(0,length(subSeq)+d))+1;
            points(x=r[sps] * cos(theta[sps]), y=r[sps] * sin(theta[sps]),
                   type="l", col="white",
                   lwd = if(puzzleCount > 1){
                             ifelse(type=="png",30*sizeX/1000,30*sizeX/10);
                         } else {
                             3;
                         });
        }
    }
    if(puzzleCount > 1){
        ec <- puzzleCount;
        loops <- numLoops;
        sr <- startRadius;
        er <- endRadius;
        basesPerPiece <- length(ss) / (numLoops * puzzleCount);
        g <- basesPerPiece;
        pieceLoc <- 2;
        pieceCount <- 1;
        while(pieceLoc < (lis + (ec*basesPerPiece))){
            ## Piece
            if(pieceLoc < (lis + basesPerPiece - 1)){
                rScale <- (r[pieceLoc-1+d] - r[pieceLoc-1+0]) /
                    (r[1+d] - r[1]);
                if((pieceCount %% 2) == 0){ ## pieces have reflexive symmetry
                    points(puzzleLine(x0=r[pieceLoc-1+0] *
                                          cos(theta[pieceLoc-1+0]),
                                      y0=r[pieceLoc-1+0] *
                                          sin(theta[pieceLoc-1+0]),
                                      x1=r[pieceLoc-1+d] *
                                          cos(theta[pieceLoc-1+d]),
                                      y1=r[pieceLoc-1+d] *
                                          sin(theta[pieceLoc-1+d]),
                                      curve=FALSE, ils=0.01 * rScale),
                           type="l", lwd=ifelse(type=="png",2*sizeX/1000,2*sizeX/10),
                           col=ifelse(linesOnly, "red", "black"));
                } else {
                    points(puzzleLine(x1=r[pieceLoc-1+0] * cos(theta[pieceLoc-1+0]),
                                      y1=r[pieceLoc-1+0] * sin(theta[pieceLoc-1+0]),
                                      x0=r[pieceLoc-1+d] * cos(theta[pieceLoc-1+d]),
                                      y0=r[pieceLoc-1+d] * sin(theta[pieceLoc-1+d]),
                                      curve=FALSE, ils=0.01 * rScale),
                           type="l", lwd=ifelse(type=="png",2*sizeX/1000,2*sizeX/10),
                           col=ifelse(linesOnly, "red", "black"));
                }
            }
            pieceLoc <- pieceLoc + basesPerPiece;
            pieceCount <- pieceCount + 1;
        }
        pieceLoc <- 2;
        pieceCount <- 1;
        while(pieceLoc < (lis + (ec*basesPerPiece))){
            ## Spiral
            points(puzzleLine(x0=r[pieceLoc-1+0] * cos(theta[pieceLoc-1+0]),
                              y0=r[pieceLoc-1+0] * sin(theta[pieceLoc-1+0]),
                              x1=r[pieceLoc-1+g] * cos(theta[pieceLoc-1+g]),
                              y1=r[pieceLoc-1+g] * sin(theta[pieceLoc-1+g]),
                              curve=TRUE, ils=0.01),
                   type="l", lwd=ifelse(type=="png", 2*sizeX/1000, 2*sizeX/10),
                   col=ifelse(linesOnly, "red", "black"));
            pieceLoc <- pieceLoc + basesPerPiece;
            pieceCount <- pieceCount + 1;
        }
        ## outer border
        polygon(data.frame(x=c(-1,1,1,-1), y=c(-1,-1,1,1)));
    }
    if(centreLabel && !linesOnly){
        text(0, 0, pos=3, sub(", ", ",\n", sub(":",":\n", rLabel)), cex=1,
             offset=-0.5);
    }
    if(useKey){
        if(!linesOnly){
            if(centreLabel){
                legend("bottom", legend=c("A","C","G","T"), inset=0.4,
                       fill=colPal[1:4], cex=0.71, ncol=2);
            } else {
                legend("bottomright", legend=c("A","C","G","T"), inset=0.05,
                       fill=colPal[1:4], cex=0.71, ncol=2);
            }
        }
    }
    invisible(dev.off());
    cat(" done [", fileNameSpiral ,"]\n");
}
