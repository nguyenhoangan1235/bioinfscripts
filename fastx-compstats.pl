#!/usr/bin/env perl
use warnings;
use strict;

## fastx-compstats.pl -- get read-level statistics on base / quality composition in a fastq/fasta file

use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

my $quiet = 0;
my $mode = "ACGT";
my $qBase = 33; ## Phred quality base (OldIllumina: 64; EverythingElse: 33)

GetOptions("quiet!" => \$quiet, "mode=s" => \$mode,
           "base=i" => \$qBase) or
  die("Error in command line arguments");

$mode = uc($mode);

# unknown commands are treated as file names
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@files, $arg);
  }
}
@ARGV = @files;

# use stdin if no files supplied
if(!@ARGV){
  @ARGV = '-' unless (-t STDIN);
}

sub calcStats{
  my ($seq, $qual, $name) = @_;
  if(!$name){
    return("name,A,C,G,T,S,W,Y,R,M,K,B,D,H,V,qualMean,qualMed,len");
  }
  my $ca = ($seq =~ tr/Aa//);
  my $cc = ($seq =~ tr/Cc//);
  my $cg = ($seq =~ tr/Gg//);
  my $ct = ($seq =~ tr/Tt//);
  my $tot = $ca + $cc + $cg + $ct;
  my $pA = sprintf("%0.4f", $ca / $tot);
  my $pC = sprintf("%0.4f", $cc / $tot);
  my $pG = sprintf("%0.4f", $cg / $tot);
  my $pT = sprintf("%0.4f", $ct / $tot);
  $name =~ s/ .*$//;
  my $qualTotal = 0;
  my $qualCount = 0;
  my $qualMean = 0;
  my $qualMed = 0;
  if($qual ne ""){
    my @qspl = split("", $qual);
    my @quals = map { 10 ** (-(ord($_) - $qBase) / 10) } @qspl;
    for(@quals){
      $qualTotal += $_;
      $qualCount ++;
    }
    my @qualsSorted = sort {$a <=> $b} @quals;
    $qualMean = sprintf("%0.4f", (1 - ($qualTotal / $qualCount)));
    my $mid = int(scalar(@qualsSorted) / 2);
    if(scalar(@qualsSorted) % 2){
      $qualMed = sprintf("%0.4f",
                         1 - $qualsSorted[$mid]);
    } else {
      $qualMed = sprintf("%0.4f",
                         1 - ($qualsSorted[$mid-1] + $qualsSorted[$mid])/2);
    }
  }
  return(join(",",
              $name,
              $pA, $pC, $pG, $pT,
              $pC + $pG, $pA + $pT,
              $pC + $pT, $pA + $pG,
              $pA + $pC, $pG + $pT,
              $pC + $pG + $pT,
              $pA + $pG + $pT,
              $pA + $pC + $pT,
              $pA + $pC + $pG,
              $qualMean, $qualMed, length($seq)));
}

my %hpCounts = ();

my $baseCount = 0;
my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";

foreach my $file (@ARGV) {
  # This little gunzip dance makes sure the script can handle both
  # gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($file, MultiStream => 1)
    or die "gunzip failed: $GunzipError\n";
  while(<$z>){
    s/\s+$//; # remove ending whitespace
    if (!$inQual) {
      if (/^(>|@)((.+?)( .*?\s*)?)$/) {
        my $newSeqID = $2;
        my $newShortID = $3;
        $baseCount += length($seq);
        print(calcStats($seq, $qual, $seqID)."\n");
        $seq = "";
        $qual = "";
        $seqID = $newSeqID;
      } elsif (/^\+(.*)$/) {
        $inQual = 1;            # true
        $qualID = $1;
      } else {
        $seq .= uc($_);
      }
    } else {
      $qual .= $_;
      if (length($qual) >= length($seq)) {
        $inQual = 0;            # false
      }
    }
  }
  close($z);
}

$baseCount += length($seq);
print(calcStats($seq, $qual, $seqID)."\n");

printf(STDERR "Total sequence length: %d\n", $baseCount) unless $quiet;
