#!/usr/bin/env python3

"""Script to find ideal primers covering a target sequence; checking
for melt, GC clamp, tertiary structures, and kmer duplication

Note: this uses the primer3 python bindings; install via

 'pip install primer3-py'

or your alternative preferred method
"""

from primer3 import calcTm, calcHomodimer, calcHairpin # primer Tm calculations
import re # excluding homopolymer stretches
import fileinput # for processing from stdin
import sys # for error messages
import random # for randomising primer start locations
import getopt # for command-line argument parsing
import copy # for backtracking
from collections import defaultdict # for dicts of dicts
from timeit import default_timer as timer # for timing progress

rcTransTab = str.maketrans('ACGTUYRSWMKDVHBXN-acgtuyrswmkdvhbxn',
                           'TGCAARYSWKMHBDVXN-tgcaaryswkmhbdvxn')

primersAdded = dict({1: dict(), 2: dict()})
excludeKmers = set()

def rc(seq):
  return ((seq[::-1]).translate(rcTransTab))

def primer3_check(ps, minLength=23, rcPrimer=False):
  if(rcPrimer):
    ps = rc(ps)
  tooLong = False
  ## check for GC clamp in last 4 bases
  if(not(("G" in ps[-4:]) or ("C" in ps[-4:]))):
    return ""
  ## exclude sequences with N (e.g. concatenated viromes)
  if("N" in ps):
    return ""
  ## check/exclude homopolymer stretches of >=4
  if(re.search(r'([ACGT])\1{3,}', ps) is not None):
    return ""
  ## see primalscheme/settings.py
  ## [github.com/aresti/primalscheme/blob/master/primalscheme/settings.py]
  ## Tm too low (needs to be longer)
  if(calcTm(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6) < 60):
    return ""
  ## exclude kmers from pre-generated exclusion set
  if(len(excludeKmers) > 0):
    for pi in range(0, len(ps) - kmerCheckSize + 1):
      kmer = ps[pi:(pi+kmerCheckSize)]
      if(kmer in excludeKmers):
        return ""
  ## Tm too high (needs to be shorter)
  if(calcTm(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6) > 63):
    tooLong = True
    #sys.stderr.write("Tm too high -> ")
    ps = ps[1:]
    tm = calcTm(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6)
    while((len(ps) >= minLength) and (tm > 62)):
      ## Make shorter, but preserve end
      ps = ps[1:]
      tm = calcTm(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6)
    if((tm > 63) or (tm < 60)):
      #sys.stderr.write("Tm out of range\n")
      return ""
    if(len(ps) < minLength):
      #sys.stderr.write("Length too short\n")
      return ""
    #sys.stderr.write("Tm restored to %0.1f (%d bp)" % (tm, len(ps)))
  ## check homodimers and hairpins
  if(calcHomodimer(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6).tm > 47):
    #sys.stderr.write("; homodimer fail\n")
    return ""
  if(calcHairpin(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6).tm > 47):
    #sys.stderr.write("; hairpin fail\n")
    return ""
  if(calcHomodimer(rc(ps), mv_conc=50, dv_conc=1.5, dntp_conc=0.6).tm > 47):
    #sys.stderr.write("; homodimer (RC) fail\n")
    return ""
  if(calcHairpin(rc(ps), mv_conc=50, dv_conc=1.5, dntp_conc=0.6).tm > 47):
    #sys.stderr.write("; hairpin (RC) fail\n")
    return ""
  if(tooLong):
    pass
    #sys.stderr.write("; Success!\n")
  return ps

def processSeq(seqID, shortID, seq, qual, containsFQ):
  return(dict({'seqID': seqID, 'shortID': shortID,
               'seq': seq, 'qual': qual}))

def nextFastx():
  inQual = False
  seqID = ""
  shortID = ""
  qualID = ""
  seq = ""
  qual = ""
  containsFQ = False
  for line in fileinput.input():
    line = line.rstrip()
    if(not inQual):
      if((line[0] == '>') or (line[0] == '@')):
        m = re.search(r'^(>|@)((.+?)( .*?\s*)?)$', line)
        newSeqID = m.group(2)
        newShortID = m.group(3)
        if(len(seqID) > 0):
          yield(processSeq(seqID, shortID, seq, qual, containsFQ))
        seqID = newSeqID
        shortID = newShortID
        seq = ""
        qual = ""
      elif(line[0] == '+'):
        m = re.search(r'\+(.*)', line)
        inQual = True
        containsFQ = True
        qualID = m.group(1)
      else:
        if(('@' in line) or ('>' in line) or ('\0' in line)):
          sys.stderr.write(("Warning: corrupt sequence found at %s " +
                            "[header or NUL in sequence string]\n") % (shortID))
          seqID = ""
          shortID = ""
          qualID = ""
          seq = ""
          qual = ""
          continue
        else:
          seq += line
    else:
      qual += line
      if(len(qual) > (len(seq) + 2)):
        sys.stderr.write(("Warning: corrupt sequence found at %s " +
                          "[quality string too long]\n") % (shortID))
        seqID = ""
        shortID = ""
        qualID = ""
        seq = ""
        qual = ""
        inQual = False
        continue
      elif(len(qual) >= len(seq)):
        inQual = False
  if(len(seqID) > 0):
    yield(processSeq(seqID, shortID, seq, qual, containsFQ))

def getMaxDelta(potentialPrimers, targetPoss, poolSet=3, priorDelta=0, verbosity=0):
    poolFirstFwd = False
    worstPrimer = ""
    worstPos = -1
    worstDelta = 0
    worstSeq = ""
    worstDictLookup = ()
    for epi in sorted(targetPoss):
      if(verbosity > 2):
        sys.stderr.write("  Target Site: %d; %d [F] / %d [RC] primers\n" %
                         (epi, len(potentialPrimers["F"][epi]),
                          len(potentialPrimers["RC"][epi])))
      smallestDelta = gapLength
      smallestPos = -1
      smallestSeq = ""
      ## TODO: work out why forward/RC is wrong in split-pool output
      if(poolSet == 1):
        dirn = "F" if (reverseComp != poolFirstFwd) else "RC"
        psgn = -1 if poolFirstFwd else 1
        for pri in potentialPrimers[dirn][epi]:
          delta = (pri - epi) * psgn
          if((delta >= 0) and (delta < smallestDelta)):
            smallestDelta = delta
            smallestPos = pri
            smallestSeq = potentialPrimers[dirn][epi][pri]
            smallestDictLookup = (dirn, epi, pri)
      if(poolSet == 2):
        dirn = "RC" if (reverseComp != poolFirstFwd) else "F"
        psgn = 1 if poolFirstFwd else -1
        for pri in potentialPrimers[dirn][epi]:
          delta = (pri - epi) * psgn
          if((delta >= 0) and (delta < smallestDelta)):
            smallestDelta = delta
            smallestPos = pri
            smallestSeq = potentialPrimers[dirn][epi][pri]
            smallestDictLookup = (dirn, epi, pri)
      if(poolSet == 3): # not pooled, so combine neg and pos deltas
        dirn = "RC" if reverseComp else "F"
        for pri in potentialPrimers[dirn][epi]:
          delta  = (pri - epi)
          if(abs(delta) < abs(smallestDelta)):
            smallestDelta = delta
            smallestPos = pri
            smallestSeq = potentialPrimers[dirn][epi][pri]
            smallestDictLookup = (dirn, epi, pri)
      if((smallestDelta < gapLength) and (abs(smallestDelta) > abs(worstDelta))):
        worstDelta = smallestDelta
        worstPos = smallestPos
        worstSeq = smallestSeq
        worstDictLookup = smallestDictLookup
      poolFirstFwd = not poolFirstFwd
    if(verbosity > 1):
      if(worstPos == -1):
        sys.stderr.write("No suitable primer could be found\n")
      else:
        sys.stderr.write("Largest minimum delta: %d; %s at %d\n" % (worstDelta, worstSeq, worstPos))
    return(dict({"delta": worstDelta, "pos": worstPos, "seq": worstSeq,
                 "dictLookup": worstDictLookup}))

def filterKmers(potentialPrimers, targetPoss, seq, kmerCheckSize, poolSet = 3):
  kmersToAdd = set()
  poolFirstFwd = False
  possToDelete = list()
  primersToDelete = list()
  for pi in range(0, len(seq) - kmerCheckSize + 1):
    kmer = seq[pi:(pi+kmerCheckSize)]
    kmersToAdd.add(rc(kmer)) # should just be able to check for RC matches
    # kmersToAdd.add(kmer) ## these shouldn't create primer dimers
  for dirn in potentialPrimers:
    for epi in targetPoss:
      if(not epi in potentialPrimers[dirn]):
        poolFirstFwd = not poolFirstFwd
        continue
      for pri in potentialPrimers[dirn][epi]:
        primer = potentialPrimers[dirn][epi][pri]
        if(primer in (seq, rc(seq))):
          if(poolSet in (1, 2)):
            possToDelete.append((dirn, epi))
          else:
            possToDelete.append(("F", epi))
            possToDelete.append(("RC", epi))
          break
        for pi in range(0, len(primer) - kmerCheckSize + 1):
          kmer = primer[pi:(pi+kmerCheckSize)]
          if(kmer in kmersToAdd):
            primersToDelete.append((dirn, epi, pri))
      poolFirstFwd = not poolFirstFwd
  for delPos in possToDelete:
    if(delPos[1] in potentialPrimers[delPos[0]]): # palindromic sequences could be deleted twice
      del potentialPrimers[delPos[0]][delPos[1]]
  for delPos in primersToDelete:
    if((delPos[1] in potentialPrimers[delPos[0]]) and
       (delPos[2] in potentialPrimers[delPos[0]][delPos[1]])):
      del potentialPrimers[delPos[0]][delPos[1]][delPos[2]]

def usage():
  sys.stderr.write("primeCoverage.py - finds ideal primers covering a target sequence\n\n")
  sys.stderr.write("usage: primeCoverage.py [options] sequence.fa\n\n")
  sys.stderr.write("additional options: \n")
  sys.stderr.write("  -h [--help]           - usage information\n")
  sys.stderr.write("  -x [--max=X]          - maximum primer length (default 50)\n")
  sys.stderr.write("  -n [--min=X]          - minimum primer length (default 23)\n")
  sys.stderr.write("  -r [--reverse-comp]   - reverse-complement primers\n")
  sys.stderr.write("  -s [--stop-at=X]      - choose the best set after X loops (default 100)\n")
  sys.stderr.write("  -e [--exclude]        - pre-exclude F and RC kmers from sequence X\n")
  sys.stderr.write("  -a [--ampliconSize=X] - minimum amplicon length (default 1300)\n")
  sys.stderr.write("  -k [--kmerSize=X]     - kmer filter size (default 6)\n")
  sys.stderr.write("  --no-pools            - generate probe set (forward only)\n")
  sys.stderr.write("  -f [--forward-clamp]  - apply clamp search to template direction\n")
  sys.stderr.write("  -v [--verbose]        - increase message verbosity\n")
  sys.stderr.write("\n")

## Option parameters
maxLength = 50       # maximum primer length
minLength = 23       # minimum primer length
gapLength = 1300     # ideal amplicon length
kmerCheckSize = 6    # length for sub-sequence reverse-complementarity check
stopAt = 100         # fudge factor
makePools = True     # should a split-pool of non-overlapping primer sets be made?
combineKmers = False # should pool kmer lookup hashes be combined?
verbosity = 0        # include verbose output
reverseComp = False  # reverse-complement primers? (needed for some protocols)
forwardClamp = False # should the clamp apply to the original sequence?
excludeSeqs = list() # list of sequences to pre-filter candidate primers

import sys

argv = sys.argv[1:]

opts, args = getopt.getopt(argv, "x:n:a:k:s:e:vhrf",
                           ["max=", "min=", "ampliconSize=", "kmer-size=",
                            "kmerSize=", "gaplength=", "gap-length=",
                            "exclude=", "reverse-comp",
                            "makePools", "no-pool", "no-pools",
                            "no-group", "no-groups",
                            "help", "forward-clamp", "stop-at"])

if(len(argv) == 0):
  sys.stderr.write("Error: no command-line arguments specified\n\n")
  usage()
  exit(0)

for o, a in opts:
  if(o in("-h", "--help")):
    usage()
    exit(0)
  if(o in("-x", "--max")):
    maxLength = int(a)
  elif(o in ("-n", "--min")):
    minLength = int(a)
  elif(o in ("-s", "--stop-at")):
    stopAt = int(a)
  elif(o in ("-r", "--reverse-comp")):
    reverseComp = True
  elif(o in ("-a", "--ampliconSize", "--gaplength", "--gap-length")):
    gapLength = int(a)
  elif(o in ("-e", "--exclude")):
    excludeSeqs.append(a)
  elif(o in ("-k", "--kmerSize", "--kmer-size")):
    kmerCheckSize = int(a)
  elif(o in ("--makePools")):
    makePools = True
  elif(o in ("--no-pool", "--no-pools", "--no-group", "--no-groups")):
    makePools = False
  elif(o in ("-f", "--forward-clamp")):
    forwardClamp = True
  elif(o in ("-v", "--verbose")):
    verbosity += 1

sys.argv[1:] = args

if(forwardClamp and reverseComp):
  sys.stderr.write("Warning: Forward clamp and reverse-complement are not compatible; disabling reverse-complement\n\n")
  reverseComp = False

for seq in excludeSeqs:
  for pi in range(0, len(seq) - kmerCheckSize + 1):
    kmer = seq[pi:(pi+kmerCheckSize)]
    if(not ('N' in kmer)):
      excludeKmers.add(rc(kmer))
      excludeKmers.add(kmer)

lastDelta = 0
groupIndicator = 3 if makePools else 1
blockShort = False
lastPrimerSeq = ""
lastPrimerPos = 0

for seqRecord in nextFastx():
  if(len(seqRecord["seq"]) > 0):
    if(verbosity > 0):
      sys.stderr.write("Sequence: %s\n" % (seqRecord["shortID"]))
    seq = seqRecord["seq"]
    if(verbosity > 0):
      sys.stderr.write("Finding best end primer\n")
    for si in range(len(seq) - minLength, 0, -1):
      testPrimer = primer3_check(seq[si:(si+maxLength)],
                                 minLength, rcPrimer=(makePools != reverseComp))
      # [Note: at this stage, it's not established if the end primer is in group 1 or 2]
      if(len(testPrimer) >= minLength):
        endGap = len(seq) - (si + len(testPrimer))
        if(verbosity > 0):
          sys.stderr.write("[RC] Position: %d; Primer: %s (end gap: %d)\n" %
                          (si, testPrimer, endGap))
        if(verbosity > 1):
          sys.stderr.write("RC sequence at this location: %s\n" %
                           (rc(seq[si:(si + len(testPrimer))])))
        lastPrimerSeq = testPrimer
        lastPrimerPos = si
        break
    if(verbosity > 0):
      sys.stderr.write("Finding best start primer\n")
    for si in range(len(seq) - maxLength + 1):
      group = 1
      doRC = reverseComp
      testPrimer = primer3_check(seq[si:(si+maxLength)],
                                 minLength, rcPrimer=doRC)
      if(len(testPrimer) >= minLength):
        endGap = len(seq) - (si + len(testPrimer))
        if(doRC):
          if(verbosity > 0):
            sys.stderr.write("[RC] Position: %d; Group: %d; Primer: %s (start gap: %d)\n" %
                             (si, group, testPrimer, si))
          primersAdded[group][si] = testPrimer
          if(verbosity > 1):
            sys.stderr.write("RC sequence at this location: %s\n" %
                             (rc(seq[si:(si + len(testPrimer))])))
        else:
          endPos = si + maxLength
          sp = endPos - len(testPrimer)
          if(verbosity > 0):
            sys.stderr.write("[ F] Position: %d; Group: %d; Primer: %s (start gap: %d)\n" %
                             (sp, group, testPrimer, sp))
          primersAdded[group][sp] = testPrimer
          if(verbosity > 1):
            sys.stderr.write("F sequence at this location: %s\n" %
                             (seq[sp:(sp + len(testPrimer))]))
            sys.stderr.write("Blocking positions from %d to %d\n" %
                             (0,
                              si + 2 * len(testPrimer) - kmerCheckSize + 1))
        firstPrimerSeq = testPrimer
        firstPrimerPos = si
        break
    searchStart = firstPrimerPos + 2 * len(firstPrimerSeq) - kmerCheckSize + 1
    searchEnd = lastPrimerPos - len(lastPrimerSeq) + kmerCheckSize - 1
    numAmplicons = int((searchEnd - searchStart) / gapLength + 0.5)
    gapLength = int((searchEnd - searchStart) / numAmplicons + 0.5)
    searchEnd = searchStart + numAmplicons * gapLength
    if(verbosity > 0):
      sys.stderr.write("Number of amplicons: %d; adjusted ideal gap length: %d\n" %
                       (numAmplicons, gapLength))
    if(verbosity > 0):
      sys.stderr.write("Finding other potential primers (ignoring kmer clashes)\n")
    ## Can now work out the group of the last primer
    if(makePools):
      primersAdded[(numAmplicons + 1) % 2 + 1][lastPrimerPos] = lastPrimerSeq
    else:
      primersAdded[1][lastPrimerPos] = lastPrimerSeq
    poss = list(range(searchStart, searchEnd))
    refPoss = set()
    potentialPrimers = dict({"F" : defaultdict(dict),
                             "RC" : defaultdict(dict)})
    foundPrimers = dict({"F": 0, "RC": 0})
    possDone = 0
    lastPctDone = 0
    foundInRange = False
    if(verbosity > 0):
      startTime = timer()
      sys.stderr.write("|--------10--------20--------30--------40--------50" +
                       "--------60--------70--------80--------90-------100|\n")
      sys.stderr.write("|")
      sys.stderr.flush()
    for si in poss:
      for dirn in ("F", "RC"):
        testPrimer = primer3_check(seq[si:(si+maxLength)],
                                   minLength, rcPrimer=(dirn == "RC"))
        if(len(testPrimer) >= minLength):
          refPos = (int((si - searchStart) / gapLength + 0.5) * gapLength) + searchStart
          if((si - searchStart) < gapLength):
            refPos = (searchStart + gapLength)
          if(refPos >= (searchEnd - gapLength / 2)):
            refPos = (searchEnd - gapLength)
          refPoss.add(refPos)
          delta = si - refPos
          foundInRange = True
          foundPrimers[dirn] += 1
          potentialPrimers[dirn][refPos][si] = testPrimer
      possDone += 1
      while(int(possDone * 100 / len(poss)) > lastPctDone):
        lastPctDone += 1
        if(verbosity > 0):
          if(foundInRange):
            sys.stderr.write("*")
          else:
            sys.stderr.write(".")
          foundInRange = False
          sys.stderr.flush()
    while(100 > lastPctDone):
      lastPctDone += 1
      if(verbosity > 0):
        if(foundInRange):
          sys.stderr.write("*")
        else:
          sys.stderr.write(".")
        foundInRange = False
    if(verbosity > 0):
      sys.stderr.write("|\n")
    if(verbosity > 0):
      sys.stderr.write("Found %d F and %d RC potential primer sites in %0.2f s\n" %
                       (foundPrimers["F"], foundPrimers["RC"], (timer() - startTime)))
    poolCount = 2 if makePools else 1
    # filter out forward and reverse primers from target pool
    filterKmers(potentialPrimers, refPoss, firstPrimerSeq, kmerCheckSize, poolSet = 1)
    filterKmers(potentialPrimers, refPoss, lastPrimerSeq, kmerCheckSize,
                poolSet = 1 if (not makePools) else ((numAmplicons + 1) % 2 + 1))
    origPrimerStore = copy.deepcopy(potentialPrimers)
    for pool in range(1, poolCount + 1):
      poolSet = pool if makePools else 3
      primersToFind = numAmplicons - 1
      if(verbosity > 0):
        startTime = timer()
        sys.stderr.write("Pool %d: trying to find %d more primer(s), considering kmer clashes\n" %
                         (pool, primersToFind))
      primerStore = copy.deepcopy(potentialPrimers)
      candidatePrimers = dict()
      bestCandidates = dict()
      bestFirstDelta = bestDelta = gapLength
      firstPrimer = md = getMaxDelta(potentialPrimers, targetPoss=refPoss,
                                     poolSet=poolSet, priorDelta=0, verbosity=verbosity)
      if(md["pos"] != -1):
        candidatePrimers[md["pos"]] = md["seq"]
      highestDelta = firstDelta = abs(md["delta"])
      loopsRun = 0
      loopsAborted = 0
      firstRun = True
      while((firstRun or (highestDelta > firstDelta)) and ((loopsRun < stopAt) and (loopsAborted < stopAt))):
        firstDelta = md["delta"]
        if(verbosity > 1):
          sys.stderr.write("Initial delta: %d for %s at %d\n" %
                           (md["delta"], md["seq"], md["pos"]))
        primersToFind = numAmplicons - 2
        while(primersToFind > 0):
          filterKmers(potentialPrimers, refPoss, md["seq"], kmerCheckSize, poolSet = pool)
          md = getMaxDelta(potentialPrimers, targetPoss=refPoss,
                           poolSet=poolSet, priorDelta=0, verbosity=verbosity)
          if(md["pos"] != -1):
            candidatePrimers[md["pos"]] = md["seq"]
            if(abs(md["delta"]) > highestDelta):
              highestDelta = abs(md["delta"])
          else:
            loopsAborted += 1
            if(verbosity > 1):
              sys.stderr.write("Amplicon missed...\n")
            elif(verbosity > 0):
              sys.stderr.write("X")
              sys.stderr.flush()
            highestDelta = gapLength
            break
          primersToFind -= 1
        firstRun = False
        if(highestDelta > firstDelta):
          if(verbosity > 1):
            sys.stderr.write("Higher delta found (%d) after kmer exclusion; trying again...\n" %
                             highestDelta)
          elif((verbosity > 0) and (md["pos"] != -1)):
            sys.stderr.write(".")
            sys.stderr.flush()
          if((primersToFind == 0) and (bestDelta > highestDelta)):
            bestDelta = highestDelta
            bestFirstDelta = firstDelta
            bestCandidates = copy.deepcopy(candidatePrimers)
          candidatePrimers = dict()
          potentialPrimers = copy.deepcopy(primerStore)
          fp = firstPrimer["dictLookup"]
          if(len(fp) > 0):
            del potentialPrimers[fp[0]][fp[1]][fp[2]]
          primerStore = copy.deepcopy(potentialPrimers)
          firstPrimer = md = getMaxDelta(potentialPrimers, targetPoss=refPoss,
                                         poolSet=poolSet, priorDelta=0, verbosity=verbosity)
          if(md["pos"] != -1):
            candidatePrimers[md["pos"]] = md["seq"]
            highestDelta = firstDelta = abs(md["delta"])
          firstRun = True
        loopsRun += 1
      if(verbosity > 0):
        sys.stderr.write(" done in %0.2f s\n" % (timer() - startTime))
        if(loopsAborted >= stopAt):
          sys.stderr.write("Unable to continue - chosen primers are too similar. Try a longer tile length or longer kmer length.\n\n")
          usage()
          exit(1)
        if(loopsRun >= stopAt):
          sys.stderr.write("Reached loop stop limit; choosing best prior solution\n")
      if(bestDelta < highestDelta):
        if(verbosity > 0):
          sys.stderr.write("A better max delta (than %d) was produced from a prior selection; choosing that instead\n" %
                           (highestDelta))
        candidatePrimers = copy.deepcopy(bestCandidates)
        highestDelta = bestDelta
        firstDelta = bestFirstDelta
      if(verbosity > 0):
        sys.stderr.write(("Found a candidate set of %d primers: " +
                          "first delta = %d; highest delta = %d\n") %
                         (len(candidatePrimers), firstDelta, highestDelta))
      primersAdded[pool].update(candidatePrimers)
      primerStore = copy.deepcopy(origPrimerStore)
      potentialPrimers = copy.deepcopy(origPrimerStore)
  nextDir = dict({"F": "RC", "RC": "F"})
  for group in sorted(primersAdded):
    lastPos = 0
    primers = primersAdded[group]
    ampliconCount = 1
    direction = "F" if (not reverseComp) else "RC"
    for pos in sorted(primers):
      side = "Left" if ((direction == "F") != reverseComp) else "Right"
      if(not makePools):
        side = "Probe"
      primer = primers[pos]
      if(forwardClamp):
        primer = rc(primer)
      tm = calcTm(primer, mv_conc=50, dv_conc=1.5, dntp_conc=0.6)
      gcCount = 0
      for c in primer:
        if (c in ('G', 'C', 'g', 'c', 'S', 's')):
          gcCount += 1
      gcPct = (gcCount * 100) / len(primer)
      sys.stdout.write(">%s_Pool%d_%02d_%s_%s len=%d pos=%d gcPct=%d tm=%0.1f%s%s\n%s\n" %
                       (seqRecord["shortID"], group, ampliconCount, side,
                        direction if (not forwardClamp) else nextDir[direction],
                        len(primer), pos, gcPct, tm,
                        "" if (lastPos == 0) else " fragLen=%d" % (pos - lastPos),
                        "" if (makePools) else " gapLen=%d" % (pos - lastPos),
                        primer))
      if((not makePools) or (lastPos == 0)):
        lastPos = pos
      else:
        lastPos = 0
      if(makePools and (direction == ("F" if (not reverseComp) else "RC"))):
        direction = nextDir[direction]
      else:
        if(makePools):
          direction = nextDir[direction]
        ampliconCount += 1
