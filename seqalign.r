#!/usr/bin/Rscript
library(Biostrings);

dateStr <- format(Sys.Date(), "%Y-%b-%d-lambda");

setwd("/home/gringer/bioinf/MIMR-2014-Aug-01-GBIS/nanopore");
dataDir <- "data/lambda_2014-Aug-07";
qrys <- readDNAStringSet(sprintf("%s/fastq/processed_all.fasta",dataDir));
## mouse mitochondria
##ref <- readDNAStringSet("data/reference/Mus_musculus.GRCm38.75.dna.chromosome.MT.fa");
## lambda
ref <- readDNAStringSet("data/reference/bphagelambda.fasta");
qry.names <- names(qrys);

counts.channel <- table(sub("_read.*$","",
                            sub("^(2D|compl|templ)_","",qry.names)));
names(counts.channel) <- sub("channel_","",sub("^(2D|compl|templ)_","",
                                               names(counts.channel)));
barplot(counts.channel, xlab = "Channel", ylab = "Counts");

getAlign <- function(a){
    mat <- nucleotideSubstitutionMatrix(match = 1, mismatch = -1,
                                        baseOnly = TRUE);
    aln <- pairwiseAlignment(ref, a, type = "overlap",
                             substitutionMatrix = mat,
                             gapOpening = 0, gapExtension = 1);
    return(aln);
}

## could generate a likelihood statistic based on alignment to other
## non-mtDNA sequences (e.g. e. coli, hela chr4)

# generate alignments
alns <- list();
for(qryID in 1:length(qrys)){
    aln <- getAlign(qrys[qryID]);
    aln.name <- names(qrys[qryID]);
    cat(aln.name, score(aln), (score(aln) / nchar(qrys[qryID])),
        fill = TRUE);
    alns[qryID] <- aln;
}

scores.raw <-
    sapply(1:length(qrys),
           function(x){score(alns[[x]])});
lengths <- sapply(qrys,nchar);

read.names <- names(lengths);
channel.df <- data.frame(ch = as.numeric(sub("channel_","",sub("_read.*$","",
                             sub("^(2D|compl|templ)_","",read.names)))),
                         read = as.numeric(sub("^.*?read_","",sub(" .*$","",
                             sub("^(2D|compl|templ)_","",read.names)))),
                         rc = grepl("^compl_",read.names), length = lengths);
channel.df$group <- floor((channel.df$ch-1) / 8)+1;
channel.df$cell <- ((channel.df$ch-1) %% 8) + 1;
channel.df$col <- floor((channel.df$cell - 1) / 4) + ((channel.df$group-1) %% 16) * 2 + 1;
channel.df$row <- ((channel.df$cell - 1) %% 4) + floor((channel.df$group-1) / 16) * 4 + 1;

channel.mcounts <- xtabs( ~ group + cell, data = channel.df);

seqsource <- rep(1,length(read.names));
seqsource[grep("^templ",read.names)] <- 1;
seqsource[grep("^compl",read.names)] <- 2;
seqsource[grep("^2D",read.names)] <- 3;

scores.df <- data.frame(length = lengths, raw = scores.raw,
                        adj = scores.raw / lengths,
                        cpl = grepl("^rc_",names(qrys)),
                        run = c(3,5)[grepl("mtDNA_5",names(qrys))+1]);

valToSci <- function(val, unit = ""){
    sci.prefixes <- c("","k","M","G");
    units <- rep(paste(sci.prefixes,unit,sep=""), each=3);
    logRegion <- floor(log10(val))+1;
    conv.units <- units[logRegion];
    conv.div <- 10^rep(0:3 * 3, each = 3)[logRegion];
    conv.val <- val / conv.div;
    conv.val[val == 0] <- 0;
    conv.units[val == 0] <- unit;
    return(sprintf("%s %s",conv.val,conv.units));
}

sequence.hist <- function(lengths){
    par(mgp = c(4,1,0), mar = c(6,6,1,2));
    lhist <- hist(lengths, plot = FALSE);
    seqd.bases <- tapply(lengths,cut(lengths,breaks = lhist$breaks),sum);
    barPos <- barplot(seqd.bases, log = "x", las = 1, axes = FALSE, col = "steelblue",
                      horiz = TRUE, names.arg = FALSE, ylab = "Fragment size",
                      xlab = "Sequenced bases (number of sequences)");
    barGap <- diff(barPos)[1];
    barOffset <- barPos[1] - barGap/2;
    axis(2, at = seq(barOffset,by=barGap,length.out = length(lhist$breaks)), labels = lhist$breaks, las = 2);
    axis(1, at = axTicks(1), labels = valToSci(axTicks(1), "bp"));
    text.poss <- ((log10(seqd.bases) < mean(par("usr")[1:2]))+1)*2;
    text.poss[is.na(text.poss)] <- 4;
    text.col <- c("white","black")[((log10(seqd.bases) < mean(par("usr")[1:2]))+1)];
    text(seqd.bases,barPos,paste(seqd.bases, " (", lhist$counts, ")", sep = ""),
         pos=text.poss, col=text.col);
}

typeCols <- c("#0000FF80","#FF000080","#FFFF0080");
pdf(sprintf("results/alignment_statistics_%s.pdf",dateStr),
    paper = "a4r", width = 11, height = 8);
##png(sprintf("results/alignment_statistics_%s.png", dateStr),width = 1280, height = 960);
sequence.hist(lengths);
plot(lengths,scores.raw, xlab = "Sequenced length",
     bg = typeCols[seqsource],
     ylab = "Alignment score (raw)", pch = 21);
legend("bottomright",legend=c("Template","Complement","2D"), fill = typeCols, inset = 0.05);
plot(lengths,scores.df$adj, xlab = "Sequenced length",
     bg = typeCols[seqsource], xaxt = "n",
     ylab = "Alignment score (adjusted)", log = "x", pch = 21);
axis(1,axTicks(1),valToSci(axTicks(1),"bp"), las = 1);
## generate a colour gradient that has 8 colours
primaries <- colorRampPalette(c("darkgrey","blue","magenta","red","yellow","green","cyan","white"));
layout(matrix(1:4,2,2));
## show counts by channel group
barplot(xtabs( ~ cell + group, data = channel.df), legend.text = FALSE, col = primaries(8), las = 2,
        cex.names = 0.5,
        args.legend = list(title="Cell"), xlab = "Flow-cell channel group", ylab = "Sequenced reads");
## show counts by column
barplot(xtabs( ~ row + col, data = channel.df), col = primaries(16), las = 2,
        cex.names = 0.75,
        xlab = "Flow-cell Column", ylab = "Sequenced reads");
## show counts by row
barplot(xtabs( ~ col + row, data = channel.df), col = primaries(32), las = 2,
        xlab = "Flow-cell Row", ylab = "Sequenced reads");
## show sequenced bases by row
par(mgp = c(4.5,1,0), mar = c(6,6,1,2));
barplot(xtabs(length ~ row, data = channel.df), las = 2, axes = FALSE,
        xlab = "Flow-cell Row", ylab = "Sequenced bases", log = "");
axis(2,axTicks(2),valToSci(axTicks(2),"bp"), las = 2);
graphics.off();

subType <- unlist(list(CT = "YR", AG = "YR", CG = "SW", AT = "SW",
                       AC = "MK", GT = "MK",
                       TC = "YR", GA = "YR", GC = "SW", TA = "SW",
                       CA = "MK", TG = "MK"));

stCols <- unlist(list(YR = "orange", SW = "magenta", "MK" = "maroon"));

scores.df$mm.pval <- NA;
scores.df$start <- sapply(alns,function(x){start(pattern(x))});
scores.df$end <- sapply(alns,function(x){end(pattern(x))});

plot(NA, ylim = c(0,1), xlim = c(1,nchar(ref)));
segments(x0 = scores.df$start, x1 = scores.df$end, y0 = scores.df$adj);

genome.coverage <- rep(0, nchar(ref));
genome.mismatches <- rep(0, nchar(ref));
for(x in seq_along(scores.df$start)[(scores.df$length > 500) & (scores.df$adj > 0.2)]){
    genome.coverage[scores.df[x,"start"]:scores.df[x,"end"]] <-
        (genome.coverage[scores.df[x,"start"]:scores.df[x,"end"]] + 1);
    mismatch.poss <- mismatch(pattern(alns[[x]]))[[1]];
    genome.mismatches[mismatch.poss] <-
        (genome.mismatches[mismatch.poss] + 1);
}

pdf(sprintf("results/coverage_plot_%s.pdf", dateStr),width = 11, height = 8);
##png(sprintf("results/coverage_plot_%s.png", dateStr),width = 1280, height = 960);
plot(genome.coverage, type = "l", ylim = c(0,max(genome.coverage)),
     ylab = "Aligned read coverage",
     xaxt = "n", xlab = "Genome Location (bacteriophage lambda)");
axis(1,axTicks(1),valToSci(axTicks(1),"bp"), las = 1);
points(runmed(genome.mismatches,501, endrule = "constant"), type = "l", ylim = c(0,1),
       col = "steelblue");
text(x=nchar(ref)/2,
     y=(median(genome.mismatches) + median(genome.coverage)) * (1/3),
     sprintf("Median coverage: %g, Median mismatch rate: %0.2f%%",
             median(genome.coverage), median(genome.mismatches*100/genome.coverage)));
graphics.off();

## create alignment plots
{
    pdf(sprintf("results/alignments_gr500bp_above0.2_%s.pdf", dateStr),
        paper="a4r",
        width = 11, height = 8);
    layout(c(1,2));
    scores.order <- order(-scores.df$adj, scores.df$length);
    for(qryID in scores.order[scores.df$adj[scores.order] > 0.2]){
    ##for(qryID in c(175,205)){
#
        letter.threshold <- 100; # threshold for sequence display
        qry.len <- nchar(qrys[qryID]);
        aln <- alns[[qryID]];
        aln.name <- sub("_strand.fast5\\]","",names(qrys[qryID]));
        if(((score(aln) / qry.len) > 0.2) & (qry.len > 500)){
            cat(aln.name, qry.len, score(aln), (score(aln) / qry.len),
                fill = TRUE);
            adj.score <- score(aln) / qry.len;
            ## ins / del are inverted because pattern is actually the subject
            ## - this fixes some alignment issues because insertion/deletion
            ##   are relative to the pattern
            del.matchStr <- gregexpr("-+",subject(aln));
            deletionMod <- start(pattern(aln)) - 1;
            dels <- unlist(start(insertion(aln))) + deletionMod;
            dele <- unlist(end(insertion(aln))) + deletionMod;
            delw <- unlist(width(insertion(aln)));
            ins.matchStr <- gregexpr("-+",pattern(aln));
            insertionMod <- start(pattern(aln)) - 1;
            inss <- unlist(start(deletion(aln))) + insertionMod;
            inse <- unlist(end(deletion(aln))) + insertionMod;
            insw <- unlist(width(deletion(aln)));
            mm <- mismatchTable(aln)$PatternStart;
            mm.pat <- mismatchTable(aln)$SubjectSubstring;
            mm.sub <- mismatchTable(aln)$PatternSubstring;
            mm.subType <- subType[paste(mm.pat,mm.sub,sep="")];
            ## generate plot of alignment
            sub.lim <- c(start(pattern(aln)),end(pattern(aln)));
            mismatch.str <- "";
            if(length(mm.subType) > 0){
                m.tab <- table(factor(mm.subType, levels = c("YR","SW","MK")));
                mismatch.test <- chisq.test(m.tab, rescale.p = TRUE);
                mismatch.str <-
                    sprintf("\n[Mismatch chi^2 probability %0.2g]",
                            mismatch.test$p.value);
                scores.df[qryID,"mm.pval"] <- mismatch.test$p.value;
            }
            plot(NA, xlim = sub.lim,
                 ylim = c(-max(1,delw),max(1,insw)),
                 main = sprintf("%s (%d bp, adjusted score: %0.3f)%s",
                     aln.name, qry.len, adj.score, mismatch.str),
                 ylab = "Indel Size", xlab = "Mitochondrial location");
            ##letter.cex = 50 / diff(sub.lim);
            ##letter.cex <- min(4,letter.cex);
            letter.cex = 0.5;
            if(diff(sub.lim) <= letter.threshold){
                abline(v = sub.lim[1]:sub.lim[2], col = "yellow");
            }
            grid(nx = NULL, ny = NA);
            ## plot alignment
            if(diff(sub.lim) > letter.threshold){
                rect(xleft = start(pattern(aln)), xright = end(pattern(aln)),
                     ytop = 0.1, ybottom = -0.1, col = "black", border = NA);
            }
            ## plot insertions
            if(length(inss) > 0){
                for(id in 1:length(inss)){
                    polygon(x = c(inss[id]-0.5,inss[id]-0.5,inss[id]+insw[id]-0.5,
                                inss[id]+insw[id]-0.5),
                            y = c(0,insw[id],insw[id],insw[id]-0.2), col = "chartreuse", border = NA);
                    if(diff(sub.lim) <= letter.threshold){
                        insertStr <-
                            substring(subject(aln),unlist(ins.matchStr)[id],
                                      unlist(ins.matchStr)[id] + insw[id] - 1);
                        for(p1 in 1:nchar(insertStr)){
                            text(p1+inss[id]-1,insw[id]-0.1,substring(insertStr,p1,p1), cex = letter.cex);
                        }
                    }
                }
            }
            ## plot deletions
            if(length(dels) > 0){
                for(id in 1:length(dels)){
                    ## apparently not pattern-relative after all...
                    ## [need to adjust both for prior deletions and for prior insertions]
                    adjust <- sum(delw[1:id]) - delw[id] - sum(insw[inss < dels[id]])
                    polygon(x = c((dels[id]+dele[id])/2,dels[id]-0.5,dels[id]-0.5,
                                dele[id]+0.5,dele[id]+0.5) + adjust,
                            y = c(-delw[id],-0.1,0.1,0.1,-0.1), col = "steelblue", border = NA);
                }
            }
            ## plot mismatches
            if(length(mm) > 0){
                rect(xleft = mm-0.5, xright = mm+0.5,
                     ytop = 0.1, ybottom = -0.3, col = "red", border = NA);
                rect(xleft = mm-0.5, xright = mm+0.5,
                     ytop = -0.3, ybottom = -0.6, col = stCols[mm.subType], border = NA);
                if(diff(sub.lim) <= letter.threshold){
                    text(mm,-0.2,mm.pat, cex = letter.cex);
                }
            }
            if(diff(sub.lim) <= letter.threshold){
                text(sub.lim[1]:sub.lim[2],0,
                     as.matrix(ref)[1,sub.lim[1]:sub.lim[2]], cex = letter.cex);
            }
        }
#
    }
    graphics.off();
}

align.file <- file(sprintf("results/good_aligned_%s.fasta",dateStr), open = "w+");
for(qryID in which((scores.df$adj > 0.2) & (scores.df$length > 500) & (scores.df$mm.pval < 0.01))){
    cat(file=align.file, ">",names(qrys[qryID]),"\n",
        as.character(qrys[qryID]),"\n",sep="");
}
close(align.file);



# 205, 175
