#!/usr/bin/env Rscript
## spiralign.r - Spiral alignment plot

argLoc <- 1;

usage <- function(){
    cat("usage: ./spiralign.r",
        "<fasta/fastq file> [options]\n");
    cat("\nOther Options:\n");
    cat("-help              : Only display this help message\n");
    cat("-outfmt <string>   : Output file format (png|pdf|svg)\n");
    cat("-type <string>     : Sequence type (aa|nucl)\n");
    cat("-loops <numeric>   : Number of spiral loops (default: 2.75)\n");
    cat("-noalign           : Bypass alignment step\n");
    cat("-gap               : Add a gap between sequences\n");
    cat("-noborder          : Don't draw a border around bases\n");
    cat("-title <string>    : Text to put in the centre of the image\n");
    cat("-textsize <num>    : Magnification factor for text\n");
    cat("-size <x>x<y>      : Image size (default 1200x1200 for png, 12x12 for PDF)\n");
    cat("\n");
}

format <- "pdf";
type <- "aa";
seqFile <- "";
loops <- 2.75;
centreText <- "";
doAlign <- TRUE;
doBorder <- TRUE;
doGap <- FALSE;
sizeX <- -1;
sizeY <- -1;
textSize <- 1;

while(!is.na(commandArgs(TRUE)[argLoc])){
  if(file.exists(commandArgs(TRUE)[argLoc])){ # file existence check
      seqFile <- commandArgs(TRUE)[argLoc];
  } else {
    if(commandArgs(TRUE)[argLoc] == "-help"){
      usage();
      quit(save = "no", status=0);
    }
    else if(commandArgs(TRUE)[argLoc] == "-outfmt"){
      format <- commandArgs(TRUE)[argLoc+1];
      argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-textsize"){
      textSize <- as.numeric(commandArgs(TRUE)[argLoc+1]);
      argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-type"){
      type <- commandArgs(TRUE)[argLoc+1];
      argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-title"){
      centreText <- commandArgs(TRUE)[argLoc+1];
      argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-loops"){
      loops <- as.numeric(commandArgs(TRUE)[argLoc+1]);
      argLoc <- argLoc + 1;
    }
    else if(commandArgs(TRUE)[argLoc] == "-noalign"){
      doAlign <- FALSE;
    }
    else if(commandArgs(TRUE)[argLoc] == "-gap"){
      doGap <- TRUE;
    }
    else if(commandArgs(TRUE)[argLoc] == "-noborder"){
      doBorder <- FALSE;
    } else if(commandArgs(TRUE)[argLoc] == "-size"){
        arg <- unlist(strsplit(commandArgs(TRUE)[argLoc+1], "x"));
        sizeX <- as.numeric(arg[1]);
        sizeY <- as.numeric(arg[2]);
        argLoc <- argLoc + 1;
    }
    else {
      cat("Error: Argument '",commandArgs(TRUE)[argLoc],
          "' is not understood by this program\n\n", sep="");
      usage();
      quit(save = "no", status=0);
    }
  }
  argLoc <- argLoc + 1;
}

if(format == "png"){
    if(sizeX == -1){
        sizeX <- 1200;
    }
    if(sizeY == -1){
        sizeY <- 1200;
    }
} else if((format == "pdf") || (format == "svg")){
    if(sizeX == -1){
        sizeX <- 12;
    }
    if(sizeY == -1){
        sizeY <- 12;
    }
}

library(Biostrings);
input.seqs <- if(type == "aa"){
                  readAAStringSet(seqFile);
              } else {
                  readDNAStringSet(seqFile);
              }
names(input.seqs) <- sub(" .*$","",names(input.seqs));
msa.df <-
    if(doAlign){
        library(msa);
        data.frame(t(as.matrix(msa(input.seqs, order="input"))),
                   stringsAsFactors=FALSE);
    } else {
        data.frame(t(as.matrix(input.seqs)),
                   stringsAsFactors=FALSE);
    }
print(str(msa.df));
msa.df <- msa.df[nrow(msa.df):1,]
seq.cols <-
    if(type == "aa"){
        c("D" = "#E60A0A", "E" = "#E60A0A", ## colours from rasmol
          "C" = "#E6E600", "M" = "#E6E600",
          "K" = "#145AFF", "R" = "#145AFF",
          "S" = "#FA9600", "T" = "#FA9600",
          "F" = "#3232AA", "Y" = "#3232AA",
          "N" = "#00DCDC", "Q" = "#00DCDC",
          "G" = "#EBEBEB",
          "L" = "#0F820F", "V" = "#0F820F", "I" = "#0F820F",
          "A" = "#C8C8C8",
          "W" = "#B45AB4",
          "H" = "#8282D2",
          "P" = "#DC9682",
          "-" = "grey20", "X" = "grey20");
    } else {
        ## colourblind-friendly base colours,
        ## based on standard electrophoresis colours
        ## [A - green / C - blue / G - yellow / T - red]
        c("A" = "#006400",
          "C" = "#0000FF",
          "G" = "#FFD700",
          "T" = "#FA8072",
          "-" = "grey20",
          "N" = "grey20");
    }
seq.cats <- tapply(names(seq.cols),seq.cols,paste,collapse=",");

if(format == "pdf"){
    cairo_pdf(sprintf("msa_%s.pdf", type), width=sizeX, height=sizeY, pointsize=12 * (sizeX/12));
} else if(format == "svg"){
    svg(sprintf("msa_%s.svg", type), width=sizeX, height=sizeY, pointsize=12 * (sizeX/12));
} else if(format == "png"){
    png(sprintf("msa_%s.png", type), width=sizeX, height=sizeY, pointsize=24 * sizeX/1024,
        antialias="gray", type="cairo-png");
}
par(mar=c(1,1,1,1), bg="white", cex = 1 * sqrt(4 / loops));
lstt <- 3;
lend <- loops+lstt;
if(loops <= 1){
  lend <- lstt;
}
## integrate(2*pi*r,r=lstt..x)
## => pi(x²-(lstt)²)
dTot <- pi*((lstt + loops)^2 - (lstt)^2); ## total "distance" travelled
if(loops <= 1){
  dTot <- pi * (lstt * 2);
}
## s = pi(x²-(lstt)²)
## => s/pi = x² - (lstt)²
## => x = sqrt((lstt)² + s/pi)
msa.df <- rbind(msa.df, rep("-",ncol(msa.df)));
msa.df$s <- seq(0,dTot, length.out=nrow(msa.df)); ## distance at each pos
msa.df$r <- sqrt(lstt^2 + msa.df$s/pi); ## path radius at each pos
if(loops <= 1){
  msa.df$r <- lstt;
}
msa.df$theta <- msa.df$r * 2*pi; ## traversed angle at each pos
if(loops <= 1){
  msa.df$theta <- msa.df$s / dTot * (2 * pi * loops);
}
msa.df$deg <- (msa.df$theta / (2*pi)) * 360;
msa.df$x <- msa.df$r * cos(msa.df$theta);
msa.df$y <- msa.df$r * sin(msa.df$theta);
if(loops > 1){
  plot(NA,xlim=c(-lend,lend), ylim=c(-lend,lend), ann=FALSE, axes=FALSE);
} else {
  plot(NA,xlim=c(-lend-1,lend+1), ylim=c(-lend-1,lend+1), ann=FALSE, axes=FALSE);
}
pcex <- 0.8;
pym <- length(input.seqs);
pyr <- (seq(0.25/pym, 1-0.25/pym, length.out=pym+1) - 0.5);
for(p in seq(1,nrow(msa.df))){
    pr <- msa.df$r[c(p,p,p+1,p+1)];
    pt <- msa.df$theta[c(p,p,p+1,p+1)];
    for(py in 1:length(input.seqs)){
      polygon(x=-(pr+pyr[c(py,py+1,py+1,py)])*cos(pt),
              y=(pr+pyr[c(py,py+1,py+1,py)])*sin(pt),
              col=seq.cols[msa.df[p,py]], border=if(doBorder){NULL} else {NA},
              lwd=0.5);
    }
}
## Add labels for sequences
pyrt <- seq(-(pym-2)/(pym-1),(pym-2)/(pym-1), length.out=pym)/2;
if(all(pyrt == c(0,0))){ ## hack to stop labels appearing on the same line
    pyrt <- c(-0.2, 0.2);
}
print(pyrt);
for(py in 1:length(input.seqs)){
    text(x=-(max(msa.df$r)+pyrt[py])*cos(max(msa.df$theta)),
         y=(max(msa.df$r)+pyrt[py])*sin(max(msa.df$theta))-0.02,
         labels=names(input.seqs)[py],
         srt=0, pos=2,
         cex=0.6 * textSize, col="grey21");
}
legend("topleft", fill=names(seq.cats), text.col="grey21",
       legend=seq.cats, ncol=2, cex=0.8);
if(centreText != ""){ ## add centre text
    ctElts <- unlist(strsplit(centreText, "\\\\n"));
    for(x in seq_along(ctElts)){
        text(0, 0.5 - ((x-1) / (length(ctElts)-1)), ctElts[x], col="grey21");
    }
}
invisible(dev.off());
