Bioinformatics Scripts
=============

Bioinformatics scripts created or modified by David Eccles (gringer).

For comments / suggestions, feel free to create an issue, or email ``<bioinformatics@gringene.org>``.

Where not otherwise specified, code here that is written by me is licensed
under the terms of the GNU GPL v3 (see file LICENSE). This is because I want
to have the right to modify and distribute code that is derived from code
that I have written.

Installation
=============

The majority of these scripts are standalone files that can be individually downloaded and run from anywhere on the computer. Some scripts (e.g. `repaver`, `fastx-dental`) depend on other files being present in the same directory as the script. The simplest way to make sure all scripts work properly is to clone this gitlab repository using git:

    cd ~ # change to home folder
    git clone https://gitlab.com/gringer/bioinfscripts.git # clone the repository

Commands can then be run from anywhere on the computer by providing the full path to the script:

    ~/bioinfscripts/fastx-dental.pl my_fastq_file.fq > barcode_counts.txt

Frequently-requested Code
=========================

Here are some scripts from this repository that are a frequent request (either from me, doing analysis for other people, or from other people trying out my own code):

* **REPAVER**: DNA sequence repeat visualisation [[code](https://gitlab.com/gringer/bioinfscripts/-/blob/master/repaver.r); also needs [hashasher](https://gitlab.com/gringer/bioinfscripts/-/blob/master/hashasher.cc)]
* **fastx-fetch**: used for barcode demultiplexing and sequence filtering [[code](https://gitlab.com/gringer/bioinfscripts/-/blob/master/fastx-fetch.pl)]
* **longvish**: visualise DNA sequences by wrapping them into a spiral or matrix [[code](https://gitlab.com/gringer/bioinfscripts/-/blob/master/longvish.r)]
* **Read Stomper**: collapses variants to produce a 'stomped' variant pileup plot that is easier to interpret [[code](https://gitlab.com/gringer/bioinfscripts/-/blob/master/readstomper.pl)]
* **csplot**: produce chromosome-segmented scatter plots (manhattan plots) [[code](https://gitlab.com/gringer/bioinfscripts/-/blob/master/csplot.r)]
