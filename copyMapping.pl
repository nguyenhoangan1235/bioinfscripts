#!/usr/bin/perl

use warnings;
use strict;

use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

our $VERSION = "0.1";

## copyMapping.pl -- Transfer additional Rhapsody sample tag mapping information to cell barcode header information

my $mapFileName = "";
my %mappingHash = ();

sub usage {
  printf(STDERR "Usage: ./copyMapping.pl [opts] -map <mapFileName.csv>\n");
}

GetOptions("map=s" => \$mapFileName)
    or exit(1);

if(!$mapFileName) {
  printf(STDERR "Error: mapping file name not provided\n");
  usage();
  exit(1);
}

my $mapFile = IO::Uncompress::Gunzip->new($mapFileName)
    or die "gunzip failed for '$mapFileName': $GunzipError\n";
print(STDERR "Loading mapping from '$mapFileName' [one '.' per 100,000 lines]...");
my $linesRead = 0;
while(<$mapFile>){
  chomp;chomp;
  my $line = $_;
  my @F = split(/[\s,]/);
  ## Do both ways round so that order doesn't matter
  $mappingHash{$F[0]} = $F[1];
  $mappingHash{$F[1]} = $F[0];
  if(++$linesRead % 1000000 == 0){
    print(STDERR ".");
  }
}
print(STDERR " done\n");

while(<>){
  chomp;chomp;
  if(/^[>@]?(.*?)(\s|,|$)/){
    if(exists($mappingHash{$1})){
      printf("%s mapping=%s\n", $_, $mappingHash{$1});
    } else {
      print($_."\n");
    }
  } else {
    print($_."\n");
  }
}
