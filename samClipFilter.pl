#!/usr/bin/env perl
use warnings;
use strict;

use Pod::Usage; ## uses pod documentation in usage code
use Getopt::Long qw(:config auto_help pass_through);

my $pos = -1;
my $clipThreshold = -1;
my $seqName = "";
my $bestFlags = "";
my $bestID = "";
my $bestSeq = "";
my $bestQual = "";
my $bestLine = "";
my $seenCount = 0;
my $targetReference = "";
my $targetPos = -1;
my %posReads = ();
my %windowReads = ();
my $output = "sam"; # can be "sam" or "fastq" or "csv"

our $VERSION = "0.1";

=head1 NAME

samVarSplitter.pl
 - Split reads based on variant sequences

=head1 SYNOPSIS

samtools view mapped_reads.bam | ./samClipFilter.pl -len <length> [options]

=head2 Options

=over 2

=item B<-help>

Only display this help message

=item B<-format> (sam|fastq)

Change output format (default: sam)

=item B<-length> I<length>

Set I<length> as the length threshold for hard/soft clipping

=back

=head1 DESCRIPTION

Filters out sequences that have excessive clipping.

=cut

GetOptions("format=s" => \$output, "length=s" => \$clipThreshold)
  or pod2usage(1);

if($clipThreshold == -1){
  print(STDERR "Error: please specify clip length (-length)\n");
  pod2usage(1);
}

sub rc {
  my ($seq) = @_;
  $seq =~ tr/ACGTUYRSWMKDVHBXN-/TGCAARYSWKMHBDVXN-/;
  # work on masked sequences as well
  $seq =~ tr/acgtuyrswmkdvhbxn/tgcaaryswkmhbdvxn/;
  return(scalar(reverse($seq)));
}

sub printSeq {
  my ($id, $seq, $qual) = @_;
  if($id){
    printf("@%s\n%s\n+\n%s\n", $id, $seq, $qual);
  }
}

sub dumpSamples {
  my %posReads = %{shift @_};
  ## Collect Read Group Names
  my %readGroups = ();
  foreach my $pos (keys(%posReads)){
    my $rgSeq = getPosSeq($posReads{$pos}{"seq"},
			  $posReads{$pos}{"flags"},
			  $posReads{$pos}{"pos"},
			  $targetPos,
			  $posReads{$pos}{"cigar"});
    $readGroups{$rgSeq}{"present"}++;
    $posReads{$pos}{"rgSeq"} = $rgSeq;
  }
  ## Print headers and assign read group IDs
  my $rgID = 0;
  foreach my $rgSeq (keys(%readGroups)){
    my $addedID = sprintf("%d_%s_%d%s",
			  $rgID,
                          ($targetReference ? $targetReference : "ALL"),
                          $targetPos, $rgSeq);
    $readGroups{$rgSeq}{"id"} = $addedID;
    $rgID++;
  }
  if($output eq "fastq"){
    foreach my $pos (sort {$posReads{$a}{"pos"} <=>
			       $posReads{$b}{"pos"}} keys(%posReads)){
      printSeq($posReads{$pos}{"id"}.
               " start=".$posReads{$pos}{"pos"}.
               " mappedLen=".$posReads{$pos}{"len"},
               $posReads{$pos}{"seq"},
               $posReads{$pos}{"qual"});
    }
  } elsif($output eq "sam"){
    foreach my $pos (sort {$posReads{$a}{"pos"} <=>
			       $posReads{$b}{"pos"}} keys(%posReads)){
      my $line = $posReads{$pos}{"line"}; chomp($line);
      print($line . "\n");
    }
  } elsif($output eq "csv"){
    foreach my $pos (sort {$posReads{$a}{"pos"} <=>
			       $posReads{$b}{"pos"}} keys(%posReads)){
      printf("%s,%s,%s,%s,%s\n",
             $readGroups{$posReads{$pos}{"rgSeq"}}{"id"},
             $pos,
             -((($posReads{$pos}{"flags"} & 0x10) >> 3) - 1),
             $posReads{$pos}{"pos"},
             $posReads{$pos}{"len"});
    }
  }
}

sub getPosSeq {
  my ($seq, $flags, $startPos, $targetPos, $cigar) = @_;
  if($targetPos == -1){
    return("-");
  }
  my $refPos = $startPos - 1;
  my $seqPos = 0;
  my $lastRef = $startPos - 1;
  my $posSeq = "";
  if($flags & 0x10){
    # Note: don't reverse complement - seq field is adjusted to match ref
  }
  while($cigar =~ s/^([0-9]+)([MIDNSHP=X])//){
    my $subLen = $1;
    my $op = $2;
    if($op =~ /[M=XDPN]/){
      $refPos += $subLen;
    }
    if($op eq "I"){
      if($lastRef == ($targetPos - 1)){
        $posSeq .= substr($seq, $seqPos, $subLen);
      }
    } elsif(($lastRef <= ($targetPos-1)) && ($refPos >= $targetPos)){
      # now we know precisely in the sequence where the variant is
      if($op =~ /[M=X]/){
        $posSeq .= substr($seq, $seqPos + ($targetPos - $lastRef) - 1, 1);
      } elsif($op =~ /[DPN]/){
	$posSeq .= "-";
      }
    }
    if($op =~ /[MIX=S]/){
      $seqPos += $subLen;
    }
    $lastRef = $refPos;
  }
  return($posSeq);
}

sub getMappedLength {
  my $cigar = shift @_;
  my $mappedLen = 0;
  while($cigar =~ s/^([0-9]+)([MIDNSHP=X])//){
    my $subLen = $1;
    my $op = $2;
    if($op =~ /[M=XD]/){
      $mappedLen += $subLen;
    }
  }
  return($mappedLen);
}

sub getMaxClipLength {
  my $cigar = shift @_;
  my $maxClipLength = 0;
  while($cigar =~ s/^([0-9]+)([MIDNSHP=X])//){
    my $subLen = $1;
    my $op = $2;
    if($op =~ /[SH]/){
      if($subLen > $maxClipLength){
        $maxClipLength = $subLen;
      }
    }
  }
  return($maxClipLength);
}

if($output eq "csv"){
  printf("group,readID,dir,start,mappedLength\n");
}

while(<>){
  if(/^@/){
    print;
    next;
  }
  my $line = $_;
  chomp;
  my @F = split(/\t/);
  if(getMaxClipLength($F[5]) <= $clipThreshold){
    my $readID = $F[0];
    $posReads{$readID}{"line"} = $line;
    $posReads{$readID}{"id"} = $F[0];
    $posReads{$readID}{"flags"} = $F[1];
    $posReads{$readID}{"seq"} = $F[9];
    $posReads{$readID}{"pos"} = $F[3];
    $posReads{$readID}{"cigar"} = $F[5];
    $posReads{$readID}{"len"} = getMappedLength($F[5]);
    $posReads{$readID}{"qual"} = $F[10];
    $seqName = $F[2];
  }
}
dumpSamples(\%posReads);
