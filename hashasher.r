#!/usr/bin/env Rscript

## HashAsher -- A fast hash-based repeat finder
## This R script provides text-based command-line utilities
##   to complement the hashasher.cc library. For graphical
##   output, see 'repaver.r'
## usage: ./hashasher.r [options] <file.fasta>

argLoc <- 1;

sourceDir <- ".";
scriptName <- "hashasher.r";
libName <- "hashasher.cc";
if(any(grepl("--file=", commandArgs()))){
    sourceDir <- dirname(sub("--file=", "", grep("--file=", commandArgs(), value=TRUE)));
    scriptName <- basename(sub("--file=", "", grep("--file=", commandArgs(), value=TRUE)));
    libName <- sub("\\.r$", ".cc", scriptName);
}

usage <- function(){
    cat(sprintf("usage: %s <input.fasta> [options]\n", file.path(sourceDir, scriptName)), file=stderr());
    cat("\nOther Options:\n", file=stderr());
    cat("-help         : Only display this help message\n", file=stderr());
    cat("-count <file> : Save repeat count table to <file>\n", file=stderr());
    cat("-bed <file>   : Save BED output to <file>\n", file=stderr());
    cat("-bg <file>    : Save BedGraph output to <file>\n", file=stderr());
    cat("-plot <file>  : Basic test of plotting difference data\n", file=stderr());
    cat("-diff <file>  : Save base difference output to <file>\n", file=stderr());
    cat("-map <file>   : Map reads in <file> to input sequence\n", file=stderr());
    cat("-bpp <int>    : Set number of bases per pixel (for 'diff' output)\n", file=stderr());
    cat("-k <int>      : Set kmer length in bp (default: 17)\n", file=stderr());
    cat("-filter <str> : Filter based on repeat count/proportions\n", file=stderr());
    cat("                e.g. -filter 'RC>0.4;C<10' to keep reads with more than\n", file=stderr());
    cat("                     40% reverse-complement repeats, and less than 10 comp repeats\n",
        file=stderr());
    cat("-v            : Invert filter logic\n", file=stderr());
    cat("-noseqs       : Disable sequence output in BED file\n", file=stderr());
    cat("-merge        : Merge overlapping regions that are broken by a non-repetitive kmer\n",
        file=stderr());
    cat("-split        : Explicitly output every repetitive kmer\n", file=stderr());
    cat("-test         : Run program operation tests\n", file=stderr());
    cat("-nocheck      : Bypass C++ source checksum validation\n", file=stderr());
    cat("\n", file=stderr());
}

## Default arguments

fastaFileName <- "";
countFileName <- "";
bedFileName <- "";
bedGraphFileName <- "";
plotFileName <- "";
diffFileName <- "";
mapFileName <- "";
writeKmers <- TRUE;
mergeRegions <- FALSE;
splitKmers <- FALSE;
kmerLength <- 17;
basesPerChunk <- 1;
doTests <- FALSE;
ignoreChecksum <- FALSE;
doFilter <- FALSE;
invertFilter <- FALSE;
filterVals <- list("F>" = 0,   "R>" = 0,   "C>" = 0,   "RC>" = 0,
                   "F<" = 1.1, "R<" = 1.1, "C<" = 1.1, "RC<" = 1.1);

## SHA256 digest of c++ library code; used to maintain version consistency
expectedDigest <-
    "a26a85a88744d2ae713ce53b9b92f4bebace99cb874952e3642d2074f70293bd";

required.packages <- c("digest", "Rcpp");
cat("Loading R libraries...", file=stderr());
for(pack in required.packages){
    if(!suppressMessages(require(pack, character.only=TRUE))){
        cat(sprintf("Error: '%s' package must be installed\n", pack));
        usage();
        quit(save="no");
    }
}
cat(" done!\n", file=stderr());

cat("Loading C++ libraries...", file=stderr());
sourceCpp(file.path(sourceDir, libName), cacheDir=file.path(sourceDir, "cppCache"));
cat(" done!\n", file=stderr());

while(!is.na(commandArgs(TRUE)[argLoc])){
    if(file.exists(commandArgs(TRUE)[argLoc])){ # file existence check
        fastaFileName <- commandArgs(TRUE)[argLoc];
    } else {
        if(commandArgs(TRUE)[argLoc] == "-help"){
            usage();
            quit(save = "no", status=0);
        } else if(commandArgs(TRUE)[argLoc] == "-count"){
            countFileName <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-bed"){
            bedFileName <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-plot"){
            plotFileName <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-diff"){
            diffFileName <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-map"){
            mapFileName <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-bg"){
            bedGraphFileName <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-bpp"){
            basesPerChunk <- as.numeric(commandArgs(TRUE)[argLoc+1]);
            cat(sprintf("Setting chunk size to %d bases\n", basesPerChunk), file=stderr());
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-k"){
            kmerLength <- as.numeric(commandArgs(TRUE)[argLoc+1]);
            cat(sprintf("Setting kmer length to %d\n", kmerLength), file=stderr());
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-filter"){
            filterStr <- unlist(strsplit(commandArgs(TRUE)[argLoc+1], ";"));
            for(f in filterStr){
                components <- unlist(strsplit(f, "[<>]"));
                dir <- substr(f, nchar(components[1])+1, nchar(components[1])+1);
                type <- substr(f, 1, nchar(components[1])+1);
                val <- as.numeric(components[2]);
                filterVals[[type]] <- val;
                cat(sprintf("Filtering '%s' %s %s\n", components[1], dir, val), file=stderr());
                doFilter <- TRUE;
            }
            argLoc <- argLoc + 1;
        } else if(commandArgs(TRUE)[argLoc] == "-v"){
            invertFilter <- TRUE;
            cat(sprintf("Filter logic will be inverted\n"), file=stderr());
        } else if(commandArgs(TRUE)[argLoc] == "-plot"){
            cat("Note: will plot results\n", file=stderr(), file=stderr());
            plotResults <- TRUE;
        } else if(commandArgs(TRUE)[argLoc] == "-noseqs"){
            cat("Note: won't add starting kmer sequences to the BED file\n", file=stderr());
            writeKmers <- FALSE;
        } else if(commandArgs(TRUE)[argLoc] == "-merge"){
            cat("Note: overlapping regions including non-repetitive kmers will be merged\n", file=stderr());
            mergeRegions <- TRUE;
        } else if(commandArgs(TRUE)[argLoc] == "-split"){
            cat("Note: all repetitive kmers will be output separately\n", file=stderr());
            splitKmers <- TRUE;
        } else if(commandArgs(TRUE)[argLoc] == "-test"){
            cat("Note: will run unit tests\n");
            doTests <- TRUE;
        } else if(commandArgs(TRUE)[argLoc] == "-nocheck"){
            cat("Note: will not verify C++ code checksum\n", file=stderr());
            ignoreChecksum <- TRUE;
        } else {
            cat("Error: Argument '",commandArgs(TRUE)[argLoc],
                "' is not understood by this program\n\n", sep="", file=stderr());
            usage();
            quit(save = "no", status=1);
        }
    }
    argLoc <- argLoc + 1;
}

codeDigest <- digest(readLines(file.path(sourceDir, libName)), algo="sha256");
if(ignoreChecksum){
    cat("Code checksum: ", codeDigest, "\n", sep="", file=stderr());
} else if(!identical(codeDigest, expectedDigest)) {
    cat("Error: C++ code checksum does not match expected. Has the c++ code been updated?\n", file=stderr());
    cat("    Code checksum: ", codeDigest, "\n", sep="", file=stderr());
    cat("    Expected     : ", expectedDigest, "\n", sep="", file=stderr());
    cat("To continue anyway, use option '-nocheck'\n\n", file=stderr());
    usage();
    quit(save = "no", status=1);
}

if(splitKmers && mergeRegions){
    cat("Error: '-split' and '-merge' are mutually exclusive. Please choose only one.\n", file=stderr());
    usage();
    quit(save = "no", status=1);
}

if((!doTests) && (!doFilter) &&
   ((identical(countFileName, "") &&
     identical(bedGraphFileName, "") && identical(bedFileName, "") &&
     identical(diffFileName, "") && identical(plotFileName, "") &&
     identical(mapFileName, "")) ||
    identical(fastaFileName, ""))){
    cat("Nothing to do!\n", file=stderr());
    usage();
    quit(save = "no", status=1);
}

if(!identical(fastaFileName, "") && doFilter){
    cat("Filtering reads...\n", file=stderr());
    prepareFastxFile(fastaFileName, kmerLength);
    while(loadNextSequence(silent=TRUE)){
        seqIsLong <- (getSeqLen() > 10^6);
        if(seqIsLong){
            cat(getSeqSig(), "\n", sep="");
        }
        hashRepeats(silent=TRUE);
        filterReads(
            invert=invertFilter,
            minF = filterVals[["F>"]],
            minC = filterVals[["C>"]],
            minR = filterVals[["R>"]],
            minRC= filterVals[["RC>"]],
            maxF = filterVals[["F<"]],
            maxC = filterVals[["C<"]],
            maxR = filterVals[["R<"]],
            maxRC= filterVals[["RC<"]]);
    }
    cat("Finished read filtering!\n", file=stderr());
}

if((countFileName != "") && (fastaFileName != "")){
    firstSequence <- TRUE;
    cat("Writing repeat counts to file...\n", file=stderr());
    prepareFastxFile(fastaFileName, kmerLength);
    while(loadNextSequence(silent=TRUE)){
        seqIsLong <- (getSeqLen() > 10^6);
        if(seqIsLong){
            cat(getSeqSig(), "\n", sep="");
        }
        hashRepeats(silent=!seqIsLong);
        writeCountOutput(countFileName, append=!firstSequence);
        firstSequence <- FALSE;
    }
    cat("Finished writing repeat counts!\n", file=stderr());
}

if((bedFileName != "") && (fastaFileName != "")){
    firstSequence <- TRUE;
    prepareFastxFile(fastaFileName, kmerLength);
    while(loadNextSequence()){
        cat(getSeqSig(), "\n", sep="");
        print(system.time(hashRepeats()));
        cat("Writing to BED file...");
        makeRepeatBed(bedFileName, writeKmers=writeKmers,
                      append=!firstSequence, merge=mergeRegions,
                      split=splitKmers);
        cat(" done.\n");
        firstSequence <- FALSE;
    }
}

if((bedGraphFileName != "") && (fastaFileName != "")){
    firstSequence <- TRUE;
    prepareFastxFile(fastaFileName, kmerLength);
    while(loadNextSequence()){
        cat(getSeqSig(), "\n", sep="");
        print(system.time(hashRepeats()));
        cat("Writing to BedGraph file...");
        makeRepeatBedGraph(bedGraphFileName, append=!firstSequence);
        cat(" done.\n");
        firstSequence <- FALSE;
    }
}

if((diffFileName != "") && (fastaFileName != "")){
    firstSequence <- TRUE;
    prepareFastxFile(fastaFileName, kmerLength);
    while(loadNextSequence()){
        cat(getSeqSig(), "\n", sep="");
        print(system.time(hashRepeats()));
        cat("Writing to Difference file...");
        writeDiffOutput(diffFileName, basesPerChunk, append=!firstSequence);
        cat(" done.\n");
        firstSequence <- FALSE;
    }
}


if((mapFileName != "") && (fastaFileName != "")){
    firstSequence <- TRUE;
    prepareFastxFile(fastaFileName, kmerLength);
    while(loadNextSequence()){
        cat(getSeqSig(), "\n", sep="");
        print(system.time(hashRepeats()));
        cat(sprintf("Mapping reads from file '%s'...", mapFileName));
        mapReads(mapFileName, header=firstSequence);
        cat(" done.\n");
        firstSequence <- FALSE;
    }
}


if((plotFileName != "") && (fastaFileName != "")){
    plotCols <- list("Left"   = c(F="#9000A040",RC="#FF7F0040",
                                  C="#00A09040",R="#A0900040"),
                     "Right"  = c(F="#8b000040",RC="#FDC08640",
                                  C="#0000FF40",R="#00A00040"),
                     "None" = c());
    firstSequence <- TRUE;
    prepareFastxFile(fastaFileName, kmerLength);
    png(plotFileName, width=1920, height=1080, pointsize=22);
    cexScale <- 1;
    par(mgp=c(2.5,1,0), mar=c(4,6.5,3,2),
        cex.axis=1.5*cexScale, cex.lab=1.5*cexScale,
        cex.main=2*cexScale, xaxs="i", yaxs="i");
    while(loadNextSequence()){
        cat(getSeqSig(), "\n", sep="");
        print(system.time(hashRepeats()));
        diffResults <-
            getDiffResults(bpc=basesPerChunk, yBins=1000,
                           doLog=TRUE, circular=FALSE);
        diffResults$chunks$type[diffResults$chunks$type == "X"] <- "RC";
        plot(NA, xlim=c(0, diffResults$len), ylim=c(0, 1000),
             main=diffResults$name);
        for(tType in unique(diffResults$chunks$type)){
            res.sub <- subset(diffResults$chunks, type == tType);
            cat(sprintf("Plotting %d %s repeats...", nrow(res.sub), tType));
            points(x=res.sub$x, y=abs(res.sub$y),
                   pch=20,
                   cex=0.5,
                   col=ifelse(res.sub$y > 0,
                   (plotCols[["Left"]])[tType],
                   (plotCols[["Right"]])[tType]));
            cat(" done!\n");
        }
    }
    invisible(dev.off());
}


complementTests <- function(){
    ## Check for forward / reverse / reverse-complement / complement
    ## Create a temporary file with small sequence
    fileName <- tempfile(fileext=".fa");
    ## a minimal synthetic test case
    cat(file=fileName, sep="\n",
        ">test_for_k5",
        "AATCGTATCGTATACGAATGCTAAAATCGCGATTTTAGCGAATCGAAAAA");
    ## a forward-repetitive sequence from the chr8 pseudo-centromere
    cat(file=fileName, sep="\n",
        ">T2T_1.0_chr8:86120781-86121680",
        "GCTGTCTGCCGACCTTGGAGCCACGGGAGCGTTGGCTGCTGCTGGCCACCCGGGTTCTCT",
        "TGGCATCTGTGTAACCTGTGACCAAGCAAGGGCTGGAAGAGTGGGCGATCGTCTTCCTCT",
        "TCCTGGGGGCTGAGATGCGGACTCCCGAGGGCCTCTCTGTCAGCCTTGGGGCGGCTGGCA",
        "AGCGGCAGGCCGATCCCCTCTGCGCAGGGAAGTAGCACGACTCCGTCACCATCTTGGGCC",
        "ACGCTGGGGGCACCGCCGGACCCCTGTTCTGGGGCTCCGCCTGGATGTCCACAAATGCTG",
        "AGGCCTGCTTGTGCATCTGGGGCACCCAGAGCCCGAAGCTCTGGGCAGGCTGATGAGAGG",
        "GCAGTGGGAATTCTGGAGCCTCGAGGGCCGCCTCCTCGGCCACCTTCTTAGCTTCTGGGT",
        "ATCCAGGTGGGAACCAGCAGGGAGCTGTGGCTCGCAACATCTTGCTGCCTTCGGGAGCAC",
        "CGGCCGGGCTCTGCTCCGCTCCCAAATGGCGGCTTGCCTCCGGGGCCGCCTCCTTGGCCA",
        "CCTTCTTAGCTTCTGGGTATCCAGGGCGGAACCAGCAGGGAGCTGTGGCTCGCAACATCT",
        "TGCTGCCTTCGGGAGCACCGGCCTGGCTCTGCTCCCCTCCCAAATGGCGGCTTGCCTCCA",
        "GGGCCGCCTCCTCGGCCACCTTCTTAGCTTCTGGGTATCCAGGGGGGAACCAGCAGGGAG",
        "CTGTGGCTCGCAACATCTTGCTGCCTTCGGGAGCACCGGCCTGGCTCTGCTCCTCTCCCA",
        "ACTGGCGGCTTCAATGAGTGCTGCGGCCGCCACTTGTCGCCTTTATATAGGCACAGGGCA",
        "GACTGGGTGGGACTTCTCCTTGATAGGTTGGTGCTTCAGTCCAATCACACTGAGCCTCAT");
    ## a low-complexity sequence with reverse complementation from chr22
    cat(file=fileName, sep="\n",
        ">T2T_1.0_chr22:20746801-20747600",
        "TATATGATATAGATTATATACTATATATGATATAAATGGTATATCATATATGATATAAAT",
        "GGTATATCATATATGATATATACAATATATCGTATATATGATATAGATGATATATCATCT",
        "ATAAGATATAGATGATATATCATCTATAAGATATAGATGATATATCATCTATAAGATATA",
        "GATGATATATCATCTATAAGATATAGATGATATATCATCTATAAGATATAGATGATATAT",
        "CATCTATAAGATATAGATGATATATCATACCTGATATAGGTGATATATCATACATGATAT",
        "AGGTGATAAATCATATATGATATAGATGATATATATCATATATGATATATCATATATTAT",
        "ATAATAAATGATATATATTATATATAATAAAAGCTATATATTACATAATAAATGATATAT",
        "ATTATGTAAAATATATGATATATATTATATATTATATCTGATATATATTATATATTATAT",
        "ATTATATATTTTATCTGATATATATTATATATTATATCTGATATATTATATATTATATTA",
        "TATATAATTATATTATATTATATTATATATTATATATTTTATATTATATTATATATAATT",
        "ATATTATATTATATTATATATTATATATAATTTATGATATATATAATGTATTATATATAA",
        "TTTATGATATATATTATATATTATAAATCATATATCATATATATTATGCTATATTATATA",
        "TAATATACCATAATATATCATATATTATATAATATATAATATAATATAATATAATGTAAT",
        "CATATATAATATATAATATA");
    prepareFastxFile(fileName, 17);
    system.time(loadNextSequence());
    cat(getSeqSig(), "\n");
    ## create repeat table
    print(system.time(hashRepeats()));
    system.time(countKmers(ordered=FALSE));
    ## showKmers(ordered=FALSE);
    ## pull out a few kmers: first, second, second-to-last, last
    cat("Showing forward kmers - first, second, second-to-last, last:\n");
    print(c(getKmerF(0), getKmerF(1),
            getKmerF(getSeqLen() - 18), getKmerF(getSeqLen() - 17)));
    cat("Showing complement kmers - first, second, second-to-last, last:\n");
    print(c(getKmerC(0), getKmerC(1),
            getKmerC(getSeqLen() - 18), getKmerC(getSeqLen() - 17)));
    cat("Showing reverse kmers - first, second, second-to-last, last:\n");
    print(c(getKmerR(0), getKmerR(1),
            getKmerR(getSeqLen() - 18), getKmerR(getSeqLen() - 17)));
    cat("Showing reverse complement kmers - first, second, second-to-last, last:\n");
    print(c(getKmerRC(0), getKmerRC(1),
            getKmerRC(getSeqLen() - 18), getKmerRC(getSeqLen() - 17)));
    cat("Showing forward comparison:\n");
    print(c(getKmerF(99), getKmerF(249), getKmerF(249)));
    cat("Forward    - "); print(eqKmer(99, 249, "F"));
    cat("Showing complement comparison:\n");
    print(c(getKmerF(470), getKmerF(783), getKmerC(783)));
    cat("Complement - "); print(eqKmer(470, 783, "C"));
    cat("Showing reverse comparison:\n"); ## TATATTATATATTATAT
    print(c(getKmerF(440), getKmerF(440), getKmerR(440)));
    cat("Reverse  - "); print(eqKmer(440, 440, "R"));
    cat("Showing reverse complement comparison:\n"); ## TATAGATGATATATCAT / ATGATATATCATCTATA
    print(c(getKmerF(101), getKmerF(106), getKmerC(106)));
    cat("Reverse Complement - "); print(eqKmer(101, 106, "RC"));
}

kmerTests <- function(){
    ## gzipped file (ideal)
    fileName <- "/home/grinja/bioinf/voluntary/repaver/Hsap/chm13.chr14.fasta.gz";
    ## plain text file (working)
    fileName <- "/home/grinja/bioinf/voluntary/repaver/Hsap/chr8_86-87M.fa";
    ##fileName <- "/mnt/ufds/chm13/chr8_86-87M.fa";
    smallSeqName <- "/home/grinja/bioinf/voluntary/repaver/Hsap/small_sequence.fa";
    ##smallSeqName <- "/mnt/ufds/chm13/small_sequence.fa";
    reallyLongFileName <- "/home/grinja/bioinf/voluntary/repaver/Hsap/chm13.chr9.fasta";
    ##fileName <- "/mnt/ufds/chm13/chr8_86-87M.fa";

    loadNextSequence(); ## should return false / 'Error: no file loaded'

    ## test loading of a long sequence
    prepareFastxFile(fileName, 17);
    system.time(loadNextSequence());
    getID();
    getSeqSig();
    ## pull out a few kmers: first, second, second-to-last, last
    c(getKmer(0), getKmer(1),
      getKmer(getSeqLen() - 18), getKmer(getSeqLen() - 17));
    ## beyond end
    ## [should return "" / 'Error: asked to retrieve kmer beyond sequence end']
    getKmer(getSeqLen() - 16);

    ## test Kmer equality
    prepareFastxFile(fileName, 3);
    system.time(loadNextSequence());
    getID();
    c(getKmer(60), getKmer(120), getKmer(180));
    c(ltKmer(60, 120), ltKmer(120, 180), ltKmer(180, 60));
    c(eqKmer(60, 120), eqKmer(120, 180), eqKmer(180, 60));

    ## test loading of a short sequence
    prepareFastxFile(smallSeqName, 17);
    loadNextSequence();
    getSeqSig();

    ## [should return "" / 'Error: asked to retrieve kmer beyond sequence end']
    getKmer(0);

    prepareFastxFile(smallSeqName, 1);
    loadNextSequence();
    getSeqSig();
    testHash(4);
    system.time(countKmers());
    showKmers();


    prepareFastxFile(fileName, 1);
    loadNextSequence();
    getSeqSig();
    testHash(159) == testHash(1); ## These should be equal
    system.time(countKmers());
    showKmers();
    system.time(countKmers(ordered=FALSE));
    showKmers(ordered=FALSE);

    prepareFastxFile(fileName, 2);
    loadNextSequence();
    getSeqSig();
    testHash(1055) == testHash(4); ## These should be equal
    testHash(319) == testHash(60); ## These should be equal
    system.time(countKmers());
    showKmers();
    system.time(countKmers(ordered=FALSE));
    showKmers(ordered=FALSE);

    prepareFastxFile(fileName, 2);
    loadNextSequence();
    verboseHash(1055); ## should return 6
    verboseHash(319); ## should return 10

    prepareFastxFile(fileName, 3);
    loadNextSequence();
    getSeqSig();
    testHash(28479) == testHash(15966); ## These should be equal
    system.time(countKmers());
    showKmers(limit=16);
    system.time(countKmers(ordered=FALSE));
    showKmers(ordered=FALSE, limit=16);

    prepareFastxFile(fileName, 3);
    loadNextSequence();
    verboseHash(28479); ## should return 52
    verboseHash(15966); ## should return 52

    prepareFastxFile(fileName, 3043);
    loadNextSequence();
    getSeqSig();
    system.time(countKmers());
    system.time(countKmers(ordered=FALSE));
    verboseHash(931831); ## should return 1.140949e+19?
    verboseHash(679663); ## should return 1.837129e+19?

    prepareFastxFile(fileName, 31);
    loadNextSequence();
    getSeqSig();
    verboseHash(946278); ## should return 12858280553
    system.time(countKmers());
    system.time(countKmers(ordered=FALSE));

    prepareFastxFile(fileName, 50);
    loadNextSequence();
    getSeqSig();
    verboseHash(946278); ## should return 12858280553
    system.time(countKmers());
    system.time(countKmers(ordered=FALSE));

    sourceCpp("kmerFetcher.cc");

    prepareFastxFile(fileName, 50);
    makeRepeatBed(file.path(dirname(fileName), "out.bed"));
    loadNextSequence();
    getSeqSig();
    system.time(hashRepeats());
    ## Check with a bad file
    makeRepeatBed("/example.dir/out.bed");
    makeRepeatBed(file.path(dirname(fileName), "out.bed"));

    ## Check hashing, then determining repeats with a bad file
    prepareFastxFile("/example.dir/out.bed", 50);
    loadNextSequence();
    system.time(hashRepeats());
    makeRepeatBed(file.path(dirname(longFileName), "out.bed"));


    ## Check hashing, then determining repeats with a bad file
    prepareFastxFile(reallyLongFileName, 50);
    loadNextSequence();
    getSeqSig();
    system.time(hashRepeats());
    makeRepeatBed(file.path(dirname(longFileName), "out.bed"));
}

if(doTests){
    complementTests();
}
