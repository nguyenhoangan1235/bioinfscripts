#!/usr/bin/env python3

"""Script to find all possible probe sequences covering a target sequence; checking
for melt, GC clamp, and tertiary structures [without kmer duplication]

Note: this uses the primer3 python bindings; install via

 'pip install primer3-py'

or your alternative preferred method
"""

from primer3 import calcTm, calcHomodimer, calcHairpin # primer Tm calculations
import re # excluding homopolymer stretches
import fileinput # for processing from stdin
import sys # for error messages
import random # for randomising primer start locations
import getopt # for command-line argument parsing
import copy # for backtracking
from collections import defaultdict # for dicts of dicts
from timeit import default_timer as timer # for timing progress

rcTransTab = str.maketrans('ACGTUYRSWMKDVHBXN-acgtuyrswmkdvhbxn',
                           'TGCAARYSWKMHBDVXN-tgcaaryswkmhbdvxn')

excludeKmers = set()

def rc(seq):
  return ((seq[::-1]).translate(rcTransTab))

def primer3_check(ps, minLength=23, rcPrimer=False):
  if(rcPrimer):
    ps = rc(ps)
  tooLong = False
  ## check for GC clamp in last 4 bases
  if(not(("G" in ps[-4:]) or ("C" in ps[-4:]))):
    sys.stderr.write("Failed GC clamp\n")
    return ""
  ## exclude sequences with N (e.g. concatenated viromes)
  if("N" in ps):
    sys.stderr.write("Failed (has N)\n")
    return ""
  ## check/exclude homopolymer stretches of >=4
  if(re.search(r'([ACGT])\1{3,}', ps) is not None):
    sys.stderr.write("Failed homopolymer\n")
    return ""
  ## see primalscheme/settings.py
  ## [github.com/aresti/primalscheme/blob/master/primalscheme/settings.py]
  ## Tm too low (needs to be longer)
  if(calcTm(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6) < 60):
    sys.stderr.write("Tm too low\n")
    return ""
  ## exclude kmers from pre-generated exclusion set
  if(len(excludeKmers) > 0):
    for pi in range(0, len(ps) - kmerCheckSize + 1):
      kmer = ps[pi:(pi+kmerCheckSize)]
      if(kmer in excludeKmers):
        sys.stderr.write("Contains forbidden kmer\n")
        return ""
  ## Tm too high (needs to be shorter)
  if(calcTm(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6) > 63):
    tooLong = True
    #sys.stderr.write("Tm too high -> ")
    ps = ps[1:]
    tm = calcTm(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6)
    while((len(ps) >= minLength) and (tm > 62)):
      ## Make shorter, but preserve end
      ps = ps[1:]
      tm = calcTm(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6)
    if((tm > 63) or (tm < 60)):
      sys.stderr.write("Tm out of range\n")
      return ""
    if(len(ps) < minLength):
      sys.stderr.write("Length too short\n")
      return ""
    #sys.stderr.write("Tm restored to %0.1f (%d bp)" % (tm, len(ps)))
  ## check homodimers and hairpins
  if(calcHomodimer(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6).tm > 47):
    sys.stderr.write("; homodimer fail\n")
    return ""
  if(calcHairpin(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6).tm > 47):
    sys.stderr.write("; hairpin fail\n")
    return ""
  if(calcHomodimer(rc(ps), mv_conc=50, dv_conc=1.5, dntp_conc=0.6).tm > 47):
    sys.stderr.write("; homodimer (RC) fail\n")
    return ""
  if(calcHairpin(rc(ps), mv_conc=50, dv_conc=1.5, dntp_conc=0.6).tm > 47):
    sys.stderr.write("; hairpin (RC) fail\n")
    return ""
  if(tooLong):
    pass
    #sys.stderr.write("; Success!\n")
  return ps

def processSeq(seqID, shortID, seq, qual, containsFQ):
  return(dict({'seqID': seqID, 'shortID': shortID,
               'seq': seq, 'qual': qual}))

def nextFastx():
  inQual = False
  seqID = ""
  shortID = ""
  qualID = ""
  seq = ""
  qual = ""
  containsFQ = False
  for line in fileinput.input():
    line = line.rstrip()
    if(not inQual):
      if((line[0] == '>') or (line[0] == '@')):
        m = re.search(r'^(>|@)((.+?)( .*?\s*)?)$', line)
        newSeqID = m.group(2)
        newShortID = m.group(3)
        if(len(seqID) > 0):
          yield(processSeq(seqID, shortID, seq, qual, containsFQ))
        seqID = newSeqID
        shortID = newShortID
        seq = ""
        qual = ""
      elif(line[0] == '+'):
        m = re.search(r'\+(.*)', line)
        inQual = True
        containsFQ = True
        qualID = m.group(1)
      else:
        if(('@' in line) or ('>' in line) or ('\0' in line)):
          sys.stderr.write(("Warning: corrupt sequence found at %s " +
                            "[header or NUL in sequence string]\n") % (shortID))
          seqID = ""
          shortID = ""
          qualID = ""
          seq = ""
          qual = ""
          continue
        else:
          seq += line
    else:
      qual += line
      if(len(qual) > (len(seq) + 2)):
        sys.stderr.write(("Warning: corrupt sequence found at %s " +
                          "[quality string too long]\n") % (shortID))
        seqID = ""
        shortID = ""
        qualID = ""
        seq = ""
        qual = ""
        inQual = False
        continue
      elif(len(qual) >= len(seq)):
        inQual = False
  if(len(seqID) > 0):
    yield(processSeq(seqID, shortID, seq, qual, containsFQ))

def usage():
  sys.stderr.write("findProbes.py - finds reasonable probe sequences covering a target sequence\n\n")
  sys.stderr.write("usage: findProbes.py [options] sequence.fa\n\n")
  sys.stderr.write("additional options: \n")
  sys.stderr.write("  -h [--help]           - usage information\n")
  sys.stderr.write("  -x [--max=X]          - maximum primer length (default 50)\n")
  sys.stderr.write("  -n [--min=X]          - minimum primer length (default 23)\n")
  sys.stderr.write("  -r [--reverse-comp]   - reverse-complement primers\n")
  sys.stderr.write("  -e [--exclude]        - pre-exclude F and RC kmers from sequence X\n")
  sys.stderr.write("  -v [--verbose]        - increase message verbosity\n")
  sys.stderr.write("\n")

## Option parameters
maxLength = 50       # maximum primer length
minLength = 20       # minimum primer length
verbosity = 0        # include verbose output
reverseComp = False  # reverse-complement primers? (needed for some protocols)
excludeSeqs = list() # list of sequences to pre-filter candidate primers
kmerCheckSize = 6    # length of kmers to check (for sequence pre-filtering)

import sys

argv = sys.argv[1:]

opts, args = getopt.getopt(argv, "x:n:e:k:vhr",
                           ["max=", "min=", "exclude=",
                            "kmerSize=", "kmer-size=",
                            "reverse-comp", "help"])

if(len(argv) == 0):
  sys.stderr.write("Error: no command-line arguments specified\n\n")
  usage()
  exit(0)

for o, a in opts:
  if(o in("-h", "--help")):
    usage()
    exit(0)
  if(o in("-x", "--max")):
    maxLength = int(a)
  elif(o in ("-n", "--min")):
    minLength = int(a)
  elif(o in ("-k", "--kmerSize", "--kmer-size")):
    kmerCheckSize = int(a)
  elif(o in ("-r", "--reverse-comp")):
    reverseComp = True
  elif(o in ("-e", "--exclude")):
    excludeSeqs.append(a)
  elif(o in ("-v", "--verbose")):
    verbosity += 1

sys.argv[1:] = args

for seq in excludeSeqs:
  for pi in range(0, len(seq) - kmerCheckSize + 1):
    kmer = seq[pi:(pi+kmerCheckSize)]
    if(not ('N' in kmer)):
      excludeKmers.add(rc(kmer))
      excludeKmers.add(kmer)

for seqRecord in nextFastx():
  primersAdded = dict({"F": dict(), "RC": dict()})
  if(len(seqRecord["seq"]) > 0):
    if(verbosity > 0):
      sys.stderr.write("Sequence: %s\n" % (seqRecord["shortID"]))
    seq = seqRecord["seq"]
    if(verbosity > 0):
      sys.stderr.write("Finding potential probe sequences\n")
    searchStart = 0
    searchEnd = len(seq)
    poss = list(range(searchStart, searchEnd))
    refPoss = set()
    potentialPrimers = dict({"F" : defaultdict(dict),
                             "RC" : defaultdict(dict)})
    foundPrimers = dict({"F": 0, "RC": 0})
    possDone = 0
    lastPctDone = 0
    if(verbosity > 0):
      startTime = timer()
      sys.stderr.write("|--------10--------20--------30--------40--------50" +
                       "--------60--------70--------80--------90-------100|\n")
      sys.stderr.write("|")
      sys.stderr.flush()
    for si in poss:
      sys.stderr.write("pos: %d\n" % si)
      dirn = ("RC" if reverseComp else "F")
      testPrimer = primer3_check(seq[si:(si+maxLength)],
                                 minLength, rcPrimer=(dirn == "RC"))
      if(len(testPrimer) >= minLength):
        foundPrimers[dirn] += 1
        primersAdded[dirn][si] = testPrimer
      possDone += 1
      while(int(possDone * 100 / len(poss)) > lastPctDone):
        lastPctDone += 1
        if(verbosity > 0):
          if(foundInRange):
            sys.stderr.write("*")
          else:
            sys.stderr.write(".")
          sys.stderr.flush()
    while(100 > lastPctDone):
      lastPctDone += 1
      if(verbosity > 0):
        if(foundInRange):
          sys.stderr.write("*")
        else:
          sys.stderr.write(".")
        foundInRange = False
    if(verbosity > 0):
      sys.stderr.write("|\n")
    if(verbosity > 0):
      sys.stderr.write("Found %d F and %d RC potential primer sites in %0.2f s\n" %
                       (foundPrimers["F"], foundPrimers["RC"], (timer() - startTime)))
  nextDir = dict({"F": "RC", "RC": "F"})
  for dirn in sorted(primersAdded):
    primers = primersAdded[dirn]
    probeCount = 1
    lastPos = 0
    direction = "F" if (not reverseComp) else "RC"
    for pos in sorted(primers):
      primer = primers[pos]
      tm = calcTm(primer, mv_conc=50, dv_conc=1.5, dntp_conc=0.6)
      gcCount = 0
      for c in primer:
        if (c in ('G', 'C', 'g', 'c', 'S', 's')):
          gcCount += 1
      gcPct = (gcCount * 100) / len(primer)
      sys.stdout.write(">%s_%02d_%s len=%d pos=%d gcPct=%d tm=%0.1f gapLen=%d\n%s\n" %
                       (seqRecord["shortID"], probeCount, dirn,
                        len(primer), pos, gcPct, tm, (pos - lastPos),
                        primer))
      lastPos = pos
      probeCount += 1
