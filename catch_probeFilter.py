"""Function to check for melt, GC clamp, and tertiary structures

This uses primer3
"""

from primer3 import calcTm, calcHomodimer, calcHairpin
import re

rcTransTab = str.maketrans('ACGTUYRSWMKDVHBXN-acgtuyrswmkdvhbxn',
                           'TGCAARYSWKMHBDVXN-tgcaaryswkmhbdvxn')

def rc(seq):
  return ((seq[::-1]).translate(rcTransTab))

def tertiary_check(probeSeq, targSeq, probeAnchorStart,
                   probeAnchorEnd, probeLen, targLen):
  ## check for GC clamp in last 4 bases
  if(not(("G" in probeSeq[-4:]) or ("C" in probeSeq[-4:]))):
    return None;
  ps = "".join(probeSeq)
  ## check/exclude homopolymer stretches of >4
  if(re.search(r'([ACGT])\1{3,}', ps) is not None):
    return None
  ## see primalscheme/settings.py
  ## [github.com/aresti/primalscheme/blob/master/primalscheme/settings.py]
  if((calcTm(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6) > 63) or
     (calcTm(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6) < 60)):
    return None
  if(calcHomodimer(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6).tm > 47):
    return None
  if(calcHairpin(ps, mv_conc=50, dv_conc=1.5, dntp_conc=0.6).tm > 47):
    return None
  if(calcHomodimer(rc(ps), mv_conc=50, dv_conc=1.5, dntp_conc=0.6).tm > 47):
    return None
  if(calcHairpin(rc(ps), mv_conc=50, dv_conc=1.5, dntp_conc=0.6).tm > 47):
    return None
  return((0, len(probeSeq)))
