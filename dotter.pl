#!/usr/bin/perl

use warnings;
use strict;

my $refString = "";
my $diffThreshold = 10;

while(<>){
  chomp;
  chomp;
  my $nextString = $_;
  my $modString = $nextString;
  if(/^$/){
    $refString = "";
    print("\n");
  } elsif (!$refString) {
    print($nextString."\n");
    $refString = $nextString;
  } else {
    my @nextString = split(//, $nextString);
    my @refString = split(//, $refString);
    my $diffCount = 0;
    for(my $i = 0; $i < scalar(@nextString); $i++){
      if(($refString[$i]) && ($refString[$i] !~ /[ 0-9]/) && ($refString[$i] eq $nextString[$i])){
        substr($modString, $i, 1, ".");
      } else {
        $diffCount++;
      }
    }
    if($diffCount > $diffThreshold){
      $modString .= " <New Reference>";
      $refString = $nextString;
    }
    print($modString."\n");
  }
}
