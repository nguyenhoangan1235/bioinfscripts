// Parametric letter module
// David Eccles (gringer) <bioinformatics@gringene.org>

$fn=30;

// get [letter] Width
function gW(val) =
  (val < 65) ? // pre-letter symbols
    ((val == 39) || // '
     (val == 44) || // ,
     (val == 46)) ? 1 : // .
    ((val == 32) || // [space]
     (val == 33) || // !
     (val == 58) || // :
     (val == 59)) ? 2 : // ;
    ((val == 34) || // "
     (val == 40) || // (
     (val == 41)) ? 3.5 : // )
    (val == 63) ? 5 : // ?
    (val == 38) ? 6 : // &
    6.5 :
  (val < 97) ? // upper-case letters
    ((val == 73)) ? 2 : // I
    ((val == 91) || // [
     (val == 93) || // ]
     (val == 96)) ? 3 : // `
    ((val == 74) || // J
     (val == 69) || // E
     (val == 94)) ? 6 : // ^
    ((val == 68) || // D
     (val == 92) || // \ 
     (val == 95)) ? 6.5 : // _
    ((val == 71) || // G
     (val == 83) || // S
     (val == 85)) ? 7 : // U
    ((val == 66) || // B
     (val == 67) || // C
     (val == 72) || // H
     (val == 75) || // K
     (val == 78) || // N
     (val == 80) || // P
     (val == 87) || // W
     (val == 90)) ? 7.5 : // Z
    8 :
  (val < 127) ? // lower-case letters
    ((val == 105)) ? 1 : // i
    ((val == 124)) ? 2 : // |
    ((val == 108)) ? 2.5 : // l
    ((val == 116) || // t
     (val == 123) || // {
     (val == 125)) ? 3 : // }
    ((val == 106)) ? 4 : // j
    ((val == 102) || // f
     (val == 114)) ? 4.5 : // r
    ((val == 100) || // d
     (val == 117)) ? 6.5 : // u
    6 :
  ((val == 207) || // Ï
   (val == 298)) ? 3 : // Ī
  ((val == 203) || // Ë
   (val == 274)) ? 6 : // Ē
  ((val == 214) || // Ö
   (val == 220) || // Ü
   (val == 332) ||  // Ō
   (val == 362)) ? 7 : // Ū
  ((val == 196) || // Ä
   (val == 256)) ? 8 : // Ā
  ((val == 239) || // ï
   (val == 299)) ? 3 : // ī
  ((val == 228) || // ä
   (val == 235) || // ë
   (val == 246) || // ö
   (val == 252) || // ü
   (val == 257) || // ā
   (val == 275) || // ē
   (val == 333) || // ō 
   (val == 363)) ? 6 : // ū
  6.5;

function sumLwds(A,start,end,value=0) =
  (start > end) ? 0 : (start == end ? value + gW(A[start]) : 
    sumLwds(A,start+1,end,value+gW(A[start])));

function makeSegment(start, end, addEnd=false) =
  let(segLen = sqrt((end[0]-start[0])*(end[0]-start[0]) + 
                    (end[1]-start[1])*(end[1]-start[1])),
      steps = ceil(segLen / (10 / ($fn))),
      resPoints = [for (i = [0 : (steps-1)])
                   start + (end-start) * (i / steps) ])
      addEnd ? concat(resPoints, [end]) : resPoints;

function arbLineTo(start, end, sAng, eAng, th) =
  let(lineAng = atan2(end[1]-start[1], end[0]-start[0]),
      ofsAng = lineAng + 90)
  concat(
    makeSegment([start[0], start[1]], [end[0]-th, end[1]]),
    makeSegment([end[0]-th, end[1]],  [end[0], end[1]]),
    makeSegment([end[0], end[1]], [start[0]+th, start[1]]),
    makeSegment([start[0]+th, start[1]], [start[0], start[1]]));


function vLineTo(start, end, th) =
  concat(
    makeSegment([start[0], start[1]], [end[0]-th, end[1]]),
    makeSegment([end[0]-th, end[1]],  [end[0], end[1]]),
    makeSegment([end[0], end[1]], [start[0]+th, start[1]]),
    makeSegment([start[0]+th, start[1]], [start[0], start[1]]));

function hLineTo(start, end, th) =
  concat(
    makeSegment([start[0], start[1]], [start[0], start[1]+th]),
    makeSegment([start[0], start[1]+th],  [end[0], end[1]]),
    makeSegment([end[0], end[1]], [end[0], end[1]-th]),
    makeSegment([end[0], end[1]-th], [start[0], start[1]]));

  //[[start[0], start[1]], [start[0], start[1]+th],
//   [end[0], end[1]], [end[0], end[1]-th]];

function arc(arcCentre, w, h, thFrom, thToMod, thickness) =
  let(thTo=(thToMod < thFrom) ? (thToMod + 360) : thToMod,
      steps=ceil(abs(thTo-thFrom) / (360 / ($fn*2))),
      incAng=(thTo-thFrom)/steps,
      th=thickness/2,
      wr=w/2,
      hr=h/2)
  (((thickness >= w/2) || (thickness >= h/2)) && (thTo-thFrom > 359)) ?
   [ for(theta = [thTo : -incAng : thFrom])
        [(wr) * cos(theta) + arcCentre[0],
         (hr) * sin(theta) + arcCentre[1]] ] :
    concat(
      [ for(theta = [thTo : -incAng : thFrom])
        [(wr) * cos(theta) + arcCentre[0],
         (hr) * sin(theta) + arcCentre[1]] ],
      [ for(theta = [thFrom : incAng : thTo])
        [(wr-thickness) * cos(theta) + arcCentre[0],
         (hr-thickness) * sin(theta) + arcCentre[1]] ],
      []);

function strLen(id, gap=0.5) =
    (len(id) == 0) ? 0 :
      let(cslWidths = cumSum([for (l = id) gW(l) + gap]))
      cslWidths[len(cslWidths) - 1];

function ltrPointsArr(id, tk=1.5, gap=0.5) =
    let(cslWidths = cumSum([for (l = id) gW(l) + gap]))
    flatten(
      [for(i = [0:(len(id) - 1)]) 
        let(lpp = (ltrPoints(id[i], tk=tk)))
        [ for(lp = lpp)
          [ for(p = lp) p + [cslWidths[i],0] ] ] ]);

function ltrPoints(id, tk=1.5) =
  (id == 33) ? // !
    concat(
          [arc([1, 10-(tk+1)/2], tk*2, tk*2, 0, 180, tk)],
          [vLineTo([1-tk/2,2], [1, 10-(tk+1)/2], tk)],
          [vLineTo([1-tk/2,2], [1+tk, 10-(tk+1)/2], tk)],
          [arc([1, 1], tk*13/12, tk*13/12, 0, 360, tk)]) :
  (id == 34) ? // "
    concat(
          [vLineTo([1-tk/4,6], [1, 9], tk/2)],
          [vLineTo([1-tk/4,6], [1+tk/2, 9], tk/2)],
          [vLineTo([2.5-tk/4,6], [2.5, 9], tk/2)],
          [vLineTo([2.5-tk/4,6], [2.5+tk/2, 9], tk/2)]) :
  (id == 35) ? // #
    concat(
      [vLineTo([0.5,1], [1.5+tk,9], tk)],
      [vLineTo([5-tk,1], [6,9], tk)],
      [hLineTo([0,6.5-tk/2], [6.5,6.5+tk/2], tk)],
      [hLineTo([0,3.5-tk/2], [6.5,3.5+tk/2], tk)]) :
  (id == 36) ? // $
    concat(
      [vLineTo([3.5-tk/2,0], [3.5+tk/2,10], tk)],
      [arc([3.5, 9-(4+tk/2)/2], 6.5, 4+tk/2, 30, 270, tk)],
      [arc([3.5, 1+(4+tk/2)/2], 6.5, 4+tk/2, 210, 90, tk)]) :
  (id == 37) ? // %
    concat(
      [arc([1.5, 7.5], 3, 3, 0, 360, tk)],
      [arc([5, 2.5], 3, 3, 0, 360, tk)],
      [vLineTo([1-tk/2,1], [5.5+tk/2,9], tk*1.25)]) :
  (id == 38) ? // & [tricky; needs work]
    concat(
      [arc([5.5, 7], 10, 12, 180, 270, tk)],
      [arc([2.5, 7], 4, 4, 0, 180, tk)],
      [arc([2.5, 7], 4, 4.5, 270, 0, tk)],
      [arc([2.5, 3], 4, 3.5+tk*2, 90, 180, tk)],
      [arc([2.5, 3], 4, 4, 180, 270, tk)],
      [arc([2.5, 7], 8, 12, 270, 315, tk)]) :
  (id == 39) ? // '
    concat(
          [vLineTo([0.5-tk/4,6], [0.5, 9], tk/2)],
          [vLineTo([0.5-tk/4,6], [0.5+tk/2, 9], tk/2)]) :
  (id == 40) ? // (
    concat(
      [arc([3.25, 5], 6.5, 10, 90, 270, tk)]) :
  (id == 41) ? // )
    concat(
      [arc([0, 5], 6.5, 10, 270, 90, tk)]) :
  (id == 42) ? // *
    concat(
      [hLineTo([6,3], [0.5,7], tk*13/12)],
      [hLineTo([0.5,3], [6,7], tk*13/12)],
      [vLineTo([3.25-tk/2, 2], [3.25+tk/2, 8], tk)]) :
  (id == 43) ? // +
    concat(
      [vLineTo([3.25-tk/2,1.75], [3.25+tk/2, 8.25], tk)],
      [hLineTo([0,5-tk/2], [6.5, 5+tk/2], tk)]) :
  (id == 44) ? // ,
    concat(
      [vLineTo([0-tk/4,-1.5], [1, 1.5], tk/2)],
      [vLineTo([0-tk/4,-1.5], [1+tk/2, 1.5], tk/2)]) :
  (id == 45) ? // -
    concat(
      [hLineTo([0.5,5-tk/2], [6, 5+tk/2], tk)]) :
  (id == 46) ? // .
    concat(
          [hLineTo([0,0], [tk, tk], tk)]) :
  (id == 47) ? // /
    concat(
          [vLineTo([0,0], [6.5, 10], tk*13/12)]) :
  (id == 48) ? // O
    concat(
      [vLineTo([6.5-tk,3.499], [6.5,6.501], tk)],
      [vLineTo([0,3.5], [tk,6.5], tk)],
      [hLineTo([0+tk,3.5], [6.5-tk,6.5], tk)],
      [arc([3.25, 6.5], 6.5, 7, 0, 180, tk)],
      [arc([3.25, 3.5], 6.5, 7, 180, 0, tk)]) :
  (id == 49) ? // 1
    concat(
      [vLineTo([1.5,7], [(6.5+tk)/2,10], tk)],
      [vLineTo([(6.5-tk)/2,0], [(6.5+tk)/2,10], tk)],
      [hLineTo([1.5,0], [5,tk], tk)]) :
  (id == 50) ? // 2
    concat(
      [hLineTo([tk/2,0], [6.5,tk], tk)],
      //[vLineTo([0,tk], [3.25+3.25*cos(45),6.5-3.5*sin(45)], tk)],
      [arc([3.25, 6.5], 6.5, 7, 270, 180, tk)],
      [arc([3.25, 0], 6.5, (3+tk)*2, 90, 180, tk)]) :
  (id == 51) ? // 3
    concat(
      [arc([3.25, 10-(10+tk)/4], 6.5, (10+tk)/2, 270, 180, tk)],
      [hLineTo([3,5-tk/2], [3.5,5+tk/2], tk)],
      [arc([3.25, (10+tk)/4], 6.5, (10+tk)/2, 180, 90, tk)]) :
  (id == 52) ? // 4
    concat(
      [vLineTo([5-tk,0], [5,10], tk)],
      [hLineTo([0,3.5-tk/2], [6.5,3.5+tk/2], tk)],
      [vLineTo([0,3.5+tk/2], [5,10], tk)]) :
  (id == 53) ? // 5
    concat(
      [vLineTo([0,7-tk], [tk,10], tk)],
      [hLineTo([0,6.5-tk], [3.5,6.5], tk)],
      [hLineTo([tk/2,10-tk], [6,10], tk)],
      [arc([3.25, 6.5/2], 6.5, 6.5, 180, 90, tk)]) :
  (id == 54) ? // 6
    concat(
      [vLineTo([0,3.5], [tk,7], tk)],
      [arc([3.25, 7], 6.5, 6, 0, 180, tk)],
      [arc([3.25, 3], 6.5, 6, 0, 360, tk)]) :
  (id == 55) ? // 7
    concat(
      [hLineTo([0,10-tk], [6.5,10], tk)],
      [vLineTo([0,0], [6.5,10-tk], tk*13/12)]) :
  (id == 56) ? // 8
    concat(
      [arc([3.25, 10-(10+tk)/4], 6.5, (10+tk)/2, 0, 360, tk)],
      [arc([3.25, (10+tk)/4], 6.5, (10+tk)/2, 0, 360, tk)]) :
  (id == 57) ? // 9
    concat(
      [vLineTo([6.5-tk,0], [6.5,6.501], tk)],
      [arc([3.25, 10-(10+tk)/4], 6.5, (10+tk)/2, 0, 360, tk)]) :
  (id == 58) ? // :
    concat(
      [hLineTo([1-tk/2,3.5-tk/2], [1+tk/2, 3.5+tk/2], tk)],
      [hLineTo([1-tk/2,6.5-tk/2], [1+tk/2, 6.5+tk/2], tk)]) :
  (id == 59) ? // ;
    concat(
      [vLineTo([0-tk/4,1.5], [1, 3.5+tk/2], tk/2)],
      [vLineTo([0-tk/4,1.5], [1+tk/2, 3.5+tk/2], tk/2)],
      [hLineTo([1-tk/2,6.5-tk/2], [1+tk/2, 6.5+tk/2], tk)]) :
  (id == 60) ? // <
    concat(
      [hLineTo([0.5,5-tk/2], [6,9], tk*13/12)],
      [hLineTo([6,1], [0.5,5+tk/2], tk*13/12)]) :
  (id == 61) ? // =
    concat(
      [hLineTo([0,6.5-tk/2], [6.5,6.5+tk/2], tk)],
      [hLineTo([0,3.5-tk/2], [6.5,3.5+tk/2], tk)]) :
  (id == 62) ? // >
    concat(
      [hLineTo([6,5-tk/2], [0.5,9], tk*13/12)],
      [hLineTo([0.5,1], [6,5+tk/2], tk*13/12)]) :
  (id == 63) ? // ?
    concat(
      [arc([2.5, 6.5], 5, 5, 0, 180, tk)],
      [arc([(7.5-tk/2)/2, 2.5+tk], 2.5+tk/2, 4, 90, 180, tk)],
      [arc([(7.5-tk/2)/2, 6.5], 2.5+tk/2, 4, 270, 0, tk)],
      [vLineTo([2.5-tk/2,2], [2.5+tk/2,4+tk-1.5], tk)],
      [arc([2.5, 1], tk*13/12, tk*13/12, 0, 360, tk)]) :
  (id == 64) ? // @
    concat(
          [arc([3.25, 5], 3.5, 4.5, 0, 360, tk)],
          [arc([(11.5 - tk)/2, 5], (6.5 - (11.5 - tk)/2)*2, 5, 180, 360, tk)],
          [arc([3.25, 5], 6.5, 8, 0, 300, tk)]) :
  (id == 65) ? // A
    concat(
      [vLineTo([0,0], [4+tk*13/24,10], tk * 13/12)],
      [vLineTo([8-tk*13/12,0], [4+tk*13/24, 10], tk * 13/12)],
      [hLineTo([(4-tk)*(2.5+tk/2)/10+tk/2,2.5], 
               [8-((4-tk)*(2.5+tk/2)/10+tk/2),2.5+tk], tk)]) :
  (id == 66) ? // B
    concat(
      [vLineTo([0,0], [tk,10], tk)],
      [hLineTo([0,0], [4.75,tk], tk)],
      [hLineTo([0,4.5],  [4.75,4.5+tk], tk)],
      [hLineTo([0,10-tk], [4.75,10], tk)],
      [arc([4.5, 7.25], 5.5, 5.5, 270, 90, tk)],
      [arc([4.5, (4.5+tk)/2], 6, 4.5+tk, 270, 90, tk)]) :
  (id == 67) ? // C
    concat(
      [vLineTo([0,3.5], [tk,6.5], tk)],
      [arc([3.75, 6.5], 7.5, 7, 0, 180, tk)],
      [arc([3.75, 3.5], 7.5, 7, 180, 0, tk)]) :
  (id == 68) ? // D
    concat(
      [vLineTo([6.5-tk,3.499], [6.5,6.501], tk)],
      [vLineTo([0,0], [tk,10], tk)],
      [hLineTo([tk,10-tk], [3.25, 10], tk)],
      [hLineTo([tk,0], [3.25, tk], tk)],
      [arc([3.25, 6.5], 6.5, 7, 0, 90, tk)],
      [arc([3.25, 3.5], 6.5, 7, 270, 0, tk)]) :
  (id == 69) ? // E
    concat(
      [vLineTo([0,0], [tk,10], tk)],
      [hLineTo([0,0], [6,tk], tk)],
      [hLineTo([0,5-tk/2],  [4.75,5+tk/2], tk)],
      [hLineTo([0,10-tk], [6,10], tk)]) :
  (id == 70) ? // F
    concat(
      [vLineTo([0,0], [tk,10], tk)],
      [hLineTo([0,5-tk/2],  [4.75,5+tk/2], tk)],
      [hLineTo([0,10-tk], [6,10], tk)]) :
  (id == 71) ? // G
    concat(
      [vLineTo([0,3.5], [tk,6.5], tk)],
      [hLineTo([3.5, 3.5], [7,3.5+tk], tk)],
      [arc([3.5, 6.5], 7, 7, 30, 180, tk)],
      [arc([3.5, 3.5], 7, 7, 180, 0, tk)]) :
  (id == 72) ? // H
    concat(
      [vLineTo([0,0], [tk,10], tk)],
      [vLineTo([7.5-tk,0], [7.5,10], tk)],
      [hLineTo([0,5-tk/2], [7.5,5+tk/2], tk)]) :
  (id == 73) ? // I
    [vLineTo([1-tk/2,0], [1+tk/2,10], tk)] :
  (id == 74) ? // J
    concat(
      [vLineTo([6-tk,3.5], [6,10], tk)],
      [arc([3, 3.5], 6, 7, 180, 0, tk)]) :
  (id == 75) ? // K
    concat(
      [vLineTo([0,0], [tk,10], tk)],
      [vLineTo([tk*13/24,5], [7.5,10], tk*13/12)],
      [vLineTo([7.5-tk*13/12,0], [tk*(1+13/24),5], tk*13/12)]) :
  (id == 76) ? // L
    concat(
      [vLineTo([0,0], [tk,10], tk)],
      [hLineTo([0,0], [5.5,tk], tk)]) :
  (id == 77) ? // M
    concat(
      [vLineTo([0,0], [tk,10], tk)],
      [vLineTo([7.5-tk,0], [7.5,10], tk)],
      [vLineTo([7.5/2-tk*13/24,4], [tk*13/12,10], tk*13/12)],
      [vLineTo([7.5/2-tk*13/24,4], [7.5,10], tk*13/12)]) :
//      [hLineTo([3,5-tk*4/6], [4.5,5+tk*4/6], tk * 4/3)]) :
  (id == 78) ? // N
    concat(
      [vLineTo([0,0], [tk,10], tk)],
      [vLineTo([7.5-tk,0], [7.5,10], tk)],
      [vLineTo([7.5-tk*13/12,0], [tk*13/12,10], tk*13/12)]) :
  (id == 79) ? // O
    concat(
      [arc([4, 5], 8, 10, 0, 360, tk)]) :
  (id == 80) ? // P
    concat(
      [vLineTo([0,0], [tk,10], tk)],
      [hLineTo([0,4],  [4.75,4+tk], tk)],
      [hLineTo([0,10-tk], [4.75,10], tk)],
      [arc([4.5, 7], 6, 6, 270, 90, tk)]) :
  (id == 81) ? // Q
    concat(
      [arc([4, 5], 8, 10, 0, 360, tk)],
      [vLineTo([8-tk*13/24,0], [4+tk*13/24,4], tk*13/12)]) :
  (id == 82) ? // R
    concat(
      [vLineTo([0,0], [tk,10], tk)],
      [hLineTo([0,4], [4.75,4+tk], tk)],
      [hLineTo([0,10-tk], [4.75,10], tk)],
      [arc([4.5, 7], 6, 6, 270, 90, tk)],
      [vLineTo([7 - tk*13/12,0], [4.5+tk*13/24,4.5], tk*13/12)]) :
  (id == 83) ? // S
    concat(
      [arc([3.5, 10-(5+tk/2)/2], 7, 5+tk/2, 0, 270, tk)],
      [arc([3.5,    (5+tk/2)/2], 7, 5+tk/2, 180, 90, tk)]) :
  (id == 84) ? // T
    concat(
      [vLineTo([4-tk/2,0], [4+tk/2,10], tk)],
      [hLineTo([0,10-tk], [8,10], tk)]) :
  (id == 85) ? // U
    concat(
      [vLineTo([0,3.499], [tk,10], tk)],
      [arc([3.5, 3.5], 7, 7, 180, 0, tk)],
      [vLineTo([7-tk,3.499], [7,10], tk)]) :
  (id == 86) ? // V
    concat(
      [vLineTo([4-tk*13/24,0], [tk*13/12,10] , tk*13/12)],
      [vLineTo([4-tk*13/24,0], [8,10], tk*13/12)]) :
  (id == 87) ? // W
    concat(
      [vLineTo([2-tk*13/24,0], [tk*13/12,10], tk*13/12)],
      [vLineTo([6-tk*13/24,0], [8,10], tk*13/12)],
      [vLineTo([2-tk*13/24,0], [4+tk*13/24,7], tk*13/12)],
      [vLineTo([6-tk*13/24,0], [4+tk*13/24,7], tk*13/12)]) :
  (id == 88) ? // X
    concat(
      [vLineTo([8-tk*13/12,0], [tk,10], tk*13/12)],
      [vLineTo([0,0], [8,10], tk*13/12)]) :
  (id == 89) ? // Y
    concat(
      [vLineTo([4-tk*13/24,5], [tk*13/12,10], tk*13/12)],
      [vLineTo([4-tk*13/24,5], [8,10], tk*13/12)],
      [vLineTo([4-tk*13/24,0], [4+tk*13/24,5], tk*13/12)]) :
  (id == 90) ? // Z
    concat(
      [vLineTo([0,tk], [7.5,10-tk], tk*13/12)],
      [hLineTo([0,10-tk], [7.5,10], tk)],
      [hLineTo([0,0], [7.5,tk], tk)]) :
  (id == 91) ? // [
    concat(
      [hLineTo([0,0], [3,tk], tk)],
      [hLineTo([0,10-tk], [3,10], tk)],
      [vLineTo([0,0], [tk,10], tk)]) :
  (id == 92) ? // \
    concat(
          [vLineTo([6.5-tk*13/12,0], [0+tk*13/12, 10], tk*13/12)]) :
  (id == 93) ? // [
    concat(
      [hLineTo([0,0], [3,tk], tk)],
      [hLineTo([0,10-tk], [3,10], tk)],
      [vLineTo([3-tk,0], [3,10], tk)]) :
  (id == 94) ? // ^
    concat(
      [vLineTo([0,6], [3+tk*13/24,10] , tk*13/12)],
      [vLineTo([6-tk*13/12,6], [3+tk*13/24,10], tk*13/12)]) :
  (id == 95) ? // _
    concat(
      [hLineTo([0,0], [6.5,tk], tk)]) :
  (id == 96) ? // `
    concat(
          [vLineTo([2,6], [1, 9], tk/2)],
          [vLineTo([2,6], [1+tk/2, 9], tk/2)]) :
  (id == 97) ? // a
    concat(
          [vLineTo([6-tk,0], [6,3], tk)],
          [arc([3, 3], 6, 6, 0, 150, tk)],
          [arc([3, 2], 6, 4, 0, 360, tk)]) :
  (id == 98) ? // b
    concat(
          [vLineTo([0,3], [tk,10], tk)],
          [arc([3, 3], 6, 6, 0, 360, tk)]) :
  (id == 99) ? // c
    concat(
          [arc([3, 3], 6, 6, 30, 330, tk)]) :
  (id == 100) ? // d
    concat(
          [vLineTo([6-tk,0], [6,10], tk)],
          [vLineTo([6-tk,0], [7,2], tk)],
          [arc([3, 3], 6, 6, 0, 360, tk)]) :
  (id == 101) ? // e
    concat(
          [hLineTo([tk/2,3-tk/2], [6-tk/2,3+tk/2], tk)],
          [vLineTo([6-tk,3-tk/2], [6,3], tk)],
          [arc([3, 3], 6, 6, 0, 180, tk)],
          [arc([3, 3], 6, 6, 180, 315, tk)]) :
  (id == 102) ? // f
    concat(
          [vLineTo([0,0], [tk,7], tk)],
          [hLineTo([0,6-tk], [3,6], tk)],
          [arc([3, 7], 6, 6, 60, 180, tk)]) :
  (id == 103) ? // g
    concat(
          [vLineTo([6-tk,0], [6,3], tk)],
          [arc([3, 0], 6, 6, 180, 360, tk)],
          [arc([3, 3], 6, 6, 0, 360, tk)]) :
  (id == 104) ? // h
    concat(
          [vLineTo([0,0], [tk,10], tk)],
          [vLineTo([6-tk,0], [6,3], tk)],
          [arc([3, 3], 6, 6, 0, 180, tk)]) :
  (id == 105) ? // i
    concat(
          [vLineTo([0,0], [tk,6], tk)],
          [arc([tk/2, 7.5], tk*13/12, tk*13/12, 0, 360, tk)]) :
  (id == 106) ? // j
    concat(
          [vLineTo([4-tk,0], [4,6], tk)],
          [arc([4-tk/2, 7.5], tk*13/12, tk*13/12, 0, 360, tk)],
          [arc([2, 0], 4, 4, 180, 360, tk)]) :
  (id == 107) ? // k
    concat(
      [vLineTo([0,0], [tk,10], tk)],
      [vLineTo([tk*13/24,3], [5,6], tk*13/12)],
      [vLineTo([5-tk*13/12,0], [tk*(1+13/24),3], tk*13/12)]) :
  (id == 108) ? // l
    concat(
          [vLineTo([0,2], [tk,10], tk)],
          [arc([2, 2], 4, 4, 180, 300, tk)]) :
  (id == 109) ? // m
    concat(
          [vLineTo([0,0], [tk,6], tk)],
          [vLineTo([4-tk/2,0], [4+tk/2,6-(4+tk/2)/2], tk)],
          [vLineTo([8-tk,0], [8,6-(4+tk/2)/2], tk)],
          [arc([(4+tk/2)/2, 6-(4+tk/2)/2], (4+tk/2), (4+tk/2), 0, 180, tk)],
          [arc([8-(4+tk/2)/2, 6-(4+tk/2)/2], (4+tk/2), (4+tk/2), 0, 180, tk)]) :
  (id == 110) ? // n
    concat(
          [vLineTo([0,0], [tk,6], tk)],
          [vLineTo([6-tk,0], [6,3], tk)],
          [arc([3, 3], 6, 6, 0, 180, tk)]) :
  (id == 111) ? // o
    concat(
          [arc([3, 3], 6, 6, 0, 360, tk)]) :
  (id == 112) ? // p
    concat(
          [vLineTo([0,-3], [tk,6], tk)],
          [arc([3, 3], 6, 6, 0, 360, tk)]) :
  (id == 113) ? // q
    concat(
          [vLineTo([6-tk,-3], [6,6], tk)],
          [vLineTo([6-tk,-3], [7,-1], tk)],
          [arc([3, 3], 6, 6, 0, 360, tk)]) :
  (id == 114) ? // r
    concat(
          [vLineTo([0,0], [tk,6], tk)],
          [arc([3, 3], 6, 6, 60, 180, tk)]) :
  (id == 115) ? // S
    concat(
      [arc([3, 6-(3+tk/2)/2], 6, 3+tk/2, 0, 270, tk)],
      [arc([3,   (3+tk/2)/2], 6, 3+tk/2, 180, 90, tk)]) :
  (id == 116) ? // t
    concat(
          [vLineTo([0,2], [tk,10], tk)],
          [hLineTo([0,6-tk], [3,6], tk)],
          [arc([2, 2], 4, 4, 180, 300, tk)]) :
  (id == 117) ? // u
    concat(
          [vLineTo([0,3], [tk,6], tk)],
          [vLineTo([6-tk,0], [6,6], tk)],
          [vLineTo([6-tk,0], [7,2], tk)],
          [arc([3, 3], 6, 6, 180, 360, tk)]) :
  (id == 118) ? // v
    concat(
      [vLineTo([3-tk*13/24,0], [tk*13/12,6] , tk*13/12)],
      [vLineTo([3-tk*13/24,0], [6,6], tk*13/12)]) :
  (id == 119) ? // w
    concat(
      [vLineTo([2-tk*13/24,0], [tk*13/12,6], tk*13/12)],
      [vLineTo([6-tk*13/24,0], [8,6], tk*13/12)],
      [vLineTo([2-tk*13/24,0], [4+tk*13/24,4], tk*13/12)],
      [vLineTo([6-tk*13/24,0], [4+tk*13/24,4], tk*13/12)]) :
  (id == 120) ? // x
    concat(
      [vLineTo([6-tk*13/12,0], [tk,6], tk*13/12)],
      [vLineTo([0,0], [6,6], tk*13/12)]) :
  (id == 121) ? // y
    concat(
          [vLineTo([0,3], [tk,6], tk)],
          [vLineTo([6-tk,0], [6,6], tk)],
          [arc([3, 0], 6, 6, 210, 360, tk)],
          [arc([3, 3], 6, 6, 180, 360, tk)]) :
  (id == 122) ? // z
    concat(
      [vLineTo([0,tk], [6,6-tk], tk*1.5)],
      [hLineTo([0,6-tk], [6,6], tk)],
      [hLineTo([0,0], [6,tk], tk)]) :
  (id == 123) ? // {
    concat(
      [arc([3, 7.5-tk/4], 3+tk, 5+tk/2, 90, 180, tk)],
      [arc([0, 7.5-tk/4], 3+tk, 5+tk/2, 270, 0, tk)],
      [arc([0, 2.5+tk/4], 3+tk, 5+tk/2, 0, 90, tk)],
      [arc([3, 2.5+tk/4], 3+tk, 5+tk/2, 180, 270, tk)]) :
  (id == 124) ? // |
    concat(
      [vLineTo([1-tk/2,0], [1+tk/2, 5-tk/2], tk)],
      [vLineTo([1-tk/2,5+tk/2], [1+tk/2, 10], tk)]) :
  (id == 125) ? // }
    concat(
      [arc([0, 7.5-tk/4], 3+tk, 5+tk/2, 0, 90, tk)],
      [arc([3, 7.5-tk/4], 3+tk, 5+tk/2, 180, 270, tk)],
      [arc([3, 2.5+tk/4], 3+tk, 5+tk/2, 90, 180, tk)],
      [arc([0, 2.5+tk/4], 3+tk, 5+tk/2, 270, 0, tk)]) :
  (id == 126) ? // ~
    concat(
      [vLineTo([0,6], [tk, 7], tk)],
      [vLineTo([6.5-tk,7], [6.5, 8], tk)],
      [arc([6.5 - (6.5+tk)/4, 7], (6.5+tk)/2, 4, 180, 360, tk)],
      [arc([(6.5+tk)/4, 7], (6.5+tk)/2, 4, 0, 180, tk)]) :
  (id == 196) ? // Ä
    concat(
      [hLineTo([2,10-tk], [2+tk,10], tk)],
      [hLineTo([6-tk,10-tk], [6,10], tk)],
      [vLineTo([0,0], [4+tk*13/24,8], tk * 13/12)],
      [vLineTo([8-tk*13/12,0], [4+tk*13/24, 8], tk * 13/12)],
      [hLineTo([(5-tk)*(2.5+tk/2)/10+tk/2,2.5], 
      [8-((5-tk)*(2.5+tk/2)/10+tk/2),2.5+tk], tk)]) :
  (id == 203) ? // Ë
    concat(
      [hLineTo([1,10-tk], [1+tk,10], tk)],
      [hLineTo([5-tk,10-tk], [5,10], tk)],
      [vLineTo([0,0], [tk,8], tk)],
      [hLineTo([0,0], [6,tk], tk)],
      [hLineTo([0,4-tk/2],  [4.75,4+tk/2], tk)],
      [hLineTo([0,8-tk], [6,8], tk)]) :
  (id == 207) ? // Ï
    concat(
      [hLineTo([0,10-tk], [0+tk,10], tk)],
      [hLineTo([3-tk,10-tk], [3,10], tk)],
      [vLineTo([1.5-tk/2,0], [1.5+tk/2,8], tk)]) :
  (id == 214) ? // Ö
    concat(
      [hLineTo([1.5,10-tk], [1.5+tk,10], tk)],
      [hLineTo([5.5-tk,10-tk], [5.5,10], tk)],
      [arc([3.5, 4], 7, 8, 0, 360, tk)]) :
  (id == 220) ? // Ü
    concat(
      [hLineTo([1.5,10-tk], [1.5+tk,10], tk)],
      [hLineTo([5.5-tk,10-tk], [5.5,10], tk)],
      [vLineTo([0,3.499], [tk,8], tk)],
      [arc([3.5, 3.5], 7, 7, 180, 0, tk)],
      [vLineTo([7-tk,3.499], [7,8], tk)]) :
  (id == 223) ? // S
    concat(
      [vLineTo([0,0], [tk,8-tk], tk)],
      [hLineTo([4,8-tk], [5-tk/2,8], tk)],
      [arc([2.5, 8-tk], 5, 4+2*tk, 0, 180, tk)],
      [arc([4, 8-(4+tk/2)/2], 5, 4+tk/2, 90, 270, tk)],
      [arc([4,    (4+tk/2)/2], 5, 4+tk/2, 210, 90, tk)]) :
  (id == 228) ? // ä
    concat(
      [hLineTo([1.5,7.5-tk/2], [1.5+tk,7.5+tk/2], tk)],
      [hLineTo([4.5-tk,7.5-tk/2], [4.5,7.5+tk/2], tk)],
      [vLineTo([6-tk,0], [6,3], tk)],
      [arc([3, 3], 6, 6, 0, 150, tk)],
      [arc([3, 2], 6, 4, 0, 360, tk)]) :
  (id == 235) ? // ë
    concat(
      [hLineTo([1.5,7.5-tk/2], [1.5+tk,7.5+tk/2], tk)],
      [hLineTo([4.5-tk,7.5-tk/2], [4.5,7.5+tk/2], tk)],
      [hLineTo([tk/2,3-tk/2], [6-tk/2,3+tk/2], tk)],
      [vLineTo([6-tk,3-tk/2], [6,3], tk)],
      [arc([3, 3], 6, 6, 0, 180, tk)],
      [arc([3, 3], 6, 6, 180, 315, tk)]) :
  (id == 239) ? // ï
    concat(
      [hLineTo([0,7.5-tk/2], [0+tk,7.5+tk/2], tk)],
      [hLineTo([3-tk,7.5-tk/2], [3,7.5+tk/2], tk)],
      [vLineTo([1.5-tk/2,0], [1.5+tk/2,6], tk)]) :
  (id == 246) ? // ö
    concat(
      [hLineTo([1.5,7.5-tk/2], [1.5+tk,7.5+tk/2], tk)],
      [hLineTo([4.5-tk,7.5-tk/2], [4.5,7.5+tk/2], tk)],
      [arc([3, 3], 6, 6, 0, 360, tk)]) :
  (id == 252) ? // ü
    concat(
      [hLineTo([1.5,7.5-tk/2], [1.5+tk,7.5+tk/2], tk)],
      [hLineTo([4.5-tk,7.5-tk/2], [4.5,7.5+tk/2], tk)],
      [vLineTo([0,3], [tk,6], tk)],
      [vLineTo([6-tk,0], [6,6], tk)],
      [vLineTo([6-tk,0], [7,2], tk)],
      [arc([3, 3], 6, 6, 180, 360, tk)]) :
  (id == 256) ? // Ā
    concat(
      [hLineTo([1.5,10-tk], [6.5,10], tk)],
      [vLineTo([0,0], [4+tk*13/24,8], tk * 13/12)],
      [vLineTo([8-tk*13/12,0], [4+tk*13/24, 8], tk * 13/12)],
      [hLineTo([(5-tk)*(2.5+tk/2)/10+tk/2,2.5], 
      [8-((5-tk)*(2.5+tk/2)/10+tk/2),2.5+tk], tk)]) :
  (id == 257) ? // ā
    concat(
      [hLineTo([0.5,7.5-tk/2], [5.5,7.5+tk/2], tk)],
      [vLineTo([6-tk,0], [6,3], tk)],
      [arc([3, 3], 6, 6, 0, 150, tk)],
      [arc([3, 2], 6, 4, 0, 360, tk)]) :
  (id == 274) ? // Ē
    concat(
      [hLineTo([0.5,10-tk], [5.5,10], tk)],
      [vLineTo([0,0], [tk,8], tk)],
      [hLineTo([0,0], [6,tk], tk)],
      [hLineTo([0,4-tk/2],  [4.75,4+tk/2], tk)],
      [hLineTo([0,8-tk], [6,8], tk)]) :
  (id == 275) ? // ē
    concat(
      [hLineTo([0.5,7.5-tk/2], [5.5,7.5+tk/2], tk)],
      [hLineTo([tk/2,3-tk/2], [6-tk/2,3+tk/2], tk)],
      [vLineTo([6-tk,3-tk/2], [6,3], tk)],
      [arc([3, 3], 6, 6, 0, 180, tk)],
      [arc([3, 3], 6, 6, 180, 315, tk)]) :
  (id == 298) ? // Ī
    concat(
      [hLineTo([0,10-tk], [3,10], tk)],
      [vLineTo([1.5-tk/2,0], [1.5+tk/2,8], tk)]) :
  (id == 299) ? // ī
    concat(
      [hLineTo([0,7.5-tk/2], [3,7.5+tk/2], tk)],
      [vLineTo([1.5-tk/2,0], [1.5+tk/2,6], tk)]) :
  (id == 332) ? // Ō
    concat(
      [hLineTo([1,10-tk], [6,10], tk)],
      [arc([3.5, 4], 7, 8, 0, 360, tk)]) :
  (id == 333) ? // ō
    concat(
      [hLineTo([0.5,7.5-tk/2], [5.5,7.5+tk/2], tk)],
      [arc([3, 3], 6, 6, 0, 360, tk)]) :
  (id == 362) ? // Ū
    concat(
      [hLineTo([1,10-tk], [6,10], tk)],
      [vLineTo([0,3.499], [tk,8], tk)],
      [arc([3.5, 3.5], 7, 7, 180, 0, tk)],
      [vLineTo([7-tk,3.499], [7,8], tk)]) :
  (id == 363) ? // ū
    concat(
      [hLineTo([0.5,7.5-tk/2], [5.5,7.5+tk/2], tk)],
      [vLineTo([0,3], [tk,6], tk)],
      [vLineTo([6-tk,0], [6,6], tk)],
      [vLineTo([6-tk,0], [7,2], tk)],
      [arc([3, 3], 6, 6, 180, 360, tk)]) :
      [[]]; // fall-through

function lettersToNumbers(letters) =
  [for(i = [0:(len(letters)-1)]) (ord(letters[i]))];

function flatten(l) = 
  [ for (a = l) for (b = a) b ];

function cumSum(l, pos=0, sum=0, res=[]) =
  (pos >= len(l)) ? concat(res, [sum]) :
    cumSum(l, pos=pos+1, sum=sum+l[pos], res=concat(res, [sum]));

module mergePolyXOR(pointSets){
  if(len(pointSets) > 0){
    al = [for (a = pointSets) len(a)];
    acl = cumSum(al);
    pointPaths = [for (pi = [0:(len(pointSets)-1)]) 
                    [for(pii = [acl[pi]:(acl[pi+1]-1)]) pii]];
    echo(flatten(pointSets));
    echo(pointPaths);
    polygon(points=flatten(pointSets), paths=pointPaths);
  }
}

module mergePolyUnion(pointSets){
  if(len(pointSets) > 0){
    for (pi = [0:(len(pointSets)-1)]){
     polygon(pointSets[pi]);
    }
  }
}

module printLetterPoints(letterArray, gap=0.5, centre=false, 
                         showBlocks=false, tk=1.5){
	translate([(centre) ? -(sumLwds(letterArray,0,
                           len(letterArray)-1) + 
                          (len(letterArray)-1)*gap) / 2 : 0,0]) {
    for(i = [0:(len(letterArray)-1)]){
      //echo(ltrPoints(letterArray[i])[0][0]);
      translate([sumLwds(letterArray,0,i-1)+i*gap,0]) 
        mergePolyUnion(ltrPoints(letterArray[i], tk=tk));
    }
  }
}

// test rig for debugging letters
*union(){
  myLetter="R";
  myWidth=gW(lettersToNumbers(myLetter)[0]);
  translate([-myWidth/2,-5,0]){ 
    linear_extrude(height=0.5){
      mergePolyUnion(ltrPoints(lettersToNumbers(myLetter)[0]));
    }
    *linear_extrude(height=0.25){
      ltr(lettersToNumbers(myLetter)[0]);
    }
  }
}

!color("pink") linear_extrude(height=2, center=true){
  letThick=(sin($t*360)+1)/1.5+0.5;
  translate([0,10])
    printLetterPoints(lettersToNumbers("āēīōū ĀĒĪŌŪ"),
       centre=true, tk=letThick, gap=1);
  translate([0,-5])
    printLetterPoints(lettersToNumbers("äëïöü ÄËÏÖÜß"),
       centre=true, tk=letThick, gap=1);
}

echo(ord("ß"));

*color("darkgrey") linear_extrude(height=2, center=true)
  translate([0, 30]){
  letThick=(sin($t*360)+1)/1.5+0.5;
	translate([0,0]) 
    printLetterPoints(lettersToNumbers("SPHINX OF BLACK QUARTZ"),
       centre=true, tk=letThick, gap=1);
	translate([0,-12]) 
    printLetterPoints(lettersToNumbers("JUDGE MY VOW"),
     centre=true, tk=letThick, gap=1);
	translate([0,-30]) 
    printLetterPoints(lettersToNumbers("sphinx of black quartz"),
     centre=true, tk=letThick, gap=1);
	translate([0,-41]) 
    printLetterPoints(lettersToNumbers("judge my vow"),
     centre=true, tk=letThick, gap=1);
	translate([0,-60]) 
    printLetterPoints(lettersToNumbers("0123456789"),
     centre=true, tk=letThick, gap=1);
}
