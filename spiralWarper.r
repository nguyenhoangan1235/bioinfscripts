#!/usr/bin/env Rscript
suppressMessages(library(imager, quietly=TRUE));

argLoc <- 1;

usage <- function(){
    cat("usage: ./spiralWarper.r <tileDir> [options]\n");
    cat("\nOther Options:\n");
    cat("-interp <string>     : Change interpolation (default: nearest)\n");
    cat("-bg <string>         : Change background colour (default: black)\n");
    cat("-fg <string>         : Change foreground colour (default: grey)\n");
    cat("-label <string>      : Add label to spiral start\n");
    cat("-font <string>       : Change font used for label\n");
    cat("\n");
    cat(paste0("'tileDir' is expected to be a directory containing [only] ",
               "identically-sized images.\n"));
    cat(paste0("This program will wrap these images into a spiral, creating ",
               "an output file 'out.png'.\n"));
}

## Default arguments

trackDir <- NA;
interp.type <- "nearest";
bg.colour <- "black";
fg.colour <- "grey";
font.family <- "";
start.label <- "";

while(!is.na(commandArgs(TRUE)[argLoc])){
    if(dir.exists(commandArgs(TRUE)[argLoc])){ # file existence check
        trackDir <- commandArgs(TRUE)[argLoc];
    } else {
        if(commandArgs(TRUE)[argLoc] == "-help"){
            usage();
            quit(save = "no", status=0);
        }
        else if(commandArgs(TRUE)[argLoc] == "-interp"){
            interp.type <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        }
        else if(commandArgs(TRUE)[argLoc] == "-bg"){
            bg.colour <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        }
        else if(commandArgs(TRUE)[argLoc] == "-fg"){
            fg.colour <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        }
        else if(commandArgs(TRUE)[argLoc] == "-label"){
            start.label <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        }
        else if(commandArgs(TRUE)[argLoc] == "-font"){
            font.family <- commandArgs(TRUE)[argLoc+1];
            argLoc <- argLoc + 1;
        }
        else {
            cat("Error: Argument '",commandArgs(TRUE)[argLoc],
                "' is not understood by this program\n\n", sep="");
            usage();
            quit(save = "no", status=1);
        }
    }
    argLoc <- argLoc + 1;
}

if(is.na(trackDir)){
    cat("Error: no tile image directory specified\n\n");
    usage();
    quit(save="no");
}

if(!dir.exists(trackDir)){
    cat(sprintf("Error: tile image directory '%s' does not exist\n\n",
                trackDir));
    usage();
    quit(save="no");
}

imgNames <- list.files(trackDir, full.names=TRUE);

im <- load.image(imgNames[1]);

imW <- imWorig <- nrow(im);
imH <- ncol(im);
imW <- imW;
numSpaces <- 0;
while(numSpaces < (length(imgNames) + 0.5)){
    imWh <- (imW / 2);
    a <- imH / (2 * pi);
    t1 <- (imWh - imH) / a;
    ## # Distance using Wolfram formula
    ## https://stackoverflow.com/a/44742854/3389895 [note: incorrect]
    ## https://mathworld.wolfram.com/ArchimedesSpiral.html
    s1 <- (1/2) * a * (t1 * sqrt(1 + t1^2) + asinh(t1));
    numSpaces <- s1 / imWorig;
    if(numSpaces < (length(imgNames) + 0.5)){
        imW <- imW + 1;
    }
}

cat(sprintf("Required image width: %d\n", imW));

mapRev <- function(x, y, offset=0){
    imWh <- imW/2;
    xs <- x - imWh;
    ys <- y - imWh;
    thetaMod <- (atan2(ys, xs) + 1/2 * pi) %% (2 * pi);
    loopProp <- thetaMod / (2 * pi);
    r <- sqrt(xs^2+ys^2);
    rStart <- imWh-imH;
    rAdj <- loopProp * imH;
    loop <- floor((imWh - r - rAdj) / imH);
    ypos <- ((imWh - r - rAdj) %% imH);
    rEnd <- r - ypos;
    thetaEnd <- thetaMod + loop * (2 * pi);
    a <- imH / (2 * pi);
    t1 <- (imWh - imH) / a;
    t2 <- t1 - thetaEnd;
    ## # Distance using Wolfram formula
    ## https://stackoverflow.com/a/44742854/3389895 [note: incorrect]
    ## https://mathworld.wolfram.com/ArchimedesSpiral.html
    s1 <- (1/2) * a * (t1 * sqrt(1 + t1^2) + asinh(t1));
    s2 <- (1/2) * a * (t2 * sqrt(1 + t2^2) + asinh(t2));
    spiralDist <- s1 - s2;
    xpos <- spiralDist - offset;
    list(x=xpos, y=ypos);
}

mapRevOfs <- function(tOffset) {
    function(x, y) {
        mapRev(x, y, offset=tOffset);
    }
}

imShadow <- res <- cimg(array(0, dim=c(imW, imW, 1, 3)));
for(iNi in seq_along(imgNames)){
    iN <- imgNames[iNi];
    im <- load.image(iN);
    cat("Loading image ", iNi, " [", iN, "]\n", sep="");
    imShadow[1:imWorig,1:imH,1,1:3] <- im[1:imWorig,1:imH,1,1:3];
    cat("Warping...");
    imShadow %>%
        imwarp(map=mapRevOfs((iNi-1) * imWorig), direction="backward",
               interpolation=interp.type) -> tmpImg;
    cat(" done!\n");
    cat("Adding to result image...");
    res[tmpImg > 0] <- tmpImg[tmpImg > 0];
    cat(" done!\n");
}

if(bg.colour != "black"){
    cat("Updating background...");
    blackPoss <-
        which(res[,,1,1] == 0 &
              res[,,1,2] == 0 &
              res[,,1,3] == 0);
    rc <- c(col2rgb(bg.colour)) / 255;
    cat("R");
    res[,,1,1][blackPoss] <- rc[1];
    cat("G");
    res[,,1,2][blackPoss] <- rc[2];
    cat("B");
    res[,,1,3][blackPoss] <- rc[3];
    cat(" done!\n");
}

cat("Creating final image [as 'out.png' in the current directory]...");
png("out.png", width=imW, height=imW);
par(mar=c(0,0,0,0), family=font.family);
plot(res, ann=FALSE, axes=FALSE);
if(start.label != ""){
    start.label <- unlist(strsplit(start.label, "\\\\n"));
    sum.heights <- sum(abs(strheight(start.label)) * 1.05);
    scale.factor <- ((imH * 0.5) / sum.heights);
    pos.y <- tail(head(seq(0, imH, length.out=(length(start.label) + 2)),
                      -1), -1);
    text(x=imW/2, y=pos.y, labels=start.label,
         col=fg.colour, pos = 2, cex = (imH * 0.5) / sum.heights);
}
invisible(dev.off());
cat(" done!\n");
