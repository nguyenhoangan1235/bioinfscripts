#!/usr/bin/env Rscript

cat("loading tidyverse...");
options(tidyverse.quiet = TRUE);
library(tidyverse);
cat(" done\n");

## load used barcode identifiers
bcNames <- read_table("barcode_counts.txt", col_names=c("count", "barcode"),
                      show_col_types=FALSE) %>% pull(barcode);

## set gene annotation file location
annotationFile <- "ensembl_GRCm39_geneFeatureLocations.txt.gz";

cat(sprintf("loading annotation data [%s]...", annotationFile));
## load ensemble transcript metadata (including gene name)
ensembl.df <- read_delim(annotationFile, delim="\t", show_col_types=FALSE);

colnames(ensembl.df) <-
    c("Transcript stable ID" = "transcript",
      "Gene description" = "Description",
      "Gene name" = "Gene",
      "Gene start (bp)" = "Start",
      "Gene end (bp)" = "End",
      "Strand" = "Strand",
      "Chromosome/scaffold name" = "Chr")[colnames(ensembl.df)];

ensembl.df$Description <- sub(" \\[.*$","",ensembl.df$Description);
ensembl.df$Description <- sub("^(.{50}).+$","\\1...",ensembl.df$Description);
cat(" done\n");

options(scipen=15); ## don't show scientific notation for large positions

for(mapper in c("LAST")){
    cat(sprintf("Processing counts mapped using '%s':\n", mapper));
    checkName <- sprintf("mapped/trnCounts_%s_%s_vs_Mmus_transcriptome.txt.gz",
                         mapper, bcNames[1]);
    if(!file.exists(checkName)){
        cat(sprintf("  [none found]\n"));
        next;
    }
    ## load count data into "narrow" array (one line per count)
    trn.counts <- tibble();
    for(bc in bcNames){
        cat("  ", bc, "\n", sep="");
        trn.counts <-
            bind_rows(trn.counts,
                         sprintf("mapped/trnCounts_%s_%s_vs_Mmus_transcriptome.txt.gz",
                                 mapper, bc) %>%
                         read_table(col_names=c("count","barcode",
                                                "transcript","dir"), show_col_types=FALSE));
    }

    ## remove revision number from transcript names (if present)
    trn.counts$transcript <- sub("\\.[0-9]+$","",trn.counts$transcript);

    cat("creating wide format table...");
    ## convert to wide format (one line per transcript)
    trn.counts.wide <- spread(trn.counts, barcode, count) %>%
        mutate(dir = c("+"="fwd", "-"="rev")[dir]);
    for(bd in colnames(trn.counts.wide %>% select(-transcript, -dir))){
        trn.counts.wide[[bd]] <- replace_na(trn.counts.wide[[bd]],0);
    }
    cat(" done\n");

    cat("merging with transcript annotations...");
    ## merge ensembl metadata with transcript counts
    gene.counts.wide <- inner_join(ensembl.df, trn.counts.wide, by="transcript");
    gene.counts.wide <- gene.counts.wide[order(-rowSums(gene.counts.wide[,-(1:8)])),];
    ## write result out to a file
    write.csv(gene.counts.wide,
              file=sprintf("wide_transcript_counts_%s_%s.csv", mapper, Sys.Date()),
              row.names=FALSE);
    cat(" done\n");
}
