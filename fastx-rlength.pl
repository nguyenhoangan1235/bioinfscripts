#!/usr/bin/env perl
use warnings;
use strict;

use Getopt::Long qw(:config auto_help pass_through);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

sub rc {
  my ($seq) = @_;
  $seq =~ tr/ACGTUYRSWMKDVHBXN-/TGCAARYSWKMHBDVX*-/;
  # work on masked sequences as well
  $seq =~ tr/acgtuyrswmkdvhbx*/tgcaaryswkmhbdvx*/;
  return(scalar(reverse($seq)));
}

sub rev {
  my ($seq) = @_;
  $seq =~ tr/XNxn/****/;
  return(scalar(reverse($seq)));
}

sub printStats {
  my ($seq, $seqID, $trim, $kmerLength) = @_;
  my $len = length($seq);
  my $sseq = "";
  if($seqID && (length($seq) > $trim) && (length($seq) > $kmerLength)){
    my $maxKmer = "";
    my %rptPos = ();
    my %allGapCounts = ();
    my %minGaps = ();
    my $revCount = 1;
    my $rcCount = 1;
    my $numRepeats = 0;
    my @allGapSeps = ();
    for(my $p = 0; ($p + $kmerLength) <= $len; $p++){
      $sseq = substr($seq, $p, $kmerLength);
      if($sseq =~ /N+$/){
        next;
      }
      if(exists($rptPos{$sseq})){
	my $gap = $p - $rptPos{$sseq};
        push(@allGapSeps, $gap);
	$allGapCounts{$gap}++;
        $numRepeats++;
	if(!exists($minGaps{$sseq}) || ($minGaps{$sseq} > $gap)){
	  $minGaps{$sseq} = $gap;
	}
      }
      if(exists($rptPos{rev($sseq)})){
	$revCount++;
      }
      if(exists($rptPos{rc($sseq)})){
	$rcCount++;
      }
      $rptPos{$sseq} = $p;
    }
    if($revCount == 1){
      $revCount = 0;
    }
    if($rcCount == 1){
      $rcCount = 0;
    }
    my $numKmers = scalar(keys(%rptPos));
    my $kmerRatio = $numKmers/($len - $kmerLength + 1);
    my @gaps = sort {$a <=> $b} (@allGapSeps);
    my $medianGap = (@gaps) ? $gaps[$#gaps / 2] : 0;
    my $medianCount = 0;
    my $modalGap = 0;
    my $modalCount = 0;
    my $rangeCountMed = 0;
    my $rangeCountMod = 0;
    if($medianGap){
      my %gapCounts = ();
      foreach my $gap (@gaps){
	$gapCounts{$gap}++;
      }
      $medianCount = ${allGapCounts{$medianGap}};
      my @modalSort = sort {$allGapCounts{$b} <=> $allGapCounts{$a}} (@gaps);
      $modalGap = $modalSort[0];
      $modalCount = $allGapCounts{$modalGap};
      for(my $gP = int($medianGap * 0.99); ($gP <= ($medianGap / 0.99));
	  $gP++){
	$rangeCountMed += $allGapCounts{$gP} if($allGapCounts{$gP});
      }
      for(my $gP = int($modalGap * 0.99); ($gP <= ($modalGap / 0.99));
	  $gP++){
	$rangeCountMod += $allGapCounts{$gP} if($allGapCounts{$gP});
      }
    }
    printf("%8d %0.3f %6d %6d %6d %6d %6d %6d %6d %6d %6d %s\n",
	   $len, $kmerRatio,
	   $numRepeats,
	   $medianGap,
	   $modalGap,
	   $medianCount,
	   $modalCount,
	   $rangeCountMed,
	   $rangeCountMod,
	   $revCount,
	   $rcCount,
	   $seqID);
  }
}

$| = 1; ## don't buffer output

my $trim = 0;
my $kmerLength = 17; ## number of bases in hash keys

GetOptions("trim=s" => \$trim, "kmerlength|k=i" => \$kmerLength) or
    die("Error in command line arguments");

# Complain about non-file command line argument
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-e $arg){
    push(@files, $arg);
  } else {
    die("Unknown argument: $arg");
  }
}
@ARGV = @files;

# use stdin if no files supplied
if(!@ARGV){
  @ARGV = '-' unless (-t STDIN);
}

if($kmerLength != 17){
  print(STDERR "Setting kmer length to $kmerLength\n");
}

my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
my $buffer = "";
printf("%8s %5s %6s %6s %6s %6s %6s %6s %6s %6s %6s %s\n",
       "length", "kRat", "cntRep",
       "medGap", "modGap",
       "medCt", "modCt",
       "RCMed", "RCMod",
       "rvCnt", "rcCnt", "SeqID");
foreach my $file (@ARGV) {
  # This little gunzip dance makes sure the script can handle both
  # gzip-compressed and uncompressed input, regardless of whether
  # or not it is piped
  my $z = new IO::Uncompress::Gunzip($file, MultiStream => 1)
    or die "gunzip failed: $GunzipError\n";
  while(<$z>){
    chomp;
    chomp;
    if (!$inQual) {
      if (/^(>|@)((.+?)( .*?\s*)?)$/) {
        my $newSeqID = $2;
        my $newShortID = $3;
        printStats($seq, $seqID, $trim, $kmerLength);
        $seq = "";
        $qual = "";
        $buffer = "";
        $seqID = $newSeqID;
      } elsif (/^\+(.*)$/) {
        $inQual = 1;            # true
        $qualID = $1;
      } else {
        $seq .= $_;
      }
    } else {
      $qual .= $_;
      if (length($qual) >= length($seq)) {
        $inQual = 0;            # false
      }
    }
  }
}

printStats($seq, $seqID, $trim, $kmerLength);
