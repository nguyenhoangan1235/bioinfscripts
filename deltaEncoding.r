#/usr/bin/env Rscript
numToDeltaBit <- function(n){
  if(n == 0){
    "00";
  } else {
    numSign <- (n < 0) + 0;
    n <- abs(n);
    numHalfWidth <- ceiling(log2(n+0.25) / 2);
    numWidth <- 2 * numHalfWidth;
    numBits <- as.integer(intToBits(n)[1:numWidth]);
    paste0(paste(rep(1, numHalfWidth), collapse=""),
           0, numSign, paste(numBits, collapse=""));
  }
}

deltaBitToNum <- function(s){
  if(s == "00"){
    0;
  }
  numArray <- as.integer(unlist(strsplit(s,"")));
  numHalfWidth <- which.min(numArray)-1;
  numNeg <- (numArray[numHalfWidth+2] == 0) * 2 - 1;
  numArray <- tail(numArray, -numHalfWidth-2);
  sum(numArray * 2^(0:(2*numHalfWidth-1))) * numNeg;
}