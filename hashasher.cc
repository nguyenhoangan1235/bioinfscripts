// HashAsher -- A fast hash-based repeat finder library

// see 'hashasher.r' for an example implementation
// [usage: ./hashasher.r [options] <file.fasta>]

#include <iostream>
#include <fstream>
#include <vector>
#include <bitset>
#include <cstdio>
#include "Rcpp.h"

#define UPPER_MASK ((1L << 62L) - 1)
#define HALF_MASK ((1L << 32L) - 1)
// The missing hash should be unlikely to occur in normal circumstances
// currently equal to
//    1011011101111011111011111101111111011111111011111111101111111111b
// or  TGCGCGTGGTGGGCGGGCGGGTGGGGTGGGGG
#define MISSING_HASH (0xB77BEFDFDFEFFBFFL)
// complement is used for flipping the upper bits, and is
// equal to 10101010 ... 10101010b
#define COMPLEMENT   (0xAAAAAAAAAAAAAAAAL)
// UPPER_2BIT is used for checking reverse bit sequences
#define UPPER_2BIT (0xC000000000000000L)

// upper nibble of location defines type
#define KT_N   (0xF000000000000000L)
#define KT_F   (0x1000000000000000L)
#define KT_C   (0x3000000000000000L)
#define KT_R   (0x5000000000000000L)
#define KT_RC  (0x7000000000000000L)
#define COMP_MASK (0x2000000000000000L)
#define REV_MASK  (0x4000000000000000L)
#define POS_MASK  (0x0FFFFFFFFFFFFFFFL)

// see https://stackoverflow.com/questions/1903954
template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

// see https://stackoverflow.com/questions/22325648
inline uint64_t rotl64 ( uint64_t x, int8_t r ) {
  return (uint64_t)(x << r) | (uint64_t)(x >> (64 - r));
}

// Magic numbers for hashing, designed so that each 2-bit chunk
// [hopefully] contains a shuffling of 00, 01, 10, 11, which should
// make the hash more representative of (and unpredictable for) DNA
// sequences.
// apply(t(replicate(16, sprintf("%x", colSums(t(replicate(2, sample(0:3))) *
//          c(1,4))))), 2, paste, collapse="")
const uint64_t baseHash[4] =  { 0x8052315a0dea934bL, 0x5f3984bd9341c9e4L,
				0xe58f5f24f8b736bdL, 0x3ae4eac3661c6c12L };

using namespace Rcpp;
using namespace std;

enum FastXFileType { fasta, fastq, fastm };

// temporary variables
vector<string> fastxBuf;
vector<string> readBuf;
string buf;

// independent state variables
fstream fastaFileFS, readFileFS;
FastXFileType seqFormat;
size_t kmerSize;
size_t kmerBitSize;
uint64_t kmerMask;
string seqID, readID;
string seqQual;
uint64_t seqLen, readLen;
uint64_t urpt, uorpt;
uint64_t hashCount;
uint64_t compCount;
vector<uint8_t> bitSeq;
vector<bool> baseMissing;
vector<bool> baseModified;
vector<bool> nearMissing;
map<uint64_t, vector<uint64_t>> repetitivePoss;
vector<bool> isInteresting;
vector<bool> isRepetitive;
vector<bool> isReverse;
vector<bool> isRevcomp;
vector<bool> isComplement;

// memoise cache for pwLogLin function, because it involves a lot of
// float computation
vector<int64_t> llCache;
bool cachePrepared = false;

class Kmer {
protected:
  const uint64_t startPos;
public:
  Kmer(uint64_t tPos);
  uint64_t getPos() const;
  uint64_t getType() const;
  virtual string getSequence() const;
  virtual bool operator< (const Kmer& b) const;
  virtual bool operator== (const Kmer& b) const;
};

namespace std {
  template<> struct hash<Kmer> {
    uint64_t operator()(Kmer const& k) const {
      // The currently-used hash for kmers is the 2bit-packed
      // representation of the first 32 bases if the kmer has no more
      // than 32 bases. Kmer_C is the same as Kmer, but base
      // representations are the complement of the original base. The
      // complement hash is extracted from the bit-packed sequence by
      // complementing as a final step.
      //
      // A = 0x41 [00.]; C=0x43 [01.]; G = 0x47 [11.]; T = 0x54 [10.]
      // a = 0x61 [00.]; c=0x63 [01.]; g = 0x67 [11.]; t = 0x74 [10.]
      // Complement bases: the upper bit is 'notted'
      // A =      [10.]; C=     [11.]; G =      [01.]; T =      [00.]
      // a =      [10.]; c=     [11.]; g =      [01.]; t =      [00.]
      // Kmer_R is traversed in reverse order.
      // Kmer_RC is both reversed and complemented.
      //
      // For k>32, this uses a rolling hash, described by Heng Li here:
      // [https://bioinformatics.stackexchange.com/questions/19#comment563_293]
      //
      // This is slightly modified to slide two bits each time, to be
      // in line with the 2bit-base shuffling used in generating the
      // magic numbers.
      //
      hashCount++;
      uint64_t basePos = k.getPos();
      uint64_t kmerType = k.getType();
      if(nearMissing[basePos]){ // short-circuit kmers containing N
        return(MISSING_HASH);
      }
      uint64_t hr = 0;
      if(kmerSize <= 32){ // bit-pack the full sequence
	if((kmerType == KT_R) || (kmerType == KT_RC)){
	  for(size_t hp = basePos; hp < (basePos + kmerSize); hp+= 1){
	    hr = (hr << 2L) | (uint64_t)bitSeq[hp]; // new base shuffled into low pos
	  }
	} else {
	  for(uint64_t hi = 0, hp=basePos; hi < kmerBitSize; hi+= 2, hp+= 1){
	    hr = hr | ((uint64_t)bitSeq[hp] << hi); // new base added on high pos
	  }
	}
	if((kmerType == KT_C) || (kmerType == KT_RC)){ // optimisation: this branch could be removed
	  hr ^= COMPLEMENT;
	}
	hr &= kmerMask;
      } else { // hash the full sequence
	uint64_t compBits = ((kmerType == KT_C) || (kmerType == KT_RC)) ? 2L : 0L;
	if((kmerType == KT_R) || (kmerType == KT_RC)){
	  for(size_t hp = basePos + kmerSize, rot=(kmerSize << 1); hp > basePos; rot-= 2){
	    hr ^= (uint64_t)rotl64(baseHash[((uint64_t)bitSeq[--hp] & 3L) ^ compBits], rot % 64);
	  }
	} else {
	  for(size_t hp = basePos, rot=(kmerSize << 1); hp < (basePos + kmerSize); hp++, rot-= 2){
	    hr ^= (uint64_t)rotl64(baseHash[((uint64_t)bitSeq[hp] & 3L) ^ compBits], rot % 64);
	  }
	}
      }
      return(hr);
    }
  };
}

// state flags
bool fileLoaded = false;
bool seqsRemaining = false;
bool readsRemaining = false;
bool kmersCounted = false;
bool repeatsHashed = false;

// dependent state variables
map<Kmer, uint64_t> kmerCountsOrdered;
unordered_map<Kmer, uint64_t> kmerCounts;
unordered_set<Kmer> kmerStore;

// base lookup table
// A/C/G/T can be described/distinguished by bits 1 & 2 (ignoring bit 0)
// A = 0x41 [00.]; C=0x43 [01.]; G = 0x47 [11.]; T = 0x54 [10.]
// a = 0x61 [00.]; c=0x63 [01.]; g = 0x67 [11.]; t = 0x74 [10.]
char baseLookup[4] = {'A', 'C', 'T', 'G'};

Kmer::Kmer(uint64_t tPos) : startPos(tPos){
  if((tPos & KT_N) == 0){
    cerr << "Error: Kmer without type at " << (tPos & POS_MASK) << endl;
  }
}

// Retrieve a string representation of the kmer
string Kmer::getSequence() const{
  uint64_t basePos = getPos();
  uint64_t kmerType = getType();
  if(basePos + kmerSize > seqLen){
    cerr << "Error: (getSequence) asked to retrieve kmer beyond sequence end ["
	 << basePos << "]\n";
    return("");
  }
  uint8_t bsp;
  string res = "";
  uint64_t compBits = ((kmerType == KT_C) || (kmerType == KT_RC)) ? 2L : 0L;
  if((kmerType == KT_R) || (kmerType == KT_RC)){
    for(uint64_t hp = basePos; hp < (basePos + kmerSize); hp++){
      bsp = (uint64_t)bitSeq[hp] ^ compBits;
      res = (baseMissing[hp] ? 'N' : (((kmerType != KT_RC) && baseModified[hp]) ? 'm' : baseLookup[bsp])) + res;
    }
  } else {
    for(uint64_t hp = basePos; hp < (basePos + kmerSize); hp++){
      bsp = (uint64_t)bitSeq[hp] ^ compBits;
      res = res + (baseMissing[hp] ? 'N' : (((kmerType != KT_C) && baseModified[hp]) ? 'm' : baseLookup[bsp]));
    }
  }
  return(res);
}

uint64_t Kmer::getPos() const{
  return(startPos & POS_MASK);
}

uint64_t Kmer::getType() const{
  return(startPos & KT_N);
}

// Check order of two kmers (note: complement / reverse not checked)
bool Kmer::operator< (const Kmer& b) const{
  uint64_t basePos1 = getPos();
  uint64_t basePos2 = b.getPos();
  if((basePos1 + kmerSize > seqLen) || (basePos2 + kmerSize > seqLen)){
    cerr << "Error: (<) asked to retrieve kmer beyond sequence end ["
	 << basePos1 << ", " << basePos2 << "]\n";
    return false;
  }
  if(getType() != b.getType()){
    cerr << "Error: (<) asked to order kmers of different types ["
	 << getType() << ", " << b.getType() << "]\n";
    return false;
  }
  // kmers containing missing bases are considered to have a value
  // less than other kmers
  if(nearMissing[basePos1] || nearMissing[basePos2]){
    return(nearMissing[basePos1] && !nearMissing[basePos2]);
  }
  uint8_t bs1, bs2; // should probably use `*` instead of variable assignment
  for(uint64_t i1 = basePos1, i2=basePos2; i1 < (basePos1 + kmerSize); i1++, i2++){
    bs1 = bitSeq[i1]; bs2 = bitSeq[i2];
    if(bs1 != bs2){
      return(bs1 < bs2);
    }
  }
  // equal
  return(false);
}

// Check equality of two kmers
// todo: implement base modification
bool Kmer::operator== (const Kmer& b) const{
  compCount++;
  uint64_t basePos1 = getPos();
  uint64_t basePos2 = b.getPos();
  // cout << "Checking for equality: " << basePos1 << " vs " << basePos2 << "...";
  bool doComplement = ((getType() ^ b.getType()) & COMP_MASK) > 0;
  bool doReverse = ((getType() ^ b.getType()) & REV_MASK) > 0;
  if((basePos1 + kmerSize > seqLen) || (basePos2 + kmerSize > seqLen)){
    cerr << "Error: (==) asked to retrieve kmer beyond sequence end ["
	 << basePos1 << ", " << basePos2 << "]\n";
    return false;
  }
  // all kmers containing 'N' are considered equal,
  // and not equal to kmers not containing 'N'
  if(nearMissing[basePos1] || nearMissing[basePos2]){
    return(nearMissing[basePos1] == nearMissing[basePos2]);
  }
  uint8_t compBits = doComplement ? 2 : 0;
  uint8_t bs1, bs2;
  if(!doReverse){
    for(uint64_t i1 = basePos1, i2=basePos2; i1 < (basePos1 + kmerSize); i1++, i2++){
      bs1 = bitSeq[i1]; bs2 = bitSeq[i2] ^ compBits;
      if(bs1 != bs2){
	// cout << " not equal (F) at " << basePos1 << "," << basePos2 << endl;
	return(false);
      }
    }
  } else {
    // reversed, so work from end of sequence 2 back to start
    for(uint64_t i1 = basePos1, i2=(basePos2 + kmerSize - 1);
	i1 < (basePos1 + kmerSize); i1++, i2--){
      bs1 = bitSeq[i1]; bs2 = bitSeq[i2] ^ compBits;
      if(bs1 != bs2){
	return(false);
      }
    }
  }
  // equal
  return(true);
}

// [[Rcpp::export]]
string getID(){
  return(seqID);
}

// [[Rcpp::export]]
string getShortID(){
  return(seqID.substr(0, seqID.find(' ')));
}

bool storeBits(string seq, bool silent=false){
  uint64_t seqPos = 0;
  uint64_t arrayPos = 0;
  uint64_t bitPos = 0;
  uint64_t h = 0;
  // set sequence length
  seqLen = seq.length();
  if(seqLen < kmerSize){
    if(!silent){
      cerr << "Warning: sequence '" << getShortID() << "' is shorter than kmer size; not storing\n";
    }
    seqLen = 0;
    return(false);
  }
  hashCount = 0;
  compCount = 0;
  // clear missing bits
  // needed so that missing comparisons work properly
  baseMissing.assign(seqLen, false);
  baseModified.assign(seqLen, false);
  nearMissing.assign(seqLen, false);
  llCache.assign(seqLen, -1);
  cachePrepared = true;
  // reserve enough space to store the sequence, fill with zero
  // [one position per base]
  bitSeq.assign(seqLen, 0);
  for(char ch : seq){
    baseMissing[seqPos] = ((ch == 'N') || (ch == 'n'));
    baseModified[seqPos] = (ch == 'm');
    // set ch to 'C' if it was previously 'm'
    // branchless ternary; see https://stackoverflow.com/a/30084391/3389895
    ch = ('C' & -(ch == 'm')) | (ch & -(ch != 'm'));
    // cerr << "  Storing " << bitset<3>(ch & 6) << " -> "
    // 	   << bitset<3>((ch & 6) >> 1) << " to " << seqPos;
    bitSeq[seqPos++] = (ch & 6) >> 1;
    // cerr << " [" << +bitSeq[seqPos - 1] << "]" << endl;
  }
  // fill in nearMissing array
  uint64_t nextMissing = 0;
  for(uint64_t i = 0; (i <= (seqLen - kmerSize)); i++){
    bool nearMissingCheck = (baseMissing[i + kmerSize - 1]);
    // if the current position is missing, update the nextMissing position
    // note: branchless ternary, see https://stackoverflow.com/a/30084391/3389895
    nextMissing = ((i + kmerSize - 1) & -nearMissingCheck) | (nextMissing & ~(-nearMissingCheck));
    // set 'nearMissing' if the next missing position is close enough
    nearMissing[i] = (nextMissing >= i);
  }
  return(true);
}

bool appendBits(string read, bool silent=false){
  uint64_t readPos = seqLen;
  uint64_t arrayPos = 0;
  uint64_t bitPos = 0;
  uint64_t h = 0;
  // set sequence length
  readLen = read.length();
  if(readLen < kmerSize){
    if(!silent){
      cerr << "Warning: read '" << readID << "' is shorter than kmer size; not mapping\n";
    }
    return(false);
  }
  hashCount = 0;
  compCount = 0;
  // reserve enough space to store the sequence, fill with zero
  // [one position per base]
  bitSeq.resize(seqLen + 1 + readLen);
  baseMissing.resize(seqLen + 1 + readLen);
  baseModified.resize(seqLen + 1 + readLen);
  baseMissing[readPos++] = true; // insert one N to avoid kmer crossover from ref to read
  for(char ch : read){
    baseMissing[readPos] = ((ch == 'N') || (ch == 'n'));
    baseModified[readPos] = (ch == 'm');
    // set ch to 'C' if it was previously 'm'
    ch = ('C' & -(ch == 'm')) | (ch & -(ch != 'm'));
    bitSeq[readPos++] = (ch & 6) >> 1;
  }
  // fill in nearMissing array
  uint64_t nextMissing = 0;
  for(uint64_t i = seqLen - kmerSize + 1; (i <= (seqLen + readLen + 1 - kmerSize)); i++){
    bool nearMissingCheck = (baseMissing[i + kmerSize - 1]);
    // if the current position is missing, update the nextMissing position
    // note: branchless ternary, see https://stackoverflow.com/a/30084391/3389895
    nextMissing = ((i + kmerSize - 1) & -nearMissingCheck) | (nextMissing & ~(-nearMissingCheck));
    // set 'nearMissing' if the next missing position is close enough
    nearMissing[i] = (nextMissing >= i);
  }
  return(true);
}


char getBase(uint64_t basePos){
  if(basePos > seqLen){
    return('-');
  }
  if(baseMissing[basePos]){
    return('N');
  }
  return(baseLookup[bitSeq[basePos]]);
}

// return value: true if a sequence has been loaded / processed
bool storeSequence(FastXFileType ft, bool silent=false){
  seqFormat = ft;
  seqQual.clear();
  switch (ft) {
  case fasta:
    if(fastxBuf.size() == 2){
      seqID = fastxBuf[0].substr(1);
      bool retVal = storeBits(fastxBuf[1], silent=silent);
      fastxBuf.clear();
      return(retVal);
    } // partially-complete sequences will not be processed
    cerr << "Warning: attempted to load incomplete sequence\n";
    break;
  case fastq:
    if(fastxBuf.size() == 4){
      seqID = fastxBuf[0].substr(1);
      seqQual = fastxBuf[3];
      bool retVal = storeBits(fastxBuf[1], silent=silent);
      fastxBuf.clear();
      return(retVal);
    } // partially-complete sequences will not be processed
    cerr << "Warning: attempted to load incomplete sequence\n";
    break;
  case fastm:
    bool retVal = false;
    for(size_t i = 0; i < fastxBuf.size(); i += 2){
      if(fastxBuf[i].find("#seq:") == 0){
        seqID = fastxBuf[i].substr(5);
        retVal = storeBits(fastxBuf[i+1].substr(1), silent=silent);
      }
      if(fastxBuf[i].find("#qual:") == 0){
        seqID = fastxBuf[i].substr(5);
        seqQual = fastxBuf[i+1].substr(1);
      }
    }
    fastxBuf.clear();
    return(retVal);
    break;
  }
  return(false);
}

// return value: true if a sequence has been loaded / processed
bool appendSequence(FastXFileType ft, bool silent=false){
  seqFormat = ft;
  seqQual.clear();
  switch (ft) {
  case fasta:
    if(fastxBuf.size() == 2){
      seqID = fastxBuf[0].substr(1);
      bool retVal = appendBits(fastxBuf[1], silent=silent);
      fastxBuf.clear();
      return(retVal);
    } // partially-complete sequences will not be processed
    cerr << "Warning: attempted to load incomplete sequence\n";
    break;
  case fastq:
    if(fastxBuf.size() == 4){
      seqID = fastxBuf[0].substr(1);
      seqQual = fastxBuf[3];
      bool retVal = appendBits(fastxBuf[1], silent=silent);
      fastxBuf.clear();
      return(retVal);
    } // partially-complete sequences will not be processed
    cerr << "Warning: attempted to load incomplete sequence\n";
    break;
  case fastm:
    bool retVal = false;
    for(size_t i = 0; i < fastxBuf.size(); i += 2){
      if(fastxBuf[i].find("#seq:") == 0){
        seqID = fastxBuf[i].substr(5);
        retVal = appendBits(fastxBuf[i+1].substr(1), silent=silent);
      }
      if(fastxBuf[i].find("#qual:") == 0){
        seqID = fastxBuf[i].substr(5);
        seqQual = fastxBuf[i+1].substr(1);
      }
    }
    fastxBuf.clear();
    return(retVal);
    break;
  }
  return(false);
}

// return value: true if a sequence has been loaded / processed
bool processFastx(bool dumpRemainder, bool silent=false){
  if(fastxBuf.size() == 1){
    return(false);
  }
  FastXFileType fileType;
  bool retVal;
  switch (fastxBuf[0][0]) {
  case '#':
    fileType = fastm;
    break;
  case '@':
    fileType = fastq;
    break;
  default:
    fileType = fasta;
  }
  if(dumpRemainder){
    retVal = storeSequence(fileType, silent=silent);
    return(retVal);
  } else {
    string lastLine = fastxBuf.back(); fastxBuf.pop_back();
    // FASTQ / FASTM loading code from Booksembler
    // [gitlab.com/gringer/booksembler/-/blob/master/booksembler.cc]
    switch (fileType) {
    case fasta: // ** fasta logic **
      if(fastxBuf.size() == 1){ // [first] sequence line
        fastxBuf.push_back(lastLine); // restore line
        return(false);
      } else if(lastLine[0] == '>'){
        retVal = storeSequence(fileType, silent=silent);
        fastxBuf.push_back(lastLine); // restore first line of next sequence
        return(retVal);
      } else { // a continuation of the sequence
        fastxBuf.back() += lastLine; // add to sequence line
        return(false);
      }
      break;
    case fastq: // ** fastq logic **
      switch (fastxBuf.size()) {
      case 0: // header line
      case 1: // [first] sequence line
      case 3: // [first] quality line
	fastxBuf.push_back(lastLine);
        return(false);
	break;
      case 2:
	if(lastLine[0] == '+'){ // reached end of sequence
	  fastxBuf.push_back(lastLine);
          return(false);
	} else { // allow multi-line fastq
	  fastxBuf.back() += lastLine; // add to sequence line
          return(false);
	}
	break;
      case 4:
	if(fastxBuf[1].size() <= fastxBuf[3].size()){ // finished sequence
	  retVal = storeSequence(fileType, silent=silent);
	  fastxBuf.push_back(lastLine); // restore first line of next sequence
          return(retVal);
	} else {
	  fastxBuf.back() += lastLine;  // add to quality line
          return(false);
	}
	break;
      default:
	cerr << "Warning: unexpected fastq condition; clearing buffer\n";
	fastxBuf.clear();
        return(false);
      }
      break;
    case fastm: // ** fastm logic **
      if(fastxBuf.size() == 1){ // [first] record line
        fastxBuf.push_back(lastLine); // restore next line
        return(false);
      } else if(lastLine[0] == ' '){ // multi-line; continuation of previous
        fastxBuf.back() += lastLine.substr(1);
        return(false);
      } else if(lastLine[0] == '#'){ // record is complete
        retVal = storeSequence(fileType, silent=silent);
        fastxBuf.push_back(lastLine); // restore first line of next record
        return(retVal);
      } else {
	cerr << "Warning: unexpected fastq condition; clearing buffer\n";
	fastxBuf.clear();
        return(false);
      }
      break;
    }
    cerr << "Warning: unexpected stream condition; clearing buffer\n";
    fastxBuf.clear();
    return(false);
  }
}

// return value: true if a read has been loaded / processed
bool processRead(bool dumpRemainder, bool silent=false){
  if(readBuf.size() == 1){
    return(false);
  }
  FastXFileType fileType;
  bool retVal;
  switch (readBuf[0][0]) {
  case '#':
    fileType = fastm;
    break;
  case '@':
    fileType = fastq;
    break;
  default:
    fileType = fasta;
  }
  if(dumpRemainder){
    retVal = appendSequence(fileType, silent=silent);
    return(retVal);
  } else {
    string lastLine = readBuf.back(); readBuf.pop_back();
    // FASTQ / FASTM loading code from Booksembler
    // [gitlab.com/gringer/booksembler/-/blob/master/booksembler.cc]
    switch (fileType) {
    case fasta: // ** fasta logic **
      if(readBuf.size() == 1){ // [first] sequence line
        readBuf.push_back(lastLine); // restore line
        return(false);
      } else if(lastLine[0] == '>'){
        retVal = appendSequence(fileType, silent=silent);
        readBuf.push_back(lastLine); // restore first line of next sequence
        return(retVal);
      } else { // a continuation of the sequence
        readBuf.back() += lastLine; // add to sequence line
        return(false);
      }
      break;
    case fastq: // ** fastq logic **
      switch (readBuf.size()) {
      case 0: // header line
      case 1: // [first] sequence line
      case 3: // [first] quality line
	readBuf.push_back(lastLine);
        return(false);
	break;
      case 2:
	if(lastLine[0] == '+'){ // reached end of sequence
	  readBuf.push_back(lastLine);
          return(false);
	} else { // allow multi-line fastq
	  readBuf.back() += lastLine; // add to sequence line
          return(false);
	}
	break;
      case 4:
	if(readBuf[1].size() <= readBuf[3].size()){ // finished sequence
	  retVal = appendSequence(fileType, silent=silent);
	  readBuf.push_back(lastLine); // restore first line of next sequence
          return(retVal);
	} else {
	  readBuf.back() += lastLine;  // add to quality line
          return(false);
	}
	break;
      default:
	cerr << "Warning: unexpected fastq condition; clearing buffer\n";
	readBuf.clear();
        return(false);
      }
      break;
    case fastm: // ** fastm logic **
      if(readBuf.size() == 1){ // [first] record line
        readBuf.push_back(lastLine); // restore next line
        return(false);
      } else if(lastLine[0] == ' '){ // multi-line; continuation of previous
        readBuf.back() += lastLine.substr(1);
        return(false);
      } else if(lastLine[0] == '#'){ // record is complete
        retVal = appendSequence(fileType, silent=silent);
        readBuf.push_back(lastLine); // restore first line of next record
        return(retVal);
      } else {
	cerr << "Warning: unexpected fastq condition; clearing buffer\n";
	readBuf.clear();
        return(false);
      }
      break;
    }
    cerr << "Warning: unexpected stream condition; clearing buffer\n";
    readBuf.clear();
    return(false);
  }
}


// [[Rcpp::export]]
bool hasMoreSeqs(){
  return(seqsRemaining);
}


// [[Rcpp::export]]
bool prepareFastxFile(StringVector fileName, uint64_t tKmerSize){
  if(fastaFileFS.is_open()){
    fastaFileFS.close();
    fastaFileFS.clear();
  }
  fastaFileFS.open(fileName[0]);
  fileLoaded = true;
  kmersCounted = false;
  repeatsHashed = false;
  // Reset the sequence variables
  seqID = "";
  bitSeq.clear();
  seqLen = 0;
  kmerSize = tKmerSize;
  kmerBitSize = kmerSize << 1;
  kmerMask = (kmerBitSize == 64) ? numeric_limits<uint64_t>::max() :
    ((kmerBitSize < 64) ? ((1L << kmerBitSize) - 1) : HALF_MASK);
  // Clear the sequence buffer
  fastxBuf.clear();
  getline(fastaFileFS, buf);
  //cout << "first line: " << buf << endl;
  seqsRemaining = fastaFileFS.good();
  if(seqsRemaining){
    fastxBuf.push_back(buf);
  }
  return(seqsRemaining);
}

// [[Rcpp::export]]
bool loadNextSequence(bool silent=false){
  if(!fileLoaded){
    cerr << "Error: no file loaded\n";
    return false;
  }
  if(!seqsRemaining){
    cerr << "Error: no more sequences\n";
    return false;
  }
  // Reset the sequence variables
  seqID = "";
  bitSeq.clear();
  seqLen = 0;
  bool shownFirstMessage = false;
  bool shownLastMessage = false;
  // Keep loading lines from the file until a full sequence record is seen
  while(fastaFileFS.good()){
    getline(fastaFileFS, buf);
    if(fastaFileFS.good()){
      if(!shownFirstMessage && !silent){
        cerr << "Loading next sequence...";
      }
      fastxBuf.push_back(buf);
      shownFirstMessage = true;
    }
    if(processFastx(!fastaFileFS.good(), silent=silent)){
      if(!shownLastMessage && !silent){
	cerr << " done!\n";
      }
      shownLastMessage = true;
      return(true);
    }
  }
  // Reached the end of the file (or other file error)
  seqsRemaining = false;
  return(false);
}

bool loadNextRead(bool silent=false){
  if(!fileLoaded){
    cerr << "Error: no reference file loaded\n";
    return false;
  }
  if(!readsRemaining){
    cerr << "Error: no more reads to process\n";
    return false;
  }
  // Reset the read variables
  readID = "";
  // Keep loading lines from the file until a full sequence record is seen
  while(readFileFS.good()){
    getline(readFileFS, buf);
    if(readFileFS.good()){
      readBuf.push_back(buf);
    }
    if(processRead(!readFileFS.good(), silent=silent)){
      return(true);
    }
  }
  // Reached the end of the file (or other file error)
  return(false);
}

// [[Rcpp::export]]
bool mapReads(StringVector fileName){
  if(seqLen < kmerSize){
    return(false);
  }
  if(readFileFS.is_open()){
    readFileFS.close();
    readFileFS.clear();
  }
  readFileFS.open(fileName[0]);
  // Reset the sequence variables
  readID = "";
  // Clear the map buffer
  readBuf.clear();
  getline(readFileFS, buf);
  //cout << "first line: " << buf << endl;
  readsRemaining = readFileFS.good();
  if(readsRemaining){
    readBuf.push_back(buf);
  }
  bool header=false;
  while(loadNextRead()){
    size_t repeatCount = 0;
    for(uint64_t i = seqLen + 1; (i <= (seqLen + readLen + 1- kmerSize)); i++){
      Kmer tk(i | KT_F);
      auto tkf = kmerStore.find(tk);
      repeatCount += (tkf != kmerStore.end());
    }
    cerr << "Reference: " << getShortID() << "; Read: " << readID << "; Repeat count: " << repeatCount << endl;
  }
  return(true);
}

// Piecewise distance mapping function (split log/linear)
// [[Rcpp::export]]
int64_t pwLogLin(int64_t x, uint64_t pxHeight=1000,
		 bool doLog=true, bool circular=false){
  int sx = sgn(x); x = abs(x);
  if(!doLog || (seqLen <= 100)){
    return((x * pxHeight * 2) / seqLen * sx);
  }
  if(!cachePrepared){
    cerr << "Error: cache not properly prepared\n";
    return(-1);
  }
  int64_t res = llCache[x];
  if(res >= 0){
    return(res * sx);
  }
  // optimisation: this branch could be removed
  if(circular && (x > (seqLen / 2))){
   x = (seqLen - x);
   sx = -sx;
  }
  int64_t a;
  double endPos;
  a = seqLen / (25 + (25 * circular));
  endPos = ((seqLen - ((seqLen/2) * circular)) - a) / (a * log(a)) + 1;
  // optimisation: this branch could be removed
  double xtr = (x < a) ?
    // logarithmic up to the split point
    log(x) / log(a) :
    // linear after the split point
    (x-a) / (a * log(a)) + 1;
  xtr = (pxHeight * 2 * xtr) / endPos + 0.5;
  res = floor(xtr);
  llCache[x] = res;
  return(res * sgn(x));
}

// [[Rcpp::export]]
string getKmerF(uint64_t tPos){
  if(tPos > (seqLen - kmerSize)){
    cerr << "Error: request for kmer beyond sequence end\n";
    return("");
  }
  Kmer tk(tPos | KT_F);
  return(tk.getSequence());
}

// [[Rcpp::export]]
string getKmerC(uint64_t tPos){
  if(tPos > (seqLen - kmerSize)){
    cerr << "Error: request for kmer beyond sequence end\n";
    return("");
  }
  Kmer tk(tPos | KT_C);
  return(tk.getSequence());
}

// [[Rcpp::export]]
string getKmerR(uint64_t tPos){
  if(tPos > (seqLen - kmerSize)){
    cerr << "Error: request for kmer beyond sequence end\n";
    return("");
  }
  Kmer tk(tPos | KT_R);
  return(tk.getSequence());
}

// [[Rcpp::export]]
string getKmerRC(uint64_t tPos){
  if(tPos > (seqLen - kmerSize)){
    cerr << "Error: request for kmer beyond sequence end\n";
    return("");
  }
  Kmer tk(tPos | KT_RC);
  return(tk.getSequence());
}


// [[Rcpp::export]]
uint64_t getSeqLen(){
  return(seqLen);
}

// [[Rcpp::export]]
bool hashRepeats(bool silent=false){
  repeatsHashed = false;
  if(seqLen < kmerSize){
    if(!silent){
      cerr << "Error: sequence is too short\n";
    }
    return(false);
  }
  urpt=0;
  uorpt=0;
  isRepetitive.clear(); // needed so that missing comparisons work properly
  isRepetitive.resize(seqLen, false);
  isInteresting.clear();
  isInteresting.resize(seqLen, false);
  isComplement.clear();
  isComplement.resize(seqLen, false);
  isReverse.clear();
  isReverse.resize(seqLen, false);
  isRevcomp.clear();
  isRevcomp.resize(seqLen, false);
  repetitivePoss.clear();
  kmerStore.clear();
  if(!silent){
    cerr << "Finding forward repeats: |" << setfill('-') << setw(49) << "|"
	 << setfill(' ') << endl
	 << "                         ";
  }
  size_t starsAdded = 0;
  uint64_t basesMod = 0;
  uint64_t propSlice = (seqLen / 50);
  uint64_t repeatCount = 0;
  uint64_t firstPos = 0;
  for(uint64_t i = 0; (i <= (seqLen - kmerSize)); i++){
    pair<unordered_set<Kmer>::iterator, bool> tki =
      kmerStore.emplace(i | KT_F);
    if(!tki.second){
      // kmer already exists, so it's a repeat
      firstPos = tki.first->getPos();
      if(!nearMissing[i]){
	isInteresting[i] = true;
	isRepetitive[i] = true;
	repeatCount++;
	if(!isRepetitive[firstPos]){
	  isInteresting[firstPos] = true;
	  isRepetitive[firstPos] = true;
	  repeatCount++;
	  // add first position to repeat location array
           // optimisation: this makes branch removal difficult
	  repetitivePoss[firstPos].push_back(firstPos);
	}
	// add current position to repeat location array
	repetitivePoss[firstPos].push_back(i);
      }
    }
    if((++basesMod > propSlice) && (starsAdded < 50)){
      R_CheckUserInterrupt();
      basesMod = 0;
      if(!silent){
	cerr << "*";
      }
      starsAdded++;
    }
  }
  while(starsAdded < 50){
    if(!silent){
      cerr << "*";
    }
    starsAdded++;
  }
  if(!silent){
    cerr << endl;
  }
  urpt = repetitivePoss.size();
  if(!silent){
    cerr << "Done. " << repeatCount << " repetitive " << kmerSize << "-mer";
    if(repeatCount != 1){
      cerr << "s";
    }
    cerr << " found in " << seqLen << " bases"
	 << " [" << urpt << " unique]\n";
  }
  repeatsHashed = true;
  if(!silent){
    cerr << "Finding other repeats:   |" << setfill('-') << setw(49) << "|"
	 << setfill(' ') << endl
	 << "                         ";
  }
  starsAdded = 0;
  basesMod = 0;
  propSlice = (seqLen / 50);
  repeatCount = 0;
  for(uint64_t i = 0; (i <= (seqLen - kmerSize)); i++){
    if(nearMissing[i]){
      continue;
    }
    Kmer tkf(i | KT_F);
    Kmer tkc(i | KT_C);
    Kmer tkr(i | KT_R);
    Kmer tkrc(i | KT_RC);
    auto tkcf = kmerStore.find(tkc);
    auto tkrf = kmerStore.find(tkr);
    auto tkrcf = kmerStore.find(tkrc);
    if(tkcf != kmerStore.end()){
      isComplement[i] = true;
      uint64_t firstPos = tkcf->getPos();
      if(!isInteresting[firstPos]){
	// kmer is not yet in repeat array
	isInteresting[firstPos] = true;
	repetitivePoss[firstPos].push_back(firstPos);
      }
      repeatCount++;
    }
    if(tkrf != kmerStore.end()){
      isReverse[i] = true;
      uint64_t firstPos = tkrf->getPos();
      if(!isInteresting[firstPos]){
	// kmer is not yet in repeat array
	isInteresting[firstPos] = true;
	repetitivePoss[firstPos].push_back(firstPos);
      }
      repeatCount++;
    }
    if(tkrcf != kmerStore.end()){
      isRevcomp[i] = true;
      uint64_t firstPos = tkrcf->getPos();
      if(!isInteresting[firstPos]){
	// kmer is not yet in repeat array
	isInteresting[firstPos] = true;
	repetitivePoss[firstPos].push_back(firstPos);
      }
      repeatCount++;
    }
    if((++basesMod > propSlice) && (starsAdded < 50)){
      R_CheckUserInterrupt();
      basesMod = 0;
      if(!silent){
	cerr << "*";
      }
      starsAdded++;
    }
  }
  while(starsAdded < 50){
    if(!silent){
      cerr << "*";
    }
    starsAdded++;
  }
  if(!silent){
    cerr << endl;
    cerr << "Done. " << repeatCount
	 << " repetitive Comp / Rev / RevComp " << kmerSize << "-mer";
    if(repeatCount != 1){
      cerr << "s";
    }
    cerr << " found in " << seqLen << " bases";
    uorpt = repetitivePoss.size();
    cerr << " [" << (uorpt-urpt) << " additionally unique]\n";
  }
  return(true);
}

// [[Rcpp::export]]
bool writeCountOutput(string outFileName, bool append=false){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  uint64_t countN=0, countAll=0, countF=0, countRC=0, countR=0, countC=0;
  for(uint64_t i = 0; i < seqLen; i++){
    if(nearMissing[i]){
      countN++;
      continue;
    }
    countAll++;
    if(isRepetitive[i]){
      countF++;
    }
    if(isRevcomp[i]){
      countRC++;
    }
    if(isReverse[i]){
      countR++;
    }
    if(isComplement[i]){
      countC++;
    }
  }
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  // output to File
  // Note: cstdio used because it's faster
  // https://stackoverflow.com/a/2085777/3389895
  FILE* of = fopen(outFileName.c_str(), (append) ? "a" : "w");
  if(of == NULL){
    cerr << "Error: File could not be opened for writing\n";
    return(false);
  }
  if(!append){
    fprintf(of, "Name\tseqLen\tuniqueF\tuniqueOther\tkmersWithN\tkmersNotN\tcountF\tcountRC\tcountR\tcountC\n");
  }
  double countAllD = (double) countAll;
  //           ID  SL   UF   UO   N    A    F    RC   R    C
  fprintf(of, "%s\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t\n",
	  getShortID().c_str(), seqLen, urpt, uorpt,
	  countN, countAll, countF, countRC, countR, countC);
  fclose(of);
  return(true);
}

// [[Rcpp::export]]
bool writeSequence(){
  bool retVal = false;
  // Note: outputs to standard output, rather than a file
  string seqStr("");
  for(uint64_t i = 0; i < seqLen; i++){
    if((i > 0) && (seqFormat != fastq) && (i % 60 == 0)){
      seqStr += "\n";
    }
    seqStr += getBase(i);
  }
  switch (seqFormat) {
  case fasta:
    printf(">%s\n", seqID.c_str());
    printf("%s\n", seqStr.c_str());
    break;
  case fastq:
    printf("@%s\n", seqID.c_str());
    printf("%s\n", seqStr.c_str());
    printf("+\n");
    printf("%s\n", seqQual.c_str());
    break;
  case fastm:
    printf("#seq:%s\n", seqID.c_str());
    printf(" %s\n", seqStr.c_str());
    printf("#qual:\n");
    printf(" %s\n", seqQual.c_str());
    break;
  }
  return(true);
}

// [[Rcpp::export]]
bool filterReads(bool invert=false,
                 double minF=0, double minR=0, double minC=0, double minRC=0,
                 double maxF=1.1, double maxR=1.1, double maxC=1.1, double maxRC=1.1){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  uint64_t countN=0, countAll=0, countF=0, countRC=0, countR=0, countC=0;
  for(uint64_t i = 0; i < seqLen; i++){
    if(nearMissing[i]){
      countN++;
      continue;
    }
    countAll++;
    if(isRepetitive[i]){
      countF++;
    }
    if(isRevcomp[i]){
      countRC++;
    }
    if(isReverse[i]){
      countR++;
    }
    if(isComplement[i]){
      countC++;
    }
  }
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  bool keep = true;
  bool retVal = false;
  // filter on maximum values
  if( (((maxF > 1.5) && (countF > maxF )) || ((maxF < 1.5) && (countF > (maxF  * countAll)))) ||
      (((maxR > 1.5) && (countR > maxR )) || ((maxR < 1.5) && (countR > (maxR  * countAll)))) ||
      (((maxC > 1.5) && (countC > maxC )) || ((maxC < 1.5) && (countC > (maxC  * countAll)))) ||
      (((maxRC> 1.5) && (countRC> maxRC)) || ((maxRC< 1.5) && (countRC> (maxRC * countAll)))) ){
    keep = false;
  }
  // filter on minimum values
  if( (((minF > 1.5) && (countF < minF )) || ((minF < 1.5) && (countF < (minF  * countAll)))) ||
      (((minR > 1.5) && (countR < minR )) || ((minR < 1.5) && (countR < (minR  * countAll)))) ||
      (((minC > 1.5) && (countC < minC )) || ((minC < 1.5) && (countC < (minC  * countAll)))) ||
      (((minRC> 1.5) && (countRC< minRC)) || ((minRC< 1.5) && (countRC< (minRC * countAll)))) ){
    keep = false;
  }
  if(keep != invert){
    retVal = writeSequence();
  }
  return(retVal);
}

// [[Rcpp::export]]
bool makeRepeatBed(string outFileName, bool writeKmers=true, bool append=false,
		   bool merge=false, bool split=false){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  // Note: cstdio used because it's faster
  // https://stackoverflow.com/a/2085777/3389895
  FILE* of = fopen(outFileName.c_str(), (append) ? "a" : "w");
  if(of == NULL){
    cerr << "Error: File could not be opened for writing\n";
    return(false);
  }
  if(split){
    for(uint64_t i = 0; (i <= (seqLen - kmerSize)); i++){
      if(nearMissing[i]){
	// ignore Kmers containing missing values
	continue;
      }
      if(isRepetitive[i]){
	if(writeKmers){
	  fprintf(of, "%s\t%lu\t%lu\t%s_FWD\n", seqID.c_str(),
		  i, i + kmerSize, getKmerF(i).c_str());
	} else {
	  fprintf(of, "%s\t%lu\t%lu\tFWD_RPT\n", seqID.c_str(),
		  i, i + kmerSize);
	}
      }
      if(isComplement[i]){
	if(writeKmers){
	  fprintf(of, "%s\t%lu\t%lu\t%s_CMP\n", seqID.c_str(),
		  i, i + kmerSize, getKmerF(i).c_str());
	} else {
	  fprintf(of, "%s\t%lu\t%lu\tCMP_RPT\n", seqID.c_str(),
		  i, i + kmerSize);
	}
      }
      if(isRevcomp[i]){
	if(writeKmers){
	  fprintf(of, "%s\t%lu\t%lu\t%s_RVC\n", seqID.c_str(),
		  i, i + kmerSize, getKmerF(i).c_str());
	} else {
	  fprintf(of, "%s\t%lu\t%lu\tRVC_RPT\n", seqID.c_str(),
		  i, i + kmerSize);
	}
      }
      if(isReverse[i]){
	if(writeKmers){
	  fprintf(of, "%s\t%lu\t%lu\t%s_REV\n", seqID.c_str(),
		  i, i + kmerSize, getKmerF(i).c_str());
	} else {
	  fprintf(of, "%s\t%lu\t%lu\tREV_RPT\n", seqID.c_str(),
		  i, i + kmerSize);
	}
      }
    }
    fclose(of);
    return(true);
  }
  uint64_t repeatStart = 0;
  uint64_t lastRegionEnd = 0;
  for(uint64_t i = 0; (i <= (seqLen - kmerSize)); i++){
    if(isRepetitive[i] || (merge && (lastRegionEnd > i))){
      if(repeatStart == 0){
        repeatStart = i+1;
      }
      if(isRepetitive[i]){
	lastRegionEnd = i + kmerSize - 1;
      }
    } else {
      if(repeatStart > 0){
	if(writeKmers){
	  fprintf(of, "%s\t%lu\t%lu\t%s\n", seqID.c_str(),
		  repeatStart-1, i + kmerSize, getKmerF(repeatStart-1).c_str());
	} else {
	  fprintf(of, "%s\t%lu\t%lu\n", seqID.c_str(),
		  repeatStart-1, i + kmerSize);
	}
        repeatStart = 0;
      }
    }
  }
  if(repeatStart > 0){
    if(writeKmers){
      fprintf(of, "%s\t%lu\t%lu\t%s\n", seqID.c_str(),
	      repeatStart-1, seqLen, getKmerF(seqLen - kmerSize).c_str());
    } else {
      fprintf(of, "%s\t%lu\t%lu\n", seqID.c_str(),
	      repeatStart-1, seqLen);
    }
  }
  fclose(of);
  return(true);
}

// [[Rcpp::export]]
// Note: the BedGraph format doesn't allow overlaps, so regions are automatically merged
bool makeRepeatBedGraph(string outFileName, bool append=false){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  // Note: cstdio used because it's faster
  // https://stackoverflow.com/a/2085777/3389895
  FILE* of = fopen(outFileName.c_str(), (append) ? "a" : "w");
  if(of == NULL){
    cerr << "Error: File could not be opened for writing\n";
    return(false);
  }
  if(!append){
    fprintf(of, "track type=bedGraph name=Repetitive_regions\n");
  }
  vector<bool> dropCoverage(seqLen + 1, false);
  uint64_t regionStart = 0;
  uint64_t currentCoverage = 0;
  int coverageDelta = 0;
  for(uint64_t i = 0; i < seqLen; i++){
    if((i <= (seqLen - kmerSize)) && isRepetitive[i]){
      coverageDelta++;
      dropCoverage[i + kmerSize] = true;
    }
    if(dropCoverage[i]){
      coverageDelta--;
    }
    if((coverageDelta != 0) || (i == (seqLen-1))){
      fprintf(of, "%s\t%lu\t%lu\t%lu\n", seqID.c_str(),
	      regionStart, i, currentCoverage);
      currentCoverage += coverageDelta;
      coverageDelta = 0;
      regionStart = i;
    }
  }
  fclose(of);
  return(true);
}

// [[Rcpp::export]]
bool writeDiffOutput(string outFileName, uint64_t basesPerChunk=1,
		     bool append=false){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  // Note: cstdio used because it's faster
  // https://stackoverflow.com/a/2085777/3389895
  FILE* of = fopen(outFileName.c_str(), (append) ? "a" : "w");
  if(of == NULL){
    cerr << "Error: File could not be opened for writing\n";
    return(false);
  }
  if(!append){
    fprintf(of, "Name\tBase\tType\tDeltas\n");
  }
  map<char, set<int64_t>> repeatDeltas;
  uint64_t currentChunk = 0;
  cerr << "Finding other repeats:   |" << setfill('-') << setw(49) << "|"
       << setfill(' ') << endl
       << "                         ";
  for(uint64_t i = 0; i < seqLen; i++){
    if(i / basesPerChunk > currentChunk){
      // output to File
      for(auto it : repeatDeltas){
	fprintf(of, "%s\t%lu\t%s\t",
		seqID.c_str(),
		currentChunk * basesPerChunk,
		((it.first == 'X') ? "RC" : string(1, it.first)).c_str());
	bool first=true;
	for(auto itd : it.second){
	  fprintf(of, "%s%ld", (first ? "" : ";"), itd);
	  first = false;
	}
	fprintf(of, "\n");
      }
      currentChunk = i / basesPerChunk;
      repeatDeltas.clear();
    }
    if(isRepetitive[i]){
      Kmer tkf(i | KT_F);
      auto tkff = kmerStore.find(tkf); // fetch first repeat loc from kmer store
      for(uint64_t it : repetitivePoss[tkff->getPos()]){
	if(i != it){
	  repeatDeltas['F'].insert(it - i);
	}
      }
    }
    if(isComplement[i]){
      Kmer tkc(i | KT_C);
      auto tkf = kmerStore.find(tkc);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
	repeatDeltas['C'].insert(it - i);
      }
    }
    if(isRevcomp[i]){
      Kmer tkrc(i | KT_RC);
      auto tkf = kmerStore.find(tkrc);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
	repeatDeltas['X'].insert(it - i);
      }
    }
    if(isReverse[i]){
      Kmer tkr(i | KT_R);
      auto tkf = kmerStore.find(tkr);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
	repeatDeltas['R'].insert(it - i);
      }
    }
  }
  // output to File
  for(auto it : repeatDeltas){
    fprintf(of, "%s\t%lu\t%s\t",
            seqID.c_str(),
            currentChunk * basesPerChunk,
            ((it.first == 'X') ? "RC" : string(1, it.first)).c_str());
    bool first=true;
    for(auto itd : it.second){
      fprintf(of, "%s%ld", (first ? "" : ";"), itd);
      first = false;
    }
    fprintf(of, "\n");
  }
  fclose(of);
  return(true);
}


// [[Rcpp::export]]
List getMissingRanges(){
  vector<int64_t> rangeStarts;
  vector<int64_t> rangeEnds;
  int64_t currentStart = -1;
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  for(uint64_t i = 0; i < seqLen; i++){
    if(nearMissing[i] && (currentStart == -1)){
      // start of a missing region
      currentStart = i;
    } else if(currentStart != -1) {
      // end of a missing region
      rangeStarts.push_back(currentStart);
      rangeEnds.push_back(i - 1);
      currentStart = -1;
    }
  }
  return(List::create(_["starts"] = rangeStarts,
		      _["ends"] = rangeEnds));
}


// [[Rcpp::export]]
List getDiffResults(uint64_t bpc, uint64_t yBins,
		    bool doLog=true, bool circular=false){
  if(!repeatsHashed){
    cerr << "Error: repeats have not yet been hashed\n";
    return(false);
  }
  map<char, unordered_set<int64_t>> repeatDeltas;
  uint64_t currentChunk = 0;
  uint64_t chunkPos = currentChunk * bpc;
  string chunkPosStr = to_string(chunkPos);
  cerr << "Calculating differences: |" << setfill('-') << setw(49) << "|"
       << setfill(' ') << endl
       << "                         ";
  size_t starsAdded = 0;
  uint64_t basesMod = 0;
  uint64_t propSlice = (seqLen / 50);
  vector<char> typeVec;
  vector<int64_t> xVec;
  vector<int64_t> yVec;
  for(uint64_t i = 0; i < seqLen; i++){
    if(i / bpc > currentChunk){
      // add to output array
      for(auto it : repeatDeltas){
	char deltaType = it.first;
	for(auto itd : it.second){
	  typeVec.push_back(deltaType);
	  xVec.push_back(chunkPos);
	  yVec.push_back(itd);
	}
      }
      repeatDeltas.clear();
      currentChunk = i / bpc;
      chunkPos = currentChunk * bpc;
      chunkPosStr = to_string(chunkPos);
    }
    if(isRepetitive[i]){
      Kmer tkf(i | KT_F);
      auto tkff = kmerStore.find(tkf); // fetch first repeat loc from kmer store
      for(uint64_t it : repetitivePoss[tkff->getPos()]){
	if(i != it){
	  repeatDeltas['F'].insert(pwLogLin(it - i, yBins,
					    doLog=doLog, circular=circular));
	}
      }
    }
    if(isComplement[i]){
      Kmer tkc(i | KT_C);
      auto tkf = kmerStore.find(tkc);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
	repeatDeltas['C'].insert(pwLogLin(it - i, yBins,
					  doLog=doLog, circular=circular));
      }
    }
    if(isRevcomp[i]){
      Kmer tkrc(i | KT_RC);
      auto tkf = kmerStore.find(tkrc);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
	repeatDeltas['X'].insert(pwLogLin(it - i, yBins,
					  doLog=doLog, circular=circular));
      }
    }
    if(isReverse[i]){
      Kmer tkr(i | KT_R);
      auto tkf = kmerStore.find(tkr);
      for(uint64_t it : repetitivePoss[tkf->getPos()]){
	repeatDeltas['R'].insert(pwLogLin(it - i, yBins,
					  doLog=doLog, circular=circular));
      }
    }
    if((++basesMod > propSlice) && (starsAdded < 50)){
      R_CheckUserInterrupt();
      basesMod = 0;
      cerr << "*";
      starsAdded++;
    }
  }
  while(starsAdded < 50){
    cerr << "*";
    starsAdded++;
  }
  cerr << endl;
  // add to output array
  cerr << "Hash calculated " << hashCount << " times\n";
  cerr << "Equality compared " << compCount << " times\n";
  cerr << "Converting to DataFrame...";
  for(auto it : repeatDeltas){
    char deltaType = it.first;
    for(auto itd : it.second){
      typeVec.push_back(deltaType);
      xVec.push_back(chunkPos);
      yVec.push_back(itd);
    }
  }
  repeatDeltas.clear();
  cerr << " done!\n";
  return(List::create(_["name"] = seqID.substr(0, seqID.find(' ')),
		      _["len"] = seqLen,
		      _["blockSize"] = bpc,
		      _["chunks"] = DataFrame::create(_["type"] = typeVec,
						      _["x"] = xVec,
						      _["y"] = yVec)));
}

// [[Rcpp::export]]
bool countKmers(bool ordered=true){
  if(!ordered){
    kmerCounts.clear();
    uint64_t maxCount = 0;
    uint64_t maxPos = -1;
    for(uint64_t i = 0; (i <= (seqLen - kmerSize)); i++){
      Kmer tk(i | KT_F);
      kmerCounts[tk]++;
      uint64_t kct = kmerCounts[tk];
      if(kct > maxCount){
	maxCount = kct;
	maxPos = tk.getPos();
      }
    }
    if(maxPos >= 0){
      Kmer tk(maxPos | KT_F);
      cout << "Counted! Max count == " << maxCount << "; " <<
	tk.getSequence() << " [" << maxPos << "]" << endl;
    }
    return(true);
  } else {
    kmerCountsOrdered.clear();
    uint64_t maxCount = 0;
    uint64_t maxPos = -1;
    for(uint64_t i = 0; (i <= (seqLen - kmerSize)); i++){
      Kmer tk(i | KT_F);
      kmerCountsOrdered[tk]++;
      uint64_t kct = kmerCountsOrdered[tk];
      if(kct > maxCount){
	maxCount = kct;
	maxPos = tk.getPos();
      }
    }
    if(maxPos >= 0){
      Kmer tk(maxPos | KT_F);
      cout << "Counted! Max count == " << maxCount << "; " <<
	tk.getSequence() << " [" << maxPos << "]" << endl;
    }
    return(true);
  }
}

// [[Rcpp::export]]
uint64_t testHash(uint64_t pos){
  if(seqLen >= (kmerSize + pos)){
    return(hash<Kmer>{}(pos));
  } else {
    return(0);
  }
}

// [[Rcpp::export]]
void showKmers(bool ordered=true, size_t limit=30){
  if(!ordered){
    cout << "Kmer count total: " << kmerCounts.size() << endl;
    for(pair<Kmer, uint64_t> ik : kmerCounts){
      cout << "Kmer: "
	   << ik.first.getPos() << "(" << ik.first.getSequence()
	   << " - "
	   << hex << setfill('0') << setw(32) << testHash(ik.first.getPos())
	   << ")" << dec << "; count: " << ik.second << endl;
      if(--limit <= 0){
	return;
      }
    }
  } else {
    cout << "Kmer count total: " << kmerCountsOrdered.size() << endl;
    for(pair<Kmer, uint64_t> ik : kmerCountsOrdered){
      cout << "Kmer: " << ik.first.getPos() << "(" << ik.first.getSequence()
	   << " - "
	   << hex << setfill('0') << setw(32) << testHash(ik.first.getPos())
	   << ")" << "; count: " << ik.second << endl;
      if(--limit <= 0){
	return;
      }
    }
  }
}


// [[Rcpp::export]]
string getSeqSig(){
  string res = getID() + ":";
  for(uint64_t i = 0; ((i < 32) && (i < seqLen)); i++){
    res += getBase(i);
  }
  if(seqLen > 32){
    if(seqLen > 64){
      res += "...";
    }
    for(uint64_t i = (seqLen < 64) ? 32 : seqLen-32; i < seqLen; i++){
      res += getBase(i);
    }
  }
  if(seqLen > 64){
    res += " (" + to_string(seqLen) + " bases)";
  }
  return(res);
}

// [[Rcpp::export]]
bool ltKmer(uint64_t tPos1, uint64_t tPos2, string type1 = "F"){
  uint64_t or1 =
    (type1 == "F")  ? KT_F  :
    (type1 == "C")  ? KT_C  :
    (type1 == "R")  ? KT_R  :
    (type1 == "RC") ? KT_RC : KT_N;
  Kmer tk1(tPos1 | or1);
  Kmer tk2(tPos2 | KT_F);
  return(tk1 < tk2);
}

// [[Rcpp::export]]
bool eqKmer(uint64_t tPos1, uint64_t tPos2, string type1 = "F"){
  uint64_t or1 =
    (type1 == "F")  ? KT_F  :
    (type1 == "C")  ? KT_C  :
    (type1 == "R")  ? KT_R  :
    (type1 == "RC") ? KT_RC : KT_N;
  Kmer tk1(tPos1 | or1);
  Kmer tk2(tPos2 | KT_F);
  return(tk1 == tk2);
}

// [[Rcpp::export]]
uint64_t verboseHash(uint64_t basePos){
  if(seqLen < (kmerSize + 1 + basePos)){
    return(0);
  }
  return(0);
  // TODO: fix this up
  // cout << setfill(' ');
  // cout <<   "Kmer size:  " << setw(64) << kmerSize << endl;
  // cout <<   "Kmer mask:  " << bitset<64>(kmerMask) << endl;
  // if(kmerSize < 32){
  //   cout << "Kmer:       " << getKmerF(basePos) << endl;
  //   cout << "basePos:    " << setw(64) << basePos << endl;
  //   uint64_t bitPos = basePos >> 5L;
  //   cout << "bitPos:     " << setw(64) << basePos << endl;
  //   uint64_t bitBit = (basePos & 0x1F) << 1L;
  //   cout << "bitBit:     " << setw(64) << bitBit << endl;
  //   cout << "bS[bP]:     " << bitset<64>(bitSeq[bitPos]) << endl;
  //   uint64_t h1 = (bitSeq[bitPos] >> bitBit);
  //   cout << "bs[bP]>>bb  " << bitset<64>(h1) << endl;
  //   size_t bitsSeen = 64 - bitBit;
  //   cout << "bitsSeen:   " << setw(64) << bitsSeen << endl;
  //   if(bitsSeen <= kmerBitSize){
  //     cout << "bs[bP+1]: " << bitset<64>(bitSeq[bitPos+1]) << endl;
  //     cout << "merge:    "
  // 	   << bitset<64>(h1 | (bitSeq[bitPos+1] << bitsSeen)) << endl;
  //   }
  // } else {
  //   string kmer = getKmerF(basePos);
  //   cout << "Kmer:       " << kmer.substr(0, 16) << "..."
  // 	 << kmer.substr(kmer.size() - 16) << endl;
  //   cout << "basePos:    " << setw(64) << basePos << endl;
  //   uint64_t firstPos = basePos >> 5L;
  //   cout << "firstPos:   " << setw(64) << firstPos << endl;
  //   uint64_t firstBit = (basePos & 0x1FL) << 1L;
  //   cout << "firstBit:   " << setw(64) << firstBit << endl;
  //   cout << "bS[fP]:     " << bitset<64>(bitSeq[firstPos]) << endl;
  //   if(firstBit > 32){
  //     cout << "            " << setfill('^') << setw(64 - firstBit) << "^" << endl;
  //     cout << setfill(' ');
  //     cout << "bS[fP+1]:   " << bitset<64>(bitSeq[firstPos+1]) << endl;
  //     cout << "            "
  // 	   << setfill(' ') << setw(96 - firstBit) << ' '
  // 	   << setfill('^') << setw(firstBit - 32) << "^" << endl;
  //     cout << setfill(' ');
  //   } else {
  //     cout << "            "
  // 	   << setfill(' ') << setw(32 - firstBit) << " "
  // 	   << setfill('^') << setw(32) << "^" << endl;
  //     cout << setfill(' ');
  //   }
  //   uint64_t lastPos = (basePos + kmerSize - 16) >> 5L;
  //   cout << "lastPos:    " << setw(64) << lastPos << endl;
  //   uint64_t lastBit = ((basePos + kmerSize - 16) & 0x1FL) << 1L;
  //   cout << "lastBit:    " << setw(64) << lastBit << endl;
  //   cout << "bS[lP]:     " << bitset<64>(bitSeq[lastPos]) << endl;
  //   if(lastBit > 32){
  //     cout << "            " << setfill('^') << setw(64 - lastBit) << "^" << endl;
  //     cout << setfill(' ');
  //     cout << "bS[lP+1]:   " << bitset<64>(bitSeq[lastPos+1]) << endl;
  //     cout << "            "
  // 	   << setfill(' ') << setw(96 - lastBit) << ' '
  // 	   << setfill('^') << setw(lastBit - 32) << "^" << endl;
  //     cout << setfill(' ');
  //   } else {
  //     cout << "            "
  // 	   << setfill(' ') << setw(32 - lastBit) << " "
  // 	   << setfill('^') << setw(32) << "^" << endl;
  //     cout << setfill(' ');
  //   }
  // }
  // cout << "result:     " << bitset<64>(hash<Kmer>{}(basePos)) << endl;
  // cout << "            |->" << setw(29) << "<-|" << "|->" << setw(29) << "<-|" << endl;
  // return(hash<Kmer>{}(basePos));
}
