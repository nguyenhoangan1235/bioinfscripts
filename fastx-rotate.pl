#!/usr/bin/env perl
use warnings;
use strict;

## fastx-rotate.pl -- Rotates a sequence to add start bits to the end

use Getopt::Long qw(:config auto_help pass_through);

my $quiet = 0;
my $preserve = 0;
my $rtOffset = 0;
my $newLen = 0;
my $revComp = 0;
my $endPos = 0;

sub rc {
  my ($seq) = @_;
  $seq =~ tr/ACGTUYRSWMKDVHBXN-/TGCAARYSWKMHBDVXN-/;
  # work on masked sequences as well
  $seq =~ tr/acgtuyrswmkdvhbxn/tgcaaryswkmhbdvxn/;
  return(scalar(reverse($seq)));
}

sub rt {
  my ($seq, $offset, $targLen, $doRC) = @_;
  if($targLen == 0){
    $targLen = length($seq);
  }
  if($doRC){
    $seq = rc($seq);
  }
  $seq = substr($seq.$seq, $offset, $targLen);
  return($seq);
}

GetOptions("quiet!" => \$quiet, "preserve!" => \$preserve, "end=i" => \$endPos,
           "offset=i" => \$rtOffset, "length=i" => \$newLen, "rc!" => \$revComp) or
  die("Error in command line arguments");

if($endPos > 0){
  $newLen = $endPos - $rtOffset;
}

# unknown commands are treated as identifiers
my @files = ();
while(@ARGV){
  my $arg = shift(@ARGV);
  if(-f $arg){
    push(@files, $arg);
  }
}
@ARGV = @files;

my $inQual = 0; # false
my $seqID = "";
my $qualID = "";
my $seq = "";
my $qual = "";
while(<>){
  chomp;
  chomp;
  if(!$inQual){
    if(/^(>|@)((.+?)( .*?\s*)?)$/){
      my $newSeqID = $2;
      my $newShortID = $3;
      if($seqID || ($seqID eq "0")){
	if($preserve){
	  if($qual){
	    printf("@%s\n%s\n+\n%s\n", $seqID, $seq, $qual);
	  } else {
	    $seq =~ s/\n//g;
	    $seq =~ s/(.{70})/$1\n/g;
	    $seq =~ s/\n$//;
	    printf(">%s\n%s\n", $seqID, $seq);
	  }
	}
        if($qual){
          printf("@%s [+ %d]\n%s\n+\n%s\n", $seqID, $rtOffset, rt($seq, $rtOffset, $newLen, $revComp), scalar(reverse($qual)));
        } else {
          $seq =~ s/\n//g;
          $seq = rt($seq, $rtOffset, $newLen, $revComp);
          $seq =~ s/(.{70})/$1\n/g;
          $seq =~ s/\n$//;
          printf(">%s [+ %d]\n%s\n", $seqID, $rtOffset, $seq);
        }
      }
      $seq = "";
      $qual = "";
      $seqID = $newSeqID;
    } elsif(/^\+(.*)$/) {
      $inQual = 1; # true
      $qualID = $1;
    } else {
      $seq .= $_;
    }
  } else {
    $qual .= $_;
    if(length($qual) >= length($seq)){
      $inQual = 0; # false
    }
  }
}

if($seqID || ($seqID eq "0")){
  if($preserve){
    if($qual){
      printf("@%s\n%s\n+\n%s\n", $seqID, $seq, $qual);
    } else {
      $seq =~ s/\n//g;
      $seq =~ s/(.{70})/$1\n/g;
      $seq =~ s/\n$//;
      printf(">%s\n%s\n", $seqID, $seq);
    }
  }
  if($qual){
    printf("@%s [+ %d]\n%s\n+\n%s\n", $seqID, $rtOffset, rc($seq, $rtOffset, $newLen, $revComp), scalar(reverse($qual)));
  } else {
    $seq =~ s/\n//g;
    $seq = rt($seq, $rtOffset, $newLen, $revComp);
    $seq =~ s/(.{70})/$1\n/g;
    $seq =~ s/\n$//;
    printf(">%s [+ %d]\n%s\n", $seqID, $rtOffset, $seq);
  }
}
