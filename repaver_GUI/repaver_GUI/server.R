#
# This is the server logic of a Shiny web application. You can run the
# application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)

repaverCommand <- "lib/repaver.r";
hashasherCommand <- "lib/hashasher.r";

# Define server logic required to draw a histogram
shinyServer(function(input, output, session) {
    values <- reactiveValues();
    set.seed(NULL);
    projectName <- 
        paste0(sample(readLines("word-list-verbs.txt"), 1),
               sample(readLines("word-list-adjectives.txt"), 1),
               sample(readLines("word-list-nouns.txt"), 1));
    tDir <- paste0(tempdir(),"/",projectName);
    dir.create(tDir);
    #cat("Temp dir:", tDir);
    
    appImageName <- paste0(tDir, "/", "output_repaver.png");
    pngFileOutput <- reactivePoll(500, session,
                         # This function returns the time that log_file was last modified
                         checkFunc = function() {
                             if (file.exists(appImageName))
                                 file.info(appImageName)$mtime[1]
                             else
                                 ""
                         },
                         # This function returns the content of log_file
                         valueFunc = function() {
                             appImageName
                         }
    )

    output$repaverPlot <- renderImage({
        if(!file.exists(pngFileOutput())){
            png(appImageName, width=1000, height=400);
            plot(NA, xlim=c(0, 1), ylim=c(0, 1), axes=FALSE, ann=FALSE);
            text(0.5, 0.5, labels = "Click \"Run Repaver\" to create an image", pos=3,
                 cex=3);
            dev.off();
        }
        list(src=pngFileOutput(), contentType="image/png",
             height="350")
    },
    deleteFile=FALSE);
    
    output$logStdout <- renderText({
        outLog <- NULL;
        if(!is.null(values$commandArguments)){
            outLog <- c(outLog, paste0("Command: ", paste(values$commandArguments, collapse=" ")));
        }
        outLog <- 
            c(outLog, reactiveFileReader(500, session,
                                         paste0(tDir,"/log_output.txt"), function(x){
                                             if(file.exists(x)){readLines(x)} else {""}})());
        outLog <- c(outLog, "---");
        outLog <- 
            c(outLog, reactiveFileReader(500, session,
                                         paste0(tDir,"/log_messages.txt"), function(x){
                                             if(file.exists(x)){readLines(x)} else {""}})());
        paste(outLog, collapse="\n");
    })

    observeEvent(input$uploadedSequence, {
        updateTextAreaInput(session, "inputSequenceRepaver", value="[uploaded sequence]");
    }, ignoreNULL = TRUE, ignoreInit = TRUE);

    observeEvent(input$uploadedSequenceHA, {
        updateTextAreaInput(session, "inputSequenceHashasher", value="[uploaded sequence]");
    }, ignoreNULL = TRUE, ignoreInit = TRUE);
        
    observeEvent(input$runRepaverButton, {
        repaverOutput <- list.files(tDir, pattern = "^repaver_");
        if(length(repaverOutput) > 0){
            file.remove(paste0(tDir,"/",repaverOutput));
        }
        inputFileName <- paste0(tDir,"/repaver_input.fx");
        if(input$inputSequenceRepaver == "[uploaded sequence]"){
            file.copy(input$uploadedSequence$datapath, inputFileName);
        } else {
            cat(input$inputSequenceRepaver, file=inputFileName);
        }
        cat("\n", file=inputFileName, append=TRUE); ## make sure file ends with a new line
        withProgress({
            setProgress(value=0, message="Running REPAVER...");
            repaverArguments <- c(repaverCommand, "-prefix", paste0(tDir,"/repaver"),
                                 "-style", input$outputStyle,
                                 "-k", input$kmerSize, inputFileName);
            values$commandArguments <- repaverArguments;
            system2("Rscript", 
                    repaverArguments,
                    wait=FALSE,
                    stdout=paste0(tDir,"/log_output.txt"),
                    stderr=paste0(tDir,"/log_messages.txt"));
            repaverFinished = FALSE;
            oldOutput = "";
            lastOutput = "";
            while(!repaverFinished){
                if(file.exists(paste0(tDir,"/log_output.txt"))){
                    lastOutput <- tail(readLines(paste0(tDir,"/log_output.txt")), 1);
                }
                if(lastOutput != oldOutput){
                    setProgress(message=lastOutput);
                    oldOutput = lastOutput;
                }
                if(lastOutput == ""){
                }
                if(lastOutput == "finished!"){
                    repaverFinished <- TRUE;
                }
                Sys.sleep(0.5);
            }
            setProgress(value=1, message="Done!");
        });
        repaverOutput <- list.files(tDir, pattern = "^repaver_.*\\.png");
        if(length(repaverOutput) > 0){
            #cat("Found", repaverOutput);
            file.copy(paste0(tDir,"/",head(repaverOutput, 1)), to=appImageName, overwrite=TRUE);
        }
    });

    output$runHashasherButton <- downloadHandler(
        filename = function() {
            paste0("hashasher_output",
                   c("BED" = ".bed", "BedGraph" = ".bg",
                     "Base difference" = ".diff.tsv")[input$outputFormatHA]);
        },
        contentType = "text/csv",
        content = function(fileName){
            inputFileName <- paste0(tDir,"/hashasher_input.fx");
            if(input$inputSequenceHashasher == "[uploaded sequence]"){
                file.copy(input$uploadedSequenceHA$datapath, inputFileName);
            } else {
                cat(input$inputSequenceHashasher, file=inputFileName);
            }
            extraOptions <- NULL;
            if(input$mergeSeqsHA){
                extraOptions <- c(extraOptions, "-merge");
            }
            if(input$splitRegionsHA){
                extraOptions <- c(extraOptions, "-split");
            }
            if(!input$includeSeqsHA){
                extraOptions <- c(extraOptions, "-noseqs");
            }
            outputFormatArgument <- c("BED" = "-bed", "BedGraph" = "-bg",
                                      "Base difference" = "-diff")[input$outputFormatHA];
            hashAsherArguments <- c(hashasherCommand, outputFormatArgument, fileName,
                                    extraOptions,
                                    "-k", input$kmerSizeHA, inputFileName);
            values$commandArguments <- hashAsherArguments;
            withProgress({
                setProgress(value=0, message="Running HashAsher...");
                system2("Rscript", hashAsherArguments);
                setProgress(value=1, message="Done!");
            });
        }
    );

    output$saveOutput <- downloadHandler(
        filename = function() {
            "repaver.png";
        },
        contentType = "image/png",
        content = function(fileName) {
            file.copy(appImageName, fileName, overwrite = TRUE);
        }
    );

})
