#!/usr/bin/env Rscript

if(length(commandArgs(TRUE)) < 4){
    cat("Error: more command-line arguments needed\n");
    cat("Usage: gifski.r <bounce> <width> <height> <fps>\n");
    quit(save="no");
}

doPause <- TRUE;
bounce <- (commandArgs(TRUE)[1] == "TRUE");
wid <- as.numeric(commandArgs(TRUE)[2]);
hgt <- as.numeric(commandArgs(TRUE)[3]);
fps <- as.numeric(commandArgs(TRUE)[4]);

library(gifski);
frame.files <- list.files(pattern="frame.*\\.png");

if(doPause){
  frame.files <- c(rep(frame.files[1], fps-1), frame.files, rep(tail(frame.files,1), fps-1));
}

if(bounce){
    frame.files <- c(frame.files, tail(head(rev(frame.files),-1),-1));
}
gifski(frame.files, "animated.gif", delay=1/fps, width=wid, height=hgt);
